/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_GRAPH_PASSES_TRANSOP_BREADTH_FUSION_PASS_H_
#define GE_GRAPH_PASSES_TRANSOP_BREADTH_FUSION_PASS_H_

#include <map>
#include <string>
#include <vector>

#include "inc/graph_pass.h"

namespace ge {
///
/// Transform operators breadth fusion
///
class TransOpBreadthFusionPass : public GraphPass {
 public:
  Status Run(ge::ComputeGraphPtr graph) final;

 private:
  std::string GetNodeId(const int32_t anchor_index, const NodePtr& node);

  std::map<std::string, std::vector<NodePtr>> GetOutputTransOpNodes(const NodePtr& node);

  graphStatus Fusion(const std::vector<NodePtr>& trans_nodes, ComputeGraphPtr& graph) const;

  std::string JoinDims(const std::string& sp, const std::vector<int64_t>& dims) const;
};
}  // namespace ge

#endif  // GE_GRAPH_PASSES_TRANSOP_BREADTH_FUSION_PASS_H_
