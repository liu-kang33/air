/**
 * Copyright 2020-2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fusion_rule_manager/fusion_cycle_detector.h"

namespace fe {
FusionCycleDetector::FusionCycleDetector() {}

FusionCycleDetector::~FusionCycleDetector() {}

std::vector<FusionPattern *> FusionCycleDetector::DefinePatterns() {
  std::vector<FusionPattern *> ret;
  return ret;
}

Status FusionCycleDetector::Fusion(ge::ComputeGraph &graph, Mapping &mapping, std::vector<ge::NodePtr> &new_nodes) {
  return SUCCESS;
}
}