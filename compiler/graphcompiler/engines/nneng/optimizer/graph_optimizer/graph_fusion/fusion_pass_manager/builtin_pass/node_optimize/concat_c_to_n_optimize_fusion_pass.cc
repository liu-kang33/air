/**
 * Copyright 2020-2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph_optimizer/graph_fusion/fusion_pass_manager/builtin_pass/node_optimize/concat_c_to_n_optimize_fusion_pass.h"
#include "graph_optimizer/graph_fusion/fusion_pass_manager/fusion_pass_registry.h"
#include "common/unknown_shape_util.h"

namespace fe {
const int kNDimIndex = 0;
const int kRealDimNchwTo5Hd = 0;
const int kValidNcDimSize = 2;
static const string CONCAT_C_TO_N_OPTIMIZE_PASS_NAME = "ConcatCToNOptimizeFusionPass";

void ConcatCToNOptimizeFusionPass::GetRealConcatDimFromNCHWTo5HD(const ge::OpDescPtr &op_desc, int64_t &concat_dim) {
  bool meet_condition = true;
  for (const auto &cur_desc_ptr : op_desc->GetAllInputsDescPtr()) {
    if (cur_desc_ptr == nullptr) {
      return;
    }
    auto ori_shape = cur_desc_ptr->GetOriginShape().GetDims();
    if (ori_shape.size() < kValidNcDimSize) {
      FE_LOGE("[%s]: original input shape is invalid", op_desc->GetName().c_str());
      return;
    }
    int64_t origin_n = ori_shape[0];
    int64_t origin_c = ori_shape[1];
    ge::DataType data_type = cur_desc_ptr->GetDataType();
    bool cond = (origin_n == 1 && ((data_type == ge::DT_FLOAT16 && origin_c % 16 == 0) ||
			           (data_type == ge::DT_INT8 && origin_c % 32 == 0) ||
				   (data_type == ge::DT_INT4 && origin_c % 64 == 0)));
    if (!cond) {
      meet_condition = false;
      break;
    }
  }
  if (meet_condition) {
    concat_dim = kRealDimNchwTo5Hd;
    FE_LOGD("Meet condition_nchw_to_nc1hwc0: %d, change concat_dim to %d.", meet_condition, kRealDimNchwTo5Hd);
  }
}

bool ConcatCToNOptimizeFusionPass::Checker(const ge::NodePtr &node, const ge::OpDescPtr &op_desc) {
  string node_name = op_desc->GetName();
  // check whether concat_dim can be changed from C(1) to N(0)
  int64_t concat_dim = -1;
  (void)ge::AttrUtils::GetInt(op_desc, CONCAT_DIM, concat_dim);
  auto input_tensor = op_desc->MutableInputDesc(0);
  if (input_tensor == nullptr) {
    return false;
  }
  auto input_format = ge::GetPrimaryFormat(input_tensor->GetFormat());
  ge::Format input_orinal_format = input_tensor->GetOriginFormat();

  bool condition_nchw_5hd = (input_orinal_format == ge::FORMAT_NCHW && input_format == ge::FORMAT_NC1HWC0 && concat_dim == 1);
  if (!condition_nchw_5hd) {
    FE_LOGD("Not meet condition_nchw_to_nc1hwc0, graph not change.");
    return false;
  }

  if (IsUnKnownShapeOp(*(op_desc.get()))) {
    FE_LOGD("Concat Unknown Shape check failed, %s can not optimize.", node_name.c_str());
    return false;
  }

  GetRealConcatDimFromNCHWTo5HD(op_desc, concat_dim);
  // normal check
  if (concat_dim != kNDimIndex) {
    FE_LOGD("concat_dim is not 0, %s can not optimize.", node_name.c_str());
     return false;
  }

  if (IsFeSupportedDynamicOp(*op_desc)) {
    FE_LOGD("Concat op[%s] is unknown shape op, can not optimize.", node_name.c_str());
    return false;
  }

  if (!checker_helper.InputCheck(node)) {
    FE_LOGD("Concat input check failed, %s can not optimize.", node_name.c_str());
    return false;
  }

  if (!checker_helper.OutputCheck(node)) {
    return false;
  }

  if (InvalidMemType(op_desc)) {
    FE_LOGD("Concat mem type check failed, %s can not optimize.", node_name.c_str());
    return false;
  }

  if (!checker_helper.IsFirstVirtualConcatNodeOfParent(node)) {
    return false;
  }
  // check whether pre node is valid
  bool fusion_virtual_op_flag = true;
  for (auto input_anchor : node->GetAllInDataAnchors()) {
    if (input_anchor == nullptr || input_anchor->GetPeerOutAnchor() == nullptr ||
        input_anchor->GetPeerOutAnchor()->GetOwnerNode() == nullptr) {
      return false;
    }
    auto pre_node_desc = input_anchor->GetPeerOutAnchor()->GetOwnerNode()->GetOpDesc();
    if (!checker_helper.IsPreNodeAttrValid(pre_node_desc, fusion_virtual_op_flag, node_name)) {
      return false;
    }
  }
  return true;
}

Status ConcatCToNOptimizeFusionPass::DoFusion(ge::ComputeGraph &graph, ge::NodePtr &node,
                                              vector <ge::NodePtr> &fusion_nodes) {
  string node_name = node->GetName();
  FE_LOGD("Node[%s]: start ConcatCToNOptimizeFusionPass.", node_name.c_str());
  ge::OpDescPtr op_desc = node->GetOpDesc();
  if (!Checker(node, op_desc)) {
    FE_LOGD("Node[%s]: can not be set to no_task flag, skip.", node_name.c_str());
    return NOT_CHANGED;
  }

  SetGeAttrForConcat(op_desc, kNDimIndex);
  FE_LOGD("Node[%s]: end ConcatCToNOptimizeFusionPass.", node_name.c_str());
  return SUCCESS;
}

vector<string> ConcatCToNOptimizeFusionPass::GetNodeTypes() { return vector<string>{CONCATD, CONCATV2D}; }

string ConcatCToNOptimizeFusionPass::GetPatternName() { return CONCAT_C_TO_N_OPTIMIZE_PASS_NAME; }

REGISTER_PASS(CONCAT_C_TO_N_OPTIMIZE_PASS_NAME, SECOND_ROUND_BUILT_IN_GRAPH_PASS, ConcatCToNOptimizeFusionPass);
}
