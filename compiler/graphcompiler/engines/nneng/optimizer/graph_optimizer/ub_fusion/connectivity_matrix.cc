/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "connectivity_matrix.h"

namespace fe {
ConnectivityMatrix::ConnectivityMatrix(const ge::ComputeGraph &graph) : size_(graph.GetDirectNode().size()) {
  bit_maps.reserve(size_);
  int64_t index_loop = 0;
  for (const auto &node : graph.GetDirectNode()) {
    name_to_index_[node->GetName()] = index_loop;
    bit_maps.emplace_back(size_);
    index_loop++;
  }
};

ConnectivityMatrix::~ConnectivityMatrix() {}


std::shared_ptr<ConnectivityMatrix> ConnectivityMatrix::Generate(ge::ComputeGraph &graph) {
  std::shared_ptr<ConnectivityMatrix> result =
      std::shared_ptr<ConnectivityMatrix>(new (std::nothrow) ConnectivityMatrix(graph));
  FE_CHECK((result == nullptr), FE_LOGE("Reachability Map alloc failed"), return nullptr);
  std::vector<ge::NodePtr> inputs;

  for (auto &node : graph.GetDirectNode()) {
    inputs.clear();
    for (auto &input_node : node->GetInAllNodes()) {
      inputs.push_back(input_node);
    }
    result->SetConnectivity(inputs, node);
  }
  return result;
}

void ConnectivityMatrix::Update(const ge::ComputeGraph &graph, vector<ge::NodePtr> &fusion_nodes) {
  LargeBitMap new_bit_vector(graph.GetDirectNode().size());
  new_bit_vector.SetValues(0);
  for (size_t i = 0; i < fusion_nodes.size(); i++) {
    new_bit_vector.Or(GetBitMap(fusion_nodes[i]));
  }
  for (auto &node : graph.GetDirectNode()) {
    bool is_connected_to_fusion = false;
    for (size_t i = 0; i < fusion_nodes.size(); i++) {
      if (GetBitMap(node).GetBit(GetIndex(fusion_nodes[i]))) {
        is_connected_to_fusion = true;
        break;
      }
    }
    if (is_connected_to_fusion) {
      GetBitMap(node).Or(new_bit_vector);
    }
  }
}

void ConnectivityMatrix::SetConnectivity(const std::vector<ge::NodePtr> &inputs,
                                         const ge::NodePtr &node) {
  LargeBitMap &bitmap = GetBitMap(node);
  if (std::find(inputs.begin(), inputs.end(), node) == inputs.end()) {
    bitmap.SetValues(0);
  }

  bitmap.SetBit(GetIndex(node));
  for (const ge::NodePtr &input : inputs) {
    if (input != node) {
      bitmap.Or(GetBitMap(input));
    }
  }
}

int64_t ConnectivityMatrix::GetIndex(const ge::NodePtr &node) const {
  auto iter = name_to_index_.find(node->GetName());
  if (iter != name_to_index_.end()) {
    return iter->second;
  } else {
    FE_LOGW("node %s is not found in name_to_index_", node->GetName().c_str());
    return 0;
  }
}

bool ConnectivityMatrix::IsConnected(const ge::NodePtr &a, const ge::NodePtr &b) const {
  return GetBitMap(b).GetBit(GetIndex(a));
}

const LargeBitMap &ConnectivityMatrix::GetBitMap(const ge::NodePtr &node) const {
  return bit_maps[GetIndex(node)];
}

LargeBitMap &ConnectivityMatrix::GetBitMap(const ge::NodePtr &node) {
  return bit_maps[GetIndex(node)];
}
}