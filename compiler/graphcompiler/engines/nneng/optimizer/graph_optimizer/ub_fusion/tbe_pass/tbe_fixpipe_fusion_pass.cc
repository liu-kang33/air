/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "graph_optimizer/ub_fusion/tbe_pass/tbe_fixpipe_fusion_pass.h"
#include <algorithm>
#include <string>
#include <vector>
#include "graph_optimizer/graph_fusion/fusion_pass_manager/builtin_pass/fixpipe_common.h"
#include "common/aicore_util_constants.h"
#include "common/configuration.h"
namespace fe {
using std::vector;

const std::string kPatternTransData = "transdata";
static const std::string kPatternCube = "convolution";
constexpr uint32_t MAX_OUTDATA_SIZE = 2;
static const std::string kOpTypeTransData = "TransData";
static const std::string kOpTypeFixPipe = "FixPipe";

vector<BufferFusionPattern *> TbeFixPipeFusionPass::DefinePatterns() {
  vector<BufferFusionPattern *> patterns;
  string pass_name = "TbeFixPipeFusionPass";
  BufferFusionPattern *pattern = new (std::nothrow) BufferFusionPattern(pass_name);
  FE_CHECK((pattern == nullptr), REPORT_FE_ERROR("[SubGraphOpt][FixPipeRules][DefPtn] New an object failed."),
           return patterns);
  FE_LOGD("Start to define %s pass pattern.", pass_name.c_str());
  // define pattern rules
  pattern
      ->AddOpDesc(kPatternTransData, {kOpTypeTransData}, TBE_PATTERN_NUM_NONE, TBE_PATTERN_NUM_DEFAULT,
                  TBE_PATTERN_GROUPID_INVALID, IGNORE_SHAPE_TYPE, true)
      .AddOpDesc(kPatternCube, {OP_PATTERN_CONV, OP_PATTERN_GEMM}, TBE_PATTERN_NUM_NONE, TBE_PATTERN_NUM_DEFAULT,
                 TBE_PATTERN_GROUPID_INVALID, IGNORE_SHAPE_TYPE)
      .SetHead({kPatternCube, kPatternTransData})
      .SetOutputs(kPatternTransData, {kPatternCube})
      .SetOutputs(kPatternCube, {}, TBE_OUTPUT_BRANCH_SINGLE, true);

  patterns.push_back(pattern);

  FE_LOGD("End to define %s pass pattern.", pass_name.c_str());
  return patterns;
}

void TbeFixPipeFusionPass::AddFixPipeNode(vector<ge::NodePtr> &cube_nodes, vector<ge::NodePtr> &fusion_nodes) {
  for (auto cube_node : cube_nodes) {
    auto out_nodes_dataanchor = cube_node->GetOutDataAnchor(0);
    if (out_nodes_dataanchor == nullptr) {
      continue;
    }
    for (auto outnodes_peeranchor : out_nodes_dataanchor->GetPeerInDataAnchors()) {
      if (outnodes_peeranchor == nullptr) {
        continue;
      }
      auto outchild_node = outnodes_peeranchor->GetOwnerNode();
      if (outchild_node->GetType() == kOpTypeFixPipe && outnodes_peeranchor->GetIdx() == 0) {
        fusion_nodes.push_back(outchild_node);
        break;
      }
    }
  }
}

bool TbeFixPipeFusionPass::ReadConfig(const std::string &soc_version) {
  PlatFormInfos platform_infos;
  OptionalInfos opti_compilation_infos;
  if (PlatformInfoManager::Instance().GetPlatformInfos(soc_version, platform_infos, opti_compilation_infos) !=
      SUCCESS) {
    FE_LOGW("Fail to get platform info by soc version[%s].", soc_version.c_str());
    return false;
  }
  std::map<std::string, std::map<std::string, std::vector<CONFIGDTYPE>>> fixpipe_map;
  std::map<std::string, std::vector<std::string>> depends_units;
  std::vector<std::string> m_units;
  ReadPlatFormConfig(soc_version, m_units, depends_units, fixpipe_map);
  if (m_units.empty()) {
    FE_LOGD("failto get fixpipeconfig [%s].", soc_version.c_str());
    return false;
  }
  return true;
}

bool TbeFixPipeFusionPass::ReadConfig() {
  std::string soc_version = Configuration::Instance(AI_CORE_NAME).GetSocVersion();
  return ReadConfig(soc_version);
}
/*
 * @brief: parse nodes matched in mapping and call DoFusion
 * @param [in] graph: original graph
 * @param [out] mapping: nodes matched by pattern
 * @return bool: fusion status ok or not.
 */
Status TbeFixPipeFusionPass::GetFusionNodes(const BufferFusionMapping &mapping, vector<ge::NodePtr> &fusion_nodes) {
  FE_LOGD("Begin to do TbeFixPipeFusionPass!");
  if (!ReadConfig()) {
    return SUCCESS;
  }
  vector<ge::NodePtr> cube_nodes = GetMatchedNodesByDescName(kPatternCube, mapping);
  vector<ge::NodePtr> trans_nodes = GetMatchedNodesByDescName(kPatternTransData, mapping);
  if (cube_nodes.empty()) {
    return SUCCESS;
  }
  fusion_nodes = GetMatchedNodes(mapping);
  if (!trans_nodes.empty() && NotSupportCubeWeightSupportTrans(trans_nodes[0], cube_nodes[0]->GetType())) {
    auto iter = find(fusion_nodes.begin(), fusion_nodes.end(), trans_nodes[0]);
    if (iter != fusion_nodes.end()) {
      fusion_nodes.erase(iter);
    }
  }
  int64_t group = 1;
  (void)ge::AttrUtils::GetInt(cube_nodes[0]->GetOpDesc(), "groups", group);
  bool no_group = (group == 1);
  auto in_data_nodes = cube_nodes[0]->GetInDataNodes();
  if (no_group && in_data_nodes.size() >= MAX_OUTDATA_SIZE) {
    ge::NodePtr weight_node = in_data_nodes.at(1);
    if (weight_node->GetType() == kOpTypeTransData && CubeWeightSupportTrans(weight_node, cube_nodes[0]->GetType())) {
      fusion_nodes.emplace_back(weight_node);
    }
  }
  AddFixPipeNode(cube_nodes, fusion_nodes);
  if (fusion_nodes.size() == 1) {
    fusion_nodes.clear();
  }
  return SUCCESS;
}

bool TbeFixPipeFusionPass::CubeWeightSupportTrans(const ge::NodePtr &node, const string &type) {
  if (type == "Conv2D") {
    return node->GetOpDesc()->GetInputDesc(0).GetFormat() == ge::FORMAT_NHWC &&
           node->GetOpDesc()->GetOutputDesc(0).GetFormat() == ge::FORMAT_FRACTAL_Z;
  } else if (type == "MatMulV2") {
    return node->GetOpDesc()->GetInputDesc(0).GetFormat() == ge::FORMAT_ND &&
           node->GetOpDesc()->GetOutputDesc(0).GetFormat() == ge::FORMAT_FRACTAL_NZ;
  } else {
    return false;
  }
}

bool TbeFixPipeFusionPass::NotSupportCubeWeightSupportTrans(const ge::NodePtr &node, const string &type) {
  auto input_desc = node->GetOpDesc()->GetInputDesc(0);
  auto output_desc = node->GetOpDesc()->GetOutputDesc(0);
  auto input_format = input_desc.GetFormat();
  auto output_format = output_desc.GetFormat();
  if (type == "Conv2D" || type == "Conv2DBackpropInputD") {
    if (input_format != ge::FORMAT_NHWC || output_format != ge::FORMAT_NC1HWC0) {
      return true;
    }
    return false;
  }
  if (type == "MatMulV2") {
    if (input_format != ge::FORMAT_ND || output_format != ge::FORMAT_FRACTAL_NZ) {
      return true;
    }
    return false;
  }
  return false;
}
}  // namespace fe
