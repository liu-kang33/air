/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FFTS_PLUS_OPTIMIZER_GRAPH_OPTIMIZER_FFTS_PLUS_GRAPH_OPTIMIZER_H_
#define FFTS_PLUS_OPTIMIZER_GRAPH_OPTIMIZER_FFTS_PLUS_GRAPH_OPTIMIZER_H_

#include <map>
#include <memory>
#include <string>
#include <vector>
#include "adapter/adapter_itf/op_store_adapter.h"
#include "common/fe_inner_error_codes.h"
#include "common/fe_utils.h"
#include "common/graph/fe_graph_utils.h"
#include "common/optimizer/graph_optimizer.h"
#include "common/optimizer/graph_optimizer_types.h"
#include "common/optimizer/graph_optimizer_types.h"
#include "common/optimizer/optimize_utility.h"
#include "fusion_rule_manager/fusion_rule_manager.h"
#include "graph/compute_graph.h"
#include "graph_optimizer/fusion_common/fusion_pass_manager.h"
#include "graph_optimizer/graph_fusion/graph_fusion.h"
#include "ops_kernel_store/fe_ops_kernel_info_store.h"
#include "common/fe_error_code.h"
#include "common/util/error_manager/error_manager.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"

namespace fe {

using FEOpsKernelInfoStorePtr = std::shared_ptr<FEOpsKernelInfoStore>;
using FFTSOptimizerFunc = std::function<tune::Status(ge::ComputeGraph &, bool)>;
using SubGraphNodeMap = std::map<uint32_t, std::vector<ge::NodePtr>>;

class FFTSPlusGraphOptimizer : public ge::GraphOptimizer {
 public:
  FFTSPlusGraphOptimizer();
  ~FFTSPlusGraphOptimizer() override;

  /**
   * @ingroup fe
   * @brief prohibit copy and assign construct
   */
  FFTSPlusGraphOptimizer(const FFTSPlusGraphOptimizer&) = delete;
  FFTSPlusGraphOptimizer& operator=(const FFTSPlusGraphOptimizer&) = delete;

  /*
   *  @ingroup fe
   *  @brief   initialize graph optimizer
   *  @param   [in] options
   *  @return  SUCCESS or FAILED
   */
  Status Initialize(const std::map<string, string>& options,
                    ge::OptimizeUtility *const optimize_utility) override;

  /*
   *  @ingroup fe
   *  @brief   close graph optimizer
   *  @return  SUCCESS or FAILED
   */
  Status Finalize() override;

  /*
   *  @ingroup fe
   *  @brief   optimize original graph
   *  @param   [in|out] graph  compute graph
   *  @return  SUCCESS or FAILED
   */
  Status OptimizeOriginalGraph(ge::ComputeGraph& graph) override;

  /*
   *  @ingroup fe
   *  @brief   optimize fused graph
   *  @param   [in|out] graph   compute graph
   *  @return  SUCCESS or FAILED
   */
  Status OptimizeFusedGraph(ge::ComputeGraph& graph) override;

  /*
   *  @ingroup fe
   *  @brief   get attribute of graph optimizer
   *  @param   [in|out] attrs
   *  @return  SUCCESS or FAILED
   */
  Status GetAttributes(ge::GraphOptimizerAttribute& attrs) const override;

  Status OptimizeWholeGraph(ge::ComputeGraph& graph) override;

    /*
   *  @ingroup fe
   *  @brief   optimize complete graph
   *  @param   [in|out] graph   compute graph
   *  @return  SUCCESS or FAILED
   */
  Status OptimizeGraphBeforeBuild(ge::ComputeGraph& graph) override;
 private:
  Status TransSubGraphToFunctionOp(ge::ComputeGraph& graph);
  Status GetSubGraphNodes(const ge::ComputeGraph &graph, SubGraphNodeMap &node_map) const;
  ge::OpDescPtr CreateFunctionOp(vector<ge::NodePtr> &node_vec) const;
  Status AddFunctionNodeInputDesc(ge::OpDescPtr fus_op, vector<FusionDataFlow> &fus_input_edge_list);
  Status AddFunctionNodeOutputDesc(ge::OpDescPtr fus_op, vector<FusionDataFlow> &fus_output_edge_list);
  Status CreateFunctionOpSubGraph(ge::NodePtr &function_node,
                                  std::vector<ge::NodePtr> &node_vec,
                                  vector<FusionDataFlow> &input_edge_list,
                                  vector<FusionDataFlow> &output_edge_list);
  Status TransSingleSubGraph(ge::ComputeGraph &graph, std::vector<ge::NodePtr> &node_vec);
  Status PostSubGraph(ge::ComputeGraph& graph);

  Status CacheManagement(ge::ComputeGraph& graph);

  void GetSliceInfo(const ge::ComputeGraph &graph);

  void JudgeThreadTensorAlignedAndAlarm(const ge::NodePtr &node, 
                                        vector<vector<vector<ffts::DimRange>>> &tensor_slice, 
                                        const bool &is_input) const;

  ffts::ThreadSliceMapPtr GetSliceInfoFromJson(const ge::OpDescPtr &op_desc_ptr) const;
 private:
  PluginManagerPtr lx_fusion_plugin_ffts_plus_;
  FFTSOptimizerFunc FFTSOptimizer_{nullptr};
  ge::GraphOptimizerAttribute graph_optimizer_attr_;
  std::shared_ptr<GraphComm> graph_comm_ptr_;
  size_t sgt_graph_index_{0};
  bool init_flag_;

};
}  // namespace fe
#endif  // FUSION_ENGINE_OPTIMIZER_GRAPH_OPTIMIZER_FE_GRAPH_OPTIMIZER_H_
