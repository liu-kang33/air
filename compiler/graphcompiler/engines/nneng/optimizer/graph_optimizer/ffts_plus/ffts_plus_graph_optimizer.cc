/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph_optimizer/ffts_plus/ffts_plus_graph_optimizer.h"
#include <memory>
#include <mutex>
#include <vector>
#include "graph_optimizer/ffts_plus/cache_manager.h"
#include "common/configuration.h"
#include "common/aicore_util_constants.h"
#include "common/fe_inner_attr_define.h"
#include "common/fe_utils.h"
#include "ge/ge_api_types.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/ge_context.h"
#include "graph/tuning_utils.h"

namespace fe {

void FFTSPlusGraphOptimizer::JudgeThreadTensorAlignedAndAlarm(const ge::NodePtr &node,
                                                              vector<vector<vector<ffts::DimRange>>> &tensor_slice,
                                                              const bool &is_input) const {
  if (!node) {
    return;
  }
  for (size_t i = 0; i < tensor_slice.size(); i++) {
    for (size_t j = 0; j < tensor_slice[i].size(); j++) {
      const auto &tensor_desc = is_input ? node->GetOpDesc()->MutableInputDesc(j) : node->GetOpDesc()->MutableOutputDesc(j);
      if (!tensor_desc) {
        continue;
      }
      ge::DataType tensor_type = tensor_desc->GetDataType();
      auto data_size = ge::GetSizeByDataType(tensor_type);
      int64_t size = data_size;
      for (size_t k = 0; k < tensor_slice[i][j].size(); k++) {
        size *= (tensor_slice[i][j][k].higher - tensor_slice[i][j][k].lower);
      }

      if (size % 32 != 0) {
        FE_LOGW("slice op: %s,%s is not 32 aligned, %zu-%zu, %s", node->GetOpDesc()->GetName().c_str(),
                node->GetOpDesc()->GetType().c_str(), i, j, is_input ? "input" : "output");
      }
    }
  }
}

void FFTSPlusGraphOptimizer::GetSliceInfo(const ge::ComputeGraph &graph) {
  for (auto &node : graph.GetDirectNode()) {
    ffts::ThreadSliceMapPtr slice_info_ptr = GetSliceInfoFromJson(node->GetOpDesc());
    if (slice_info_ptr) {
      (void)node->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
      FE_LOGD("GetSliceInfo: %u of op: %s success.", slice_info_ptr->thread_scope_id,
              node->GetOpDesc()->GetName().c_str());
      (void)ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadScopeId, slice_info_ptr->thread_scope_id);
      (void)ge::AttrUtils::SetBool(node->GetOpDesc(), kTypeFFTSPlus, true);
      (void)ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadMode, slice_info_ptr->thread_mode);
      (void)JudgeThreadTensorAlignedAndAlarm(node, slice_info_ptr->input_tensor_slice, true);
      (void)JudgeThreadTensorAlignedAndAlarm(node, slice_info_ptr->output_tensor_slice, false);
    }
  }
}

ffts::ThreadSliceMapPtr FFTSPlusGraphOptimizer::GetSliceInfoFromJson(const ge::OpDescPtr &op_desc_ptr) const {
  std::string str_slice_info;
  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;

  (void)ge::AttrUtils::GetStr(op_desc_ptr, ffts::kAttrSgtJsonInfo, str_slice_info);
  FE_LOGD("Slice info: %s of op: %s success.", str_slice_info.c_str(), op_desc_ptr->GetName().c_str());
  if (str_slice_info.empty()) {
    FE_LOGW("Node:%s, slice info is empty.", op_desc_ptr->GetName().c_str());
  } else {
    try {
      nlohmann::json slice_info_json = nlohmann::json::parse(str_slice_info);
      if (slice_info_json.is_null()) {
        FE_LOGW("Get l2 info: %s is empty.", str_slice_info.c_str());
      } else {
        FE_MAKE_SHARED(slice_info_ptr = std::make_shared<ffts::ThreadSliceMap>(), return nullptr);
      }
    } catch (const nlohmann::json::exception &e) {
      FE_LOGE("Parse json str failed, %s", e.what());
      return nullptr;
    }
  }
  return slice_info_ptr;
}

FFTSPlusGraphOptimizer::FFTSPlusGraphOptimizer()
    : graph_optimizer_attr_({kFFTSPlusCoreName, ge::ENGINE}),
      init_flag_(false) {}

FFTSPlusGraphOptimizer::~FFTSPlusGraphOptimizer() {}


Status FFTSPlusGraphOptimizer::Initialize(const std::map<string, string>& options,
    ge::OptimizeUtility *const optimize_utility) {
  FE_LOGI("Begin to init FFTSPlusGraphOptimizer.");
  // if graph optimizer has been initialized, return success
  if (GetPlatformAICoreMode() != FFTS_MODE_FFTS_PLUS) {
    FE_LOGW("FFTSPlusGraphOptimizer ffts_plus flag is 0");
    return SUCCESS;
  }
  if (init_flag_) {
    FE_LOGW("FFTSPlusGraphOptimizer has been initialized.");
    return SUCCESS;
  }
  const string FFTS_OPTIMIZER_FUNC_NAME = "FFTSOptimize";
  const std::string LX_FUSION_PLUGIN = "libgraph_tuner.so";

  string plugin_path = Configuration::Instance(graph_optimizer_attr_.engineName).GetFeLibPath() + LX_FUSION_PLUGIN;
  FE_MAKE_SHARED(lx_fusion_plugin_ffts_plus_ = std::make_shared<PluginManager>(plugin_path), return FAILED);
  FE_CHECK(lx_fusion_plugin_ffts_plus_ == nullptr, REPORT_FE_ERROR("[GraphOpt][Init][InitLxFusPlg] Create lx fusion \
           plugin manager ptr failed."),
  return FAILED);
  if (lx_fusion_plugin_ffts_plus_->OpenPlugin(plugin_path) != SUCCESS) {
    FE_LOGE("Failed to open %s.", plugin_path.c_str());
    return FAILED;
  }
  Status ret = lx_fusion_plugin_ffts_plus_->GetFunction<tune::Status, ge::ComputeGraph &, bool>(
      FFTS_OPTIMIZER_FUNC_NAME, FFTSOptimizer_);
  if (ret != SUCCESS) {
    FE_LOGW("Failed to get the function %s in the plugin %s.", FFTS_OPTIMIZER_FUNC_NAME.c_str(), plugin_path.c_str());
    return FAILED;
  }
  init_flag_ = true;

  FE_LOGI("Initialize success.");
  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::Finalize() {
  if (!init_flag_) {
    FE_LOGW("FFTSPlusGraphOptimizer finalize is not allowed, initialize first is necessary.");
    return SUCCESS;
  }

  init_flag_ = false;
  FE_LOGD("Finalized success.");

  return SUCCESS;
}


Status FFTSPlusGraphOptimizer::OptimizeOriginalGraph(ge::ComputeGraph& graph) {
  if (!init_flag_) {
    REPORT_FE_ERROR("[GraphOpt][init] FEGraphOptimizer has not been initialized.");
    return FAILED;
  }

  FeGraphUtils::DumpGraphAndOnnx(graph, "OptimizeOriginalGraph_FeTopoSortingAfter");
  FeGraphUtils::DumpSubGraphAndOnnx(graph, "OptimizeOriginalGraph_FeTopoSortingAfter_Subgraph");

  FE_LOGI("Optimize original graph[%s] success, node_size:%zu.", graph.GetName().c_str(), graph.GetAllNodesSize());
  return SUCCESS;
}


Status FFTSPlusGraphOptimizer::OptimizeFusedGraph(ge::ComputeGraph& graph) {
  FE_LOGD("Begin to optimizing fused graph in engine[%s].", graph_optimizer_attr_.engineName.c_str());

  if (!init_flag_) {
    FE_LOGW("OptimizeFusedGraph is not allowed, initialize firstly.");
    return FAILED;
  }
  FE_TIMECOST_START(OptimizeFusedGraph);

  // do sgt slice
  FE_CHECK(FFTSOptimizer_ == nullptr, REPORT_FE_ERROR("[SubGraphOpt][BufFusProc] FFTSOptimizerFunc is nullptr."),
  return FAILED);
  Status ret = FFTSOptimizer_(graph, true);
  if (ret != tune::SUCCESS) {
    REPORT_FE_ERROR("[SubGraphOpt][BufFusProc][fuc] FFTSOptimizerFunc failed.");
    return FAILED;
  }

  GetSliceInfo(graph);

  CacheManager::SetPersistWeightForGraph(graph);
  const auto &parent_node = graph.GetParentNode();
  bool is_control_graph =
       (parent_node != nullptr && (CONTROL_OP_V2_TYPE.count(parent_node->GetType())));
  if (is_control_graph) {
    FE_LOGD("graph %s 's parent node is control Op %s, skipped gen FunctionOp.",
            graph.GetName().c_str(), parent_node->GetName().c_str());
    return SUCCESS;
  }

  if (TransSubGraphToFunctionOp(graph) != SUCCESS) {
    FE_LOGE("TransSubGraphToFunctionOp failed, graph %s.", graph.GetName().c_str());
    return FAILED;
  }
  FE_TIMECOST_END(OptimizeFusedGraph, "FFTSPlusGraphOptimizer::OptimizeFusedGraph");
  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::GetAttributes(ge::GraphOptimizerAttribute& attrs) const {
  attrs = graph_optimizer_attr_;
  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::OptimizeWholeGraph(ge::ComputeGraph& graph) {
  return SUCCESS;
}

ge::OpDescPtr FFTSPlusGraphOptimizer::CreateFunctionOp(vector<ge::NodePtr> &node_vec) const {
  if (node_vec.empty()) {
    return nullptr;
  }
  ge::NodePtr first_node = node_vec[0];
  FE_CHECK(first_node == nullptr, FE_LOGE("CreateFusionOp nullptr Pointer."), return nullptr);
  ge::OpDescPtr function_opdef = nullptr;
  FE_MAKE_SHARED(function_opdef = std::make_shared<ge::OpDesc>(ge::OpDesc()), return nullptr);
  std::string fusion_node_name;

  for (ge::NodePtr &node : node_vec) {
    FE_CHECK(node == nullptr, REPORT_FE_ERROR("[FFTSSubGraphOpt][CrtFuncOp] node is nullptr."), return nullptr);
    fusion_node_name += node->GetOpDesc()->GetName();
      if (fusion_node_name.size() > kMaxOpNmaLen) {
      fusion_node_name = first_node->GetOpDesc()->GetName() + "_ffts_fusion";
      break;
    }
  }
  function_opdef->SetName(fusion_node_name);
  function_opdef->SetType("PartitionedCall");

  // copy session graph id
  string session_graph_id;
  if (ge::AttrUtils::GetStr(first_node->GetOpDesc(), ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id)) {
    if (!ge::AttrUtils::SetStr(function_opdef, ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id)) {
      FE_LOGE("Op[%s]: fail to set the attribute %s.", fusion_node_name.c_str(),
              ge::ATTR_NAME_SESSION_GRAPH_ID.c_str());
      return nullptr;
    }
  }

  function_opdef->SetOpEngineName(kFFTSPlusCoreName);

  return function_opdef;
}

Status EstablishConnection(const ge::NodePtr &node,
                           const unordered_set<std::string> &node_name_set,
                           ge::CompleteGraphBuilder &builder) {
  const auto &src_node_name = node->GetName();
  for (const auto &out_data_anchor : node->GetAllOutDataAnchors()) {
    FE_CHECK_NOTNULL(out_data_anchor);
    auto src_idx = out_data_anchor->GetIdx();
    for (auto &in_data_anchor : out_data_anchor->GetPeerInDataAnchors()) {
      FE_CHECK_NOTNULL(in_data_anchor);
      auto dst_node = in_data_anchor->GetOwnerNode();
      FE_CHECK_NOTNULL(dst_node);
      if (!node_name_set.count(dst_node->GetName())) {
        FE_LOGD("Node[name:%s] is out op, skip.", dst_node->GetName().c_str());
        continue;
      }
      builder.AddDataLink(src_node_name, src_idx, dst_node->GetName(), in_data_anchor->GetIdx());
    }
  }
  auto out_ctrl_anchor =  node->GetOutControlAnchor();
  FE_CHECK_NOTNULL(out_ctrl_anchor);
  for (auto &in_ctrl_anchor : out_ctrl_anchor->GetPeerInControlAnchors()) {
    FE_CHECK_NOTNULL(in_ctrl_anchor);
    auto dst_node = in_ctrl_anchor->GetOwnerNode();
    FE_CHECK_NOTNULL(dst_node);
    if (!node_name_set.count(dst_node->GetName())) {
      FE_LOGD("Dst Node[name:%s] linked by control edge is out op, skip.", dst_node->GetName().c_str());
      continue;
    }
    builder.AddControlLink(src_node_name, dst_node->GetName());
  }
  return SUCCESS;
}

static void SetReuseInputAttr(const ge::OpDescPtr &function_op_desc)
{
  for (size_t i = 0; i < function_op_desc->GetAllOutputsDescSize(); i++) {
    auto out_desc = function_op_desc->MutableOutputDesc(i);
    if (out_desc == nullptr) {
      continue;
    }
    ge::TensorUtils::SetReuseInput(*out_desc.get(), false);
  }
}

Status FFTSPlusGraphOptimizer::CreateFunctionOpSubGraph(ge::NodePtr &function_node,
                                                        std::vector<ge::NodePtr> &node_vec,
                                                        vector<FusionDataFlow> &input_edge_list,
                                                        vector<FusionDataFlow> &output_edge_list) {
  if (node_vec.empty()) {
    return FAILED;
  }
  auto graph = node_vec[0]->GetOwnerComputeGraph();
  FE_LOGD("ai-core sub graph name is %s.", graph->GetName().c_str());
  auto func_graph = function_node->GetOwnerComputeGraph();
  FE_LOGD("func owner graph name is %s.", func_graph->GetName().c_str());
  ge::ComputeGraphPtr src_graph = graph->TryGetExtAttr("part_src_graph", ge::ComputeGraphPtr());
  FE_CHECK_NOTNULL(src_graph);
  FE_LOGD("src graph name is %s", src_graph->GetName().c_str());
  auto root_graph = ge::GraphUtils::FindRootGraph(src_graph);
  FE_CHECK_NOTNULL(root_graph);
  FE_LOGD("root graph name is %s", root_graph->GetName().c_str());
  unordered_set<std::string> node_name_set;
  for (auto &node : node_vec) {
    node_name_set.emplace(node->GetName());
  }

  string sgt_graph_name = func_graph->GetName() + "_sgt_graph_" + to_string(sgt_graph_index_++);
  FE_LOGD("function_node: %s, sgt graph name is %s", function_node->GetName().c_str(), sgt_graph_name.c_str());
  ge::CompleteGraphBuilder builder(sgt_graph_name, false);

  for (auto &node : node_vec) {
    /* Adds a node to the builder. Currently, only the input parameter op_desc
       is supported, and the connection information will be lost. Therefore,
       the AddDataLink interface needs to be invoked to re-establish the
       connection. */
    builder.AddNode(node->GetOpDesc());

    FE_LOGD("Begin re-establish connection of node[name:%s]. out data size is %u.",
            node->GetName().c_str(), node->GetAllOutDataAnchorsSize());
    (void)EstablishConnection(node, node_name_set, builder);
  }

  FE_LOGD("input_edge_list size is %zu", input_edge_list.size());
  std::map<uint32_t, uint32_t> input_mapping;
  uint32_t index = 0;
  for (auto &input_edge : input_edge_list) {
    FE_CHECK(input_edge.edge.first == nullptr, FE_LOGW("input_edge.first is null"), return FAILED);
    FE_CHECK(input_edge.edge.second == nullptr, FE_LOGW("input_edge.second is null"), return FAILED);
    std::uint32_t src_idx = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.first)->GetIdx();
    std::uint32_t fusion_idx = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.second)->GetIdx();
    ge::NodePtr src_node = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.first)->GetOwnerNode();
    ge::NodePtr fusion_node = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.second)->GetOwnerNode();
    FE_LOGD("srcIdx is %d, fusion_idx is %d. src_node is : %s, fusion_node is : %s.", src_idx, fusion_idx,
            src_node->GetName().c_str(), fusion_node->GetName().c_str());
    // add relation: input index of new graph to input index of original graph.
    std::vector<std::string> ffts_node_names{fusion_node->GetName()};
    std::vector<uint32_t> dst_indexes{fusion_idx};
    builder.SetInput(index, ffts_node_names, dst_indexes);
    input_mapping.emplace(index, index);
    index++;
  }
  builder.SetInputMapping(input_mapping);

  FE_LOGD("fusionOpOutputAnchorsMap size is %zu", output_edge_list.size());
  std::map<uint32_t, uint32_t> output_mapping;
  std::vector<ge::AnchorPtr> sgt_output;
  uint32_t output_index = 0;
  for (auto &output_edge : output_edge_list) {
    if (std::find(sgt_output.begin(), sgt_output.end(), output_edge.edge.first) == sgt_output.end()) {
      sgt_output.push_back(output_edge.edge.first);
      FE_CHECK(output_edge.edge.first == nullptr, FE_LOGW("output_edge.first is null"), return FAILED);
      FE_CHECK(output_edge.edge.second == nullptr, FE_LOGW("iterVec->second is null"), return FAILED);
      std::uint32_t src_idx = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.first)->GetIdx();
      std::uint32_t fusion_idx = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.second)->GetIdx();
      ge::NodePtr src_node = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.first)->GetOwnerNode();
      ge::NodePtr fusion_node = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.second)->GetOwnerNode();
      FE_LOGD("out src_idx is %d, out fusion_idx is %d. src_node is : %s, fusion_node is : %s", src_idx, fusion_idx,
              src_node->GetName().c_str(), fusion_node->GetName().c_str());
      // add relation: output index of new graph to output index of original graph.
      std::vector<std::string> input_node_names{src_node->GetName()};
      builder.AddOutput(src_node->GetName(), src_idx);
      output_mapping.emplace(output_index, output_index);
      output_index++;
    }
  }

  builder.SetOutputMapping(output_mapping);
  builder.SetParentNode(function_node);

  ge::graphStatus status = ge::GRAPH_SUCCESS;
  string err_msg;
  auto sgt_graph = builder.Build(status, err_msg);
  if (status != ge::GRAPH_SUCCESS) {
    FE_LOGE("Build graph not success. status is : %d, err_msg is : %s.", status, err_msg.c_str());
    return FAILED;
  }

  sgt_graph->SetParentGraph(graph);
  sgt_graph->SetParentNode(function_node);
  ge::OpDescPtr function_op_desc = function_node->GetOpDesc();
  function_op_desc->AddSubgraphName(sgt_graph->GetName());
  function_op_desc->SetSubgraphInstanceName(0, sgt_graph->GetName());
  (void)ge::AttrUtils::SetGraph(function_op_desc, "_sgt_sub_graph", sgt_graph);
  (void)ge::AttrUtils::SetStr(function_op_desc, "_ffts_plus_sub_graph", sgt_graph->GetName().c_str());
  (void)ge::AttrUtils::SetBool(function_op_desc, "_sgt_function_op", true);
  (void)ge::AttrUtils::SetStr(function_op_desc, kAttrCompositeEngineName, kFFTSPlusCoreName);
  (void)ge::AttrUtils::SetStr(function_op_desc, kAttrCompositeKernelLibName, kFFTSPlusCoreName);
  SetReuseInputAttr(function_op_desc);
  if (root_graph->AddSubgraph(sgt_graph->GetName(), sgt_graph) != ge::GRAPH_SUCCESS) {
    FE_LOGE("Add sub graph not success. status is : %d, root graph: %s.", status, root_graph->GetName().c_str());
    return FAILED;
  }
  FE_LOGD("Add sub graph success. root graph: %s, subgraph: %s.",
          root_graph->GetName().c_str(), sgt_graph->GetName().c_str());

  for (auto &node : node_vec) {
    std::vector<int> io_map = {0};
    if (node->GetAllOutDataAnchors().size() == 0) {
      io_map = {};
    } else if (node->GetAllInDataAnchors().size() == 0) {
      io_map = {-1};
    }
    if (ge::GraphUtils::IsolateNode(node, io_map) != ge::GRAPH_SUCCESS ) {
      FE_LOGE("Isolate Node %s not success.", node->GetName().c_str());
      return FAILED;
    }
    if (ge::GraphUtils::RemoveNodeWithoutRelink(graph, node) != ge::GRAPH_SUCCESS) {
      FE_LOGE("Remove node: %s without relink failed", node->GetName().c_str());
      return FAILED;
    }
  }
  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::TransSingleSubGraph(ge::ComputeGraph &graph, std::vector<ge::NodePtr> &node_vec) {
  vector<FusionDataFlow> input_edge_list;
  vector<FusionDataFlow> output_edge_list;
  vector<FusionDataFlow> input_ctrl_edge_list;
  vector<FusionDataFlow> output_ctrl_edge_list;

  FE_LOGD("TransSingleSubGraph enter");

  if (graph_comm_ptr_->GetFusionNodeEdgeList(node_vec, input_edge_list, output_edge_list) != SUCCESS) {
    FE_LOGE("GetFusionNodeEdgeList failed.");
    return FAILED;
  }

  if (graph_comm_ptr_->GetFusionNodeCtrlEdgeList(node_vec, input_ctrl_edge_list, output_ctrl_edge_list) != SUCCESS) {
    FE_LOGE("GetFusionNodeCtrlEdgeList failed.");
    return FAILED;
  }

  ge::OpDescPtr function_op = CreateFunctionOp(node_vec);
  if (function_op == nullptr) {
    FE_LOGE("CreateFunctionOp failed.");
    return FAILED;
  }

  if (AddFunctionNodeInputDesc(function_op, input_edge_list) != SUCCESS) {
    FE_LOGE("Failed to AddFusionNodeInputDesc.");
    return FAILED;
  }

  if (AddFunctionNodeOutputDesc(function_op, output_edge_list) != SUCCESS) {
    FE_LOGE("Failed to AddFusionNodeOutputDesc.");
    return FAILED;
  }

  ge::NodePtr function_node = graph.AddNode(function_op);
  FE_CHECK_NOTNULL(function_node);

  // Merge Same scope node
  if (graph_comm_ptr_->MergeFusionNodeEdgeList(function_node, node_vec, input_edge_list, output_edge_list) !=
      SUCCESS) {
    FE_LOGE("MergeFusionNodeEdgeList failed!");
    return FAILED;
  }

  if (graph_comm_ptr_->MergeFusionNodeCtrlEdgeList(function_node, node_vec, input_ctrl_edge_list,
                                                   output_ctrl_edge_list) != SUCCESS) {
    FE_LOGE("MergeFusionNodeCtrlEdgeList failed!");
    return FAILED;
  }

  return CreateFunctionOpSubGraph(function_node, node_vec, input_edge_list, output_edge_list);
}


Status FFTSPlusGraphOptimizer::GetSubGraphNodes(const ge::ComputeGraph &graph, SubGraphNodeMap &node_map) const {
  for (auto const &node : graph.GetDirectNode()) {
    FE_CHECK(node == nullptr, FE_LOGE("node is nullptr."), return FAILED);
    auto op_desc_ptr = node->GetOpDesc();

    uint32_t thread_scope_id = 0;
    (void)ge::AttrUtils::GetInt(op_desc_ptr, kThreadScopeId, thread_scope_id);
    if (thread_scope_id == 0) {
      FE_LOGD("op %s is not belong to any subgraph.", node->GetName().c_str());
      continue;
    }

    const SubGraphNodeMap::iterator nodelist_it = node_map.find(thread_scope_id);
    if (nodelist_it == node_map.end()) {
      std::vector<ge::NodePtr> node_list_new;
      node_list_new.push_back(node);
      node_map.insert(std::pair<uint32_t, std::vector<ge::NodePtr>>(thread_scope_id, node_list_new));
      FE_LOGD("add %u. op %s.", thread_scope_id, node->GetName().c_str());
    } else {
      nodelist_it->second.push_back(node);
      FE_LOGD("op %s push back %u.", node->GetName().c_str(), thread_scope_id);
    }
  }
  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::AddFunctionNodeInputDesc(ge::OpDescPtr fus_op,
                                                        vector<FusionDataFlow> &fus_input_edge_list) {
  int32_t fusion_dst_index = -1;
  graph_comm_ptr_->ClearFusionSrc();
  vector<bool> fusion_is_input_const_vector;
  uint32_t tensor_desc_index = 0;

  for (FusionDataFlow &dataflow : fus_input_edge_list) {
    auto inedge = dataflow.edge;
    std::pair<string, ge::AnchorPtr> &node_dstindex_pair = dataflow.node_dataindex_pair;
    int64_t src_op_id = inedge.first->GetOwnerNode()->GetOpDesc()->GetId();

    // only support data edge
    fusion_dst_index = fusion_dst_index + 1;
    vector<int64_t> input_vector;
    input_vector = fus_op->GetInputOffset();
    if (static_cast<uint32_t>(fusion_dst_index) < input_vector.size()) {
      return FAILED;
    }

    ge::DataAnchorPtr in_edge_dst_data_anchor_ptr = std::static_pointer_cast<ge::DataAnchor>(inedge.second);
    FE_CHECK(in_edge_dst_data_anchor_ptr == nullptr, FE_LOGE("inEdgeDstDataAnchorPtr is nullptr."), return FAILED);
    ge::OpDescPtr in_edge_dst_op_desc_ptr = in_edge_dst_data_anchor_ptr->GetOwnerNode()->GetOpDesc();
    vector<bool> is_input_const_vector = in_edge_dst_op_desc_ptr->GetIsInputConst();
    uint32_t dst_anchor_index = static_cast<uint32_t>(in_edge_dst_data_anchor_ptr->GetIdx());
    if (dst_anchor_index < in_edge_dst_op_desc_ptr->GetInputsSize()) {
      if (fus_op->AddInputDesc(*(in_edge_dst_op_desc_ptr->GetInputDescPtr(in_edge_dst_data_anchor_ptr->GetIdx()))) !=
          ge::GRAPH_SUCCESS) {
        FE_LOGE("Add input desc failed.");
        return FAILED;
      }
      FE_LOGD("fusOp name %s, %d", fus_op->GetName().c_str(), in_edge_dst_data_anchor_ptr->GetIdx());

      if (dst_anchor_index < is_input_const_vector.size()) {
        fusion_is_input_const_vector.push_back(is_input_const_vector[in_edge_dst_data_anchor_ptr->GetIdx()]);
      } else {
        fusion_is_input_const_vector.push_back(false);
      }
      tensor_desc_index++;
    } else {
      int32_t input_desc_size = in_edge_dst_op_desc_ptr->GetInputsSize();
      int32_t is_input_const_size = is_input_const_vector.size();
      int32_t DstIndex = in_edge_dst_data_anchor_ptr->GetIdx();
      FE_LOGE("AddFusionNodeInput input_desc_size %u, is_input_const_size %u, input DstIndex %u.",
              (uint32_t)input_desc_size, (uint32_t)is_input_const_size, (uint32_t)DstIndex);
      return FAILED;
    }
    fus_op->SetIsInputConst(fusion_is_input_const_vector);
    graph_comm_ptr_->AddFusionInputSrc(src_op_id, inedge.first, fusion_dst_index, node_dstindex_pair);
  }

  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::AddFunctionNodeOutputDesc(ge::OpDescPtr fus_op,
                                                         vector<FusionDataFlow> &fus_output_edge_list) {
  graph_comm_ptr_->ClearFusionSrc();
  graph_comm_ptr_->ClearFusionDst();

  int32_t fusion_src_index = 0;
  for (FusionDataFlow &dataflow : fus_output_edge_list) {
    auto outedge = dataflow.edge;
    std::pair<string, ge::AnchorPtr> &node_srcindex_pair = dataflow.node_dataindex_pair;
    int64_t dst_op_id = outedge.second->GetOwnerNode()->GetOpDesc()->GetId();
    ge::DataAnchorPtr out_edge_dst_data_anchor_ptr = std::static_pointer_cast<ge::DataAnchor>(outedge.second);
    FE_CHECK(out_edge_dst_data_anchor_ptr == nullptr, FE_LOGE("outEdgeDstDataAnchorPtr is nullptr."), return FAILED);
    if (graph_comm_ptr_->IsFusionDstExist(dst_op_id, outedge.second)) {
      FE_LOGI("MergeFusionNodeOutputEdgeList Dstid %u, DstIndex %u.", (uint32_t)dst_op_id,
              (uint32_t)out_edge_dst_data_anchor_ptr->GetIdx());
      continue;
    }
    graph_comm_ptr_->SaveFusionDst(dst_op_id, outedge.second);

    int32_t fusion_src_exist_index;
    int32_t fusion_dst_exist_index;

    ge::DataAnchorPtr out_edge_src_data_anchor_ptr = std::static_pointer_cast<ge::DataAnchor>(outedge.first);
    FE_CHECK(out_edge_src_data_anchor_ptr == nullptr, FE_LOGE("outEdgeSrcDataAnchorPtr is nullptr."), return FAILED);
    ge::OpDescPtr out_edge_src_op_desc_ptr = out_edge_src_data_anchor_ptr->GetOwnerNode()->GetOpDesc();

    auto src_op_id = outedge.first->GetOwnerNode()->GetOpDesc()->GetId();
    if (!graph_comm_ptr_->GetFusionSrc(src_op_id, outedge.first, fusion_src_exist_index, fusion_dst_exist_index)) {
      FE_CHECK(out_edge_src_op_desc_ptr == nullptr, FE_LOGE("outEdgeSrcOpDescPtr is nullptr."), return FAILED);
      if (static_cast<uint32_t>(out_edge_src_data_anchor_ptr->GetIdx()) < out_edge_src_op_desc_ptr->GetOutputsSize()) {
        FE_LOGI_IF(fus_op->AddOutputDesc(*(out_edge_src_op_desc_ptr->GetOutputDescPtr(
            out_edge_src_data_anchor_ptr->GetIdx()))) != ge::GRAPH_SUCCESS,
                   "AddOutputDesc not success");
        graph_comm_ptr_->AddFusionOutputSrc(src_op_id, outedge.first, fusion_src_index, node_srcindex_pair);
      } else {
        int32_t output_desc_size = out_edge_src_op_desc_ptr->GetOutputsSize();
        int32_t SrcIndex = out_edge_src_data_anchor_ptr->GetIdx();
        FE_LOGE("MergeFusionNodeOutputEdgeList output_desc_size %u, SrcIndex %u.", (uint32_t)output_desc_size,
                (uint32_t)SrcIndex);
        return FAILED;
      }
      fusion_src_index++;
    }
  }

  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::TransSubGraphToFunctionOp(ge::ComputeGraph& graph) {
  SubGraphNodeMap node_map;
  if (GetSubGraphNodes(graph, node_map) != SUCCESS) {
    FE_LOGE("Get SubGraph Nodes failed, graph %s.", graph.GetName().c_str());
    return FAILED;
  }
  FE_LOGD("TransSubGraphToFunctionOp subgraph %s, %p.", graph.GetName().c_str(), &graph);
  std::shared_ptr<GraphComm> graph_comm_ptr = nullptr;
  FE_MAKE_SHARED(graph_comm_ptr = std::make_shared<GraphComm>(kFFTSPlusCoreName),
                 return FAILED);
  FE_CHECK(graph_comm_ptr == nullptr, FE_LOGE("graphCommPtr is nullptr."), return FAILED);
  graph_comm_ptr_ = graph_comm_ptr;
  for (auto &it : node_map) {
    std::vector<ge::NodePtr> node_vec = it.second;
    if (TransSingleSubGraph(graph, node_vec) != SUCCESS) {
      FE_LOGE("Get SubGraph Nodes failed, graph %s.", graph.GetName().c_str());
      return FAILED;
    }
  }
  return SUCCESS;
}

Status FFTSPlusGraphOptimizer::OptimizeGraphBeforeBuild(ge::ComputeGraph &graph) {
  FE_LOGD("Begin to OptimizeGraphBeforeBuild [%s].", graph_optimizer_attr_.engineName.c_str());
  for (auto const &node : graph.GetDirectNode()) {
    auto op_desc_ptr = node->GetOpDesc();
    if (!ge::AttrUtils::SetBool(op_desc_ptr, kNoMemReuse, true)) {
      FE_LOGE("Set not mem reuse attr of node %s failed.", op_desc_ptr->GetName().c_str());
      return FAILED;
    }
    if (!ge::AttrUtils::SetBool(op_desc_ptr, kNoStreamSplit, true)) {
      FE_LOGE("Set not stream split attr of node %s failed.", op_desc_ptr->GetName().c_str());
      return FAILED;
    }
  }

  return SUCCESS;
}
}  // namespace fe
