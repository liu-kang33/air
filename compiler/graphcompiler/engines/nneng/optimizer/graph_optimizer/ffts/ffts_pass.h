/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FUSION_ENGINE_OPTIMIZER_GRAPH_OPTIMIZER_FFTS_FFTS_PASS_H_
#define FUSION_ENGINE_OPTIMIZER_GRAPH_OPTIMIZER_FFTS_FFTS_PASS_H_

#include <map>
#include <memory>
#include <string>
#include <vector>
#include "common/sgt_slice_type.h"
#include "common/fe_inner_error_codes.h"
#include "common/fe_utils.h"
#include "common/ffts_plus_type.h"
#include "common/fusion_statistic/buffer_fusion_info_collecter.h"
#include "common/fusion_statistic/fusion_statistic_writer.h"
#include "common/optimizer/graph_optimizer.h"
#include "common/optimizer/graph_optimizer_types.h"
#include "graph/compute_graph.h"

namespace fe {

using SubGraphNodeMap = std::map<uint32_t, std::vector<ge::NodePtr>>;

class FftsPass {
 public:
  FftsPass() {};
  virtual ~FftsPass() {};

  void GetSliceInfo(const ge::ComputeGraph &graph);
  Status TransSubGraphToFunctionOp(ge::ComputeGraph &graph);
  Status SetAttrOfNodesInSubGraph(std::vector<ge::NodePtr> &node_vec) const;

 private:
  ffts::ThreadSliceMapPtr GetSliceInfoFromJson(const ge::OpDescPtr &op_desc_ptr) const;
  Status TransSingleSubGraph(ge::ComputeGraph &graph, std::vector<ge::NodePtr> &node_vec);
  Status GetSubGraphNodes(const ge::ComputeGraph &graph, SubGraphNodeMap &node_map) const;
  ge::OpDescPtr CreateFunctionOp(vector<ge::NodePtr> &node_vec) const;
  Status AddFunctionNodeInputDesc(ge::OpDescPtr fus_op, vector<FusionDataFlow> &fus_input_edge_list);
  Status AddFunctionNodeOutputDesc(ge::OpDescPtr fus_op, vector<FusionDataFlow> &fus_output_edge_list);
  Status CreateFunctionOpSubGraph(ge::NodePtr &function_node,
                                  std::vector<ge::NodePtr> &node_vec,
                                  vector<FusionDataFlow> &input_edge_list,
                                  vector<FusionDataFlow> &output_edge_list);
  Status GenSubTaskTickCacheInfo(ge::OpDescPtr fuc_op,
                                std::vector<ge::NodePtr> &node_vec,
                                map<string, TickCacheMap> &sub_graph_tick_cache) const;
  Status SetTickCacheAttr(const ge::ComputeGraphPtr &sgt_graph,
                          map<string, TickCacheMap> &sub_graph_tick_cache) const;
  Status RemoveIsolateNode(const ge::ComputeGraphPtr &graph, const std::vector<ge::NodePtr> &node_vec) const;
  FftsPass(const FftsPass&) = delete;
  FftsPass& operator=(const FftsPass&) = delete;
  std::shared_ptr<GraphComm> graph_comm_ptr_;
  size_t sgt_graph_index_{0};
};

class ManualFftsPass : public FftsPass {
 public:
  ManualFftsPass() {};
  ~ManualFftsPass() override {};

  void GenThreadDependency(ge::ComputeGraph &graph);
  Status GenDmuPrams();

 private:
  ManualFftsPass(const ManualFftsPass&) = delete;
  ManualFftsPass& operator=(const ManualFftsPass&) = delete;
};
}  // namespace fe
#endif  // FUSION_ENGINE_OPTIMIZER_GRAPH_OPTIMIZER_FE_GRAPH_OPTIMIZER_H_
