/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph_optimizer/ffts/ffts_pass.h"
#include <memory>
#include <mutex>
#include <vector>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include "common/fe_inner_attr_define.h"
#include "common/graph/fe_graph_utils.h"
#include "ge/ge_api_types.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/ge_context.h"
#include "graph/tuning_utils.h"
#include "graph/node.h"

namespace fe {

void FftsPass::GetSliceInfo(const ge::ComputeGraph &graph) {
  for (auto &node : graph.GetDirectNode()) {
    ffts::ThreadSliceMapPtr slice_info_ptr = GetSliceInfoFromJson(node->GetOpDesc());
    if (slice_info_ptr) {
      (void)node->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
      (void)ge::AttrUtils::SetBool(node->GetOpDesc(), NEED_RE_PRECOMPILE, true);
      (void)ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadScopeId, slice_info_ptr->thread_scope_id);
    }
  }
}

ffts::ThreadSliceMapPtr FftsPass::GetSliceInfoFromJson(const ge::OpDescPtr &op_desc_ptr) const {
  std::string str_slice_info;
  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
  (void)ge::AttrUtils::GetStr(op_desc_ptr, ffts::kAttrSgtJsonInfo, str_slice_info);
  FE_LOGD("Slice info: %s of op: %s success.", str_slice_info.c_str(), op_desc_ptr->GetName().c_str());
  if (str_slice_info.empty()) {
    FE_LOGW("Node:%s, slice info is empty.", op_desc_ptr->GetName().c_str());
  } else {
    try {
      nlohmann::json slice_info_json = nlohmann::json::parse(str_slice_info);
      if (slice_info_json.is_null()) {
        FE_LOGW("Get l2 info: %s is empty.", str_slice_info.c_str());
      } else {
        slice_info_ptr = std::make_shared<ffts::ThreadSliceMap>();
      }
    } catch (...) {
      FE_LOGE("Parse json str failed, please check input str.");
      return nullptr;
    }
  }
  return slice_info_ptr;
}

ge::OpDescPtr FftsPass::CreateFunctionOp(vector<ge::NodePtr> &node_vec) const {
  ge::NodePtr first_node = node_vec[0];
  FE_CHECK(first_node == nullptr, FE_LOGE("CreateFusionOp nullptr Pointer."), return nullptr);
  ge::OpDescPtr function_opdef = std::shared_ptr<ge::OpDesc>(new (std::nothrow) ge::OpDesc());
  FE_CHECK(function_opdef == nullptr, FE_LOGE("CreateFusionOp nullptr Pointer."), return nullptr);

  std::string fusion_node_name;
  fusion_node_name.clear();

  for (ge::NodePtr &node : node_vec) {
    FE_CHECK(node == nullptr, FE_LOGE("pScopeAllocator is nullptr."), return nullptr);
    fusion_node_name += node->GetOpDesc()->GetName();
  }
  function_opdef->SetName(fusion_node_name);

  function_opdef->SetType("PartitionedCall");

  // copy session graph id
  string session_graph_id;
  if (ge::AttrUtils::GetStr(first_node->GetOpDesc(), ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id)) {
    if (!ge::AttrUtils::SetStr(function_opdef, ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id)) {
      FE_LOGE("Op[%s]: fail to set the attribute %s.", fusion_node_name.c_str(),
              ge::ATTR_NAME_SESSION_GRAPH_ID.c_str());
      return nullptr;
    }
  }

  int64_t imply_type = -1;
  for (ge::NodePtr node : node_vec) {
    if (ge::AttrUtils::GetInt(node->GetOpDesc(), FE_IMPLY_TYPE, imply_type)) {
      break;
    }
  }
  FE_CHECK(imply_type == -1, FE_LOGE("The imply_type -1 is invalid, it must be in [0,11]."), return nullptr);

  if (!ge::AttrUtils::SetInt(function_opdef, FE_IMPLY_TYPE, imply_type)) {
    FE_LOGE("Op[%s]: set imply_type failed, the imply_type is %ld.", fusion_node_name.c_str(), imply_type);
    return nullptr;
  }

  function_opdef->SetOpEngineName(AI_CORE_NAME);
  function_opdef->SetOpKernelLibName(AI_CORE_NAME);

  FE_LOGD("Op[%s]: the implytype is %ld", fusion_node_name.c_str(), imply_type);
  return function_opdef;
}
static void SetReuseInputAttr(const ge::OpDescPtr &function_op_desc)
{
  for (size_t i = 0; i < function_op_desc->GetAllOutputsDescSize(); i++) {
    auto out_desc = function_op_desc->MutableOutputDesc(i);
    if (out_desc == nullptr) {
      continue;
    }
    ge::TensorUtils::SetReuseInput(*out_desc.get(), false);
  }
}
Status FftsPass::CreateFunctionOpSubGraph(ge::NodePtr &function_node,
                                          std::vector<ge::NodePtr> &node_vec,
                                          vector<FusionDataFlow> &input_edge_list,
                                          vector<FusionDataFlow> &output_edge_list) {
  vector<std::string> node_name_list;
  auto graph = node_vec[0]->GetOwnerComputeGraph();
  FE_LOGD("ai-core sub graph name is %s, %p", graph->GetName().c_str(), graph.get());
  auto func_graph = function_node->GetOwnerComputeGraph();
  FE_LOGD("func owner graph name is %s, %p", func_graph->GetName().c_str(), func_graph.get());
  ge::ComputeGraphPtr src_graph;
  src_graph = graph->TryGetExtAttr("part_src_graph", ge::ComputeGraphPtr());
  FE_CHECK_NOTNULL(src_graph);
  FE_LOGD("src graph name is %s", src_graph->GetName().c_str());
  auto root_graph = ge::GraphUtils::FindRootGraph(src_graph);
  FE_CHECK_NOTNULL(root_graph);
  FE_LOGD("root graph name is %s", root_graph->GetName().c_str());

  for (auto &node : node_vec) {
    node_name_list.emplace_back(node->GetName());
  }

  string sgt_graph_name = func_graph->GetName() + "_sgt_graph_" + to_string(sgt_graph_index_++);
  FE_LOGD("function_node: %s, sgt graph name is %s", function_node->GetName().c_str(), sgt_graph_name.c_str());
  ge::CompleteGraphBuilder builder(sgt_graph_name, false);

  for (auto &node : node_vec) {
    /* Adds a node to the builder. Currently, only the input parameter op_desc
       is supported, and the connection information will be lost. Therefore,
       the AddDataLink interface needs to be invoked to re-establish the
       connection. */
    ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
    slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
    FE_LOGD("ffts::kAttrSgtStructInfo CreateFunctionOpSubGraph add before of Op name: %s, Op id: %ld, %p, %p.",
            node->GetOpDesc()->GetName().c_str(), node->GetOpDesc()->GetId(), node->GetOpDesc().get(),
            slice_info_ptr.get());
    slice_info_ptr = nullptr;
    builder.AddNode(node->GetOpDesc());
    slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
    FE_LOGD("ffts::kAttrSgtStructInfo CreateFunctionOpSubGraph add after of Op name: %s, Op id: %ld, %p, %p.",
            node->GetOpDesc()->GetName().c_str(), node->GetOpDesc()->GetId(), node->GetOpDesc().get(),
            slice_info_ptr.get());
    FE_LOGD("Begin re-establish connection of node[name:%s]. out data size is %u.",
            node->GetName().c_str(), node->GetAllOutDataAnchorsSize());
    // Re-establish connection
    const auto &src_node_name = node->GetName();
    for (auto &out_data_anchor : node->GetAllOutDataAnchors()) {
      auto src_idx = out_data_anchor->GetIdx();
      for (auto &in_data_anchor : out_data_anchor->GetPeerInDataAnchors()) {
        auto dst_node = in_data_anchor->GetOwnerNode();
        if (dst_node == nullptr) {
          return FAILED;
        }
        auto iter = std::find(node_name_list.begin(), node_name_list.end(), dst_node->GetName());
        if (iter == node_name_list.end()) {
          FE_LOGD("Node[name:%s] is out op, skip.", dst_node->GetName().c_str());
          continue;
        }
        builder.AddDataLink(src_node_name, src_idx, dst_node->GetName(), in_data_anchor->GetIdx());
      }
    }
  }

  FE_LOGD("input_edge_list size is %zu", input_edge_list.size());
  std::map<uint32_t, uint32_t> input_mapping;
  uint32_t index = 0;
  for (auto &input_edge : input_edge_list) {
    FE_CHECK(input_edge.edge.first == nullptr, FE_LOGW("input_edge.first is null"), return FAILED);
    FE_CHECK(input_edge.edge.second == nullptr, FE_LOGW("input_edge.second is null"), return FAILED);
    std::uint32_t src_idx = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.first)->GetIdx();
    std::uint32_t fusion_idx = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.second)->GetIdx();
    ge::NodePtr src_node = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.first)->GetOwnerNode();
    ge::NodePtr fusion_node = std::static_pointer_cast<ge::DataAnchor>(input_edge.edge.second)->GetOwnerNode();
    FE_LOGD("srcIdx is %d, fusion_idx is %d. src_node is : %s, fusion_node is : %s.", src_idx, fusion_idx,
            src_node->GetName().c_str(), fusion_node->GetName().c_str());
    // add relation: input index of new graph to input index of original graph.
    std::vector<std::string> ffts_node_names{fusion_node->GetName()};
    std::vector<uint32_t> dst_indexes{fusion_idx};
    builder.SetInput(index, ffts_node_names, dst_indexes);
    input_mapping.emplace(index, index);
    index++;
  }
  builder.SetInputMapping(input_mapping);

  FE_LOGD("fusionOpOutputAnchorsMap size is %zu", output_edge_list.size());
  std::map<uint32_t, uint32_t> output_mapping;
  std::vector<ge::AnchorPtr> sgt_output;
  index = 0;
  for (auto &output_edge : output_edge_list) {
    if (std::find(sgt_output.begin(), sgt_output.end(), output_edge.edge.first) == sgt_output.end()) {
      sgt_output.push_back(output_edge.edge.first);
      FE_CHECK(output_edge.edge.first == nullptr, FE_LOGW("output_edge.first is null"), return FAILED);
      FE_CHECK(output_edge.edge.second == nullptr, FE_LOGW("iterVec->second is null"), return FAILED);
      std::uint32_t src_idx = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.first)->GetIdx();
      std::uint32_t fusion_idx = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.second)->GetIdx();
      ge::NodePtr src_node = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.first)->GetOwnerNode();
      ge::NodePtr fusion_node = std::static_pointer_cast<ge::DataAnchor>(output_edge.edge.second)->GetOwnerNode();
      FE_LOGD("out src_idx is %d, out fusion_idx is %d. src_node is : %s, fusion_node is : %s", src_idx, fusion_idx,
              src_node->GetName().c_str(), fusion_node->GetName().c_str());
      // add relation: output index of new graph to output index of original graph.
      std::vector<std::string> input_node_names{src_node->GetName()};
      // std::vector<uint32_t> dst_indexes{src_idx};
      builder.AddOutput(src_node->GetName(), src_idx);
      output_mapping.emplace(index, index);
      index++;
    }
  }

  builder.SetOutputMapping(output_mapping);
  builder.SetParentNode(function_node);

  ge::graphStatus status = ge::GRAPH_SUCCESS;
  string err_msg;
  auto sgt_graph = builder.Build(status, err_msg);
  if (status != ge::GRAPH_SUCCESS) {
    FE_LOGE("Build graph not success. status is : %d, err_msg is : %s.", status, err_msg.c_str());
    return FAILED;
  }

  sgt_graph->SetParentGraph(graph);
  sgt_graph->SetParentNode(function_node);
  ge::OpDescPtr function_op_desc = function_node->GetOpDesc();
  function_op_desc->AddSubgraphName(sgt_graph->GetName());
  function_op_desc->SetSubgraphInstanceName(0, sgt_graph->GetName());
  (void)ge::AttrUtils::SetGraph(function_op_desc, "_sgt_sub_graph", sgt_graph);
  (void)ge::AttrUtils::SetStr(function_op_desc, "_ffts_sub_graph", sgt_graph->GetName().c_str());
  (void)ge::AttrUtils::SetBool(function_op_desc, "_sgt_function_op", true);
  SetReuseInputAttr(function_op_desc);
  if (root_graph->AddSubgraph(sgt_graph->GetName(), sgt_graph) != ge::GRAPH_SUCCESS) {
    FE_LOGE("Add sub graph not success. status is : %d, root graph: %s.", status, root_graph->GetName().c_str());
    return FAILED;
  }
  FE_LOGD("Add sub graph success. root graph: %s, subgraph: %s.",
          root_graph->GetName().c_str(), sgt_graph->GetName().c_str());

  map<string, TickCacheMap> sub_graph_tick_cache;
  if (GenSubTaskTickCacheInfo(function_op_desc, node_vec, sub_graph_tick_cache) != SUCCESS) {
    FE_LOGE("SetSubTaskTickCacheInfo not success.");
    return FAILED;
  }
  FE_LOGD("SetSubTaskTickCacheInfo success.");

  if (SetTickCacheAttr(sgt_graph, sub_graph_tick_cache) != SUCCESS) {
    FE_LOGE("SetTickCacheAttr not success.");
    return FAILED;
  }

  if (RemoveIsolateNode(graph, node_vec) != SUCCESS) {
    return FAILED;
  }
  return SUCCESS;
}

Status FftsPass::GenSubTaskTickCacheInfo(ge::OpDescPtr fuc_op,
                                         std::vector<ge::NodePtr> &node_vec,
                                         map<string, TickCacheMap> &sub_graph_tick_cache) const {
  uint8_t tick_cache_max = 0;
  uint8_t tick_cache_index = 0;

  for (auto const &node : node_vec) {
    const auto &node_name = node->GetName();
    for (auto &out_data_anchor : node->GetAllOutDataAnchors()) {
      if (!out_data_anchor) {
        continue;
      }
      auto output_index = out_data_anchor->GetIdx();
      tick_cache_index = tick_cache_max;
      tick_cache_max++;
      FE_LOGD("node %s, ouput %d, alloc tick cache index %d.", node_name.c_str(), output_index, tick_cache_index);
      sub_graph_tick_cache[node_name].output_cache_table[output_index] = tick_cache_index;
    }
  }
  for (auto const &node : node_vec) {
    const auto &node_name = node->GetName();
    for (auto &in_data_anchor : node->GetAllInDataAnchors()) {
      if (!in_data_anchor || !in_data_anchor->GetPeerOutAnchor()) {
        continue;
      }
      auto input_index = in_data_anchor->GetIdx();
      if (sub_graph_tick_cache[node_name].input_cache_table.count(input_index) == 0) {
        int32_t output_index = in_data_anchor->GetPeerOutAnchor()->GetIdx();
        string peer_node_name = in_data_anchor->GetPeerOutAnchor()->GetOwnerNode()->GetName();
        if (sub_graph_tick_cache[peer_node_name].output_cache_table.count(output_index) == 0) {
          FE_LOGE("can not find %s output %d 's cache id.", peer_node_name.c_str(), output_index);
          return FAILED;
        }
        FE_LOGD("node %s, input %d, set tick cache index %d.", node_name.c_str(), input_index,
                sub_graph_tick_cache[peer_node_name].output_cache_table[output_index]);
        sub_graph_tick_cache[node_name].input_cache_table[input_index] =
            sub_graph_tick_cache[peer_node_name].output_cache_table[output_index];
      }
    }
  }
  uint32_t graph_sub_task_num = node_vec.size();

  (void)ge::AttrUtils::SetInt(fuc_op, "_graph_tick_cache_num", static_cast<uint32_t>(tick_cache_max));
  (void)ge::AttrUtils::SetInt(fuc_op, "_graph_sub_task_num", graph_sub_task_num);
  FE_LOGD("GenSubTaskTickCacheInfo succcess");
  return SUCCESS;
}

Status FftsPass::SetTickCacheAttr(const ge::ComputeGraphPtr &sgt_graph,
                                  map<string, TickCacheMap> &sub_graph_tick_cache) const {
  for (auto const &node : sgt_graph->GetDirectNode()) {
    vector<uint32_t> dst_tick_cache_id;
    vector<uint32_t> src_tick_cache_id;
    uint32_t src_data_out_of_sub_graph_bitmap = 0;
    string node_name = node->GetName();
    ge::OpDescPtr op_desc = node->GetOpDesc();
    TickCacheMap tick_cache_table = sub_graph_tick_cache[node_name];

    if ((tick_cache_table.input_cache_table.size() > RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK) ||
        (tick_cache_table.output_cache_table.size() > RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK)) {
      FE_LOGE("tick cache num invalid, should both less than 8, input %zu, output %zu.",
              tick_cache_table.input_cache_table.size(), tick_cache_table.output_cache_table.size());
      return FAILED;
    }

    uint32_t input_index = 0;
    for (auto const cache_id : tick_cache_table.input_cache_table) {
      src_tick_cache_id.push_back(cache_id.second);
      auto vec = tick_cache_table.src_out_of_graph_input_index;
      if (find(vec.begin(), vec.end(), cache_id.first) != vec.end()) {
        src_data_out_of_sub_graph_bitmap |= 1 << input_index;
      }
      input_index++;
    }

    for (auto const cache_id : tick_cache_table.output_cache_table) {
      dst_tick_cache_id.push_back(cache_id.second);
    }

    (void)ge::AttrUtils::SetListInt(op_desc, "_src_tick_cache_id", src_tick_cache_id);
    (void)ge::AttrUtils::SetListInt(op_desc, "_dst_tick_cache_id", dst_tick_cache_id);
    (void)ge::AttrUtils::SetInt(op_desc, "_src_data_out_of_sub_graph_bitmap", src_data_out_of_sub_graph_bitmap);
    (void)op_desc->SetExtAttr("_tick_cache_map", tick_cache_table);
  }
  return SUCCESS;
}

Status FftsPass::RemoveIsolateNode(const ge::ComputeGraphPtr &graph, const std::vector<ge::NodePtr> &node_vec) const {
  for (auto &node : node_vec) {
    if (ge::GraphUtils::IsolateNode(node, {0}) != ge::GRAPH_SUCCESS ) {
      return FAILED;
    }
    if (ge::GraphUtils::RemoveNodeWithoutRelink(graph, node) != ge::GRAPH_SUCCESS) {
      return FAILED;
    }
  }
  return SUCCESS;
}

Status FftsPass::TransSingleSubGraph(ge::ComputeGraph &graph, std::vector<ge::NodePtr> &node_vec) {
  vector<FusionDataFlow> input_edge_list;
  vector<FusionDataFlow> output_edge_list;
  vector<FusionDataFlow> input_ctrl_edge_list;
  vector<FusionDataFlow> output_ctrl_edge_list;

  if (graph_comm_ptr_->GetFusionNodeEdgeList(node_vec, input_edge_list, output_edge_list) != SUCCESS) {
    FE_LOGE("GetFusionNodeEdgeList failed.");
    return FAILED;
  }

  if (graph_comm_ptr_->GetFusionNodeCtrlEdgeList(node_vec, input_ctrl_edge_list, output_ctrl_edge_list) != SUCCESS) {
    FE_LOGE("GetFusionNodeCtrlEdgeList failed.");
    return FAILED;
  }

  ge::OpDescPtr function_op = CreateFunctionOp(node_vec);
  if (function_op == nullptr) {
    FE_LOGE("CreateFunctionOp failed.");
    return FAILED;
  }

  if (AddFunctionNodeInputDesc(function_op, input_edge_list) != SUCCESS) {
    FE_LOGE("Failed to AddFusionNodeInputDesc.");
    return FAILED;
  }

  if (AddFunctionNodeOutputDesc(function_op, output_edge_list) != SUCCESS) {
    FE_LOGE("Failed to AddFusionNodeOutputDesc.");
    return FAILED;
  }

  ge::NodePtr function_node = graph.AddNode(function_op);
  FE_CHECK_NOTNULL(function_node);

  // Merge Same scope node
  if (graph_comm_ptr_->MergeFusionNodeEdgeList(function_node, node_vec, input_edge_list, output_edge_list) !=
      SUCCESS) {
    FE_LOGE("MergeFusionNodeEdgeList failed!");
    return FAILED;
  }

  if (graph_comm_ptr_->MergeFusionNodeCtrlEdgeList(function_node, node_vec, input_ctrl_edge_list,
                                                   output_ctrl_edge_list) != SUCCESS) {
    FE_LOGE("MergeFusionNodeCtrlEdgeList failed!");
    return FAILED;
  }

  return CreateFunctionOpSubGraph(function_node, node_vec, input_edge_list, output_edge_list);
}

Status FftsPass::SetAttrOfNodesInSubGraph(std::vector<ge::NodePtr> &node_vec) const {
  for (auto &node : node_vec) {
    if (!ge::AttrUtils::SetBool(node->GetOpDesc(), kNoMemReuse, true)) {
      FE_LOGE("Set not mem reuse attr of node %s failed.", node->GetName().c_str());
      return FAILED;
    }
    if (!ge::AttrUtils::SetBool(node->GetOpDesc(), kNoStreamSplit, true)) {
      FE_LOGE("Set not stream split attr of node %s failed.", node->GetName().c_str());
      return FAILED;
    }
  }
  return SUCCESS;
}

Status FftsPass::TransSubGraphToFunctionOp(ge::ComputeGraph &graph) {
  SubGraphNodeMap node_map;
  if (GetSubGraphNodes(graph, node_map) != SUCCESS) {
    FE_LOGE("Get SubGraph Nodes failed, graph %s.", graph.GetName().c_str());
    return FAILED;
  }
  FE_LOGD("TransSubGraphToFunctionOp subgraph %s, %p.", graph.GetName().c_str(), &graph);
  std::shared_ptr<GraphComm> graph_comm_ptr = nullptr;
  FE_MAKE_SHARED(graph_comm_ptr = std::make_shared<GraphComm>("engineName"),
                 return FAILED);
  FE_CHECK(graph_comm_ptr == nullptr, FE_LOGE("graphCommPtr is nullptr."), return FAILED);
  graph_comm_ptr_ = graph_comm_ptr;
  for (auto &it : node_map) {
    std::vector<ge::NodePtr> node_vec = it.second;
    if (TransSingleSubGraph(graph, node_vec) != SUCCESS) {
      FE_LOGE("Get SubGraph Nodes failed, graph %s.", graph.GetName().c_str());
      return FAILED;
    }
    if (SetAttrOfNodesInSubGraph(node_vec) != SUCCESS) {
      FE_LOGE("Set Attr of Nodes failed, graph %s.", graph.GetName().c_str());
      return FAILED;
    }
  }
  return SUCCESS;
}

Status FftsPass::GetSubGraphNodes(const ge::ComputeGraph &graph, SubGraphNodeMap &node_map) const {
  for (auto const &node : graph.GetDirectNode()) {
    auto op_desc_ptr = node->GetOpDesc();
    FE_CHECK(op_desc_ptr == nullptr, FE_LOGE("opDescPtr is nullptr."), return FAILED);
    uint32_t thread_scope_id = 0;
    (void)ge::AttrUtils::GetInt(op_desc_ptr, kThreadScopeId, thread_scope_id);
    if (thread_scope_id == 0) {
      FE_LOGD("op %s is not belong to any subgraph.", node->GetName().c_str());
      continue;
    }

    const SubGraphNodeMap::iterator nodelist_it = node_map.find(thread_scope_id);
    if (nodelist_it == node_map.end()) {
      std::vector<ge::NodePtr> node_list_new;
      node_list_new.push_back(node);
      node_map.insert(std::pair<uint32_t, std::vector<ge::NodePtr>>(thread_scope_id, node_list_new));
      FE_LOGD("add %d. op %s.", thread_scope_id, node->GetName().c_str());
    } else {
      nodelist_it->second.push_back(node);
      FE_LOGD("op %s push back %d.", node->GetName().c_str(), thread_scope_id);
    }
  }
  return SUCCESS;
}

Status FftsPass::AddFunctionNodeInputDesc(ge::OpDescPtr fus_op, vector<FusionDataFlow> &fus_input_edge_list) {
  int32_t fusion_dst_index = -1;
  graph_comm_ptr_->ClearFusionSrc();
  vector<bool> fusion_is_input_const_vector;
  uint32_t tensor_desc_index = 0;

  for (FusionDataFlow &dataflow : fus_input_edge_list) {
    auto inedge = dataflow.edge;
    std::pair<string, ge::AnchorPtr> &node_dstindex_pair = dataflow.node_dataindex_pair;
    int64_t src_op_id = inedge.first->GetOwnerNode()->GetOpDesc()->GetId();

    // only support data edge
    fusion_dst_index = fusion_dst_index + 1;
    vector<int64_t> input_vector;
    input_vector = fus_op->GetInputOffset();
    if (static_cast<uint32_t>(fusion_dst_index) < input_vector.size()) {
      return FAILED;
    }

    ge::DataAnchorPtr in_edge_dst_data_anchor_ptr = std::static_pointer_cast<ge::DataAnchor>(inedge.second);
    FE_CHECK(in_edge_dst_data_anchor_ptr == nullptr, FE_LOGE("inEdgeDstDataAnchorPtr is nullptr."), return FAILED);
    ge::OpDescPtr in_edge_dst_op_desc_ptr = in_edge_dst_data_anchor_ptr->GetOwnerNode()->GetOpDesc();
    vector<bool> is_input_const_vector;
    is_input_const_vector = in_edge_dst_op_desc_ptr->GetIsInputConst();
    uint32_t dst_anchor_index = static_cast<uint32_t>(in_edge_dst_data_anchor_ptr->GetIdx());
    if (dst_anchor_index < in_edge_dst_op_desc_ptr->GetInputsSize()) {
      auto input_desc_ptr = in_edge_dst_op_desc_ptr->GetInputDescPtr(in_edge_dst_data_anchor_ptr->GetIdx());
      FE_CHECK(input_desc_ptr == nullptr, FE_LOGE("input_desc_ptr is nullptr."), return FAILED);
      if (fus_op->AddInputDesc(*input_desc_ptr) != ge::GRAPH_SUCCESS) {
        FE_LOGE("Add input desc failed.");
        return FAILED;
      }
      FE_LOGD("fusOp name %s, %d", fus_op->GetName().c_str(), in_edge_dst_data_anchor_ptr->GetIdx());

      if (dst_anchor_index < is_input_const_vector.size()) {
        fusion_is_input_const_vector.push_back(is_input_const_vector[in_edge_dst_data_anchor_ptr->GetIdx()]);
      } else {
        fusion_is_input_const_vector.push_back(false);
      }
      tensor_desc_index++;
    } else {
      int32_t input_desc_size = in_edge_dst_op_desc_ptr->GetInputsSize();
      int32_t is_input_const_size = is_input_const_vector.size();
      int32_t DstIndex = in_edge_dst_data_anchor_ptr->GetIdx();
      FE_LOGE("AddFusionNodeInput input_desc_size %u, is_input_const_size %u, input DstIndex %u.",
              (uint32_t)input_desc_size, (uint32_t)is_input_const_size, (uint32_t)DstIndex);
      return FAILED;
    }
    fus_op->SetIsInputConst(fusion_is_input_const_vector);
    graph_comm_ptr_->AddFusionInputSrc(src_op_id, inedge.first, fusion_dst_index, node_dstindex_pair);
  }

  return SUCCESS;
}

Status FftsPass::AddFunctionNodeOutputDesc(ge::OpDescPtr fus_op, vector<FusionDataFlow> &fus_output_edge_list) {
  graph_comm_ptr_->ClearFusionSrc();
  graph_comm_ptr_->ClearFusionDst();

  int32_t fusion_src_index = 0;
  for (FusionDataFlow dataflow : fus_output_edge_list) {
    auto outedge = dataflow.edge;
    std::pair<string, ge::AnchorPtr> &node_srcindex_pair = dataflow.node_dataindex_pair;
    int64_t dst_op_id = outedge.second->GetOwnerNode()->GetOpDesc()->GetId();
    ge::DataAnchorPtr out_edge_dst_data_anchor_ptr = std::static_pointer_cast<ge::DataAnchor>(outedge.second);
    FE_CHECK(out_edge_dst_data_anchor_ptr == nullptr, FE_LOGE("outEdgeDstDataAnchorPtr is nullptr."), return FAILED);
    if (graph_comm_ptr_->IsFusionDstExist(dst_op_id, outedge.second)) {
      FE_LOGI("MergeFusionNodeOutputEdgeList Dstid %u, DstIndex %u.", (uint32_t)dst_op_id,
              (uint32_t)out_edge_dst_data_anchor_ptr->GetIdx());
      continue;
    }
    graph_comm_ptr_->SaveFusionDst(dst_op_id, outedge.second);

    int32_t fusion_src_exist_index;
    int32_t fusion_dst_exist_index;

    ge::DataAnchorPtr out_edge_src_data_anchor_ptr = std::static_pointer_cast<ge::DataAnchor>(outedge.first);
    FE_CHECK(out_edge_src_data_anchor_ptr == nullptr, FE_LOGE("outEdgeSrcDataAnchorPtr is nullptr."), return FAILED);
    ge::OpDescPtr out_edge_src_op_desc_ptr = out_edge_src_data_anchor_ptr->GetOwnerNode()->GetOpDesc();

    int64_t src_op_id = outedge.first->GetOwnerNode()->GetOpDesc()->GetId();

    if (!graph_comm_ptr_->GetFusionSrc(src_op_id, outedge.first, fusion_src_exist_index, fusion_dst_exist_index)) {
      FE_CHECK(out_edge_src_op_desc_ptr == nullptr, FE_LOGE("outEdgeSrcOpDescPtr is nullptr."), return FAILED);
      if (static_cast<uint32_t>(out_edge_src_data_anchor_ptr->GetIdx()) < out_edge_src_op_desc_ptr->GetOutputsSize()) {
        auto output_desc_ptr = out_edge_src_op_desc_ptr->GetOutputDescPtr(out_edge_src_data_anchor_ptr->GetIdx());
        FE_CHECK(output_desc_ptr == nullptr, FE_LOGE("Output_desc_ptr is nullptr."), return FAILED);
        FE_LOGI_IF(fus_op->AddOutputDesc(*output_desc_ptr) != ge::GRAPH_SUCCESS,
                   "AddOutputDesc not success");
        graph_comm_ptr_->AddFusionOutputSrc(src_op_id, outedge.first, fusion_src_index, node_srcindex_pair);
      } else {
        int32_t output_desc_size = out_edge_src_op_desc_ptr->GetOutputsSize();
        int32_t SrcIndex = out_edge_src_data_anchor_ptr->GetIdx();
        FE_LOGE("MergeFusionNodeOutputEdgeList output_desc_size %u, SrcIndex %u.", (uint32_t)output_desc_size,
                (uint32_t)SrcIndex);
        return FAILED;
      }
      fusion_src_index++;
    }
  }

  return SUCCESS;
}

}  // namespace fe
