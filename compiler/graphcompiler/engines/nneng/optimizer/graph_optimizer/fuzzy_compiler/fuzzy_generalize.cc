/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph_optimizer/fuzzy_compiler/fuzzy_generalize.h"
#include "common/graph/fe_graph_utils.h"

namespace fe {
using namespace nlohmann;

const int64_t kExpandNum = 2;

FuzzyGeneralize::FuzzyGeneralize(ge::OptimizeUtility *optimize_utility,
                                 const FEOpsKernelInfoStorePtr &ops_kernel_info_store_ptr,
                                 const OpStoreAdapterManagerPtr &op_store_adapter_manager_ptr)
    : optimize_utility_(optimize_utility),
      ops_kernel_info_store_ptr_(ops_kernel_info_store_ptr),
      op_store_adapter_manager_ptr_(op_store_adapter_manager_ptr) {}

FuzzyGeneralize::~FuzzyGeneralize() {}

bool FuzzyGeneralize::CheckIsFirstNode(const ge::NodePtr &node) const {
  for (const auto &in_node : node->GetInDataNodes()) {
    if (CheckIsExternalNode(in_node)) {
      return true;
    }
  }

  return false;
}

Status FuzzyGeneralize::CheckAndUpdateLimitedNodes(const OpStoreAdapterPtr &op_store_adapter,
                                                   const std::vector<ge::NodePtr> &limited_nodes,
                                                   bool &generalize_flag) {
  Status ret;
  bool is_supported = true;
  NodeGeneralInfoPtr node_info_ptr;
  FE_LOGD("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] Begin to check and update limited nodes, size is [%zu].",
          limited_nodes.size());

  for (const auto &limited_node : limited_nodes) {
    std::vector<size_t> upper_limited_input_indexs;
    std::vector<size_t> lower_limited_input_indexs;
    FE_LOGD("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] Current node[%s].", limited_node->GetName().c_str());

    if (!IsUnKnownShapeOp(*(limited_node->GetOpDesc().get()))) {
      FE_LOGD("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] Current node[%s] is non-unknownShape op.",
              limited_node->GetName().c_str());
      continue;
    }

    if (CheckIsFirstNode(limited_node)) {
      FE_LOGD("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] first node[%s] no need to do range check.",
          limited_node->GetName().c_str());
      continue;
    }

    auto iter = node_info_map_.find(limited_node);
    if (iter == node_info_map_.end()) {
      FE_LOGW("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] Cannot find current node[%s].",
              limited_node->GetName().c_str());
      return FAILED;
    }
    node_info_ptr = iter->second;
    FE_CHECK_NOTNULL(node_info_ptr);

    if (op_store_adapter->LimitedNodesCheck(is_supported, *(node_info_ptr->op_info.get()), upper_limited_input_indexs,
                                            lower_limited_input_indexs) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] Node[%s] LimitedNodesCheck failed.",
              limited_node->GetName().c_str());
      return FAILED;
    }

    FE_LOGD("[GraphOpt][Prepare][Generalize] Node[%s] LimitedNodesCheck successfully.",
            limited_node->GetName().c_str());
    if (!is_supported) {
      ret = InputNodeDowngrades(limited_node, upper_limited_input_indexs, lower_limited_input_indexs);
      if (ret != SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][CheckAndUpdateLimitedNodes] input node[%s] downgrades failed.",
                limited_node->GetName().c_str());
        return FAILED;
      }
      FE_LOGD("[GraphOpt][Prepare][Generalize] Node[%s], Run func[InputNodeDowngrades] successfully.",
              limited_node->GetName().c_str());
      return SUCCESS;
    }
  }

  generalize_flag = false;
  return SUCCESS;
}

Status FuzzyGeneralize::DeleteShapeRange(const ge::NodePtr &external_node) {
  if (++unset_dynamic_shape_counts_ >= MAX_UNSET_DYNAMIC_TIMES) {
    FE_LOGW(
        "[GraphOpt][Prepare][DeleteShapeRange] the count of unset_dynamic_shape reaches 5, \
        generalize process would be terminated, cur node is: %s.",
        external_node->GetName().c_str());
    return FAILED;
  }
  external_node->GetOpDesc()->MutableInputDesc(0)->DelAttr(ORIGIN_SHAPE_RANGE);
  external_node->GetOpDesc()->MutableInputDesc(0)->DelAttr(SHAPE_RANGE);

  return SUCCESS;
}

Status FuzzyGeneralize::SingleOpDowngrades(const ge::NodePtr &external_node, const bool &is_upper_limited) {
  if (is_upper_limited) {
    std::string op_name = external_node->GetName();
    auto iter_decent = decent_times_count_.find(op_name);
    if (iter_decent == decent_times_count_.end()) {
      FE_LOGW("[GraphOpt][Prepare][SingleOpDowngrades] cannot find decent info by node[%s], which is unexpected.",
              op_name.c_str());
      return FAILED;
    }
    if (iter_decent->second >= MAX_DECENT_TIMES) {
      if (DeleteShapeRange(external_node) != SUCCESS) {
        return FAILED;
      }
    } else {
      if (RangeDecent(external_node, iter_decent->second) != SUCCESS) {
        FE_LOGW(
            "[GraphOpt][Prepare][SingleOpDowngrades] Unexpected exceptions happened when \
        making range-decent[node:%s].",
            op_name.c_str());
        return FAILED;
      }
    }
  } else {
    if (DeleteShapeRange(external_node) != SUCCESS) {
      return FAILED;
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::Downgrades(const ge::NodePtr &cur_node, const bool &is_upper_limited,
                                   const std::vector<size_t> &limited_input_indexs) {
  const auto iter_node = node_info_map_.find(cur_node);
  if (iter_node == node_info_map_.end()) {
    FE_LOGW("[GraphOpt][Prepare][Downgrades] Node[%s], cannot find cur_node in node_info_map.",
            cur_node->GetName().c_str());
    return FAILED;
  }
  bool all_input_nodes_static = true;
  auto node_info = iter_node->second;
  for (auto &idx : limited_input_indexs) {
    auto input_desc = cur_node->GetOpDesc()->MutableInputDesc(idx);
    auto iter = node_info->inputs_root_map.find(input_desc);
    if (iter == node_info->inputs_root_map.end()) {
      FE_LOGW("[GraphOpt][Prepare][Downgrades] Cannot find the root set by idx[%zu], node[%s].", idx,
              cur_node->GetName().c_str());
      return FAILED;
    }
    for (auto &external_node : iter->second) {
      if (!IsUnKnownShapeOp(*(external_node->GetOpDesc().get()))) {
        continue;
      }

      all_input_nodes_static = false;
      if (SingleOpDowngrades(external_node, is_upper_limited) != SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][Downgrades] node[%s] downgrades failed.", external_node->GetName().c_str());
        return FAILED;
      }
    }
  }

  if (all_input_nodes_static) {
    FE_LOGW(
        "[GraphOpt][Prepare][Downgrades] All input_nodes of node[%s] has been changed to static shape, \
        but it is still not support.",
        cur_node->GetName().c_str());
    return FAILED;
  }
  FE_LOGD("[GraphOpt][Prepare][Downgrades] Node[%s], down grades by lower_limitedt_indexs successfully.",
          cur_node->GetName().c_str());
  return SUCCESS;
}

Status FuzzyGeneralize::InputNodeDowngrades(const ge::NodePtr &cur_node,
                                            const std::vector<size_t> &upper_limited_input_indexs,
                                            const std::vector<size_t> &lower_limited_input_indexs) {
  bool is_upper_limited = true;
  if (!upper_limited_input_indexs.empty()) {
    if (Downgrades(cur_node, is_upper_limited, upper_limited_input_indexs) != SUCCESS) {
      return FAILED;
    }
  }

  if (!lower_limited_input_indexs.empty()) {
    is_upper_limited = false;
    if (Downgrades(cur_node, is_upper_limited, lower_limited_input_indexs) != SUCCESS) {
      return FAILED;
    }
  }

  FE_LOGD("[GraphOpt][Prepare][InputNodeDowngrades] Node[%s], run func[InputNodeDowngrades] successfully.",
          cur_node->GetName().c_str());
  return SUCCESS;
}

Status FuzzyGeneralize::UpdateDynamicShapeToNewInputNode(const std::unordered_set<ge::NodePtr> &external_input_nodes,
                                                         const std::map<std::string, ge::NodePtr> &new_input_nodes)
                                                         const {
  for (const ge::NodePtr &input_node : external_input_nodes) {
    if (!IsUnKnownShapeOp(*(input_node->GetOpDesc().get()))) {
      FE_LOGD("[GraphOpt][Prepare][Generalize] Current node[%s] is non-UnknownShape op.",
              input_node->GetName().c_str());
      continue;
    }

    auto iter = new_input_nodes.find(input_node->GetName());
    if (iter == new_input_nodes.end()) {
      FE_LOGW("[GraphOpt][Prepare][Generalize] Node[%s], cannot find this node on ori_graph, which is unexpected.",
              input_node->GetName().c_str());
      return FAILED;
    }

    FE_LOGD("[GraphOpt][Prepare][Generalize] Update dynamic shape to origraph for node [%s, %s].",
            input_node->GetName().c_str(), input_node->GetType().c_str());
    ge::GeTensorDescPtr cur_input_tensor_desc = input_node->GetOpDesc()->MutableInputDesc(0);
    ge::GeTensorDescPtr cur_output_tensor_desc = input_node->GetOpDesc()->MutableOutputDesc(0);
    FE_CHECK_NOTNULL(cur_input_tensor_desc);
    FE_CHECK_NOTNULL(cur_output_tensor_desc);
    bool no_need_update =
        (!cur_input_tensor_desc->HasAttr(ORIGIN_SHAPE_RANGE) && !cur_input_tensor_desc->HasAttr(SHAPE_RANGE));
    if (no_need_update) {
      continue;
    }

    ge::GeTensorDescPtr ori_input_tensor_desc = iter->second->GetOpDesc()->MutableInputDesc(0);
    ge::GeTensorDescPtr ori_output_tensor_desc = iter->second->GetOpDesc()->MutableOutputDesc(0);
    FE_CHECK_NOTNULL(ori_input_tensor_desc);
    FE_CHECK_NOTNULL(ori_output_tensor_desc);
    UpdateTensorDesc(cur_input_tensor_desc, ori_input_tensor_desc);
    UpdateTensorDesc(cur_output_tensor_desc, ori_output_tensor_desc);
    if (ge::AttrUtils::HasAttr(ori_input_tensor_desc, ge::ATTR_NAME_VALUE) &&
        !ge::AttrUtils::HasAttr(cur_input_tensor_desc, ge::ATTR_NAME_VALUE)) {
      ori_input_tensor_desc->DelAttr(ge::ATTR_NAME_VALUE);
      if (ge::AttrUtils::HasAttr(ori_output_tensor_desc, ge::ATTR_NAME_VALUE)) {
        ori_output_tensor_desc->DelAttr(ge::ATTR_NAME_VALUE);
      }
      FE_LOGD("[GraphOpt][Prepare][Generalize] Peer node has ATTR_NAME_VALUE.");
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::UpdateDynamicShapeToNewBakGraph(const ge::ComputeGraph &graph) const {
  std::map<std::string, ge::NodePtr> new_inputs_node;
  for (const ge::NodePtr &cur_node : graph.GetDirectNode()) {
    if (CheckIsExternalNode(cur_node)) {
      new_inputs_node.emplace(cur_node->GetName(), cur_node);
    }
  }

  Status ret = UpdateDynamicShapeToNewInputNode(external_input_nodes_, new_inputs_node);
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][Generalize] Graph[%s] UpdateDynamicShapeToNewInputNode to new bak graph failed.",
            graph.GetName().c_str());
    return FAILED;
  }

  return SUCCESS;
}

Status FuzzyGeneralize::GetReshapeTypeByOpStore(const ge::NodePtr &node, const std::string &input_name,
                                                std::string &reshape_type) const {
  auto iter_node_info = node_info_map_.find(node);
  if (iter_node_info == node_info_map_.end()) {
    FE_LOGW("[GraphOpt][Prepare][Generalize] can not find node[%s] in node_info_map_.", node->GetName().c_str());
    return FAILED;
  }

  auto node_info = iter_node_info->second;
  FE_CHECK_NOTNULL(node_info);

  auto op_kernel = node_info->op_kernel;
  if (op_kernel == nullptr) {
    FE_LOGD("[GraphOpt][Prepare][Generalize] op kernel of node[%s] is null.", node->GetName().c_str());
    return SUCCESS;
  }

  InputOrOutputInfoPtr input_info;
  (void)op_kernel->GetInputInfoByName(input_name, input_info);
  if (input_info == nullptr) {
    FE_LOGD("[GraphOpt][Prepare][Generalize] input_info of input name[%s] of node[%s] is null .", input_name.c_str(),
            node->GetName().c_str());
    return FAILED;
  }

  reshape_type = input_info->GetReshapeType();

  return SUCCESS;
}

Status FuzzyGeneralize::GetReshapeType(const ge::Format &origin_format, ge::GeShape &ori_shape,
                                       const ge::NodePtr &first_node, const std::string &input_name,
                                       std::string &reshape_type) const {
  std::string op_name = first_node->GetName();
  reshape_type = ConstFormatToStr(origin_format);
  if (origin_format == ge::FORMAT_ND) {
    Status ret = GetReshapeTypeByOpStore(first_node, input_name, reshape_type);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][Generalize] input[%s] of node[%s] get reshape type failed.", input_name.c_str(),
              op_name.c_str());
      return FAILED;
    }
  } else {
    if (ori_shape.GetDims().size() < DIM_DEFAULT_SIZE) {
      std::string reshape_type_temp;
      if (GetReshapeTypeByOpStore(first_node, input_name, reshape_type_temp) != SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][Generalize] input[%s] of node[%s] get reshape type failed.", input_name.c_str(),
                op_name.c_str());
        return FAILED;
      }
      FE_LOGD("[GraphOpt][Prepare][Generalize] node[%s] reshape_type from opStore is %s.", op_name.c_str(),
              reshape_type_temp.c_str());

      if (reshape_type_temp.empty() &&
          GetDefaultReshapeType(origin_format, ori_shape.GetDims().size(), reshape_type_temp) != SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][Generalize] node[%s] get default reshape type failed.", op_name.c_str());
        return FAILED;
      }
      reshape_type = reshape_type_temp;
    }
  }

  FE_LOGD("[GraphOpt][Prepare][Generalize] node[%s] origin_format[%s], shape size is %zu, reshape_type[%s].",
          op_name.c_str(), ConstFormatToStr(origin_format).c_str(), ori_shape.GetDims().size(), reshape_type.c_str());
  return SUCCESS;
}

Status FuzzyGeneralize::CorrectCAxisByOriginalFormat(const ge::Format &origin_format, const ge::NodePtr &input_node,
                                                     const ge::NodePtr &first_node, const std::string &input_name)
                                                     const {
  const std::string input_node_name = input_node->GetName();
  auto iter_input = original_input_nodes_.find(input_node_name);
  if (iter_input == original_input_nodes_.end()) {
    FE_LOGW("[GraphOpt][Prepare][Generalize] can not find node[%s] in original_input_nodes_.", input_node_name.c_str());
    return FAILED;
  }
  ge::NodePtr ori_input_node = iter_input->second;
  FE_CHECK_NOTNULL(ori_input_node);
  ge::GeTensorDescPtr ori_tensor_desc = ori_input_node->GetOpDesc()->MutableInputDesc(0);
  FE_CHECK_NOTNULL(ori_tensor_desc);
  ge::GeShape ori_shape = ori_tensor_desc->GetOriginShape();

  std::string reshape_type;
  if (GetReshapeType(origin_format, ori_shape, first_node, input_name, reshape_type) != SUCCESS) {
    return FAILED;
  }

  size_t c_idx = reshape_type.find('C');
  if (c_idx == std::string::npos) {
    FE_LOGD("[GraphOpt][Prepare][Generalize] reshape_type does not contains char C, input name[%s] node[%s].",
            input_name.c_str(), input_node_name.c_str());
    return SUCCESS;
  }

  if (reshape_type.size() <= DIM_DEFAULT_SIZE) {
    ge::GeTensorDescPtr tensor_desc = input_node->GetOpDesc()->MutableInputDesc(0);
    FE_CHECK_NOTNULL(tensor_desc);
    std::vector<std::pair<int64_t, int64_t>> range;
    (void)tensor_desc->GetOriginShapeRange(range);
    ge::GeShape shape = tensor_desc->GetOriginShape();
    FE_LOGD("[GraphOpt][Prepare][Generalize] before corrent C axis, c_idx[%zu], ori_shape[%s], shape[%s], range[%s].",
            c_idx, ShapeToString(ori_shape.GetDims()).c_str(), ShapeToString(shape.GetDims()).c_str(),
            RangeToString(range).c_str());
    if (c_idx >= range.size()) {
      FE_LOGW("[GraphOpt][Prepare][Generalize] Param invalid, c idx is %zu, but range size is %zu.", c_idx,
              range.size());
      return FAILED;
    }

    auto dim_val = ori_shape.GetDim(c_idx);
    (void)shape.SetDim(c_idx, dim_val);
    range[c_idx].first = dim_val;
    range[c_idx].second = dim_val;
    tensor_desc->SetOriginShape(shape);
    (void)tensor_desc->SetOriginShapeRange(range);
    ge::GeTensorDescPtr out_tensor_desc = input_node->GetOpDesc()->MutableOutputDesc(0);
    UpdateTensorDesc(tensor_desc, out_tensor_desc);
    FE_LOGD("[GraphOpt][Prepare][Generalize] after corrent C axis, shape is %s, range is %s.",
            ShapeToString(shape.GetDims()).c_str(), RangeToString(range).c_str());
  } else {
    FE_LOGW("[GraphOpt][Prepare][Generalize] format size of node[%s] is bigger then 4.", input_node_name.c_str());
    return FAILED;
  }

  return SUCCESS;
}

Status FuzzyGeneralize::CorrectInputNodeCAxisByFirstNode(const ge::NodePtr &input_node) const {
  Status ret;
  for (auto &output_anchor : input_node->GetAllOutDataAnchors()) {
    auto peer_in_anchors = output_anchor->GetPeerInDataAnchors();
    for (size_t i = 0; i < peer_in_anchors.size(); ++i) {
      ge::NodePtr next_node = peer_in_anchors.at(i)->GetOwnerNode();
      FE_CHECK_NOTNULL(next_node);

      ge::OpDescPtr next_node_desc = next_node->GetOpDesc();
      FE_CHECK_NOTNULL(next_node_desc);

      uint32_t in_data_anchor_index = peer_in_anchors.at(i)->GetIdx();
      ge::GeTensorDescPtr tensor_desc = next_node_desc->MutableInputDesc(in_data_anchor_index);
      FE_CHECK_NOTNULL(tensor_desc);

      ret = CorrectCAxisByOriginalFormat(tensor_desc->GetOriginFormat(), input_node, next_node,
                                         next_node_desc->GetInputNameByIndex(in_data_anchor_index));
      if (ret != SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][Generalize] node[%s] CorrectCAxisByOriginalFormat failed.",
                input_node->GetName().c_str());
        return FAILED;
      }
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::CAxisCorrection() const {
  Status ret;
  for (const auto &input_node : external_input_nodes_) {
    if (!IsUnKnownShapeOp(*(input_node->GetOpDesc().get()))) {
      FE_LOGD("[GraphOpt][Prepare][Generalize] Current node[%s] is non-unknownShape op.",
              input_node->GetName().c_str());
      continue;
    }

    ge::GeTensorDescPtr tensor_desc = input_node->GetOpDesc()->MutableInputDesc(0);
    FE_CHECK_NOTNULL(tensor_desc);
    int64_t format = ge::FORMAT_RESERVED;
    (void)ge::AttrUtils::GetInt(*tensor_desc, ge::ATTR_NAME_STORAGE_FORMAT, format);
    ge::Format storage_format = static_cast<ge::Format>(format);
    if (storage_format != ge::FORMAT_NC1HWC0) {
      FE_LOGD("[GraphOpt][Prepare][Generalize] storage_format of node[%s] is not NC1HWC0.",
              input_node->GetName().c_str());
      continue;
    }

    ret = CorrectInputNodeCAxisByFirstNode(input_node);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][Generalize] node[%s] CorrectInputNodeCAxisByFirstNode failed.",
              input_node->GetName().c_str());
      return FAILED;
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::UpdateDynamicShapeToFirstNode(const ge::NodePtr &ori_input_node) const {
  for (auto &output_anchor : ori_input_node->GetAllOutDataAnchors()) {
    auto peer_in_anchors = output_anchor->GetPeerInDataAnchors();
    auto output_desc_ptr = ori_input_node->GetOpDesc()->MutableInputDesc(output_anchor->GetIdx());
    for (size_t i = 0; i < peer_in_anchors.size(); ++i) {
      ge::NodePtr next_node = peer_in_anchors.at(i)->GetOwnerNode();
      FE_CHECK_NOTNULL(next_node);

      ge::OpDescPtr next_node_desc = next_node->GetOpDesc();
      FE_CHECK_NOTNULL(next_node_desc);

      uint32_t in_data_anchor_index = peer_in_anchors.at(i)->GetIdx();
      ge::GeTensorDescPtr tensor_desc = next_node_desc->MutableInputDesc(in_data_anchor_index);
      FE_CHECK_NOTNULL(tensor_desc);

      UpdateTensorDesc(output_desc_ptr, tensor_desc);
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::UpdateDynamicShapeToOriginalGraph(const ge::ComputeGraph &graph) const {
  std::string graph_name = graph.GetName();
  Status ret = CAxisCorrection();
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][Generalize] Graph[%s] CAxisCorrection failed.", graph_name.c_str());
    return FAILED;
  }

  ret = UpdateDynamicShapeToNewInputNode(external_input_nodes_, original_input_nodes_);
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][Generalize] Graph[%s] UpdateDynamicShape to ori graph failed.",
            graph.GetName().c_str());
    return FAILED;
  }

  for (auto &original_input_node : original_input_nodes_) {
    ret = UpdateDynamicShapeToFirstNode(original_input_node.second);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][Generalize] Graph[%s] UpdateDynamicShape to first node failed.",
              graph.GetName().c_str());
      return FAILED;
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::GraphDynamicShapeInfer(const OpStoreAdapterPtr &op_store_adapter,
                                               ge::ComputeGraphPtr &graph_bak, const ge::ComputeGraphPtr &ori_graph) {
  Status ret;
  bool generalize_flag = true;
  bool is_first_bak_graph = true;
  uint32_t graph_bak_index = 0;
  std::string graph_name = graph_bak->GetName();
  FE_LOGD("[GraphOpt][Prepare][GraphDynamicShapeInfer] Current graph_name %s.", graph_name.c_str());
  FE_TIMECOST_START(GraphDynamicShapeInfer);

  /* 1.generalize_flag = true means need to continue to generalize, if passed the limited node check,
   *   it will be changed to false
   * 2.graph_bak_index means downgrades total count, current threshold is 30, prevent unlimited downgrades
   **/
  while (generalize_flag && graph_bak_index < kDowngradesTimeMax) {
    // not first time to infer shape dynamic shape graph, need to copy new graph from original graph
    if (!is_first_bak_graph) {
      graph_name = ori_graph->GetName() + "_bak_" + std::to_string(graph_bak_index);
      FE_LOGD("[GraphOpt][Prepare][GraphDynamicShapeInfer] graph_bak name %s.", graph_name.c_str());
      ge::ComputeGraphPtr new_graph_bak = nullptr;
      FE_MAKE_SHARED(new_graph_bak = std::make_shared<ge::ComputeGraph>(graph_name), return FAILED);

#ifdef ONLY_COMPILE_OPEN_SRC
      ge::Graph tmp_graph = ge::GraphUtils::CreateGraphFromComputeGraph(ori_graph);
      ge::Graph tmp_graph_bak(graph_name);
      if (ge::GraphUtils::CopyGraph(tmp_graph, tmp_graph_bak) != ge::GRAPH_SUCCESS) {
        FE_LOGW("[GraphOpt][Generalize][GeneralizeGraph] Copy graph[%s] by ge failed.", graph_name.c_str());
        return FAILED;
      }
      new_graph_bak = ge::GraphUtils::GetComputeGraph(tmp_graph_bak);
#else
      if (ge::GraphUtils::CopyComputeGraph(ori_graph, new_graph_bak) != ge::GRAPH_SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] Graph[%s] copy by ge failed.", graph_name.c_str());
        return FAILED;
      }
#endif

      if (UpdateDynamicShapeToNewBakGraph(*new_graph_bak) != SUCCESS) {
        FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] Graph[%s] update dynamic shape to original failed.",
                graph_name.c_str());
        return FAILED;
      }

      graph_bak = new_graph_bak;
      graph_bak_index++;
    } else {
      is_first_bak_graph = false;
    }
    ret = optimize_utility_->InferShape(*graph_bak);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] graph[%s] dynamic shape infer by ge failed.",
              graph_name.c_str());
      return FAILED;
    }

    FeGraphUtils::DumpGraphAndOnnx(*graph_bak, "OptimizeGraph_GeneralizeDowngradesAfter");
    FeGraphUtils::DumpSubGraphAndOnnx(*graph_bak, "OptimizeGraph_GeneralizeDowngradesAfter_Subgraph");

    // traversing graph, limit type notarize, get input_nodes/limited_nodes/input_nodes... of every node
    ret = GraphPreprocessing(*graph_bak, op_store_adapter);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] graph[%s] preprocessing failed.", graph_name.c_str());
      return FAILED;
    }

    // fater infer shape, call tbe generalize func to check the validity of shape and range for limited nodes
    ret = CheckAndUpdateLimitedNodes(op_store_adapter, limited_range_nodes_, generalize_flag);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] graph[%s] limited nodes check failed.", graph_name.c_str());
      return FAILED;
    }
  }

  // if dynamic shape graph generalize success, update shape and range of input_nodes from bak graph to original graph
  FE_LOGD("[GraphOpt][Prepare][GraphDynamicShapeInfer] generalize_flag %d.", generalize_flag);
  if (!generalize_flag) {
    if (UpdateDynamicShapeToOriginalGraph(*ori_graph) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] graph[%s] update dynamic shape to original failed.",
              graph_name.c_str());
      return FAILED;
    }
  }

  FE_TIMECOST_END(GraphDynamicShapeInfer, "FEGraphOptimizer::GraphDynamicShapeInfer");
  return SUCCESS;
}

Status FuzzyGeneralize::InitOriginalGraphInfos(const ge::ComputeGraph &graph) {
  if (external_input_nodes_.empty()) {
    FE_LOGW("[GraphOpt][Prepare][InitOriginalGraphInfos] external_input_nodes_ of graph[%s] is null.",
            graph.GetName().c_str());
    return FAILED;
  }

  std::vector<double> non_vec;
  for (auto &node : external_input_nodes_) {
    decent_times_count_.insert(std::make_pair(node->GetName(), 1));
    decent_steps_.insert(std::make_pair(node->GetName(), non_vec));
  }

  for (const auto &cur_node : graph.GetDirectNode()) {
    FE_LOGD("[GraphOpt][Prepare][InitOriginalGraphInfos] Graph[%s] Initializing original graph infos, node[%s, %s].",
            graph.GetName().c_str(), cur_node->GetName().c_str(), cur_node->GetType().c_str());
    bool node_can_be_generalized = (CheckIsExternalNode(cur_node) && cur_node->GetType() != CONSTANT &&
                                    cur_node->GetType() != CONSTANTOP && cur_node->GetType() != VARIABLE);
    if (node_can_be_generalized) {
      original_input_nodes_.emplace(std::make_pair(cur_node->GetName(), cur_node));
    }
  }

  return SUCCESS;
}

Status FuzzyGeneralize::GeneralizeGraph(ge::ComputeGraph &graph) {
  const std::string graph_name = graph.GetName();
  const std::string graph_bak_name = graph_name + "_bak";

  ge::ComputeGraphPtr tmp_graph_ptr = nullptr;
  FE_MAKE_SHARED(tmp_graph_ptr = std::make_shared<ge::ComputeGraph>(graph), return FAILED);
  ge::ComputeGraphPtr new_graph_bak = nullptr;
  FE_MAKE_SHARED(new_graph_bak = std::make_shared<ge::ComputeGraph>(graph_bak_name), return FAILED);

#ifdef ONLY_COMPILE_OPEN_SRC
  ge::Graph tmp_graph = ge::GraphUtils::CreateGraphFromComputeGraph(tmp_graph_ptr);
  ge::Graph tmp_graph_bak(graph_bak_name);
  if (ge::GraphUtils::CopyGraph(tmp_graph, tmp_graph_bak) != ge::GRAPH_SUCCESS) {
    FE_LOGW("[GraphOpt][Generalize][GeneralizeGraph] Copy graph[%s] by ge failed.", graph_name.c_str());
    return FAILED;
  }
  new_graph_bak = ge::GraphUtils::GetComputeGraph(tmp_graph_bak);
#else
  if (ge::GraphUtils::CopyComputeGraph(tmp_graph_ptr, new_graph_bak) != ge::GRAPH_SUCCESS) {
    FE_LOGW("[GraphOpt][Generalize][GeneralizeGraph] Copy graph[%s] by ge failed.", graph_name.c_str());
    return FAILED;
  }
#endif

  // static shape integral graph infer shape for first node generalize
  if (optimize_utility_ == nullptr) {
    FE_LOGW("[[GraphOpt][Generalize][GeneralizeGraph] optimize_utility_ is null.");
    return FAILED;
  }
  Status ret = optimize_utility_->InferShape(*new_graph_bak);
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Generalize][GeneralizeGraph] graph[%s] static shape infer by ge failed.",
            graph_bak_name.c_str());
    return FAILED;
  }

  FE_CHECK_NOTNULL(op_store_adapter_manager_ptr_);
  OpStoreAdapterPtr op_store_adapter = nullptr;
  if (op_store_adapter_manager_ptr_->GetOpStoreAdapter(EN_IMPL_HW_TBE, op_store_adapter) != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][GeneralizeGraph] Get op_store_adapter failed, graph will not be generalized.");
    return FAILED;
  }
  FE_CHECK_NOTNULL(op_store_adapter);

  // traversing graph, limit type notarize, get input_nodes/limited_nodes/input_nodes of every node
  ret = GraphPreprocessing(*new_graph_bak, op_store_adapter);
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][GeneralizeGraph] graph[%s] preprocessing failed.", graph_bak_name.c_str());
    return FAILED;
  }
  FE_LOGD("[GraphOpt][Prepare][GeneralizeGraph] Preprocessing graph[%s] successfully.", graph_bak_name.c_str());

  if (!is_need_generalize_graph_) {
    FE_LOGD("[GraphOpt][Prepare][GeneralizeGraph] Graph[%s] no need to generalize.", graph_bak_name.c_str());
    return SUCCESS;
  }

  // initialize range_decent infos for input nodes
  ret = InitOriginalGraphInfos(graph);
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][GeneralizeGraph] Initiazlize range_decent infos failed.");
    return FAILED;
  }
  FE_LOGD("[GraphOpt][Prepare][GeneralizeGraph] Init range_decent infos with graph[%s] successfully.",
          graph_bak_name.c_str());

  // all input_nodes generalize
  InputNodeGeneralize input_node_generalize(external_input_nodes_, is_range_limited_graph_, node_info_map_,
                                            op_store_adapter);
  ret = input_node_generalize.GeneralizeAllInputNodesInGraph();
  if (ret != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][GeneralizeGraph] graph[%s] generalize input nodes failed.", graph_bak_name.c_str());
    return FAILED;
  }
  FE_LOGD("[GraphOpt][Prepare][GeneralizeGraph] Generalize all inputs nodes in graph[%s] successfully.",
          graph_bak_name.c_str());

  if (!is_range_limited_graph_) {
    FE_LOGD(
        "[GraphOpt][Prepare][GeneralizeGraph] All nodes on graph[%s] is range_unlimited, \
        begin to update copy graph to origin graph.",
        graph_bak_name.c_str());
    if (UpdateDynamicShapeToOriginalGraph(graph) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphDynamicShapeInfer] graph[%s] update dynamic shape to original failed.",
              graph_name.c_str());
      return FAILED;
    }
  } else {
    // dynamic shape limited graph infer shape, include limited_nodes check
    ret = GraphDynamicShapeInfer(op_store_adapter, new_graph_bak, tmp_graph_ptr);
    if (ret != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][Generalize] graph[%s] dynamic shape infer failed.", graph_bak_name.c_str());
      return FAILED;
    }
    FE_LOGD("[GraphOpt][Prepare][GeneralizeGraph] graph[%s] dynamic shape infer successfully.", graph_bak_name.c_str());
  }

  return SUCCESS;
}

Status FuzzyGeneralize::CalDecentSteps(const ge::NodePtr &external_node, const ge::OpDescPtr &opdesc) {
  auto output_desc = external_node->GetOpDesc()->MutableOutputDesc(0);
  const std::string op_name = external_node->GetName();
  FE_LOGD("[GraphOpt][Prepare][CalDecentSteps]FeedDecentSteps %s.", op_name.c_str());

  std::vector<std::pair<int64_t, int64_t>> shape_range;
  (void)output_desc->GetShapeRange(shape_range);

  double step_temp;
  for (size_t idx = 0; idx < shape_range.size(); ++idx) {
    ge::GeTensorDescPtr ori_output_desc = opdesc->MutableOutputDesc(0);
    FE_CHECK_NOTNULL(ori_output_desc);
    int64_t shape_dim = ori_output_desc->GetShape().GetDim(idx);
    FE_LOGD("[GraphOpt][Prepare][CalDecentSteps] Tensor_idx %lu, shape_dim %ld, ori_shape_dim %ld.", idx, shape_dim,
            output_desc->GetShape().GetDim(0));
    if (shape_range[idx].second == MAX_RANGE_UPPER) {
      step_temp = static_cast<double>(shape_dim) / static_cast<double>(TOTAL_DECENT_TIMES - 1);
    } else {
      step_temp = static_cast<double>(shape_range[idx].second - shape_dim) / static_cast<double>(TOTAL_DECENT_TIMES);
    }
    FE_LOGD("[GraphOpt][Prepare][CalDecentSteps]idx %zu, %lf.", idx, step_temp);
    decent_steps_[op_name].emplace_back(step_temp);
  }

  return SUCCESS;
}

Status FuzzyGeneralize::RangeDecent(const ge::NodePtr &external_node, uint32_t &decent_times) {
  auto op_desc = external_node->GetOpDesc();
  const std::string op_name = external_node->GetName();
  FE_LOGD("[GraphOpt][Prepare][RangeDecent] RangeDecent %s.", op_name.c_str());
  std::vector<std::pair<int64_t, int64_t>> range;

  const auto iter_input = original_input_nodes_.find(op_name);
  if (iter_input == original_input_nodes_.end()) {
    FE_LOGW("Cannot find the node[%s] in original_input_nodes_.", op_name.c_str());
    return FAILED;
  }
  const ge::OpDescPtr ori_op_desc = iter_input->second->GetOpDesc();

  if (decent_times == 1) {  // The first time to decend shape-range, we need calculate decent-steps.
    if (CalDecentSteps(external_node, ori_op_desc) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][RangeDecent]Node[%s] CalDecentSteps failed.", op_name.c_str());
      return FAILED;
    }
  }

  ge::GeTensorDescPtr input_tensor_desc = op_desc->MutableInputDesc(0);
  ge::GeTensorDescPtr output_tensor_desc = op_desc->MutableOutputDesc(0);
  input_tensor_desc->GetOriginShapeRange(range);
  FE_LOGD("[GraphOpt][Prepare][RangeDecent]ori range is %s.", RangeToString(range).c_str());
  for (size_t idx = 0; idx < range.size(); ++idx) {
    int64_t range_high = range[idx].second;
    int64_t dim_num = ori_op_desc->MutableInputDesc(0)->GetShape().GetDim(idx);
    if (range_high == MAX_RANGE_UPPER) {
      range[idx].second = dim_num * kExpandNum;
      continue;
    }
    double step = decent_steps_[op_name][idx];
    range_high = ceil(range[idx].second - step);
    FE_LOGW("[GraphOpt][Prepare][RangeDecent]get idx %zu, step:%lf, %ld.", idx, step, range_high);
    if (range_high <= dim_num) {
      range_high = dim_num + 1;
    }
    range[idx].second = range_high;
  }
  input_tensor_desc->SetShapeRange(range);
  input_tensor_desc->SetOriginShapeRange(range);
  output_tensor_desc->SetShapeRange(range);
  output_tensor_desc->SetOriginShapeRange(range);
  FE_LOGD("[GraphOpt][Prepare][RangeDecent]decent range is %s.", RangeToString(range).c_str());

  ++decent_times;
  return SUCCESS;
}

void FuzzyGeneralize::CheckIsSubGraphNode(const ge::NodePtr &node_ptr, const NodeGeneralInfoPtr &node_info_ptr) const {
  if (node_ptr->GetOwnerComputeGraph()->GetParentGraph() != nullptr) {
    node_info_ptr->is_sub_graph_node = true;
  }
}

bool FuzzyGeneralize::CheckIsExternalNode(const ge::NodePtr &node) const {
  if (node->GetInAllNodes().empty() && (node->GetOwnerComputeGraph()->GetParentGraph() == nullptr)) {
    return true;
  }
  FE_LOGD("[GraphOpt][Prepare][CheckIsExternalNode] Current node[%s] is not external-input node.",
          node->GetName().c_str());
  return false;
}

Status FuzzyGeneralize::FeedInputsRootSet(const ge::NodePtr &node_ptr, const NodeGeneralInfoPtr &node_info_ptr) const {
  for (const ge::InDataAnchorPtr &input_data_anchor : node_ptr->GetAllInDataAnchors()) {
    if (input_data_anchor == nullptr) {
      FE_LOGD("[GraphOpt][Prepare][FeedInputsRootSet] input_data_anchor of node[%s] is nullptr.",
              node_ptr->GetName().c_str());
      continue;
    }
    ge::OutDataAnchorPtr peer_node_out_anchor = input_data_anchor->GetPeerOutAnchor();
    if (peer_node_out_anchor == nullptr) {
      FE_LOGD("[GraphOpt][Prepare][FeedInputsRootSet] peer_node_out_anchor of node[%s] is nullptr.",
              node_ptr->GetName().c_str());
      continue;
    }

    ge::NodePtr peer_node = peer_node_out_anchor->GetOwnerNode();
    if (peer_node == nullptr) {
      FE_LOGD("[GraphOpt][Prepare][FeedInputsRootSet] peer out node of node[%s] is nullptr.",
              node_ptr->GetName().c_str());
      continue;
    }
    auto iter = node_info_map_.find(peer_node);
    if (iter == node_info_map_.end()) {
      FE_LOGD("[GraphOpt][Prepare][FeedInputsRootSet] Have not find node[%s] in node_info_map.",
              peer_node->GetName().c_str());
      continue;
    }
    NodeGeneralInfoPtr peer_node_info = iter->second;
    if (peer_node_info == nullptr) {
      FE_LOGW("[GraphOpt][Prepare][FeedInputsRootSet] GeneralInfo of node[%s] is nullptr.",
              peer_node->GetName().c_str());
      return FAILED;
    }

    for (auto &i : peer_node_info->disjoint_root_set) {
      node_info_ptr->disjoint_root_set.emplace(i);
    }

    if (node_ptr->GetOpDesc() == nullptr) {
      FE_LOGD("[GraphOpt][Prepare][FeedInputsRootSet] Opdesc of node[%s] is nullptr.", peer_node->GetName().c_str());
      continue;
    }

    ge::GeTensorDescPtr cur_input = node_ptr->GetOpDesc()->MutableInputDesc(input_data_anchor->GetIdx());
    if (cur_input == nullptr) {
      FE_LOGD("[GraphOpt][Prepare][FeedInputsRootSet] Null pointer! node[%s].", peer_node->GetName().c_str());
      continue;
    }
    node_info_ptr->inputs_root_map.insert(std::make_pair(cur_input, peer_node_info->disjoint_root_set));
  }
  return SUCCESS;
}

Status FuzzyGeneralize::GetCurNodeInfo(const ge::NodePtr &node, NodeGeneralInfoPtr &node_info_ptr) {
  FE_LOGD("[GraphOpt][Prepare][GetCurNodeInfo] Begin to feed node_info for current node[%s].", node->GetName().c_str());
  CheckIsSubGraphNode(node, node_info_ptr);
  FE_CHECK_NOTNULL(ops_kernel_info_store_ptr_);
  Status ret = ops_kernel_info_store_ptr_->FeedNodeGeneralInfoFromOpStore(node, node_info_ptr);
  if (ret != SUCCESS) {
    FE_LOGD(
        "[GraphOpt][Prepare][GetCurNodeInfo] Feed node general info by opstore failed, \
        current node[%s].",
        node->GetName().c_str());
    return FAILED;
  }
  FE_LOGD(
      "[GraphOpt][Prepare][GetCurNodeInfo] Feed node general info by opstore successfully, \
      current node[%s].",
      node->GetName().c_str());

  if (FeedInputsRootSet(node, node_info_ptr) != SUCCESS) {
    FE_LOGW("[GraphOpt][Prepare][GetCurNodeInfo] Feed input root set for node[%s] failed.", node->GetName().c_str());
    return FAILED;
  }
  node_info_map_.insert(std::make_pair(node, node_info_ptr));
  return SUCCESS;
}

Status FuzzyGeneralize::GetRangeLimitValue(const OpStoreAdapterPtr &op_store_adapter,
                                           const NodeGeneralInfoPtr &node_info_ptr,
                                           const ge::NodePtr &node) {
  auto op_kernel_ptr = node_info_ptr->op_kernel;
  if (op_kernel_ptr == nullptr) {
    FE_LOGW("[GraphOpt][Prepare][GetRangeLimitValue] OpkernelPtr from node[%s] is nullptr.", node->GetName().c_str());
    return SUCCESS;
  }
  std::string range_limit_value = op_kernel_ptr->GetRangeLimitValue();
  if (!range_limit_value.empty()) {
    auto iter = RANGE_LIMIT_BOOL_MAP.find(range_limit_value);
    if (iter != RANGE_LIMIT_BOOL_MAP.end()) {
      node_info_ptr->is_limited_range = iter->second;
    } else if (range_limit_value == STR_RANGE_UNKNOWN) {
      bool is_limit_range;
      (void)op_store_adapter->GetRangeLimitType(node, *(node_info_ptr->op_info.get()), is_limit_range);
      node_info_ptr->is_limited_range = is_limit_range;
    } else {
      FE_LOGW("[GraphOpt][Prepare][GetRangeLimitValue] Invalid limited value for node[%s].", node->GetName().c_str());
      return FAILED;
    }
  } else {
    FE_LOGD(
        "[GraphOpt][Prepare][GetRangeLimitValue] Get rangeLimit value from opkernel is null, \
    try to get the correct result by tefusion, node[%s].",
        node->GetName().c_str());
    bool has_generalize_func = false;
    if (op_store_adapter->IsGeneralizeFuncRegistered(has_generalize_func, *(node_info_ptr->op_info)) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GetRangeLimitValue] Check rangelimit value from registered_func failed, node[%s].",
              node->GetName().c_str());
    }
    node_info_ptr->is_limited_range = has_generalize_func;
  }
  if (node_info_ptr->is_limited_range) {
    limited_range_nodes_.emplace_back(node);
    FE_LOGD("[GraphOpt][Prepare][GetRangeLimitValue] Current node[%s] is range_limited.", node->GetName().c_str());
  }
  return SUCCESS;
}

Status FuzzyGeneralize::GraphPreprocessing(const ge::ComputeGraph &graph,
                                           const OpStoreAdapterPtr &op_store_adapter) {
  node_info_map_.clear();
  external_input_nodes_.clear();
  limited_range_nodes_.clear();
  FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Begin to pre-processing graph[%s].", graph.GetName().c_str());
  for (auto &node_ptr : graph.GetAllNodes()) {
    NodeGeneralInfo node_info;
    NodeGeneralInfoPtr node_info_ptr;
    FE_MAKE_SHARED(node_info_ptr = std::make_shared<NodeGeneralInfo>(node_info), return FAILED);
    FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Ready to process current node[%s], .",
            node_ptr->GetName().c_str());

    if (CheckIsExternalNode(node_ptr)) {
      node_info_ptr->disjoint_root_set.emplace(node_ptr);
      if (node_ptr->GetType() != CONSTANT && node_ptr->GetType() != CONSTANTOP && node_ptr->GetType() != VARIABLE) {
        external_input_nodes_.emplace(node_ptr);
      }
      node_info_map_.insert(std::make_pair(node_ptr, node_info_ptr));
      FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Current node[%s] is external input node.",
              node_ptr->GetName().c_str());
      continue;
    }

    if (GetCurNodeInfo(node_ptr, node_info_ptr) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphPreprocessing] Get current node[%s] info failed.", node_ptr->GetName().c_str());
      return FAILED;
    }

    if (!is_need_generalize_graph_ && node_info_ptr->is_found_in_opstore && node_info_ptr->is_support_dynamic_shape) {
      is_need_generalize_graph_ = true;
      FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Graph[%s] need to be generalized.", graph.GetName().c_str());
    }

    FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Get current node[%s] info successfully.",
            node_ptr->GetName().c_str());
    if (GetRangeLimitValue(op_store_adapter, node_info_ptr, node_ptr) != SUCCESS) {
      FE_LOGW("[GraphOpt][Prepare][GraphPreprocessing] Get range_limit value failed, node[%s].",
              node_ptr->GetName().c_str());
      return FAILED;
    }

    FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Get range limit value successfully, node[%s].",
            node_ptr->GetName().c_str());
    if (!is_range_limited_graph_ && node_info_ptr->is_limited_range) {
      is_range_limited_graph_ = true;
      FE_LOGD("[GraphOpt][Prepare][GraphPreprocessing] Graph[%s] is range-limited graph.", graph.GetName().c_str());
    }
  }
  return SUCCESS;
}
}  // namespace fe