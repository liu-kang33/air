/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_DATA_TASK_BUILDER_H
#define AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_DATA_TASK_BUILDER_H

#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "graph/compute_graph.h"
#include "common/memory_slice.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/tensor_utils.h"
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"

namespace fe {
using ContextType = rtFftsPlusContextType_t;

class DataTaskBuilder : public FFTSPlusTaskBuilder {
 public:
  DataTaskBuilder();
  explicit DataTaskBuilder(CACHE_OPERATION operation);
  ~DataTaskBuilder() override;

  /*
   * @ingroup fe
   * @brief   Generate tasks
   * @param   [in] node Node of compute graph
   * @param   [in] context Context for generate tasks
   * @param   [out] task_defs Save the generated tasks.
   * @return  SUCCESS or FAILED
   */
  Status GenManualDataCtxDef(const ge::NodePtr &node,
                             domi::FftsPlusTaskDef *ffts_plus_task_def) const;

  virtual Status FillManualDataCtx(size_t out_anchor_index, const ge::NodePtr &node,
                                   const DataContextParam &param,
                                   domi::FftsPlusTaskDef *ffts_plus_task_def,
                                   domi::FftsPlusDataCtxDef *data_ctx_def) const {
    (void)out_anchor_index;
    (void)node;
    (void)param;
    (void)ffts_plus_task_def;
    (void)data_ctx_def;
    return SUCCESS;
  };

  Status GenAutoDataCtxDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def);

  virtual Status FillAutoDataCtx(size_t out_anchor_index, const ge::NodePtr &node,
                                 const std::vector<DataContextParam> &params, domi::FftsPlusTaskDef *ffts_plus_task_def,
                                 domi::FftsPlusDataCtxDef *data_ctx_def, const size_t &window_id) {
    (void)out_anchor_index;
    (void)node;
    (void)params;
    (void)ffts_plus_task_def;
    (void)data_ctx_def;
    (void)window_id;
    return SUCCESS;
  };

  Status GenDynamicDataCtxDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) const;

  virtual Status FillDynamicDataCtx(const size_t &out_anchor_index, const ge::NodePtr &node,
                                    domi::FftsPlusTaskDef *ffts_plus_task_def,
                                    const rtFftsPlusContextType_t &context_type,
                                    const vector<uint32_t> &context_id_list) const {
    (void)out_anchor_index;
    (void)node;
    (void)ffts_plus_task_def;
    (void)context_type;
    (void)context_id_list;
    return SUCCESS;
  };

  void SetOperation(CACHE_OPERATION operation);

  void SetBurstLen(int64_t burst_len);

  DataTaskBuilder(const DataTaskBuilder &builder) = delete;
  DataTaskBuilder &operator=(const DataTaskBuilder &builder) = delete;

 protected:
  /*
   * Prefetch, invalid and write_back will use the following method.
   */
  std::vector<int> GetIndices(const ge::NodePtr &node) const;

  bool ExceedMaxCtxNum(size_t curr_num, size_t pending_num) const;

  void FillAutoThreadingParam(const vector<DataContextParam> &params, domi::FftsPlusDataCtxDef *data_ctx_def,
                              const uint32_t &slice_num) const;

  void FillManualThreadingParam(const DataContextParam &param, domi::FftsPlusDataCtxDef *data_ctx_def) const;


  /*
   * Prefetch will use the following method.
   * for manual and auto mode
   */
  Status GetAddrBase(size_t in_anchor_index, const ge::NodePtr &node, uint64_t &addr_base) const;


  Status UpdateSrcSlotAndPfBm(domi::FftsPlusTaskDef *ffts_plus_task_def, uint32_t context_id) const;

  template<typename T>
  Status AddSrcSlotAndBmToCtx(uint32_t prefetch_ctx_id, T *ctx) const {
    size_t src_slot_size = static_cast<size_t>(ctx->src_slot_size());
    if (src_slot_size >= kMaxPretchNum) {
      REPORT_CM_ERROR("Already reach the maximum size of prefetch bitmap of aic/aiv context.");
      return FAILED;
    }
    ctx->add_src_slot(prefetch_ctx_id);

    uint32_t enable_bm = ctx->prefetch_enable_bitmap();
    uint32_t once_bm = ctx->prefetch_once_bitmap();
    SetBitOne(src_slot_size, enable_bm);
    SetBitOne(src_slot_size, once_bm);
    ctx->set_prefetch_enable_bitmap(enable_bm);
    ctx->set_prefetch_once_bitmap(once_bm);
    return SUCCESS;
  }


  /*
   * Invalid and write_back will use the following method.
   * Manual mode need override this method.
   *
   * Prefetch data context does not need to know successors.
   * succ_list is an output parameter. It contains all context ids and labeled
   * context ids if the peer node has more than 26 successors.
   * cons_cnt is the total of all successors.
   */
  virtual Status GetSuccessorContextId(uint32_t out_anchor_index, const ge::NodePtr &node,
                                       std::vector<uint32_t> &succ_list, uint32_t &cons_cnt) const;

  CACHE_OPERATION operation_;

  int64_t burst_len_ = 0;
};

}  // namespace fe
#endif // AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_TASK_BUILDER_H
