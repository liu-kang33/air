/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "prefetch_task_builder.h"

namespace fe {
PrefetchTaskBuilder::PrefetchTaskBuilder()
    : DataTaskBuilder(CACHE_OPERATION::PREFETCH) {}

PrefetchTaskBuilder::~PrefetchTaskBuilder() {}

Status PrefetchTaskBuilder::FillManualDataCtx(size_t in_anchor_index, const ge::NodePtr &node,
                                              const DataContextParam &param,
                                              domi::FftsPlusTaskDef *ffts_plus_task_def,
                                              domi::FftsPlusDataCtxDef *data_ctx_def) const {
  CM_LOGD("Fill manual prefetch context for node %s, input %zu.", node->GetName().c_str(), in_anchor_index);
  auto op_desc = node->GetOpDesc();
  uint32_t context_id = 0;
  if (!ge::AttrUtils::GetInt(op_desc, kContextId, context_id)) {
    REPORT_CM_ERROR("[GenTsk][PrefetchTsk][FillCtxt][node %s, type %s] Cannot get context id for this node.",
                    op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return FAILED;
  }

  data_ctx_def->set_aten(0);

  /* In manual mode, only serve one node.
   * prefetch context does not need the dependency table and it
   * is executed by the hardware instead of the MCU.
   * We just init it with a non-zero number in case the MCU will
   * execute it immediately. */
  uint32_t cons_cnt = 1;
  data_ctx_def->set_cnt(cons_cnt);
  data_ctx_def->set_cnt_init(cons_cnt);
  /* Only do the prefetch when orig_consumer_counter is not equal to
   * the run_consumer_counter. */
  data_ctx_def->set_orig_consumer_counter(cons_cnt);
  data_ctx_def->set_run_consumer_counter(cons_cnt);

  /* when we do prefetch we need to get the peer output addr */
  uint64_t addr_base = 0;
  if (GetAddrBase(in_anchor_index, node, addr_base) != SUCCESS) {
    return FAILED;
  }
  data_ctx_def->set_addr_base(addr_base);
  data_ctx_def->set_addr_offset(param.base_addr_offset);
  FillManualThreadingParam(param, data_ctx_def);
  return UpdateSrcSlotAndPfBm(ffts_plus_task_def, context_id);
}
}
