/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_PREFETCH_DYNAMIC_TASK_BUILDER_H
#define AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_PREFETCH_DYNAMIC_TASK_BUILDER_H
#include "ffts_plus_task_builder/data/data_task_builder.h"

namespace fe {
class PrefetchDynamicTaskBuilder : public DataTaskBuilder {
 public:
  PrefetchDynamicTaskBuilder();
  ~PrefetchDynamicTaskBuilder() override;

  Status FillDynamicDataCtx(const size_t &in_anchor_index, const ge::NodePtr &node,
                            domi::FftsPlusTaskDef *ffts_plus_task_def, const rtFftsPlusContextType_t &context_type,
                            const vector<uint32_t> &context_id_list) const override;

  PrefetchDynamicTaskBuilder(const PrefetchDynamicTaskBuilder &builder) = delete;
  PrefetchDynamicTaskBuilder &operator=(const PrefetchDynamicTaskBuilder &builder) = delete;
};

}  // namespace fe
#endif  // AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_PREFETCH_DYNAMIC_TASK_BUILDER_H
