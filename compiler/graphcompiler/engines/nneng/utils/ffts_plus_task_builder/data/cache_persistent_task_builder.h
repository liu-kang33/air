/**
 * Copyright 2021-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_CACHE_PERSISTENT_TASK_BUILDER_H
#define AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_CACHE_PERSISTENT_TASK_BUILDER_H
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"
namespace fe {
class CachePersistTaskBuilder : public FFTSPlusTaskBuilder {
 public:
  CachePersistTaskBuilder() = default;

  ~CachePersistTaskBuilder() override = default;

  /*
   * @ingroup fe
   * @brief   Generate tasks
   * @param   [in] node Node of compute graph
   * @param   [in] context Context for generate tasks
   * @param   [out] task_defs Save the generated tasks.
   * @return  SUCCESS or FAILED
   */
  Status GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) override;

  Status GenContextDef(const ge::Node &node, const domi::FftsPlusTaskDef *ffts_plus_task_def);

  CachePersistTaskBuilder(const CachePersistTaskBuilder &builder) = delete;

  CachePersistTaskBuilder &operator=(const CachePersistTaskBuilder &builder) = delete;
};
}
#endif //AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_CACHE_PERSISTENT_TASK_BUILDER_H