/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_OUT_TASK_BUILDER_H
#define AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_OUT_TASK_BUILDER_H
#include "ffts_plus_task_builder/data/data_task_builder.h"


namespace fe {
class OutTaskBuilder : public DataTaskBuilder {
 public:
  OutTaskBuilder();
  explicit OutTaskBuilder(CACHE_OPERATION operation);
  ~OutTaskBuilder() override;

  Status FillManualDataCtx(size_t out_anchor_index,
                           const ge::NodePtr &node,
                           const DataContextParam &param,
                           domi::FftsPlusTaskDef *ffts_plus_task_def,
                           domi::FftsPlusDataCtxDef *data_ctx_def) const override;

  Status GetSuccessorContextId(uint32_t out_anchor_index, const ge::NodePtr &node, std::vector<uint32_t> &succ_list,
                               uint32_t &cons_cnt) const override;

  Status UptSuccListOfRelatedNode(const ge::NodePtr &node, const std::vector<uint32_t> &succ_list,
                                  domi::FftsPlusTaskDef *ffts_plus_task_def) const;


  OutTaskBuilder(const OutTaskBuilder &builder) = delete;
  OutTaskBuilder &operator=(const OutTaskBuilder &builder) = delete;

};

}  // namespace fe
#endif //AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_DATA_OUT_TASK_BUILDER_H
