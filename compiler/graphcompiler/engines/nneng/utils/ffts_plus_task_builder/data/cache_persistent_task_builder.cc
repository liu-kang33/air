/**
 * Copyright 2021-2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ffts_plus_task_builder/data/cache_persistent_task_builder.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/common_utils.h"
#include "common/ffts_plus_type.h"

namespace fe {
inline int64_t DivisionCeiling(int64_t dividend, int64_t divisor) {
  if (divisor == 0) {
    return 0;
  } else {
    int64_t tmp_divisor = divisor - 1;
    if (CheckInt64AddOverflow(dividend, tmp_divisor) == SUCCESS) {
      dividend = dividend + tmp_divisor;
    }
    return dividend / divisor;
  }
}

Status CachePersistTaskBuilder::GenContextDef(
    const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  return GenContextDef(*node.get(), ffts_plus_task_def);
}

Status CachePersistTaskBuilder::GenContextDef(
    const ge::Node &node,
    const domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_CHECK_NOTNULL(ffts_plus_task_def);
  ge::OpDescPtr op_desc = node.GetOpDesc();
  uint32_t persist_id;
  if (!ge::AttrUtils::GetInt(op_desc, kCachePersist, persist_id) ||
      persist_id > kMaxPersistNum) {
    return FAILED;
  }

  CM_LOGD("Graph %s need to do cache persistent with id %u.",
          node.GetName().c_str(), persist_id);

  return FAILED;
}
}