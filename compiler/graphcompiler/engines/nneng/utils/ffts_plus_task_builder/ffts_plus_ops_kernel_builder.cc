/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/ffts_plus_ops_kernel_builder.h"
#include <runtime/rt.h>
#include "common/constants_define.h"
#include "common/aicore_util_constants.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/attr_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "param_calculate/tensorsize_calculator.h"
#include "task_builder/task_builder.h"
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"
#include "register/ops_kernel_builder_registry.h"
#include "common/comm_log.h"

namespace fe {
const std::unordered_set<std::string> CONTROL_OP_V2_TYPE = {"If", "While", "Case"};

FFTSPlusOpsKernelBuilder::FFTSPlusOpsKernelBuilder() {}

FFTSPlusOpsKernelBuilder::~FFTSPlusOpsKernelBuilder() {}

Status FFTSPlusOpsKernelBuilder::Initialize(const std::map<std::string, std::string> &options) {
  (void)options;
  CM_MAKE_SHARED(ffts_plus_manual_mode_ptr_ = std::make_shared<FFTSPlusManualMode>(), return FAILED);
  CM_MAKE_SHARED(ffts_plus_auto_mode_ptr_ = std::make_shared<FFTSPlusAutoMode>(), return FAILED);
  CM_MAKE_SHARED(ffts_plus_dynamic_mode_ptr_ = std::make_shared<FFTSPlusDynamicMode>(), return FAILED);
  CM_MAKE_SHARED(ffts_plus_mix_l2_mode_ptr_ = std::make_shared<FFTSPlusMixL2Mode>(), return FAILED);
  return SUCCESS;
}

Status FFTSPlusOpsKernelBuilder::Finalize() {return SUCCESS;}

Status FFTSPlusOpsKernelBuilder::CalcOpRunningParam(ge::Node &node) {
  (void)node;
  return SUCCESS;
}

Status FFTSPlusOpsKernelBuilder::GenerateTask(const ge::Node &node, ge::RunContext &context,
                                              std::vector<domi::TaskDef> &task_defs) {
  CM_LOGD("FFTSPlusOpsKernelBuilder GenerateTask node name:%s, node type:%s",
          node.GetName().c_str(), node.GetType().c_str());
  ge::OpDescPtr op_desc = node.GetOpDesc();
  Status status;
  uint64_t ready_context_num = 0;
  uint64_t total_context_number = 0;
  domi::TaskDef task_def;
  std::vector<ge::NodePtr> sub_graph_nodes;
  ge::ComputeGraphPtr sgt_graph = nullptr;
  FFTSPlusBaseModePtr base_mode_ptr = nullptr;
  if (op_desc->HasAttr(ATTR_NAME_FFTS_PLUS_MIX_L2)) {
    ge::Node &temp_node = const_cast<ge::Node&>(node);
    ge::NodePtr node_ptr = temp_node.shared_from_this();
    sub_graph_nodes.emplace_back(node_ptr);
    CM_MAKE_SHARED(sgt_graph = std::make_shared<ge::ComputeGraph>("MIX_L2"), return FAILED);
    base_mode_ptr = ffts_plus_mix_l2_mode_ptr_;
  } else {
    std::string sub_graph_name = op_desc->GetSubgraphInstanceName(0);
    if (sub_graph_name.empty()) {
      return FAILED;
    }
    auto ai_graph = node.GetOwnerComputeGraph();
    CM_CHECK_NOTNULL(ai_graph);
    auto root_graph = ge::GraphUtils::FindRootGraph(ai_graph);
    CM_CHECK_NOTNULL(root_graph);
    sgt_graph = root_graph->GetSubgraph(sub_graph_name);
    CM_CHECK_NOTNULL(sgt_graph);
    status = GenPersistentContext(node, ready_context_num, total_context_number, task_def);
    if (status != SUCCESS) {
      return status;
    }
    base_mode_ptr = GetFftsPlusMode(*sgt_graph);
  }
  status = base_mode_ptr->Initialize();

  status = base_mode_ptr->GenFftsPlusContextId(*sgt_graph, sub_graph_nodes, ready_context_num, total_context_number);
  if (status != SUCCESS) {
    return status;
  }
  CM_LOGD("FFTSPlusOpsKernelBuilder GenerateTask node name:%s, node type:%s, readynum:%lu, totalnumber:%lu",
          node.GetName().c_str(), node.GetType().c_str(), ready_context_num, total_context_number);

  status = base_mode_ptr->GenSubGraphTaskDef(sub_graph_nodes, context, task_def);

  if (status != SUCCESS) {
    return status;
  }
  status = GenSubGraphSqeDef(task_def, ready_context_num);
  if (status != SUCCESS) {
    return status;
  }
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  ffts_plus_task_def->set_op_index(op_desc->GetId());
  task_defs.push_back(task_def);
  CM_LOGD("FFTSPlusOpsKernelBuilder GenerateTask node name:%s, node type:%s , id:%ld success",
          node.GetName().c_str(), node.GetType().c_str(), op_desc->GetId());
  return SUCCESS;
}

Status FFTSPlusOpsKernelBuilder::GenPersistentContext(const ge::Node &node, uint64_t &ready_context_num,
                                                      uint64_t &total_context_number, domi::TaskDef &task_def) {
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  CachePersistTaskBuilder cp;
  if (cp.GenContextDef(node, ffts_plus_task_def) == SUCCESS) {
    ready_context_num++;
    total_context_number++;
  }
  return SUCCESS;
}

FFTSPlusBaseModePtr FFTSPlusOpsKernelBuilder::GetFftsPlusMode(const ge::ComputeGraph &sgt_graph) {
  ModeType mode_type = ModeType::MANUAL_MODE_TYPE;

  for (const auto &node : sgt_graph.GetDirectNode()) {
    if (OpTensorUtils::IsUnKnownShapeOp(*(node->GetOpDesc().get()))) {
      // Dynamic shape, auto thread
      mode_type = ModeType::DYNAMIC_MODE_TYPE;
      break;
    } else {
      ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
      slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
      if (OpIsAutoThread(slice_info_ptr)) {
        mode_type = ModeType::AUTO_MODE_TYPE;
        break;
      }
    }
  }

  switch (mode_type) {
    case ModeType::MANUAL_MODE_TYPE:
      return ffts_plus_manual_mode_ptr_;
    case ModeType::AUTO_MODE_TYPE:
      return ffts_plus_auto_mode_ptr_;
    case ModeType::DYNAMIC_MODE_TYPE:
      return ffts_plus_dynamic_mode_ptr_;
    default:
      return nullptr;
  }
}

Status FFTSPlusOpsKernelBuilder::GenSubGraphSqeDef(domi::TaskDef &task_def,
                                                   const uint64_t &ready_context_num) const {
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  uint64_t gen_ctx_num = ffts_plus_task_def->ffts_plus_ctx_size();

  domi::FftsPlusSqeDef* ffts_plus_sqe = ffts_plus_task_def->mutable_ffts_plus_sqe();
  for (size_t i = 0; i < gen_ctx_num; i++) {
    CM_LOGD("Gen subGraph sqe def :%s.", ffts_plus_task_def->mutable_ffts_plus_ctx(static_cast<int>(i))->DebugString().c_str());
  }

  ffts_plus_sqe->set_ready_context_num(ready_context_num);
  ffts_plus_sqe->set_total_context_num(gen_ctx_num);

  return SUCCESS;
}
}  // namespace fe