/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/mix_aic_aiv_auto_task_builder.h"

#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"


namespace fe {
static const vector<std::string> kMixPrefixs = { "_mix_aic", "_mix_aiv" };

MixAICAIVAutoTaskBuilder::MixAICAIVAutoTaskBuilder() {}

MixAICAIVAutoTaskBuilder::~MixAICAIVAutoTaskBuilder() {}

Status MixAICAIVAutoTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("MixAICAIVTaskBuilder::GenContextDef begin, node name:%s, node type:%s.", node->GetName().c_str(),
          node->GetType().c_str());
  auto op_desc = node->GetOpDesc();
  Status status;
  vector<FftsPlusComCtx> sub_ffts_plus_context;
  GenFftsPlusTaskCommonInfo(node, sub_ffts_plus_context);

  uint32_t addr_size = 0;
  uint32_t thread_dim = 0;
  for (size_t i = 0; i < sub_ffts_plus_context.size(); ++i) {
    domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
    CM_CHECK_NOTNULL(ffts_plus_ctx_def);

    vector<string> thread_core_type;
    (void)ge::AttrUtils::GetListStr(op_desc, ATTR_NAME_THREAD_CUBE_VECTOR_CORE_TYPE, thread_core_type);
    if (thread_core_type.empty()) {
      return FAILED;
    }
    if (thread_core_type[0] == kCoreTypeMixAIC) {
      ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_MIX_AIC);
    } else {
      ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_MIX_AIV);
    }

    vector<uint32_t> auto_ctx_id_list;
    if (ge::AttrUtils::GetListInt(op_desc, kAutoCtxIdList, auto_ctx_id_list) && auto_ctx_id_list.size() ==
        sub_ffts_plus_context.size()) {
      ffts_plus_ctx_def->set_context_id(auto_ctx_id_list[i]);
    }
    ffts_plus_ctx_def->set_op_index(op_desc->GetId());

    CM_LOGD("GenContextDef nodetype:%s, name:%s, context_type:%u, op_index:%u", node->GetType().c_str(),
            node->GetName().c_str(), ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());

    domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def = ffts_plus_ctx_def->mutable_mix_aic_aiv_ctx();
    CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);
    status = FillContextData(node, mix_aic_aiv_ctx_def);
    if (status != SUCCESS) {
      CM_LOGE("Mix_aic_aiv context fill context data failed. Op[%s, optype[%s]]",
              op_desc->GetName().c_str(), op_desc->GetType().c_str());
      return status;
    }

    /* GE fill next node context's base_addr need this
     * node(a) has windown size context_a, generate context_a has same base_addr_a.
     * next continue node(b) has windown size context_b,
     * context_b's base_ddr_b = base_addr_a + a_size(memory for context_a)
     * FE set save_task_addr at node(a) last context for GE fill contexts(generate by b)'s base_addr_b */
    if (i == sub_ffts_plus_context.size() - 1) {
      mix_aic_aiv_ctx_def->set_save_task_addr(1);
    } else {
      mix_aic_aiv_ctx_def->set_save_task_addr(0);
    }

    mix_aic_aiv_ctx_def->set_thread_id(i);
    mix_aic_aiv_ctx_def->set_pred_cnt(sub_ffts_plus_context[i].pred_cnt);
    mix_aic_aiv_ctx_def->set_pred_cnt_init(sub_ffts_plus_context[i].pred_cnt);
    mix_aic_aiv_ctx_def->set_successor_num(0);

    addr_size = mix_aic_aiv_ctx_def->task_addr_size();
    thread_dim = mix_aic_aiv_ctx_def->thread_dim();

    if (AddAdditionalArgs(op_desc, ffts_plus_task_def, 1) != SUCCESS) {
      REPORT_CM_ERROR("[MixAICAIVTaskBuilder] [AddAdditionalArgs] Add node[%s] additional args failed.",
                      op_desc->GetName().c_str());
      return FAILED;
    }
  }

  /* cur_addr_size: total context addr size in sqe
   * addr_size: single thread addr_size
   * GE memory request size is the size of all threads, this addr_size for GE */
  uint32_t cur_addr_size = ffts_plus_task_def->addr_size();
  ffts_plus_task_def->set_addr_size(cur_addr_size + addr_size * thread_dim);
  CM_LOGD("GenContextDef nodetype:%s, name:%s, total_addr_size:%u", node->GetType().c_str(),
          node->GetName().c_str(), ffts_plus_task_def->addr_size());
  return SUCCESS;
}

Status MixAICAIVAutoTaskBuilder::FillContextData(const ge::NodePtr &node,
                                                 domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def) {
  CM_CHECK_NOTNULL(node);
  CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);
  auto op_desc = node->GetOpDesc();
  Status status = GenContextArgs(node);
  if (status != SUCCESS) {
    REPORT_CM_ERROR("[MixAICAIVTaskBuilder] [FillContextData] failed. Op[%s, optype[%s]]",
                    op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  vector<uint32_t> task_ratio_list;
  (void)ge::AttrUtils::GetListInt(op_desc, kThreadTaskRadio, task_ratio_list);
  if (task_ratio_list.size() > 1) {
    mix_aic_aiv_ctx_def->set_non_tail_block_ratio_n(task_ratio_list[0]);
    mix_aic_aiv_ctx_def->set_tail_block_ratio_n(task_ratio_list[1]);
  }

  mix_aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
  mix_aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);

  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
  slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
  mix_aic_aiv_ctx_def->set_ns(1);
  mix_aic_aiv_ctx_def->set_aten(kAutoMode);
  mix_aic_aiv_ctx_def->set_atm(kAutoMode);
  mix_aic_aiv_ctx_def->set_thread_dim(slice_info_ptr->slice_instance_num);

  vector<int32_t> block_dims;
  (void)ge::AttrUtils::GetListInt(op_desc, ge::TVM_ATTR_NAME_THREAD_BLOCKDIM, block_dims);
  if (block_dims.size() > 1) {
    mix_aic_aiv_ctx_def->set_non_tail_block_dim(static_cast<uint32_t>(block_dims[0]));
    mix_aic_aiv_ctx_def->set_tail_block_dim(static_cast<uint32_t>(block_dims[1]));
    CM_LOGD("block_dims[0]:%u, block_dims[1]:%u.", static_cast<uint32_t>(block_dims[0]),
            static_cast<uint32_t>(block_dims[1]));
  }

  // modeInArgsFirstField
  uint32_t mode = 0;
  uint64_t modeArgs = 0;
  (void)ge::AttrUtils::GetInt(op_desc, kModeInArgsFirstField, mode);
  // mode == 1 indicates we need reserve 8 Bytes for the args beginning
  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    mix_aic_aiv_ctx_def->add_task_addr(modeArgs);
  }

  // generate _register_stub_func
  vector<string> unique_ids;
  string session_graph_id = "";
  if (ge::AttrUtils::GetStr(op_desc, ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id) && !session_graph_id.empty()) {
    unique_ids.push_back(session_graph_id + "_" + op_desc->GetName() + "_0");
    unique_ids.push_back(session_graph_id + "_" + op_desc->GetName() + "_1");
  } else {
    unique_ids.push_back(op_desc->GetName() + "_0");
    unique_ids.push_back(op_desc->GetName() + "_1");
  }
  (void)ge::AttrUtils::SetListStr(op_desc, "_register_stub_func", unique_ids);

  uint32_t input_output_num = param_offset_.first_thread_input_addrs.size() +
                              param_offset_.first_thread_output_addrs.size();
  mix_aic_aiv_ctx_def->set_input_output_count(input_output_num);

  for (auto input_addr : param_offset_.first_thread_input_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(input_addr));
  }
  for (auto output_addr : param_offset_.first_thread_output_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(output_addr));
  }
  for (auto workspace_addr : param_offset_.thread_workspace_addrs[0]) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(workspace_addr));
  }
  for (auto workspace_addr : param_offset_.thread_workspace_addrs[1]) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(workspace_addr));
  }
  for (auto addr_offset : param_offset_.thread_addr_offset) {
    mix_aic_aiv_ctx_def->add_task_addr_offset(reinterpret_cast<uint64_t>(addr_offset));
  }

  for (auto &prefix : kMixPrefixs) {
    string attr_key_kernel_name = prefix + kThreadKernelName;
    string attr_kernel_name;
    (void)ge::AttrUtils::GetStr(op_desc, attr_key_kernel_name, attr_kernel_name);
    mix_aic_aiv_ctx_def->add_kernel_name(attr_kernel_name);
  }
  return SUCCESS;
}

Status MixAICAIVAutoTaskBuilder::AddAdditionalArgs(const ge::OpDescPtr &op_desc,
                                                   domi::FftsPlusTaskDef *ffts_plus_task_def,
                                                   const size_t &ctx_num) {
  CM_CHECK_NOTNULL(op_desc);
  // modeInArgsFirstField
  uint32_t mode = 0;
  (void) ge::AttrUtils::GetInt(op_desc, kModeInArgsFirstField, mode);
  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    domi::AdditionalDataDef *additional_data_def = ffts_plus_task_def->add_additional_data();
    CM_CHECK_NOTNULL(additional_data_def);
    uint32_t context_id = 0;
    (void) ge::AttrUtils::GetInt(op_desc, kContextId, context_id);
    additional_data_def->set_data_type(0);
    additional_data_def->add_context_id(context_id);
    if (ctx_num > 1) {
      (void) ge::AttrUtils::GetInt(op_desc, "_default_context_id", context_id);
      additional_data_def->add_context_id(context_id);
    }
  }
  return SUCCESS;
}
}  // namespace fe