/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FUSION_ENGINE_UTILS_FFTS_PLUS_TASK_BUILDER_AIC_AIV_DYNAMIC_TASK_BUILDER_H_
#define FUSION_ENGINE_UTILS_FFTS_PLUS_TASK_BUILDER_AIC_AIV_DYNAMIC_TASK_BUILDER_H_
#include <map>
#include <memory>
#include <vector>
#include "proto/task.pb.h"
#include "adapter/adapter_itf/task_builder_adapter.h"
#include "common/opskernel/ops_kernel_info_types.h"
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"

namespace fe {
class AICAIVDynamicTaskBuilder : public FFTSPlusTaskBuilder {
 public:
  AICAIVDynamicTaskBuilder();
  ~AICAIVDynamicTaskBuilder() override;

  /*
   * @ingroup fe
   * @brief   Generate tasks
   * @param   [in] node Node of compute graph
   * @param   [in] context Context for generate tasks
   * @param   [out] task_defs Save the generated tasks.
   * @return  SUCCESS or FAILED
   */
  Status GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) override;
  Status FillContextData(const ge::NodePtr &node, domi::FftsPlusAicAivCtxDef *aic_aiv_ctx_def);
 private:
  AICAIVDynamicTaskBuilder(const AICAIVDynamicTaskBuilder &builder) = delete;
  AICAIVDynamicTaskBuilder &operator=(const AICAIVDynamicTaskBuilder &builder) = delete;
};

}  // namespace fe
#endif  // FUSION_ENGINE_UTILS_FFTS_PLUS_TASK_BUILDER_AIC_AIV_DYNAMIC_TASK_BUILDER_H_
