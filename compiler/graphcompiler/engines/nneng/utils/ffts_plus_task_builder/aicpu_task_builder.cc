/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/aicpu_task_builder.h"

#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"

namespace fe {
AicpuTaskBuilder::AicpuTaskBuilder() {}

AicpuTaskBuilder::~AicpuTaskBuilder() {}

Status AicpuTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("AiCpuOpsTaskBuilder::GenContextDef begin, node name:%s, node type:%s.", node->GetName().c_str(),
          node->GetType().c_str());
  auto op_desc = node->GetOpDesc();

  vector<FftsPlusComCtx> sub_ffts_plus_context;
  GenFftsPlusTaskCommonInfo(node, sub_ffts_plus_context);
  FftsPlusCtxDefPtr ctx_def_ptr = nullptr;
  ctx_def_ptr = op_desc->TryGetExtAttr("_ffts_plus_aicpu_ctx_def", ctx_def_ptr);
  CM_CHECK_NOTNULL(ctx_def_ptr);
  domi::FftsPlusAicpuCtxDef *aicpu_ctx_def_ptr = ctx_def_ptr->mutable_aicpu_ctx();
  CM_CHECK_NOTNULL(aicpu_ctx_def_ptr);
  domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  CM_CHECK_NOTNULL(ffts_plus_ctx_def);
  ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_AICPU);
  ffts_plus_ctx_def->set_op_index(op_desc->GetId());

  uint32_t context_id = 0;
  if (ge::AttrUtils::GetInt(op_desc, kContextId, context_id)) {
    ffts_plus_ctx_def->set_context_id(context_id);
  }
  CM_LOGD("GenContextDef nodetype:%s, name:%s, context_type:%u, op_index:%u",
          node->GetType().c_str(), node->GetName().c_str(),
          ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());
  domi::FftsPlusAicpuCtxDef *aicpu_ctx_def = ffts_plus_ctx_def->mutable_aicpu_ctx();
  CM_CHECK_NOTNULL(aicpu_ctx_def);
  if (FillContextData(aicpu_ctx_def_ptr, aicpu_ctx_def) != SUCCESS) {
    CM_LOGE("FillContextData failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return FAILED;
  }
  if (sub_ffts_plus_context.empty()) {
    REPORT_CM_ERROR("Node[name=%s, type=%s] sub ffts plus context is empty.",
                    op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return FAILED;
  }
  aicpu_ctx_def->set_pred_cnt(sub_ffts_plus_context[0].pred_cnt);
  aicpu_ctx_def->set_pred_cnt_init(sub_ffts_plus_context[0].pred_cnt);
  aicpu_ctx_def->set_aten(0);
  aicpu_ctx_def->set_successor_num(0);

  (void)ge::AttrUtils::SetListInt(op_desc, kSuccList, sub_ffts_plus_context[0].succ_list);
  return SUCCESS;
}

Status AicpuTaskBuilder::FillContextData(const domi::FftsPlusAicpuCtxDef *aicpu_ctx_def_ptr,
                                         domi::FftsPlusAicpuCtxDef *aicpu_ctx_def) {
  aicpu_ctx_def->set_atm(aicpu_ctx_def_ptr->atm());
  aicpu_ctx_def->set_kernel_type(aicpu_ctx_def_ptr->kernel_type());
  aicpu_ctx_def->set_bm(aicpu_ctx_def_ptr->bm());
  aicpu_ctx_def->set_topic_type(aicpu_ctx_def_ptr->topic_type());
  aicpu_ctx_def->set_thread_id(aicpu_ctx_def_ptr->thread_id());
  aicpu_ctx_def->set_thread_dim(aicpu_ctx_def_ptr->thread_dim());
  aicpu_ctx_def->set_non_tail_block_dim(aicpu_ctx_def_ptr->non_tail_block_dim());
  aicpu_ctx_def->set_tail_block_dim(aicpu_ctx_def_ptr->tail_block_dim());
  aicpu_ctx_def->set_sub_topic_id(aicpu_ctx_def_ptr->sub_topic_id());
  aicpu_ctx_def->set_topic_id(aicpu_ctx_def_ptr->topic_id());
  aicpu_ctx_def->set_group_id(aicpu_ctx_def_ptr->group_id());
  aicpu_ctx_def->set_task_param_offset(aicpu_ctx_def_ptr->task_param_offset());
  domi::aicpuKernelDef *kernel_def = aicpu_ctx_def->mutable_kernel();
  CM_CHECK_NOTNULL(kernel_def);
  kernel_def->set_args_size(aicpu_ctx_def_ptr->kernel().args_size());
  kernel_def->set_args(aicpu_ctx_def_ptr->kernel().args());
  kernel_def->set_so_name(aicpu_ctx_def_ptr->kernel().so_name());
  kernel_def->set_kernel_name(aicpu_ctx_def_ptr->kernel().kernel_name());
  kernel_def->set_kernel_ext_info(aicpu_ctx_def_ptr->kernel().kernel_ext_info());
  kernel_def->set_kernel_ext_info_size(aicpu_ctx_def_ptr->kernel().kernel_ext_info_size());
  return SUCCESS;
}
}  // namespace fe
