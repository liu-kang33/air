/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ffts_plus_task_builder/mix_aic_aiv_dynamic_task_builder.h"
#include <securec.h>
#include <string>
#include "ffts_plus_task_builder/mix_aic_aiv_auto_task_builder.h"
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"
#include "common/sgt_slice_type.h"
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_attr_define.h"
#include "common/ffts_plus_type.h"
#include "common/aicore_util_types.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"

namespace fe {
static const vector<std::string> kMixPrefixs = { "_mix_aic", "_mix_aiv" };
MixAICAIVDynamicTaskBuilder::MixAICAIVDynamicTaskBuilder() {}

MixAICAIVDynamicTaskBuilder::~MixAICAIVDynamicTaskBuilder() {}

Status MixAICAIVDynamicTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("MIX AIC AIV dynamic task generate begin, node:%s.", node->GetName().c_str());
  auto op_desc = node->GetOpDesc();
  vector<FftsPlusComCtx> sub_ffts_plus_context;
  GenFftsPlusTaskCommonInfo(node, sub_ffts_plus_context);
  vector<uint32_t> auto_ctx_id_list;
  (void)ge::AttrUtils::GetListInt(op_desc, kAutoCtxIdList, auto_ctx_id_list);
  if (auto_ctx_id_list.size() != sub_ffts_plus_context.size()) {
    CM_LOGE("MIX CTX size:%zu not equal CTX num:%zu.", auto_ctx_id_list.size(), sub_ffts_plus_context.size());
    return FAILED;
  }
  std::string ai_core_type;
  (void)ge::AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, ai_core_type);
  if (ai_core_type.empty()) {
    return FAILED;
  }
  for (size_t i = 0; i < sub_ffts_plus_context.size(); ++i) {
    domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
    CM_CHECK_NOTNULL(ffts_plus_ctx_def);
    if (ai_core_type == kCoreTypeMixAIC) {
      ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_MIX_AIC);
    } else {
      ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_MIX_AIV);
    }
    ffts_plus_ctx_def->set_op_index(op_desc->GetId());
    ffts_plus_ctx_def->set_context_id(auto_ctx_id_list[i]);
    CM_LOGD("GenContextDef nodetype:%s, name:%s, context_type:%u, op_index:%u", node->GetType().c_str(),
            node->GetName().c_str(), ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());
    domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def = ffts_plus_ctx_def->mutable_mix_aic_aiv_ctx();
    CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);
    Status status = FillContextData(node, mix_aic_aiv_ctx_def);
    if (status != SUCCESS) {
      CM_LOGE("FillContextData failed. Op name:%s.", op_desc->GetName().c_str());
      return status;
    }
    if (i == sub_ffts_plus_context.size() - 1) {
      mix_aic_aiv_ctx_def->set_save_task_addr(1);
    } else {
      mix_aic_aiv_ctx_def->set_save_task_addr(0);
    }
    mix_aic_aiv_ctx_def->set_pred_cnt(sub_ffts_plus_context[i].pred_cnt);
    mix_aic_aiv_ctx_def->set_pred_cnt_init(sub_ffts_plus_context[i].pred_cnt);
    mix_aic_aiv_ctx_def->set_successor_num(0);
    mix_aic_aiv_ctx_def->set_thread_id(i);
    if (MixAICAIVAutoTaskBuilder::AddAdditionalArgs(op_desc, ffts_plus_task_def, 1) != SUCCESS) {
      REPORT_CM_ERROR("AddAdditionalArgs] Add node[%s] additional args failed.", op_desc->GetName().c_str());
      return FAILED;
    }
  }
  return SUCCESS;
}

Status MixAICAIVDynamicTaskBuilder::FillContextData(const ge::NodePtr &node,
                                                    domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def) {
  auto op_desc = node->GetOpDesc();
  vector<uint32_t> task_ratio_list;
  (void)ge::AttrUtils::GetListInt(op_desc, kThreadTaskRadio, task_ratio_list);
  if (task_ratio_list.size() > 1) {
    mix_aic_aiv_ctx_def->set_non_tail_block_ratio_n(task_ratio_list[0]);
    mix_aic_aiv_ctx_def->set_tail_block_ratio_n(task_ratio_list[1]);
  }
  // cache managemet will do at GenerateDataTaskDef()
  mix_aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
  mix_aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);
  mix_aic_aiv_ctx_def->set_aten(kAutoMode);
  mix_aic_aiv_ctx_def->set_atm(kAutoMode);

  // modeInArgsFirstField
  uint32_t mode = 0;
  (void)ge::AttrUtils::GetInt(op_desc, kModeInArgsFirstField, mode);
  // mode == 1 indicates we need reserve 8 Bytes for the args beginning
  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    mix_aic_aiv_ctx_def->add_task_addr(0);
  }
  vector<string> unique_ids;
  string session_graph_id = "";
  if (ge::AttrUtils::GetStr(op_desc, ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id) && !session_graph_id.empty()) {
    unique_ids.push_back(session_graph_id + "_" + op_desc->GetName() + "_0");
  } else {
    unique_ids.push_back(op_desc->GetName() + "_0");
  }
  (void)ge::AttrUtils::SetListStr(op_desc, "_register_stub_func", unique_ids);
  for (auto &prefix : kMixPrefixs) {
    string attr_key_kernel_name = prefix + kThreadKernelName;
    string attr_kernel_name;
    (void)ge::AttrUtils::GetStr(op_desc, attr_key_kernel_name, attr_kernel_name);
    mix_aic_aiv_ctx_def->add_kernel_name(attr_kernel_name);
  }
  CM_LOGD("FillContextData SUCCESS. Op:%s, type:%s.", op_desc->GetName().c_str(), op_desc->GetType().c_str());
  return SUCCESS;
}
}
