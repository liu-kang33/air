/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/mix_aic_aiv_task_builder.h"

#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"


namespace fe {

static const vector<std::string> kMixPrefixs = { "_mix_aic", "_mix_aiv" };

MixAICAIVTaskBuilder::MixAICAIVTaskBuilder() {}

MixAICAIVTaskBuilder::~MixAICAIVTaskBuilder() {}

Status MixAICAIVTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("MixAICAIVTaskBuilder::GenContextDef begin, node name:%s, node type:%s.", node->GetName().c_str(),
          node->GetType().c_str());
  auto op_desc = node->GetOpDesc();
  Status status;
  vector<FftsPlusComCtx> sub_ffts_plus_context;
  GenFftsPlusTaskCommonInfo(node, sub_ffts_plus_context);

  domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  CM_CHECK_NOTNULL(ffts_plus_ctx_def);

  string core_type;
  rtFftsPlusContextType_t ctx_type = RT_CTX_TYPE_MIX_AIC;
  (void)ge::AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, core_type);
  if (core_type == "MIX_AIV") {
    ctx_type = RT_CTX_TYPE_MIX_AIV;
  }
  ffts_plus_ctx_def->set_context_type(ctx_type);
  ffts_plus_ctx_def->set_op_index(op_desc->GetId());
  uint32_t context_id = 0;
  if (ge::AttrUtils::GetInt(op_desc, kContextId, context_id)) {
    ffts_plus_ctx_def->set_context_id(context_id);
  }
  CM_LOGD("GenContextDef nodetype:%s, name:%s, context_type:%u, op_index:%u", node->GetType().c_str(),
          node->GetName().c_str(), ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());
  domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def = ffts_plus_ctx_def->mutable_mix_aic_aiv_ctx();
  CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);

  status = FillContextData(node, mix_aic_aiv_ctx_def);
  if (status != SUCCESS) {
    CM_LOGE("FillContextData failed. Op[%s, optype[%s]]", op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }
  if (sub_ffts_plus_context.empty()) {
    return FAILED;
  }
  mix_aic_aiv_ctx_def->set_pred_cnt(sub_ffts_plus_context[0].pred_cnt);
  mix_aic_aiv_ctx_def->set_pred_cnt_init(sub_ffts_plus_context[0].pred_cnt);
  mix_aic_aiv_ctx_def->set_aten(0);
  mix_aic_aiv_ctx_def->set_successor_num(0);

  (void)ge::AttrUtils::SetListInt(op_desc, kSuccList, sub_ffts_plus_context[0].succ_list);
  uint32_t addr_size = mix_aic_aiv_ctx_def->task_addr_size();
  uint32_t cur_addr_size = ffts_plus_task_def->addr_size();
  ffts_plus_task_def->set_addr_size(cur_addr_size + addr_size);
  CM_LOGD("GenContextDef nodetype:%s, name:%s, total_addr_size:%u", node->GetType().c_str(),
          node->GetName().c_str(), ffts_plus_task_def->addr_size());
  if (AddAdditionalArgs(op_desc, ffts_plus_task_def, 1) != SUCCESS) {
    REPORT_CM_ERROR("[MixAICAIVTaskBuilder] [AddAdditionalArgs] Add node[%s] additional args failed.",
                    op_desc->GetName().c_str());
    return FAILED;
  }

  return SUCCESS;
}

Status MixAICAIVTaskBuilder::FillContextData(const ge::NodePtr &node,
                                             domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def) {
  CM_CHECK_NOTNULL(node);
  CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);
  auto op_desc = node->GetOpDesc();
  Status status = GenContextArgs(node);
  if (status != SUCCESS) {
    REPORT_CM_ERROR("[MixAICAIVTaskBuilder] [FillContextData] failed. Op[%s, optype[%s]]",
                    op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  uint32_t task_ratio;
  (void)ge::AttrUtils::GetInt(op_desc, kTaskRadio, task_ratio);

  mix_aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
  mix_aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);
  mix_aic_aiv_ctx_def->set_ns(1);
  mix_aic_aiv_ctx_def->set_atm(kManualMode);
  mix_aic_aiv_ctx_def->set_thread_dim(1);

  mix_aic_aiv_ctx_def->set_tail_block_ratio_n(task_ratio);
  mix_aic_aiv_ctx_def->set_non_tail_block_ratio_n(task_ratio);

  int32_t block_dim = 0;
  (void)ge::AttrUtils::GetInt(op_desc, ge::TVM_ATTR_NAME_BLOCKDIM, block_dim);
  mix_aic_aiv_ctx_def->set_tail_block_dim(static_cast<uint32_t>(block_dim));
  mix_aic_aiv_ctx_def->set_non_tail_block_dim(static_cast<uint32_t>(block_dim));

  // modeInArgsFirstField
  uint32_t mode = 0;
  (void)ge::AttrUtils::GetInt(op_desc, kModeInArgsFirstField, mode);

  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    uint64_t modeArgs = 0;
    mix_aic_aiv_ctx_def->add_task_addr(modeArgs);
  }
  for (auto input_addr : args_info_.input_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(input_addr));
  }

  for (auto output_addr : args_info_.output_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(output_addr));
  }

  for (auto workspace_addr : args_info_.workspace_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uint64_t>(workspace_addr));
  }
  for (auto &prefix : kMixPrefixs) {
    string attr_key_kernel_name = prefix + op_desc->GetName() + kKernelName;
    string attr_kernel_name;
    (void)ge::AttrUtils::GetStr(op_desc, attr_key_kernel_name, attr_kernel_name);
    mix_aic_aiv_ctx_def->add_kernel_name(attr_kernel_name);
  }
  return SUCCESS;
}

Status MixAICAIVTaskBuilder::AddAdditionalArgs(ge::OpDescPtr &op_desc, domi::FftsPlusTaskDef *ffts_plus_task_def,
                                               const size_t &ctx_num) const {
  CM_CHECK_NOTNULL(op_desc);
  // modeInArgsFirstField
  uint32_t mode = 0;
  (void) ge::AttrUtils::GetInt(op_desc, kModeInArgsFirstField, mode);
  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    uint32_t data_type = 0;
    domi::AdditionalDataDef *additional_data_def = ffts_plus_task_def->add_additional_data();
    CM_CHECK_NOTNULL(additional_data_def);
    uint32_t context_id;
    (void) ge::AttrUtils::GetInt(op_desc, kContextId, context_id);
    additional_data_def->set_data_type(data_type);
    additional_data_def->add_context_id(context_id);
    if (ctx_num > 1) {
      (void) ge::AttrUtils::GetInt(op_desc, "_default_context_id", context_id);
      additional_data_def->add_context_id(context_id);
    }
  }
  return SUCCESS;
}
}  // namespace fe
