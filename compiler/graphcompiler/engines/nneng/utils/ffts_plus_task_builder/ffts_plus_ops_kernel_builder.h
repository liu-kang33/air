/**
 * @file ffts_plus_ops_kernel_builder.h
 *
 * Copyright (c) Huawei Technologies Co., Ltd. 2019-2020. All rights reserved.
 *
 * @brief aicore param calculator and genreate task
 *
 * @version 1.0
 *
 */

#ifndef UTILS_FFTS_PLUS_TASK_BUILDER_FFTS_PLUS_OPS_KERNEL_BUILDER_H_
#define UTILS_FFTS_PLUS_TASK_BUILDER_FFTS_PLUS_OPS_KERNEL_BUILDER_H_

#include "graph_optimizer/graph_optimize_register_error_codes.h"
#include "common/opskernel/ops_kernel_builder.h"
#include "ffts_plus_task_builder/data/data_task_builder.h"
#include "ffts_plus_task_builder/data/prefetch_task_builder.h"
#include "ffts_plus_task_builder/data/out_task_builder.h"
#include "ffts_plus_task_builder/data/cache_persistent_task_builder.h"
#include "ffts_plus_task_builder/mode/ffts_plus_base_mode.h"
#include "ffts_plus_task_builder/mode/ffts_plus_manual_mode.h"
#include "ffts_plus_task_builder/mode/ffts_plus_auto_mode.h"
#include "ffts_plus_task_builder/mode/ffts_plus_dynamic_mode.h"
#include "ffts_plus_task_builder/mode/ffts_plus_mix_l2_mode.h"

namespace fe {
using FFTSPlusBaseModePtr = std::shared_ptr<FFTSPlusBaseMode>;
using FFTSPlusManualModePtr = std::shared_ptr<FFTSPlusManualMode>;
using FFTSPlusAutoModePtr = std::shared_ptr<FFTSPlusAutoMode>;
using FFTSPlusDynamicModePtr = std::shared_ptr<FFTSPlusDynamicMode>;
using FFTSPlusMixL2ModePtr = std::shared_ptr<FFTSPlusMixL2Mode>;

class FFTSPlusOpsKernelBuilder : public ge::OpsKernelBuilder {
 public:
  /**
   * Constructor for AICoreOpsKernelBuilder
   */
  FFTSPlusOpsKernelBuilder();

  /**
   * Deconstruction for AICoreOpsKernelBuilder
   */
  ~FFTSPlusOpsKernelBuilder() override;

  /**
   * Initialization
   * @param options
   * @return Status SUCCESS / FAILED or others
   */
  Status Initialize(const std::map<std::string, std::string> &options) override;

  /**
   * Finalization
   * @return Status SUCCESS / FAILED or others
   */
  Status Finalize() override;

  /**
   * Calculate the running parameters for node
   * @param node node object
   * @return Status SUCCESS / FAILED or others
   */
  Status CalcOpRunningParam(ge::Node &node) override;

  /**
   * Generate task for node
   * @param node node object
   * @param context context object
   * @param tasks Task list
   * @return Status SUCCESS / FAILED or others
   */
  Status GenerateTask(const ge::Node &node, ge::RunContext &context, std::vector<domi::TaskDef> &task_defs) override;

 private:
  Status GenPersistentContext(const ge::Node &node, uint64_t &ready_context_num, uint64_t &total_context_number,
                              domi::TaskDef &task_def);
  FFTSPlusBaseModePtr GetFftsPlusMode(const ge::ComputeGraph &sgt_graph);
  Status GenerateAutoThreadTask();
  Status GenerateManualThreadTask();
  Status WritePrefetchBitmapToFirst64Bytes(domi::FftsPlusTaskDef *ffts_plus_task_def);
  Status GenSubGraphSqeDef(domi::TaskDef &task_def,
                           const uint64_t &ready_context_num) const;
                         
 private:
  FFTSPlusManualModePtr ffts_plus_manual_mode_ptr_;
  FFTSPlusAutoModePtr ffts_plus_auto_mode_ptr_;
  FFTSPlusDynamicModePtr ffts_plus_dynamic_mode_ptr_;
  FFTSPlusMixL2ModePtr ffts_plus_mix_l2_mode_ptr_;
};
}  // namespace fe
#endif  // FUSION_ENGINE_UTILS_OPS_KERNEL_BUILDER_AICORE_OPS_KERNEL_BUILDER_H_