/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/mix_l2_aic_aiv_task_builder.h"

#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/comm_error_codes.h"
#include "common/common_utils.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_types.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "common/aicore_util_attr_define.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"

namespace fe {
MixL2AICAIVTaskBuilder::MixL2AICAIVTaskBuilder() {}
MixL2AICAIVTaskBuilder::~MixL2AICAIVTaskBuilder() {}

Status MixL2AICAIVTaskBuilder::FillContextData(const ge::NodePtr &node,
                                               const domi::FftsPlusMixAicAivCtxDef *src_ctx_def,
                                               domi::FftsPlusMixAicAivCtxDef *dst_ctx_def) {
  dst_ctx_def->set_atm(src_ctx_def->atm());
  dst_ctx_def->set_prefetch_once_bitmap(0);
  dst_ctx_def->set_prefetch_enable_bitmap(0);
  dst_ctx_def->set_ns(src_ctx_def->ns());
  dst_ctx_def->set_atm(src_ctx_def->atm());
  dst_ctx_def->set_thread_dim(src_ctx_def->thread_dim());
  dst_ctx_def->set_tail_block_ratio_n(src_ctx_def->tail_block_ratio_n());
  dst_ctx_def->set_non_tail_block_ratio_n(src_ctx_def->non_tail_block_ratio_n());
  dst_ctx_def->set_tail_block_dim(src_ctx_def->tail_block_dim());
  dst_ctx_def->set_non_tail_block_dim(src_ctx_def->non_tail_block_dim());
  uint32_t mode = 0;
  (void) ge::AttrUtils::GetInt(node->GetOpDesc(), kModeInArgsFirstField, mode);
  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    dst_ctx_def->add_task_addr(0);
  }
  for (int i = 0; i < src_ctx_def->task_addr_size(); ++i) {
    dst_ctx_def->add_task_addr(src_ctx_def->task_addr(i));
  }
  for (int i = 0; i < src_ctx_def->kernel_name_size(); ++i) {
    dst_ctx_def->add_kernel_name(src_ctx_def->kernel_name(i));
  }
  return SUCCESS;
}

Status MixL2AICAIVTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("MixL2AICAIVTaskBuilder::GenContextDef begin, node name:%s, node type:%s.", node->GetName().c_str(),
          node->GetType().c_str());
  auto op_desc = node->GetOpDesc();
  Status status;
  FftsPlusCtxDefPtr ctx_def_ptr = nullptr;
  ctx_def_ptr = op_desc->TryGetExtAttr(kMixL2CtxDef, ctx_def_ptr);
  CM_CHECK_NOTNULL(ctx_def_ptr);
  domi::FftsPlusMixAicAivCtxDef *src_ctx_def_ptr = ctx_def_ptr->mutable_mix_aic_aiv_ctx();
  CM_CHECK_NOTNULL(src_ctx_def_ptr);
  domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  CM_CHECK_NOTNULL(ffts_plus_ctx_def);

  string mix_core_type;
  rtFftsPlusContextType_t ctx_type = RT_CTX_TYPE_MIX_AIC;
  (void)ge::AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, mix_core_type);
  if (mix_core_type == "MIX_AIV") {
    ctx_type = RT_CTX_TYPE_MIX_AIV;
  }
  ffts_plus_ctx_def->set_op_index(op_desc->GetId());
  ffts_plus_ctx_def->set_context_type(ctx_type);
  uint32_t context_id = 0;
  if (ge::AttrUtils::GetInt(op_desc, kContextId, context_id)) {
    ffts_plus_ctx_def->set_context_id(context_id);
  }
  CM_LOGD("Gen Mix nodetype:%s, name:%s, context_type:%u, op_index:%u", node->GetType().c_str(),
          node->GetName().c_str(), ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());
  domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def = ffts_plus_ctx_def->mutable_mix_aic_aiv_ctx();
  CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);

  status = FillContextData(node, src_ctx_def_ptr, mix_aic_aiv_ctx_def);
  if (status != SUCCESS) {
    CM_LOGE("FillContextData failed. Op[%s, optype[%s]]", op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }
  mix_aic_aiv_ctx_def->set_pred_cnt(0);
  mix_aic_aiv_ctx_def->set_pred_cnt_init(0);
  mix_aic_aiv_ctx_def->set_aten(0);
  mix_aic_aiv_ctx_def->set_successor_num(0);

  uint32_t addr_size = mix_aic_aiv_ctx_def->task_addr_size();
  uint32_t cur_addr_size = ffts_plus_task_def->addr_size();
  ffts_plus_task_def->set_addr_size(cur_addr_size + addr_size);
  CM_LOGD("GenContextDef nodetype:%s, name:%s, total_addr_size:%u", node->GetType().c_str(),
          node->GetName().c_str(), ffts_plus_task_def->addr_size());
  if (AddAdditionalArgs(op_desc, ffts_plus_task_def, 1, mix_aic_aiv_ctx_def) != SUCCESS) {
    REPORT_CM_ERROR("[MixL2AICAIVTaskBuilder] [AddAdditionalArgs] Add additional args failed.");
    return FAILED;
  }
  return SUCCESS;
}

Status MixL2AICAIVTaskBuilder::AddAdditionalArgs(ge::OpDescPtr &op_desc, domi::FftsPlusTaskDef *ffts_plus_task_def,
                                                 const size_t &ctx_num, domi::FftsPlusMixAicAivCtxDef *ctx_def) {
  CM_CHECK_NOTNULL(op_desc);
  // modeInArgsFirstField
  uint32_t mode = 0;
  (void) ge::AttrUtils::GetInt(op_desc, kModeInArgsFirstField, mode);
  CM_LOGD("AddAdditionalArgs mode:%u", mode);
  if (mode == IS_MIX_FIRST_FIELD_MODE) {
    domi::AdditionalDataDef *additional_data_def = ffts_plus_task_def->add_additional_data();
    CM_CHECK_NOTNULL(additional_data_def);
    additional_data_def->set_data_type(0);
    uint32_t context_id;
    (void) ge::AttrUtils::GetInt(op_desc, kContextId, context_id);
    additional_data_def->add_context_id(context_id);
    if (ctx_num > 1) {
      (void) ge::AttrUtils::GetInt(op_desc, "_default_context_id", context_id);
      additional_data_def->add_context_id(context_id);
    }
  }
  return SUCCESS;
}
}  // namespace fe
