/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/aic_aiv_task_builder.h"
#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"

namespace fe {
AICAIVTaskBuilder::AICAIVTaskBuilder() {}

AICAIVTaskBuilder::~AICAIVTaskBuilder() {}

Status AICAIVTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("AIC AIV task builder genContextDef begin, node name:%s, node type:%s.", node->GetName().c_str(),
          node->GetType().c_str());
  auto op_desc = node->GetOpDesc();
  Status status;
  vector<FftsPlusComCtx> sub_ffts_plus_context;
  GenFftsPlusTaskCommonInfo(node, sub_ffts_plus_context);

  domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  CM_CHECK_NOTNULL(ffts_plus_ctx_def);
  std::string core_type;
  (void)ge::AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, core_type);
  if (core_type.empty()) {
    return FAILED;
  }
  if (core_type == kCoreTypeAIC) {
    ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_AICORE);
  } else {
    ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_AIV);
  }
  ffts_plus_ctx_def->set_op_index(op_desc->GetId());

  uint32_t context_id = 0;
  if (ge::AttrUtils::GetInt(op_desc, kContextId, context_id)) {
    ffts_plus_ctx_def->set_context_id(context_id);
  }
  CM_LOGD("GenContextDef nodetype:%s, name:%s, context_type:%u, op_index:%u", node->GetType().c_str(), node->GetName().c_str(),
          ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());
  domi::FftsPlusAicAivCtxDef *aic_aiv_ctx_def = ffts_plus_ctx_def->mutable_aic_aiv_ctx();
  CM_CHECK_NOTNULL(aic_aiv_ctx_def);
  status = FillContextData(node, aic_aiv_ctx_def);
  if (status != SUCCESS) {
    CM_LOGE("FillContextData failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }
  if (sub_ffts_plus_context.empty()) {
    return FAILED;
  }
  aic_aiv_ctx_def->set_pred_cnt(sub_ffts_plus_context[0].pred_cnt);
  aic_aiv_ctx_def->set_pred_cnt_init(sub_ffts_plus_context[0].pred_cnt);
  aic_aiv_ctx_def->set_aten(0);
  aic_aiv_ctx_def->set_successor_num(0);

  (void)ge::AttrUtils::SetListInt(op_desc, kSuccList, sub_ffts_plus_context[0].succ_list);

  uint32_t addr_size = aic_aiv_ctx_def->task_addr_size();
  uint32_t cur_addr_size = ffts_plus_task_def->addr_size();
  ffts_plus_task_def->set_addr_size(cur_addr_size + addr_size);
  CM_LOGD("GenContextDef nodetype:%s, name:%s, total_addr_size:%u", node->GetType().c_str(),
          node->GetName().c_str(), ffts_plus_task_def->addr_size());
  return SUCCESS;
}

Status AICAIVTaskBuilder::FillContextData(const ge::NodePtr &node, domi::FftsPlusAicAivCtxDef *aic_aiv_ctx_def) {
  auto op_desc = node->GetOpDesc();
  Status status = GenContextArgs(node);
  if (status != SUCCESS) {
    CM_LOGE("GenContextArgs failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  // cache managemet will do at GenerateDataTaskDef()
  aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
  aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);
  aic_aiv_ctx_def->set_atm(kManualMode);
  aic_aiv_ctx_def->set_thread_dim(1);

  int32_t block_dim = 0;
  (void)ge::AttrUtils::GetInt(op_desc, ge::TVM_ATTR_NAME_BLOCKDIM, block_dim);
  aic_aiv_ctx_def->set_tail_block_dim(static_cast<uint32_t>(block_dim));
  aic_aiv_ctx_def->set_non_tail_block_dim(static_cast<uint32_t>(block_dim));

  for (auto input_addr : args_info_.input_addrs) {
    uint64_t input_addr_tmp = reinterpret_cast<uint64_t>(input_addr);
    aic_aiv_ctx_def->add_task_addr(input_addr_tmp);
    CM_LOGD("input_addr, %lu", input_addr_tmp);
  }

  for (auto output_addr : args_info_.output_addrs) {
    uint64_t output_addr_tmp = reinterpret_cast<uint64_t>(output_addr);
    aic_aiv_ctx_def->add_task_addr(output_addr_tmp);
    CM_LOGD("output_addr, %lu", output_addr_tmp);
  }

  for (auto workspace_addr : args_info_.workspace_addrs) {
    uint64_t workspace_addr_tmp = reinterpret_cast<uint64_t>(workspace_addr);
    aic_aiv_ctx_def->add_task_addr(workspace_addr_tmp);
    CM_LOGD("workspace_addr, %lu", workspace_addr_tmp);
  }
  string attr_key_kernel_name = op_desc->GetName() + kKernelName;
  string attr_kernel_name;
  (void)ge::AttrUtils::GetStr(op_desc, attr_key_kernel_name, attr_kernel_name);
  aic_aiv_ctx_def->add_kernel_name(attr_kernel_name);

  CM_LOGD("aic_aiv_ctx_def FillContextData SUCCESS. Op:%s, optype:%s, block_dim:%u, size:%u, attr_kernel_name:%s",
          op_desc->GetName().c_str(), op_desc->GetType().c_str(), static_cast<uint32_t>(block_dim),
          aic_aiv_ctx_def->task_addr_size(), attr_kernel_name.c_str());
  return SUCCESS;
}
}  // namespace fe
