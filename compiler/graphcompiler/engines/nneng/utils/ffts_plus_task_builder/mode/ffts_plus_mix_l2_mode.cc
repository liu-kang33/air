/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language gov erning permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/mode/ffts_plus_mix_l2_mode.h"
#include "ffts_plus_task_builder/mix_l2_aic_aiv_task_builder.h"
#include "common/fe_log.h"
#include "common/string_utils.h"
#include "common/aicore_util_attr_define.h"
#include "common/fe_type_utils.h"


namespace fe {
FFTSPlusMixL2Mode::FFTSPlusMixL2Mode() {}
FFTSPlusMixL2Mode::~FFTSPlusMixL2Mode() {}

Status FFTSPlusMixL2Mode::Initialize() {
  mode_type_ = ModeType::MIX_L2_MODE_TYPE;
  CM_MAKE_SHARED(mix_l2_aic_aiv_task_builder_ptr_ = std::make_shared<MixL2AICAIVTaskBuilder>(), return FAILED);
  return SUCCESS;
}

Status FFTSPlusMixL2Mode::GenFftsPlusContextId(ge::ComputeGraph &sgt_graph,
                                               std::vector<ge::NodePtr> &sub_graph_nodes,
                                               uint64_t &ready_context_num, uint64_t &total_context_number){
  (void)sgt_graph;
  if (sub_graph_nodes.empty()) {
    REPORT_CM_ERROR("[FFTSPlusMixL2Mode] [GenFftsPlusContextId] No node to generate task.");
    return FAILED;
  }
  ge::NodePtr node = sub_graph_nodes[0];
  uint32_t contextId = total_context_number;
  ge::OpDescPtr op_desc = node->GetOpDesc();
  FE_LOGD("GenFftsPlusContextId nodetype:%s, name:%s", op_desc->GetType().c_str(),
          op_desc->GetName().c_str());
  (void)ge::AttrUtils::SetInt(op_desc, kContextId, contextId++);
  ready_context_num = contextId;
  total_context_number = contextId;
  return SUCCESS;
}

Status FFTSPlusMixL2Mode::GenSubGraphTaskDef(std::vector<ge::NodePtr> &sub_graph_nodes, const ge::RunContext &context,
                                             domi::TaskDef &task_def){
  if (sub_graph_nodes.empty()) {
    REPORT_CM_ERROR("[FFTSPlusMixL2Mode] [GenSubGraphTaskDef] No node to generate taskdef.");
    return FAILED;
  }
  ge::NodePtr node = sub_graph_nodes[0];
  FE_LOGD("GenSubGraphTaskDef name:%s", node->GetOpDesc()->GetName().c_str());
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  FE_CHECK_NOTNULL(ffts_plus_task_def);

  TaskBuilderType task_builder_type = TaskBuilderType::EN_TASK_TYPE_MIX_L2_AIC_AIV;
  FFTSPlusTaskBuilderPtr task_builder = GetTaskBuilder(task_builder_type);
  FE_CHECK_NOTNULL(task_builder);
  Status status = task_builder->GenerateTaskDef(node, context, ffts_plus_task_def);
  return status;
}
}  // namespace fe
