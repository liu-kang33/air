/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/mode/ffts_plus_dynamic_mode.h"
#include "common/fe_log.h"
#include "common/string_utils.h"
#include "common/aicore_util_attr_define.h"


namespace fe {
FFTSPlusDynamicMode::FFTSPlusDynamicMode() {}
FFTSPlusDynamicMode::~FFTSPlusDynamicMode() {}

Status FFTSPlusDynamicMode::Initialize() {
  // using auto mode with dynamic modememory_slice.cc
  CM_MAKE_SHARED(auto_mode_ptr_ = std::make_shared<FFTSPlusAutoMode>(ModeType::DYNAMIC_MODE_TYPE), return FAILED);
  auto_mode_ptr_->Initialize();
  return SUCCESS;
}

Status FFTSPlusDynamicMode::GenFftsPlusContextId(ge::ComputeGraph &sgt_graph,
                                                 std::vector<ge::NodePtr> &sub_graph_nodes,
                                                 uint64_t &ready_context_num,
                                                 uint64_t &total_context_number) {
  FE_LOGD("Ffts+ dynamic node generate contextId.");
  Status status = auto_mode_ptr_->GenFftsPlusContextId(sgt_graph, sub_graph_nodes, ready_context_num,
                                                       total_context_number);
  return status;
}

Status FFTSPlusDynamicMode::GenSubGraphTaskDef(std::vector<ge::NodePtr> &sub_graph_nodes,
                                               const ge::RunContext &context, domi::TaskDef &task_def) {
  FE_LOGD("Ffts+ dynamic node generate task.");
  return auto_mode_ptr_->GenSubGraphTaskDef(sub_graph_nodes, context, task_def);
}
}  // namespace fe
