/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_MODE_FFTS_PLUS_DYNAMIC_MODE_H_
#define AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_MODE_FFTS_PLUS_DYNAMIC_MODE_H_
#include "ffts_plus_task_builder/mode/ffts_plus_base_mode.h"
#include "ffts_plus_task_builder/mode/ffts_plus_auto_mode.h"
#include "common/opskernel/ops_kernel_builder.h"

namespace fe {
using FFTSPlusAutoModePtr = std::shared_ptr<FFTSPlusAutoMode>;

class FFTSPlusDynamicMode : public FFTSPlusBaseMode {
 public:
    FFTSPlusDynamicMode();
    ~FFTSPlusDynamicMode() override;

    Status Initialize() override;

    Status GenFftsPlusContextId(ge::ComputeGraph &sgt_graph, std::vector<ge::NodePtr> &sub_graph_nodes,
                                uint64_t &ready_context_num, uint64_t &total_context_number) override;

    Status GenSubGraphTaskDef(std::vector<ge::NodePtr> &sub_graph_nodes, const ge::RunContext &context,
                              domi::TaskDef &task_def) override;
    FFTSPlusAutoModePtr auto_mode_ptr_;
};
}  // namespace fe
#endif  // AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_MODE_FFTS_PLUS_DYNAMIC_MODE_H_