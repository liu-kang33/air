/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_MODE_FFTS_PLUS_BASE_MODE_H_
#define AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_MODE_FFTS_PLUS_BASE_MODE_H_

#include <runtime/rt.h>
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"
#include "ffts_plus_task_builder/aic_aiv_task_builder.h"
#include "ffts_plus_task_builder/aic_aiv_auto_task_builder.h"
#include "ffts_plus_task_builder/aic_aiv_dynamic_task_builder.h"
#include "ffts_plus_task_builder/mix_aic_aiv_task_builder.h"
#include "ffts_plus_task_builder/mix_aic_aiv_auto_task_builder.h"
#include "ffts_plus_task_builder/mix_aic_aiv_dynamic_task_builder.h"
#include "ffts_plus_task_builder/mix_l2_aic_aiv_task_builder.h"
#include "ffts_plus_task_builder/collection_ops_task_builder.h"
#include "ffts_plus_task_builder/runtime_ops_task_builder.h"
#include "ffts_plus_task_builder/aicpu_task_builder.h"
#include "ffts_plus_task_builder/aicpu_auto_task_builder.h"
#include "ffts_plus_task_builder/data/data_task_builder.h"
#include "ffts_plus_task_builder/data/prefetch_task_builder.h"
#include "ffts_plus_task_builder/data/out_task_builder.h"
#include "ffts_plus_task_builder/data/prefetch_auto_task_builder.h"
#include "ffts_plus_task_builder/data/out_auto_task_builder.h"
#include "ffts_plus_task_builder/data/prefetch_dynamic_task_builder.h"
#include "ffts_plus_task_builder/data/out_dynamic_task_builder.h"
#include "graph_optimizer/graph_optimize_register_error_codes.h"
#include "common/opskernel/ops_kernel_builder.h"
#include "graph/compute_graph.h"

namespace fe {

using FFTSPlusTaskBuilderPtr = std::shared_ptr<FFTSPlusTaskBuilder>;
using AICAIVTaskBuilderPtr = std::shared_ptr<AICAIVTaskBuilder>;
using AICAIVAutoTaskBuilderPtr = std::shared_ptr<AICAIVAutoTaskBuilder>;
using AICAIVDynamicTaskBuilderPtr = std::shared_ptr<AICAIVDynamicTaskBuilder>;
using MixAICAIVTaskBuilderPtr = std::shared_ptr<MixAICAIVTaskBuilder>;
using MixAICAIVAutoTaskBuilderPtr = std::shared_ptr<MixAICAIVAutoTaskBuilder>;
using MixAICAIVTaskDynamicBuilderPtr = std::shared_ptr<MixAICAIVDynamicTaskBuilder>;
using CollectionOpsTaskBuilderPtr = std::shared_ptr<CollectionOpsTaskBuilder>;
using AicpuTaskBuilderPtr = std::shared_ptr<AicpuTaskBuilder>;
using AicpuAutoTaskBuilderPtr = std::shared_ptr<AicpuAutoTaskBuilder>;
using RuntimeOpsTaskBuilderPtr = std::shared_ptr<RuntimeOpsTaskBuilder>;
using MixL2AICAIVTaskBuilderPtr = std::shared_ptr<MixL2AICAIVTaskBuilder>;
enum class ModeType {
  MANUAL_MODE_TYPE = 0,
  AUTO_MODE_TYPE,
  DYNAMIC_MODE_TYPE,
  MIX_L2_MODE_TYPE
};

enum class TaskBuilderType {
  EN_TASK_TYPE_AIC_AIV = 0,   // ai core op, aic or aiv
  EN_TASK_TYPE_AIC_AIV_AUTO,
  EN_TASK_TYPE_AIC_AIV_DYNAMIC,
  EN_TASK_TYPE_MIX_AIC_AIV,   // mix op, contain aic & aiv
  EN_TASK_TYPE_MIX_AIC_AIV_AUTO,
  EN_TASK_TYPE_MIX_AIC_AIV_DYNAMIC,
  EN_TASK_TYPE_MIX_L2_AIC_AIV,
  EN_TASK_TYPE_COLLECTION_COMMICATE,   // collection ops
  EN_TASK_TYPE_AICPU,                  // aicpu ops
  EN_TASK_TYPE_AICPU_AUTO,
  EN_TASK_TYPE_RUNTIME_CONTROL,   // runtime ops
  EN_TASK_TYPE_RESERVED                 // reserved value
};

class FFTSPlusBaseMode {
 public:
  FFTSPlusBaseMode();
  virtual ~FFTSPlusBaseMode();

  virtual Status Initialize() = 0;

  virtual Status GenFftsPlusContextId(ge::ComputeGraph &sgt_graph, std::vector<ge::NodePtr> &sub_graph_nodes,
                                      uint64_t &ready_context_num, uint64_t &total_context_number) = 0;

  virtual Status GenSubGraphTaskDef(std::vector<ge::NodePtr> &sub_graph_nodes, const ge::RunContext &context,
                                      domi::TaskDef &task_def) = 0;

 protected:
  Status GetAICoreTaskType(bool auto_mode, const std::string &core_type, TaskBuilderType &task_builder_type) const;

  Status GetNodeContextTypeByNode(const ge::NodePtr &node, TaskBuilderType &task_builder_type) const;

  FFTSPlusTaskBuilderPtr GetTaskBuilder(TaskBuilderType task_builder_type);

  Status GenerateDataTaskDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def,
                             const ModeType &mode_type_param) const;

  bool IsNoCtx(const ge::NodePtr &node) const;

  const std::unordered_set<std::string> NO_NEED_GEN_TASK_OP_TYPE = {"Data", "NetOutput", "Variable", "Const",
                                                                    "Constant", "PhonyConcat"};

  ModeType mode_type_{ModeType::MANUAL_MODE_TYPE};

  AICAIVTaskBuilderPtr aic_aiv_task_builder_ptr_;
  AICAIVAutoTaskBuilderPtr aic_aiv_auto_task_builder_ptr_;
  AICAIVDynamicTaskBuilderPtr aic_aiv_dynamic_task_builder_ptr_;
  MixAICAIVTaskBuilderPtr mix_aic_aiv_task_builder_ptr_;
  MixAICAIVAutoTaskBuilderPtr mix_aic_aiv_auto_task_builder_ptr_;
  MixAICAIVTaskDynamicBuilderPtr mix_aic_aiv_dynamic_task_builder_ptr_;
  MixL2AICAIVTaskBuilderPtr mix_l2_aic_aiv_task_builder_ptr_;
  CollectionOpsTaskBuilderPtr collection_ops_task_builder_ptr_;
  AicpuTaskBuilderPtr aicpu_task_builder_ptr_;
  AicpuAutoTaskBuilderPtr aicpu_auto_task_builder_ptr_;
  RuntimeOpsTaskBuilderPtr runtime_ops_task_builder_ptr_;
};
}  // namespace fe
#endif  // AIR_COMPILER_GRAPHCOMPILER_ENGINES_NNENG_UTILS_FFTS_PLUS_TASK_BUILDER_MODE_FFTS_PLUS_BASE_MODE_H_