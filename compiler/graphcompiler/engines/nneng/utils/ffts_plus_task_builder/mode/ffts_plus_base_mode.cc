/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/mode/ffts_plus_base_mode.h"
#include "graph/debug/ge_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "common/aicore_util_attr_define.h"
#include "common/aicore_util_constants.h"

namespace fe {
FFTSPlusBaseMode::FFTSPlusBaseMode() {}

FFTSPlusBaseMode::~FFTSPlusBaseMode() {}

Status FFTSPlusBaseMode::GetAICoreTaskType(bool auto_mode, const std::string &core_type,
                                           TaskBuilderType &task_builder_type) const
{
  bool core_type_aic_aiv = core_type == kCoreTypeAIC || core_type == kCoreTypeAIV;
  bool core_type_mix_aic_aiv = core_type == kCoreTypeMixAIC || core_type == kCoreTypeMixAIV;
  if (core_type_aic_aiv) {
    task_builder_type = auto_mode ? TaskBuilderType::EN_TASK_TYPE_AIC_AIV_AUTO : TaskBuilderType::EN_TASK_TYPE_AIC_AIV;
    if (mode_type_ == ModeType::DYNAMIC_MODE_TYPE) {
      task_builder_type = TaskBuilderType::EN_TASK_TYPE_AIC_AIV_DYNAMIC;
    }
  } else if (core_type_mix_aic_aiv) {
    task_builder_type = auto_mode ? TaskBuilderType::EN_TASK_TYPE_MIX_AIC_AIV_AUTO :
                                  TaskBuilderType::EN_TASK_TYPE_MIX_AIC_AIV;
    if (mode_type_ == ModeType::DYNAMIC_MODE_TYPE) {
      task_builder_type = TaskBuilderType::EN_TASK_TYPE_MIX_AIC_AIV_DYNAMIC;
    }
  } else {
    return FAILED;
  }
  return SUCCESS;
}

Status FFTSPlusBaseMode::GetNodeContextTypeByNode(const ge::NodePtr &node,
                                                  TaskBuilderType &task_builder_type) const {
  ge::OpDescPtr op_desc = node->GetOpDesc();
  int64_t ge_impl_type = static_cast<int>(domi::ImplyType::INVALID);
  (void)ge::AttrUtils::GetInt(op_desc, ge::ATTR_NAME_IMPLY_TYPE, ge_impl_type);
  FE_LOGD("GetNodeContextTypeByNode ge_impl_type:%ld, nodetype:%s, name:%s", ge_impl_type, node->GetType().c_str(),
          node->GetName().c_str());

  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
  slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
  bool auto_mode = OpIsAutoThread(slice_info_ptr);
  bool flag_ge_impl_type = (ge_impl_type == static_cast<int>(domi::ImplyType::BUILDIN) ||
                            ge_impl_type == static_cast<int>(domi::ImplyType::TVM));
  if (flag_ge_impl_type) {
    std::string core_type;
    if (auto_mode) {
      // auto
      vector<string> thread_core_type;
      (void)ge::AttrUtils::GetListStr(op_desc, ATTR_NAME_THREAD_CUBE_VECTOR_CORE_TYPE, thread_core_type);
      core_type = thread_core_type.empty() ? core_type : thread_core_type[0];
    } else {
      // manual
      (void)ge::AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, core_type);
    }
    FE_LOGD("core_type: %s.", core_type.c_str());

    if (core_type.size() == 0) {
      return FAILED;
    }
    return GetAICoreTaskType(auto_mode, core_type, task_builder_type);
  }

  if (kHCCLOpType.count(op_desc->GetType()) > 0) {
    task_builder_type = TaskBuilderType::EN_TASK_TYPE_COLLECTION_COMMICATE;
    return SUCCESS;
  }

  FftsPlusCtxDefPtr ctx_def_ptr = nullptr;
  ctx_def_ptr = op_desc->TryGetExtAttr("_ffts_plus_aicpu_ctx_def", ctx_def_ptr);
  if (ctx_def_ptr != nullptr) {
    task_builder_type = auto_mode ? TaskBuilderType::EN_TASK_TYPE_AICPU_AUTO : TaskBuilderType::EN_TASK_TYPE_AICPU;
    return SUCCESS;
  }

  FftsPlusCtxDefPtr runtime_control_ctx_def =  nullptr;
  runtime_control_ctx_def = op_desc->TryGetExtAttr("FFTS_PLUS_TASK_DEF", runtime_control_ctx_def);
  if (runtime_control_ctx_def != nullptr) {
    task_builder_type = TaskBuilderType::EN_TASK_TYPE_RUNTIME_CONTROL;
    return SUCCESS;
  }

  return FAILED;
}

FFTSPlusTaskBuilderPtr FFTSPlusBaseMode::GetTaskBuilder(TaskBuilderType task_builder_type) {
  switch (task_builder_type) {
    case TaskBuilderType::EN_TASK_TYPE_AIC_AIV:
      return aic_aiv_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_AIC_AIV_AUTO:
     return aic_aiv_auto_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_AIC_AIV_DYNAMIC:
      return aic_aiv_dynamic_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_MIX_AIC_AIV:
      return mix_aic_aiv_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_MIX_AIC_AIV_AUTO:
      return mix_aic_aiv_auto_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_MIX_AIC_AIV_DYNAMIC:
      return mix_aic_aiv_dynamic_task_builder_ptr_;
	case TaskBuilderType::EN_TASK_TYPE_MIX_L2_AIC_AIV:
      return mix_l2_aic_aiv_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_COLLECTION_COMMICATE:
      return collection_ops_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_AICPU:
      return aicpu_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_AICPU_AUTO:
      return aicpu_auto_task_builder_ptr_;
    case TaskBuilderType::EN_TASK_TYPE_RUNTIME_CONTROL:
      return runtime_ops_task_builder_ptr_;
    default:
      return nullptr;
  }
}

Status FFTSPlusBaseMode::GenerateDataTaskDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def,
                                             const ModeType &mode_type_param) const {
  FE_LOGD("Current ffts plus mode type is : %d.", static_cast<int>(mode_type_param));
  if (mode_type_param == ModeType::MANUAL_MODE_TYPE) {
    PrefetchTaskBuilder prefetch;
    OutTaskBuilder invalid(CACHE_OPERATION::INVALIDATE);
    OutTaskBuilder write_back(CACHE_OPERATION::WRITE_BACK);
    if (prefetch.GenManualDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
    if (invalid.GenManualDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
    if (write_back.GenManualDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
  } else if (mode_type_param == ModeType::AUTO_MODE_TYPE) {
    PrefetchAutoTaskBuilder prefetch_auto;
    OutAutoTaskBuilder invalid_auto(CACHE_OPERATION::INVALIDATE);
    OutAutoTaskBuilder write_back_auto(CACHE_OPERATION::WRITE_BACK);
    if (prefetch_auto.GenAutoDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
    if (invalid_auto.GenAutoDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
    if (write_back_auto.GenAutoDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
  } else if (mode_type_param == ModeType::DYNAMIC_MODE_TYPE) {
    PrefetchDynamicTaskBuilder prefetch_dyn;
    OutDynamicTaskBuilder invalid_dyn(CACHE_OPERATION::INVALIDATE);
    OutDynamicTaskBuilder write_back_dyn(CACHE_OPERATION::WRITE_BACK);
    if (prefetch_dyn.GenDynamicDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
    if (invalid_dyn.GenDynamicDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
    if (write_back_dyn.GenDynamicDataCtxDef(node, ffts_plus_task_def) != SUCCESS) {
      return FAILED;
    }
  }
  return SUCCESS;
}

bool FFTSPlusBaseMode::IsNoCtx(const ge::NodePtr &node) const {
  ge::OpDescPtr op_desc = node->GetOpDesc();
  if (NO_NEED_GEN_TASK_OP_TYPE.count(op_desc->GetType()) != 0) {
    return true;
  }
  bool no_task = false;
  (void)ge::AttrUtils::GetBool(op_desc, ge::ATTR_NAME_NOTASK, no_task);
  if (no_task) {
    return true;
  }
  return false;
}

}  // namespace fe