/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_plus_task_builder/aic_aiv_dynamic_task_builder.h"
#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/op_tensor_utils.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "common/sgt_slice_type.h"
#include "common/ffts_plus_type.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"
#include "runtime/rt_model.h"
#include "runtime/mem.h"
#include "ffts_plus_task_builder/ffts_plus_task_builder.h"

namespace fe {
AICAIVDynamicTaskBuilder::AICAIVDynamicTaskBuilder() {}

AICAIVDynamicTaskBuilder::~AICAIVDynamicTaskBuilder() {}

Status AICAIVDynamicTaskBuilder::GenContextDef(const ge::NodePtr &node, domi::FftsPlusTaskDef *ffts_plus_task_def) {
  CM_LOGD("AIC AIV dynamic task generate begin, node:%s, type:%s.", node->GetName().c_str(), node->GetType().c_str());
  auto op_desc = node->GetOpDesc();
  Status status;
  vector<FftsPlusComCtx> sub_ffts_plus_context;
  GenFftsPlusTaskCommonInfo(node, sub_ffts_plus_context);
  vector<uint32_t> auto_ctx_id_list;
  (void)ge::AttrUtils::GetListInt(op_desc, kAutoCtxIdList, auto_ctx_id_list);
  if (auto_ctx_id_list.size() != sub_ffts_plus_context.size()) {
    CM_LOGE("AICAIV Context Id size:%zu not equal context num:%zu.", auto_ctx_id_list.size(),
            sub_ffts_plus_context.size());
    return FAILED;
  }
  std::string core_type;
  (void)ge::AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, core_type);
  if (core_type.empty()) {
    return FAILED;
  }
  for (size_t i = 0; i < sub_ffts_plus_context.size(); ++i) {
    domi::FftsPlusCtxDef *ffts_plus_ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
    CM_CHECK_NOTNULL(ffts_plus_ctx_def);
    if (core_type == kCoreTypeAIC) {
      ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_AICORE);
    } else {
      ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_AIV);
    }
    ffts_plus_ctx_def->set_op_index(op_desc->GetId());
    ffts_plus_ctx_def->set_context_id(auto_ctx_id_list[i]);
    CM_LOGD("GenContextDef nodetype:%s, name:%s, context_type:%u, op_index:%u", node->GetType().c_str(),
            node->GetName().c_str(), ffts_plus_ctx_def->context_type(), ffts_plus_ctx_def->op_index());
    domi::FftsPlusAicAivCtxDef *aic_aiv_ctx_def = ffts_plus_ctx_def->mutable_aic_aiv_ctx();
    CM_CHECK_NOTNULL(aic_aiv_ctx_def);
    status = FillContextData(node, aic_aiv_ctx_def);
    if (status != SUCCESS) {
      CM_LOGE("Fill AiCore ContextData failed. Op name:%s.", op_desc->GetName().c_str());
      return status;
    }
    if (i == sub_ffts_plus_context.size() - 1) {
      aic_aiv_ctx_def->set_save_task_addr(1);
    } else {
      aic_aiv_ctx_def->set_save_task_addr(0);
    }
    aic_aiv_ctx_def->set_pred_cnt(sub_ffts_plus_context[i].pred_cnt);
    aic_aiv_ctx_def->set_pred_cnt_init(sub_ffts_plus_context[i].pred_cnt);
    aic_aiv_ctx_def->set_successor_num(0);
    aic_aiv_ctx_def->set_thread_id(i);
  }
  CM_LOGD("GenContextDef nodetype:%s, name:%s.", node->GetType().c_str(), node->GetName().c_str());
  return SUCCESS;
}

Status AICAIVDynamicTaskBuilder::FillContextData(const ge::NodePtr &node, domi::FftsPlusAicAivCtxDef *aic_aiv_ctx_def) {
  auto op_desc = node->GetOpDesc();
  // cache managemet will do at GenerateDataTaskDef()
  aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
  aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);
  aic_aiv_ctx_def->set_aten(kAutoMode);
  aic_aiv_ctx_def->set_atm(kAutoMode);

  // generate _register_stub_func
  vector<string> unique_ids;
  string session_graph_id = "";
  if (ge::AttrUtils::GetStr(op_desc, ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id) && !session_graph_id.empty()) {
    unique_ids.push_back(session_graph_id + "_" + op_desc->GetName() + "_0");
  } else {
    unique_ids.push_back(op_desc->GetName() + "_0");
  }
  (void)ge::AttrUtils::SetListStr(op_desc, "_register_stub_func", unique_ids);

  CM_LOGD("FillContextData SUCCESS. Op:%s, type:%s.", op_desc->GetName().c_str(), op_desc->GetType().c_str());
  return SUCCESS;
}
}  // namespace fe
