/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ffts_task_builder.h"
#include <runtime/rt.h>
#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "common/fe_type_utils.h"
#include "adapter/factory/task_builder_adapter_factory.h"
#include "ffts_task_builder_adapter.h"
#include "graph/utils/node_utils.h"
#include "graph/utils/graph_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/compute_graph.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"

namespace fe {

FftsTaskBuilder::FftsTaskBuilder() {
    (void)memset_s(&task_info_, sizeof(task_info_), 0, sizeof(task_info_));
}

FftsTaskBuilder::~FftsTaskBuilder() {}

bool IsNotSubGraphDataAndNetOutput(const ge::OpDescPtr &op_desc_ptr) {
  return !IsSubGraphData(op_desc_ptr) && !IsSubGraphNetOutput(op_desc_ptr);
}

void DebugFftsDesc(const rtFftsDescInfo_t &ffts_desc) {
  string debug_str = "\n === fftsDesc Begin ===";
  debug_str += "\n tm: " + std::to_string(ffts_desc.tm);
  debug_str += "\n di: " + std::to_string(ffts_desc.di);
  debug_str += "\n dw: " + std::to_string(ffts_desc.dw);
  debug_str += "\n df: " + std::to_string(ffts_desc.df);
  debug_str += "\n dataSplitUnit: " + std::to_string(ffts_desc.dataSplitUnit);
  debug_str += "\n prefetchOstNum: " + std::to_string(ffts_desc.prefetchOstNum);
  debug_str += "\n cacheMaintainOstNum: " + std::to_string(ffts_desc.cacheMaintainOstNum);
  debug_str += "\n aicPrefetchUpper: " + std::to_string(ffts_desc.aicPrefetchUpper);
  debug_str += "\n aicPrefetchLower: " + std::to_string(ffts_desc.aicPrefetchLower);
  debug_str += "\n aivPrefetchUpper: " + std::to_string(ffts_desc.aivPrefetchUpper);
  debug_str += "\n aivPrefetchLower: " + std::to_string(ffts_desc.aivPrefetchLower);
  debug_str += "\n === fftsDesc End ===";
  CM_LOGD("%s.", debug_str.c_str());
}

void DebugSrcPrefetch(const rtAutoThreadPrefetch_t &info) {
  string debug_str = "\n === tagAutoThreadPrefetch Begin ===";
  debug_str += "\n dataAddr: " + std::to_string(info.dataAddr);
  debug_str += "\n dataAddrOffset: " + std::to_string(info.dataAddrOffset);
  debug_str += "\n nonTailDataLen: " + std::to_string(info.nonTailDataLen);
  debug_str += "\n tailDataLen: " + std::to_string(info.tailDataLen);
  debug_str += "\n === tagAutoThreadPrefetch End ===";
  CM_LOGD("%s.", debug_str.c_str());
}
void DebugAutoThreadAicAiv(const rtAutoThreadAicAivInfo_t &info) {
  string debug_str = "\n === autoThreadAicAiv Begin ===";
  debug_str += "\n taskParamAddr: " + std::to_string(info.taskParamAddr);
  debug_str += "\n taskParamOffset: " + std::to_string(info.taskParamOffset);
  debug_str += "\n satMode: " + std::to_string(info.satMode);
  debug_str += "\n scheduleMode: " + std::to_string(info.scheduleMode);
  debug_str += "\n iCachePrefetchCnt: " + std::to_string(info.iCachePrefetchCnt);
  debug_str += "\n prefetchEnableBitmap: " + std::to_string(info.prefetchEnableBitmap);
  debug_str += "\n prefetchOnceBitmap: " + std::to_string(info.prefetchOnceBitmap);
  debug_str += "\n tailBlkDim: " + std::to_string(info.tailBlkDim);
  CM_LOGD("%s.", debug_str.c_str());

  for (uint32_t index = 0; index < RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK; index++) {
    if (info.srcPrefetch[index].dataAddr) {
      CM_LOGD("=== srcPrefetch %d Begin ===", index);
      DebugSrcPrefetch(info.srcPrefetch[index]);
      CM_LOGD("=== srcPrefetch %d End ===", index);
    }
  }

  CM_LOGD("=== autoThreadAicAiv End ===");
}
void DebugSubTask(const rtFftsSubTaskInfo_t &sub_task) {
  string debug_str = "\n === sub_task Begin ===";
  debug_str += "\n subTaskType: " + std::to_string(sub_task.subTaskType);
  debug_str += "\n threadDim: " + std::to_string(sub_task.threadDim);
  debug_str += "\n dstTickCacheVldBitmap: " + std::to_string(sub_task.dstTickCacheVldBitmap);
  debug_str += "\n srcTickCacheVldBitmap: " + std::to_string(sub_task.srcTickCacheVldBitmap);
  debug_str += "\n srcDataOutOfSubGraphBitmap: " + std::to_string(sub_task.srcDataOutOfSubGraphBitmap);
  string cache_id;
  for (uint32_t index = 0; index < RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK; index++) {
    cache_id += std::to_string(sub_task.dstTickCacheID[index]) + " ";
  }
  debug_str += "\n dstTickCacheID: " + cache_id;

  cache_id.clear();
  for (uint32_t index = 0; index < RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK; index++) {
    cache_id += std::to_string(sub_task.srcTickCacheID[index]) + " ";
  }
  debug_str += "\n srcTickCacheID: " + cache_id;
  CM_LOGD("%s.", debug_str.c_str());

  DebugAutoThreadAicAiv(sub_task.custom.autoThreadAicAiv);
  CM_LOGD("=== sub_task End ===");
}

void DebugAutoThreadCache(const rtAutoThreadCacheInfo_t &info) {
  string debug_str = "\n === rtAutoThreadCacheInfo_t Begin ===";
  debug_str += "\n dataAddr: " + std::to_string(info.dataAddr);
  debug_str += "\n dataAddrOffset: " + std::to_string(info.dataAddrOffset);
  debug_str += "\n nonTailDataLen: " + std::to_string(info.nonTailDataLen);
  debug_str += "\n tailDataLen: " + std::to_string(info.tailDataLen);
  debug_str += "\n ticketCacheRefCnt: " + std::to_string(info.ticketCacheRefCnt);
  debug_str += "\n === rtAutoThreadCacheInfo_t End ===";
  CM_LOGD("%s.", debug_str.c_str());
}
void DebugTicketCache(const rtTicketCache_t &ticket_cache) {
  string debug_str = "\n === ticket_cache Begin ===";
  debug_str += "\n cacheOption: " + std::to_string(ticket_cache.cacheOption);
  debug_str += "\n ticketCacheWindow: " + std::to_string(ticket_cache.ticketCacheWindow);
  CM_LOGD("%s.", debug_str.c_str());
  DebugAutoThreadCache(ticket_cache.custom.autoThreadCache);
  CM_LOGD("=== ticket_cache End ===");
}

void FftsTaskBuilder::DebugTaskInfo(const rtFftsTaskInfo_t &task_info) const {
  string debug_str = "\n === Task Info Begin ===";
  debug_str += "\n fftsType: " + std::to_string(task_info.fftsType);
  debug_str += "\n subTaskNum: " + std::to_string(task_info.subTaskNum);
  debug_str += "\n tickCacheNum: " + std::to_string(task_info.tickCacheNum);
  CM_LOGD("%s.", debug_str.c_str());
  debug_str.clear();

  DebugFftsDesc(task_info.fftsDesc);

  for (uint32_t index = 0; index < RT_FFTS_MAX_SUB_TASK_NUM; index++) {
    if (task_info.subTask[index].threadDim) {
      CM_LOGD("=== subTask %d Begin ===", index);
      DebugSubTask(task_info.subTask[index]);
      CM_LOGD("=== subTask %d End ===", index);
    }
  }

  for (uint32_t index = 0; index < RT_FFTS_MAX_TICKET_CACHE_NUM; index++) {
    if (task_info.ticketCache[index].ticketCacheWindow) {
      CM_LOGD("=== ticketCache %d Begin ===", index);
      DebugTicketCache(task_info.ticketCache[index]);
      CM_LOGD("=== ticketCache %d End ===", index);
    }
  }

  CM_LOGD("=== Task Info End ===");
}

Status FftsTaskBuilder::GenerateTask(const ge::Node &node, const ge::RunContext &context,
                                     std::vector<domi::TaskDef> &task_defs) {
  ge::OpDescPtr op_desc = node.GetOpDesc();
  std::string sub_graph_name = op_desc->GetSubgraphInstanceName(0);
  if (sub_graph_name.empty()) {
    return FAILED;
  }
  CM_LOGD("ThreadTaskBuilder::GenerateTask begin, node name:%s, node type:%s, sub_graph_name %s.",
          node.GetName().c_str(), node.GetType().c_str(), sub_graph_name.c_str());
  int64_t start_usec_gentask = GetMicroSecondsTime();
  ge::ComputeGraphPtr graph = nullptr;
  (void)ge::AttrUtils::GetGraph(node.GetOpDesc(), "_sgt_sub_graph", graph);
  vector<ge::NodePtr> node_vec;
  CM_CHECK_NOTNULL(graph);
  CM_CHECK_NOTNULL(context.model);
  CM_CHECK_NOTNULL(context.stream);
  CM_CHECK_NOTNULL(context.dataMemBase);
  CM_CHECK_NOTNULL(context.weightMemBase);


  context_.dataMemSize = context.dataMemSize;
  context_.dataMemBase = context.dataMemBase;
  context_.weightMemSize = context.weightMemSize;
  context_.weightMemBase = context.weightMemBase;
  context_.weightBufferHost = context.weightsBuffer;
  CM_LOGD("ThreadTaskBuilder::context_ dataMemSize %ld, dataMemBase %p, weightMemSize %ld, weightMemBase %p.",
          context.dataMemSize, context.dataMemBase, context.weightMemSize, context.weightMemBase);

  (void)memset_s(&task_info_, sizeof(task_info_), 0, sizeof(task_info_));

  auto ai_graph = node.GetOwnerComputeGraph();
  CM_LOGD("ThreadTaskBuilder::ai_graph %s, %p.", ai_graph->GetName().c_str(), ai_graph.get());
  auto root_graph = ge::GraphUtils::FindRootGraph(ai_graph);
  CM_LOGD("ThreadTaskBuilder::root_graph %s, %p.", root_graph->GetName().c_str(), root_graph.get());
  auto sgt_graph = root_graph->GetSubgraph(sub_graph_name);
  CM_LOGD("ThreadTaskBuilder::sgt_graph %s, %p.", sgt_graph->GetName().c_str(), sgt_graph.get());

  Status status = GenSubGraphTaskInfo(node, task_info_);
  if (status != SUCCESS) {
    CM_LOGE("GenSubGraphTaskInfo failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  if (sgt_graph->GetDirectNode().size() > RT_FFTS_MAX_SUB_TASK_NUM) {
    CM_LOGE("sub task num %zu invalid, should below 32.", sgt_graph->GetDirectNode().size());
    return FAILED;
  }
  for (auto &sub_node : sgt_graph->GetDirectNode()) {
    CM_LOGD("sgt_graph::sub_node %s, %p.", sub_node->GetName().c_str(), sub_node->GetOpDesc().get());
  }
  uint32_t index = 0;
  for (auto &sub_node : sgt_graph->GetDirectNode()) {
    auto op_desc_ptr = sub_node->GetOpDesc();
    if (!IsNotSubGraphDataAndNetOutput(op_desc_ptr)) {
      continue;
    }
    ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
    slice_info_ptr = sub_node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
    CM_LOGD("ffts::kAttrSgtStructInfo sgt GenerateTask of Op name: %s, Op id: %ld, %p, %p.",
            sub_node->GetOpDesc()->GetName().c_str(), sub_node->GetOpDesc()->GetId(),
            sub_node->GetOpDesc().get(), slice_info_ptr.get());
    if (slice_info_ptr == nullptr) {
      continue;
    }
    status = DoGenerateSubNodeTask(sub_node, task_info_.subTask[index]);
    if (status != SUCCESS) {
      CM_LOGE("DoGenerateSubNodeTask failed. Op[%s, optype[%s]]",
              op_desc->GetName().c_str(), op_desc->GetType().c_str());
      return status;
    }
    status = GenTicketCacheInfo(sub_node, task_info_);
    if (status != SUCCESS) {
      CM_LOGE("GenTicketCacheInfo failed. Op[%s, optype[%s]]",
              op_desc->GetName().c_str(), op_desc->GetType().c_str());
      return status;
    }
    node_vec.push_back(sub_node);
    index++;
  }

  DebugTaskInfo(task_info_);

  domi::TaskDef task_def = {};
  status = GenSubGraphTaskDef(node, node_vec, task_def);
  if (status != SUCCESS) {
    CM_LOGE("GenSubGraphTaskDef failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  task_defs.push_back(task_def);

  CM_LOGD("FftsTaskBuilder::GenerateTask end, node name:%s, node type:%s.",
          node.GetName().c_str(), node.GetType().c_str());

  int64_t end_usec_gentask = GetMicroSecondsTime();
  CM_LOGV("[FE_PERFORMANCE]The time cost of FftsTaskBuilder::GenerateTask is [%ld] micro second.",
          (end_usec_gentask - start_usec_gentask));
  return SUCCESS;
}

Status FftsTaskBuilder::DoGenerateSubNodeTask(const ge::NodePtr &node,
                                              rtFftsSubTaskInfo_t &sub_task) {
  // Create FftsTaskBuilderAdapter
  FftsTaskBuilderAdapterPtr task_builder_adapter_ptr = nullptr;
  ge::OpDescPtr op_desc = node->GetOpDesc();

  CM_MAKE_SHARED(task_builder_adapter_ptr = std::make_shared<FftsTaskBuilderAdapter>(*node, context_),
                 return FAILED);
  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
  slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
  CM_LOGI("ffts::kAttrSgtStructInfo DoGenerateSubNodeTask of Op name: %s, Op id: %ld, %p, %p.",
      node->GetOpDesc()->GetName().c_str(), node->GetOpDesc()->GetId(), node->GetOpDesc().get(), slice_info_ptr.get());
  Status status = task_builder_adapter_ptr->Init();
  task_builder_adapter_ptr_map_.insert({node->GetName(), task_builder_adapter_ptr});

  if (status != SUCCESS) {
    CM_LOGE("Init FftsTaskBuilderAdapter failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  status = GenSubFftsTaskCommonInfo(node, sub_task);
  if (status != SUCCESS) {
    CM_LOGE("GenSubFftsTaskCommonInfo failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  status = GenSubFftsTaskInfo(node, sub_task);
  if (status != SUCCESS) {
    CM_LOGE("GenSubFftsTaskInfo failed. Op[%s, optype[%s]]",
            op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  return status;
}

Status FftsTaskBuilder::GenSubGraphTaskInfo(const ge::Node &node,
                                            rtFftsTaskInfo_t &sub_graph_task_info) const {
  ge::OpDescPtr function_op_desc = node.GetOpDesc();
  uint32_t graph_tick_cache_num = 0;
  uint32_t graph_sub_task_num = 0;
  (void)ge::AttrUtils::GetInt(function_op_desc, "_graph_tick_cache_num", graph_tick_cache_num);
  (void)ge::AttrUtils::GetInt(function_op_desc, "_graph_sub_task_num", graph_sub_task_num);
  if (graph_tick_cache_num > RT_FFTS_MAX_TICKET_CACHE_NUM) {
    CM_LOGE("Tick cache num %d invalid.", graph_tick_cache_num);
    return FAILED;
  }
  if (graph_sub_task_num > RT_FFTS_MAX_SUB_TASK_NUM) {
    CM_LOGE("Sub task num %d invalid.", graph_sub_task_num);
    return FAILED;
  }
  sub_graph_task_info.fftsType = RT_FFTS_TYPE_AUTO_THREAD;
  sub_graph_task_info.subTaskNum = graph_sub_task_num;
  sub_graph_task_info.tickCacheNum = graph_tick_cache_num;

  return SUCCESS;
}

Status FftsTaskBuilder::GenSubFftsTaskCommonInfo(const ge::NodePtr &node, rtFftsSubTaskInfo_t &sub_ffts_task_info) {
  ge::OpDescPtr op_desc = node->GetOpDesc();
  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
  slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
  CM_CHECK_NOTNULL(slice_info_ptr);
  vector<std::string> core_type_vec;
  if (ge::AttrUtils::GetListStr(op_desc, ATTR_NAME_THREAD_CUBE_VECTOR_CORE_TYPE, core_type_vec)) {
    if (core_type_vec.size() == 0) {
        CM_LOGE("Sub Ffts task core type vec size zero .");
        return FAILED;
    }
    if (core_type_vec[0] == "AIC") {
      sub_ffts_task_info.subTaskType = RT_FFTS_SUB_TASK_TYPE_AIC;
    }
    if (core_type_vec[0] == "AIV") {
      sub_ffts_task_info.subTaskType = RT_FFTS_SUB_TASK_TYPE_AIV;
    }
  }
  sub_ffts_task_info.threadDim = slice_info_ptr->slice_instance_num;

  vector<uint32_t> dst_tick_cache_id;
  vector<uint32_t> src_tick_cache_id;
  uint32_t src_data_out_of_sub_graph_bitmap = 0;
  (void)ge::AttrUtils::GetListInt(op_desc, "_src_tick_cache_id", src_tick_cache_id);
  (void)ge::AttrUtils::GetListInt(op_desc, "_dst_tick_cache_id", dst_tick_cache_id);
  (void)ge::AttrUtils::GetInt(op_desc, "_src_data_out_of_sub_graph_bitmap", src_data_out_of_sub_graph_bitmap);

  uint32_t index = 0;
  for (auto const &cache_id : src_tick_cache_id) {
    sub_ffts_task_info.srcTickCacheID[index] = static_cast<uint8_t>(cache_id);
    sub_ffts_task_info.srcTickCacheVldBitmap |= 1 << index;
    CM_LOGD("node %s %d src cache id is %d, bitmap %d.",
            op_desc->GetName().c_str(), index, cache_id, sub_ffts_task_info.srcTickCacheVldBitmap);
    index++;
  }

  index = 0;
  for (auto const &cache_id : dst_tick_cache_id) {
    sub_ffts_task_info.dstTickCacheID[index] = static_cast<uint8_t>(cache_id);
    sub_ffts_task_info.dstTickCacheVldBitmap |= 1 << index;
    CM_LOGD("node %s %d dst cache id is %d, bitmap %d.",
            op_desc->GetName().c_str(), index, cache_id, sub_ffts_task_info.dstTickCacheVldBitmap);
    index++;
  }
  CM_LOGD("node %s src_data_out_of_sub_graph_bitmap is %d.",
          op_desc->GetName().c_str(), src_data_out_of_sub_graph_bitmap);
  sub_ffts_task_info.srcDataOutOfSubGraphBitmap = static_cast<uint8_t>(src_data_out_of_sub_graph_bitmap);

  return SUCCESS;
}

Status FftsTaskBuilder::GenSubGraphTaskDef(const ge::Node &node,
                                           vector<ge::NodePtr> node_vec,
                                           domi::TaskDef &task_def) {

  CM_LOGD("GenSubGraphTaskDef: subgraph = %s.", node.GetName().c_str());
  task_def.set_type(RT_MODEL_TASK_FFTS_TASK);
  domi::FftsTaskDef *ffts_task_def = task_def.mutable_ffts_task();
  if (ffts_task_def == nullptr) {
    CM_LOGE("kernel_def is nullptr.");
    return ACL_ERROR_RT_PARAM_INVALID;
  }

  ffts_task_def->set_ffts_type(task_info_.fftsType);
  ffts_task_def->set_op_index(node.GetOpDesc()->GetId());
  ffts_task_def->set_addr_size(addr_size_);

  if (node_vec.size() != task_info_.subTaskNum) {
    CM_LOGE("sub task num invalid, node num is %zu, sub task num is %u.", node_vec.size(), task_info_.subTaskNum);
    return ACL_ERROR_RT_PARAM_INVALID;
  }

  CM_LOGD("ffts_task_def = %s", ffts_task_def->DebugString().c_str());

  for (uint16_t task_index = 0; task_index < task_info_.subTaskNum; task_index++) {
    domi::FftsSubTaskDef *ffts_sub_task_def = ffts_task_def->add_sub_task();
    rtFftsSubTaskInfo_t sub_task = task_info_.subTask[task_index];
    ffts_sub_task_def->set_sub_task_type(sub_task.subTaskType);
    ffts_sub_task_def->set_thread_dim(sub_task.threadDim);
    ffts_sub_task_def->set_dst_tick_cache_vld_bitmap(sub_task.dstTickCacheVldBitmap);
    ffts_sub_task_def->set_src_tick_cache_vld_bitmap(sub_task.srcTickCacheVldBitmap);
    ffts_sub_task_def->set_src_data_out_of_subgraph_bitmap(sub_task.srcDataOutOfSubGraphBitmap);
    for (uint16_t cache_index = 0; cache_index < RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK; cache_index++) {
      ffts_sub_task_def->add_dst_tick_cache_id(sub_task.dstTickCacheID[cache_index]);
      ffts_sub_task_def->add_src_tick_cache_id(sub_task.srcTickCacheID[cache_index]);
    }

    CM_LOGD("ffts_sub_task_def %d: %s", task_index, ffts_sub_task_def->DebugString().c_str());

    ThreadParamOffset param_offset;
    task_builder_adapter_ptr_map_[node_vec[task_index]->GetName()]->GetThreadParamOffset(param_offset);
    domi::AutoThreadAicAivDef *auto_thread_aic_aiv_def = ffts_sub_task_def->mutable_auto_thread_aic_aiv();
    rtAutoThreadAicAivInfo_t autoThreadAicAiv = sub_task.custom.autoThreadAicAiv;
    size_t args_num = param_offset.first_thread_input_addrs.size() +
                      param_offset.first_thread_output_addrs.size() +
                      param_offset.thread_workspace_addrs[0].size();
    if (args_num != autoThreadAicAiv.taskParamOffset) {
      CM_LOGE("args_num invalid, args_num is %zu, param offset is %u.", args_num, autoThreadAicAiv.taskParamOffset);
      return ACL_ERROR_RT_PARAM_INVALID;
    }
    uint32_t input_output_num = param_offset.first_thread_input_addrs.size() +
                                param_offset.first_thread_output_addrs.size();
    auto_thread_aic_aiv_def->set_input_output_count(input_output_num);
    for (auto input_addr : param_offset.first_thread_input_addrs) {
      auto_thread_aic_aiv_def->add_task_addr((uint64_t)(uintptr_t)input_addr);
    }
    for (auto output_addr : param_offset.first_thread_output_addrs) {
      auto_thread_aic_aiv_def->add_task_addr((uint64_t)(uintptr_t)output_addr);
    }
    for (auto workspace_addr : param_offset.thread_workspace_addrs[0]) {
      auto_thread_aic_aiv_def->add_task_addr((uint64_t)(uintptr_t)workspace_addr);
    }
    for (auto workspace_addr : param_offset.thread_workspace_addrs[1]) {
      auto_thread_aic_aiv_def->add_task_addr((uint64_t)(uintptr_t)workspace_addr);
    }
    for (auto addr_offset : param_offset.thread_addr_offset) {
      auto_thread_aic_aiv_def->add_task_addr_offset((uint64_t)(uintptr_t)addr_offset);
    }

    auto_thread_aic_aiv_def->set_task_param_offset(autoThreadAicAiv.taskParamOffset * sizeof(uint64_t));
    auto_thread_aic_aiv_def->set_prefetch_enable_bitmap(autoThreadAicAiv.prefetchEnableBitmap);
    auto_thread_aic_aiv_def->set_prefetch_once_bitmap(autoThreadAicAiv.prefetchOnceBitmap);
    auto_thread_aic_aiv_def->set_tail_blk_dim(autoThreadAicAiv.tailBlkDim);
    auto_thread_aic_aiv_def->set_non_tail_blk_dim(autoThreadAicAiv.nonTailBlkDim);
    auto_thread_aic_aiv_def->set_non_tail_task_func_stub(autoThreadAicAiv.nonTailTaskFuncStub);
    auto_thread_aic_aiv_def->set_tail_task_func_stub(autoThreadAicAiv.tailTaskFuncStub);
    CM_LOGD("auto_thread_aic_aiv_def %d: %s", task_index, auto_thread_aic_aiv_def->DebugString().c_str());
    for (uint16_t prefetch_index = 0; prefetch_index < RT_FFTS_MAX_TICKET_CACHE_PER_SUBTASK; prefetch_index++) {
      rtAutoThreadPrefetch_t src_prefetch = autoThreadAicAiv.srcPrefetch[prefetch_index];
      domi::AutoThreadPrefetchDef *auto_thread_prefetch_def = auto_thread_aic_aiv_def->add_src_prefetch();
      auto_thread_prefetch_def->set_data_addr(src_prefetch.dataAddr);
      auto_thread_prefetch_def->set_data_addr_offset(src_prefetch.dataAddrOffset);
      auto_thread_prefetch_def->set_non_tail_data_len(src_prefetch.nonTailDataLen);
      auto_thread_prefetch_def->set_tail_data_len(src_prefetch.tailDataLen);
      CM_LOGD("auto_thread_prefetch_def %d: %s", prefetch_index, auto_thread_prefetch_def->DebugString().c_str());
    }
  }

  uint32_t cache_num = 0;
  (void)ge::AttrUtils::GetInt(node.GetOpDesc(), "_graph_tick_cache_num", cache_num);
  for (uint32_t cache_index = 0; cache_index < cache_num; cache_index++) {
    rtTicketCache_t ticket_cache = task_info_.ticketCache[cache_index];
    domi::TicketCacheDef *ticket_cache_def = ffts_task_def->add_ticket_cache();
    ticket_cache_def->set_cache_option(ticket_cache.cacheOption);
    ticket_cache_def->set_ticket_cache_window(ticket_cache.ticketCacheWindow);
    CM_LOGD("ticket_cache_def %d: %s", cache_index, ticket_cache_def->DebugString().c_str());
    domi::AutoThreadCacheDef *auto_thread_cache_def = ticket_cache_def->mutable_auto_thread_cache();
    auto_thread_cache_def->set_data_addr(ticket_cache.custom.autoThreadCache.dataAddr);
    auto_thread_cache_def->set_data_addr_offset(ticket_cache.custom.autoThreadCache.dataAddrOffset);
    auto_thread_cache_def->set_non_tail_data_len(ticket_cache.custom.autoThreadCache.nonTailDataLen);
    auto_thread_cache_def->set_tail_data_len(ticket_cache.custom.autoThreadCache.tailDataLen);
    auto_thread_cache_def->set_ticket_cache_ref_cnt(ticket_cache.custom.autoThreadCache.ticketCacheRefCnt);
    CM_LOGD("auto_thread_cache_def %d: %s", cache_index, auto_thread_cache_def->DebugString().c_str());
  }
  return SUCCESS;
}

}  // namespace fe