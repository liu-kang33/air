/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "auto_ffts_task_builder.h"
#include <runtime/rt.h>
#include <securec.h>
#include <string>
#include "common/comm_log.h"
#include "common/common_utils.h"
#include "common/comm_error_codes.h"
#include "common/fe_error_code.h"
#include "common/ffts_plus_type.h"
#include "common/aicore_util_types.h"
#include "common/aicore_util_attr_define.h"
#include "adapter/factory/task_builder_adapter_factory.h"
#include "ffts_task_builder_adapter.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/compute_graph.h"
#include "util/error_manager/error_manager.h"
#include "runtime/rt_error_codes.h"

namespace fe {

AutoFftsTaskBuilder::AutoFftsTaskBuilder() {}

AutoFftsTaskBuilder::~AutoFftsTaskBuilder() {}

Status AutoFftsTaskBuilder::GenerateStubFunc(ge::OpDescPtr &op_desc, vector<char*> &register_stub) {
  vector<string> unique_ids;
  string session_graph_id = "";
  if (ge::AttrUtils::GetStr(op_desc, ge::ATTR_NAME_SESSION_GRAPH_ID, session_graph_id) && !session_graph_id.empty()) {
    unique_ids.push_back(session_graph_id + "_" + op_desc->GetName() + "_0");
    unique_ids.push_back(session_graph_id + "_" + op_desc->GetName() + "_1");
  } else {
    unique_ids.push_back(op_desc->GetName() + "_0");
    unique_ids.push_back(op_desc->GetName() + "_1");
  }
  (void)ge::AttrUtils::SetListStr(op_desc, "_register_stub_func", unique_ids);

  if (unique_ids[0].length() == 0) {
    CM_LOGE("The length of unique id is zero.");
    return FAILED;
  }
  unsigned long long str_len = unique_ids[0].length() + 1;
  if (str_len == 0) {
    CM_LOGE("The length of unique id is too large.");
    return FAILED;
  }
  char *stub_func_0 = new char[str_len];
  CM_CHECK_NOTNULL(stub_func_0);
  (void)memset_s(stub_func_0, str_len, 0, str_len);
  if (SUCCESS != (uint32_t)memcpy_s(stub_func_0, str_len, unique_ids[0].c_str(), unique_ids[0].length())) {
    delete[] stub_func_0;
    return PARAM_INVALID;
  }
  register_stub.push_back(stub_func_0);

  char *stub_func_1 = new char[str_len];
  CM_CHECK_NOTNULL(stub_func_1);
  (void)memset_s(stub_func_1, str_len, 0, str_len);
  if (SUCCESS != (uint32_t)memcpy_s(stub_func_1, str_len, unique_ids[1].c_str(), unique_ids[1].length())) {
    delete[] stub_func_1;
    return PARAM_INVALID;
  }
  register_stub.push_back(stub_func_1);
  for (size_t i = 0; i < register_stub.size(); i++) {
    CM_LOGD("GenerateStubFunc index: %zu, stub: %s.", i, register_stub[i]);
  }
  return SUCCESS;
}

Status AutoFftsTaskBuilder::GenSubFftsTaskInfo(const ge::NodePtr &node, rtFftsSubTaskInfo_t &sub_ffts_task_info) {
  sub_ffts_task_info.custom.autoThreadAicAiv.prefetchEnableBitmap = 0;
  sub_ffts_task_info.custom.autoThreadAicAiv.prefetchOnceBitmap = 0;
  ThreadParamOffset param_offset;
  task_builder_adapter_ptr_map_[node->GetName()]->GetThreadParamOffset(param_offset);
  if (param_offset.thread_workspace_addrs.size() == 0) {
      CM_LOGE("thread_workspace_addrs size is zero.");
      return FAILED;
  }

  uint16_t thread_param_num = param_offset.first_thread_input_addrs.size() +
                              param_offset.first_thread_output_addrs.size() +
                              param_offset.thread_workspace_addrs[0].size();
  sub_ffts_task_info.custom.autoThreadAicAiv.taskParamOffset = thread_param_num;
  addr_size_ += thread_param_num * param_offset.thread_dim -
                param_offset.thread_workspace_addrs[0].size() +
                param_offset.thread_workspace_addrs[1].size();

  CM_LOGI("Op %s thread_param_num is %d, addr size %d.",
          node->GetOpDesc()->GetName().c_str(), thread_param_num, addr_size_);
  sub_ffts_task_info.custom.autoThreadAicAiv.taskParamAddr = 0;
  sub_ffts_task_info.custom.autoThreadAicAiv.satMode = 0;
  sub_ffts_task_info.custom.autoThreadAicAiv.scheduleMode = 0;
  sub_ffts_task_info.custom.autoThreadAicAiv.iCachePrefetchCnt = 0;

  ge::OpDescPtr op_desc = node->GetOpDesc();
  std::vector<int64_t> block_dim_vec;
  (void)ge::AttrUtils::GetListInt(op_desc, ge::TVM_ATTR_NAME_THREAD_BLOCKDIM, block_dim_vec);
  if (block_dim_vec.size() != 2) {
    CM_LOGE("block_dim_vec size is %zu.", block_dim_vec.size());
    return FAILED;
  }

  vector<char*> register_stub;
  Status status = GenerateStubFunc(op_desc, register_stub);
  if (status != SUCCESS || register_stub.size() <= 1) {
    CM_LOGE("Generate stub func for node %s failed.", op_desc->GetName().c_str());
    return status;
  }

  sub_ffts_task_info.custom.autoThreadAicAiv.nonTailBlkDim = block_dim_vec[0];
  sub_ffts_task_info.custom.autoThreadAicAiv.nonTailTaskFuncStub = register_stub[0];
  sub_ffts_task_info.custom.autoThreadAicAiv.tailBlkDim = block_dim_vec[1];
  sub_ffts_task_info.custom.autoThreadAicAiv.tailTaskFuncStub = register_stub[1];

  return SUCCESS;
}

Status AutoFftsTaskBuilder::GenTicketCacheInfo(const ge::NodePtr &node, rtFftsTaskInfo_t &task_info) {

  ge::OpDescPtr op_desc = node->GetOpDesc();
  TickCacheMap tick_cache_table;
  tick_cache_table = op_desc->TryGetExtAttr("_tick_cache_map", tick_cache_table);
  ThreadParamOffset param_offset;
  task_builder_adapter_ptr_map_[node->GetName()]->GetThreadParamOffset(param_offset);
  auto input_size = tick_cache_table.input_cache_table.size();
  for (auto input : tick_cache_table.input_cache_table) {
    CM_LOGI("Op %s input %d cache id is %d.", node->GetOpDesc()->GetName().c_str(), input.first, input.second);
    if (task_info.ticketCache[input.second].custom.autoThreadCache.ticketCacheRefCnt) {
      task_info.ticketCache[input.second].custom.autoThreadCache.ticketCacheRefCnt++;
      continue;
    }

    task_info.ticketCache[input.second].custom.autoThreadCache.dataAddrOffset =
        param_offset.thread_addr_offset[input.first];
    task_info.ticketCache[input.second].custom.autoThreadCache.dataAddr =
        (uint64_t)(uintptr_t)param_offset.first_thread_input_addrs[input.first];
    task_info.ticketCache[input.second].custom.autoThreadCache.nonTailDataLen =
        param_offset.thread_addr_offset[input.first];
    task_info.ticketCache[input.second].custom.autoThreadCache.tailDataLen =
        param_offset.input_tensor_sizes[input.first] -
        param_offset.thread_addr_offset[input.first] * param_offset.thread_dim;

    task_info.ticketCache[input.second].ticketCacheWindow = 2;
    task_info.ticketCache[input.second].cacheOption = RT_CACHE_OP_NONE;
    task_info.ticketCache[input.second].custom.autoThreadCache.ticketCacheRefCnt++;
  }
  for (auto output : tick_cache_table.output_cache_table) {
    CM_LOGI("Op %s output %d cache id is %d.", node->GetOpDesc()->GetName().c_str(), output.first, output.second);
    if (task_info.ticketCache[output.second].custom.autoThreadCache.ticketCacheRefCnt) {
      continue;
    }

    task_info.ticketCache[output.second].custom.autoThreadCache.dataAddrOffset =
        param_offset.thread_addr_offset[output.first + input_size];
    task_info.ticketCache[output.second].custom.autoThreadCache.dataAddr =
        (uint64_t)(uintptr_t)param_offset.first_thread_output_addrs[output.first];
    task_info.ticketCache[output.second].custom.autoThreadCache.nonTailDataLen =
        param_offset.thread_addr_offset[output.first + input_size];;
    task_info.ticketCache[output.second].custom.autoThreadCache.tailDataLen =
        param_offset.output_tensor_sizes[output.first] -
        param_offset.thread_addr_offset[output.first + input_size] * param_offset.thread_dim;

    task_info.ticketCache[output.second].ticketCacheWindow = 2;
    task_info.ticketCache[output.second].cacheOption = RT_CACHE_OP_INVALIDATE;
  }
  return SUCCESS;
}


}  // namespace fe
