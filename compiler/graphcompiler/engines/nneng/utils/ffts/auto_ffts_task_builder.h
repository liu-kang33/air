/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FUSION_ENGINE_UTILS_FFTS_AUTO_FFTS_TASK_BUILDER_H_
#define FUSION_ENGINE_UTILS_FFTS_AUTO_FFTS_TASK_BUILDER_H_
#include "ffts_task_builder.h"

namespace fe {

class AutoFftsTaskBuilder : public FftsTaskBuilder {
 public:
  AutoFftsTaskBuilder();
  ~AutoFftsTaskBuilder() override;

 private:
  AutoFftsTaskBuilder(const AutoFftsTaskBuilder &builder) = delete;
  AutoFftsTaskBuilder &operator=(const AutoFftsTaskBuilder &builder) = delete;

  Status GenerateStubFunc(ge::OpDescPtr &op_desc, vector<char*> &register_stub);
  Status GenSubFftsTaskInfo(const ge::NodePtr &node, rtFftsSubTaskInfo_t &sub_ffts_task_info) override;
  Status GenTicketCacheInfo(const ge::NodePtr &node, rtFftsTaskInfo_t &task_info) override;

 private:
  TaskBuilderContext context_;
  //shared_ptr<vector<uint64_t>> param_offset_ptr_;
};


}  // namespace fe
#endif  // FUSION_ENGINE_UTILS_FFTS_AUTO_FFTS_TASK_BUILDER_H_
