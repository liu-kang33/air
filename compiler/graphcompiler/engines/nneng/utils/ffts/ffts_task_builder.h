/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FUSION_ENGINE_UTILS_FFTS_FFTS_TASK_BUILDER_H_
#define FUSION_ENGINE_UTILS_FFTS_FFTS_TASK_BUILDER_H_
#include <map>
#include <memory>
#include <vector>
#include "proto/task.pb.h"
#include "ffts_task_builder_adapter.h"
#include "common/opskernel/ops_kernel_info_types.h"

namespace fe {
using SubGraphNodeMap = std::map<uint32_t, std::vector<ge::NodePtr>>;
using FftsTaskBuilderAdapterPtr = std::shared_ptr<FftsTaskBuilderAdapter>;

class FftsTaskBuilder {
 public:
  FftsTaskBuilder();
  virtual ~FftsTaskBuilder();

  /*
   * @ingroup fe
   * @brief   Generate tasks
   * @param   [in] node Node of compute graph
   * @param   [in] context Context for generate tasks
   * @param   [out] task_defs Save the generated tasks.
   * @return  SUCCESS or FAILED
   */
  Status GenerateTask(const ge::Node &node, const ge::RunContext &context, std::vector<domi::TaskDef> &task_defs);

 protected:
  map<string, FftsTaskBuilderAdapterPtr> task_builder_adapter_ptr_map_;
  uint32_t addr_size_ = 0;
 private:
  FftsTaskBuilder(const FftsTaskBuilder &builder) = delete;
  FftsTaskBuilder &operator=(const FftsTaskBuilder &builder) = delete;

  Status Init(ge::NodePtr &node);
  Status InitInput(ge::NodePtr &node);
  Status InitOutput(ge::NodePtr &node);
  Status InitWorkspace(ge::NodePtr &node);
  Status HandleAnchorData(size_t &input_index,
                          size_t &anchor_index, size_t &weight_index);
  Status DoGenerateSubNodeTask(const ge::NodePtr &node,
                               rtFftsSubTaskInfo_t &sub_task);

  virtual Status GenSubFftsTaskInfo(const ge::NodePtr &node, rtFftsSubTaskInfo_t &sub_ffts_task_info) = 0;
  virtual Status GenTicketCacheInfo(const ge::NodePtr &node, rtFftsTaskInfo_t &sub_ffts_task_info) = 0;

  Status GenSubFftsTaskCommonInfo(const ge::NodePtr &node, rtFftsSubTaskInfo_t &sub_ffts_task_info);
  Status GenSubGraphTaskInfo(const ge::Node &node, rtFftsTaskInfo_t &sub_graph_task_info) const;
  Status GenSubGraphTaskDef(const ge::Node &node, vector<ge::NodePtr> node_vec, domi::TaskDef &task_def);
  void DebugTaskInfo(const rtFftsTaskInfo_t &task_info) const;
 private:
  rtFftsTaskInfo_t task_info_;
  TaskBuilderContext context_;
};

class ManualFftsTaskBuilder : public FftsTaskBuilder {
 public:
  ManualFftsTaskBuilder();
  ~ManualFftsTaskBuilder() override;

 private:
  ManualFftsTaskBuilder(const ManualFftsTaskBuilder &builder) = delete;
  ManualFftsTaskBuilder &operator=(const ManualFftsTaskBuilder &builder) = delete;

  Status GenSubFftsTaskInfo(const ge::NodePtr &node, rtFftsSubTaskInfo_t &sub_ffts_task_info) override;
  Status GenTicketCacheInfo(const ge::NodePtr &node, rtFftsTaskInfo_t &task_info) override;

 private:
  TaskBuilderContext context_;
};

}  // namespace fe
#endif  // FUSION_ENGINE_UTILS_FFTS_FFTS_TASK_BUILDER_H_