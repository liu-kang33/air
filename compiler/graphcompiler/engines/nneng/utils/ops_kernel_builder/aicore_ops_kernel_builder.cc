/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ops_kernel_builder/aicore_ops_kernel_builder.h"
#include "common/ffts_plus_type.h"
#include "common/comm_log.h"
#include "common/constants_define.h"
#include "common/op_tensor_utils.h"
#include "graph/utils/attr_utils.h"
#include "common/aicore_util_attr_define.h"
#include "graph/debug/ge_attr_define.h"
#include "param_calculate/tensorsize_calculator.h"
#include "task_builder/task_builder.h"
#include "register/ops_kernel_builder_registry.h"
#include "ffts/auto_ffts_task_builder.h"
#include "adapter/tbe_adapter/tbe_task_builder_adapter.h"
namespace fe {
REGISTER_OPS_KERNEL_BUILDER(AI_CORE_NAME, AICoreOpsKernelBuilder);
REGISTER_OPS_KERNEL_BUILDER(VECTOR_CORE_NAME, AICoreOpsKernelBuilder);

AICoreOpsKernelBuilder::AICoreOpsKernelBuilder() {}

AICoreOpsKernelBuilder::~AICoreOpsKernelBuilder() {}

Status AICoreOpsKernelBuilder::Initialize(const std::map<std::string, std::string> &options) { return SUCCESS; }

Status AICoreOpsKernelBuilder::Finalize() { return SUCCESS; }

Status AICoreOpsKernelBuilder::CalcOpRunningParam(ge::Node &node) {
  ge::OpDescPtr op_desc_ptr = node.GetOpDesc();
  CM_LOGD("Begin to calculate input and output size of op [%s].", op_desc_ptr->GetName().c_str());

  if (TensorSizeCalculator::CalculateOpTensorSize(*(op_desc_ptr.get())) != SUCCESS) {
    REPORT_CM_ERROR("[GenTask][CalcOpRunningParam][Node %s, type %s] Fail to calculate running parameters.",
                    op_desc_ptr->GetName().c_str(), op_desc_ptr->GetType().c_str());
    return FAILED;
  }

  if (node.GetOpDesc()->HasAttr(ge::ATTR_NAME_UNREGST_OPPATH)) {
    if (!ge::AttrUtils::SetInt(op_desc_ptr, FE_IMPLY_TYPE, EN_IMPL_HW_TBE)) {
      REPORT_CM_ERROR("[GenTask][CalcOpRunningParam][Node %s, type %s] Fail to set fe_impl_type!",
                      op_desc_ptr->GetName().c_str(), op_desc_ptr->GetType().c_str());
      return FAILED;
    }
  }
  if (op_desc_ptr->GetType() == OP_TYPE_ROIPOOLING || op_desc_ptr->GetType() == OP_TYPE_SSD_DETECTION_OUTPUT) {
    bool has_reuse_mem_attr = true;
    (void)ge::AttrUtils::SetBool(op_desc_ptr, ATTR_NAME_NO_REUSE_MEM_FLAG, has_reuse_mem_attr);
    CM_LOGD("op:%s set no_reuse_mem_flag true.", op_desc_ptr->GetName().c_str());
  }

  return SUCCESS;
}

Status AICoreOpsKernelBuilder::GenContextArgs(const ge::Node &node, ge::RunContext &context, TaskArgs &args_info) const
{
  TaskBuilderContext task_context;
  task_context.dataMemSize = context.dataMemSize;
  task_context.dataMemBase = context.dataMemBase;
  task_context.weightMemSize = context.weightMemSize;
  task_context.weightMemBase = context.weightMemBase;
  task_context.weightBufferHost = context.weightsBuffer;
  TaskBuilderAdapterPtr task_builder_adapter_ptr = nullptr;
  CM_MAKE_SHARED(task_builder_adapter_ptr = std::make_shared<TbeTaskBuilderAdapter>(node, task_context), return FAILED);
  Status status = task_builder_adapter_ptr->Init();
  if (status != SUCCESS) {
    REPORT_CM_ERROR("[AICoreOpsKernelBuilder][GenContextArgs][Node %s] Init tbe task builder adapter failed.",
                    node.GetOpDesc()->GetName().c_str());
    return status;
  }
  (void)task_builder_adapter_ptr->GetTaskArgs(args_info);
  vector<int64_t> input_addrs;
  for (auto ele : args_info.input_addrs) {
    input_addrs.emplace_back(reinterpret_cast<int64_t>(ele));
  }
  vector<int64_t> output_addrs;
  for (auto ele : args_info.output_addrs) {
    output_addrs.emplace_back(reinterpret_cast<int64_t>(ele));
  }
  (void)ge::AttrUtils::SetListInt(node.GetOpDesc(), "input_addrs", input_addrs);
  (void)ge::AttrUtils::SetListInt(node.GetOpDesc(), "output_addrs", output_addrs);
  return SUCCESS;
}

Status AICoreOpsKernelBuilder::FillContextData(const ge::Node &node, ge::RunContext &context,
                                               domi::FftsPlusMixAicAivCtxDef *mix_aic_aiv_ctx_def) const {
  CM_LOGD("Node[%s] fill context date.", node.GetName().c_str());
  CM_CHECK_NOTNULL(mix_aic_aiv_ctx_def);
  auto op_desc = node.GetOpDesc();

  uint32_t task_ratio;
  (void)ge::AttrUtils::GetInt(op_desc, kTaskRadio, task_ratio);
  mix_aic_aiv_ctx_def->set_tail_block_ratio_n(task_ratio);
  mix_aic_aiv_ctx_def->set_non_tail_block_ratio_n(task_ratio);
  mix_aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
  mix_aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);
  mix_aic_aiv_ctx_def->set_ns(1);
  mix_aic_aiv_ctx_def->set_atm(kManualMode);
  mix_aic_aiv_ctx_def->set_thread_dim(1);
  vector<std::string> prefixs = { "_mix_aic", "_mix_aiv" };
  for (auto &prefix : prefixs) {
    string attr_key_kernel_name = prefix + op_desc->GetName() + kKernelName;
    string attr_kernel_name;
    (void)ge::AttrUtils::GetStr(op_desc, attr_key_kernel_name, attr_kernel_name);
    mix_aic_aiv_ctx_def->add_kernel_name(attr_kernel_name);
  }
  int32_t block_dim = 0;
  (void)ge::AttrUtils::GetInt(op_desc, ge::TVM_ATTR_NAME_BLOCKDIM, block_dim);
  mix_aic_aiv_ctx_def->set_tail_block_dim(static_cast<uint32_t>(block_dim));
  mix_aic_aiv_ctx_def->set_non_tail_block_dim(static_cast<uint32_t>(block_dim));

  if (OpTensorUtils::IsUnKnownShapeOp(*(op_desc.get()))) {
    CM_LOGD("Node[%s] is unknow shape.", node.GetName().c_str());
    return SUCCESS;
  }
  TaskArgs args_info;
  Status status = GenContextArgs(node, context, args_info);
  if (status != SUCCESS) {
    REPORT_CM_ERROR("[AICoreOpsKernelBuilder] [FillContextData] failed. Op[%s, optype[%s]]",
                    op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }

  for (auto input_addr : args_info.input_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uintptr_t>(input_addr));
  }
  for (auto output_addr : args_info.output_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uintptr_t>(output_addr));
  }
  for (auto workspace_addr : args_info.workspace_addrs) {
    mix_aic_aiv_ctx_def->add_task_addr(reinterpret_cast<uintptr_t>(workspace_addr));
  }
  return SUCCESS;
}

Status AICoreOpsKernelBuilder::GenerateMixL2Task(const ge::Node &node, ge::RunContext &context)
{
  ge::OpDescPtr op_desc = node.GetOpDesc();
  CM_CHECK_NOTNULL(op_desc);
  FftsPlusCtxDefPtr ffts_plus_ctx_def = nullptr;
  CM_MAKE_SHARED(ffts_plus_ctx_def = std::make_shared<domi::FftsPlusCtxDef>(), return FAILED);
  domi::FftsPlusMixAicAivCtxDef *mix_l2_ctx = ffts_plus_ctx_def->mutable_mix_aic_aiv_ctx();
  Status status = FillContextData(node, context, mix_l2_ctx);
  if (status != SUCCESS) {
    REPORT_CM_ERROR("[AICoreOpsKernelBuilder] [FillContextData] failed. Op[%s], optype[%s]",
                    op_desc->GetName().c_str(), op_desc->GetType().c_str());
    return status;
  }
  (void)op_desc->SetExtAttr(kMixL2CtxDef, ffts_plus_ctx_def);
  CM_LOGD("Node[%s] generate ffts task ok", node.GetName().c_str());
  return SUCCESS;
}
Status AICoreOpsKernelBuilder::GenerateTask(const ge::Node &node, ge::RunContext &context,
                                            std::vector<domi::TaskDef> &tasks) {
  CM_LOGD("Begin to generate task for node[%s, %s].", node.GetName().c_str(), node.GetType().c_str());
  ge::OpDescPtr op_desc_ptr = node.GetOpDesc();
  if (op_desc_ptr->HasAttr(ATTR_NAME_FFTS_PLUS_MIX_L2)) {
    return GenerateMixL2Task(node, context);
  }
  if (op_desc_ptr->HasAttr(kTypeFFTSPlus)) {
    TaskBuilder task_builder;
    Status status = task_builder.GenerateFFTSPlusCtx(node, context);
    return status;
  }
  auto owner_graph = node.GetOwnerComputeGraph();
  CM_CHECK_NOTNULL(owner_graph);
  bool is_unknown_shape = owner_graph->GetGraphUnknownFlag();
  CM_LOGD("Graph[name:%s] is_unknown_shape flag is %d.", owner_graph->GetName().c_str(),
          is_unknown_shape);
  if (is_unknown_shape) {
    ge::NodePtr atomic_clean_node = nullptr;
    atomic_clean_node = op_desc_ptr->TryGetExtAttr(ATTR_NAME_ATOMIC_CLEAN_NODE_PTR, atomic_clean_node);
    if (atomic_clean_node != nullptr) {
      int64_t op_desc_id = op_desc_ptr->GetId();
      CM_LOGD("Op:%s op id is %ld", op_desc_ptr->GetName().c_str(), op_desc_id);
      atomic_clean_node->GetOpDesc()->SetId(op_desc_id);
      (void)ge::AttrUtils::SetInt(atomic_clean_node->GetOpDesc(), ATTR_NAME_IS_UNKNOWN_GRAPH, IS_UNKNOWN_SHAPE_VALUE);
      TaskBuilder atomic_task_builder;
      auto atomic_clean_context = context;
      if (atomic_task_builder.GenerateKernelTask(*atomic_clean_node, atomic_clean_context, tasks) == FAILED) {
        REPORT_CM_ERROR("[GenTask][AtomicGen][Node %s, type %s] generate task failed!",
                        atomic_clean_node->GetName().c_str(), atomic_clean_node->GetType().c_str());
        CM_LOGE("op:%s is atomic clean op, generate task failed", atomic_clean_node->GetName().c_str());
        return FAILED;
      }
    }
    (void)ge::AttrUtils::SetInt(op_desc_ptr, ATTR_NAME_IS_UNKNOWN_GRAPH, IS_UNKNOWN_SHAPE_VALUE);
  }

  Status status;
  if (op_desc_ptr->HasAttr("_sgt_function_op")) {
    AutoFftsTaskBuilder* task_builder_ptr = new (std::nothrow) AutoFftsTaskBuilder;
    if (task_builder_ptr == nullptr) {
        CM_LOGE("Auto Ffts Task Builder new fail.");
        return FAILED;
    }
    status = task_builder_ptr->GenerateTask(node, context, tasks);
    delete task_builder_ptr;
  } else {
    TaskBuilder task_builder;
    status = task_builder.GenerateKernelTask(node, context, tasks);
  }

  return status;
}
}  // namespace fe
