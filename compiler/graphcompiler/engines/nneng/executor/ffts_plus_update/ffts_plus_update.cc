﻿/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "ffts_plus_update.h"
#include <nlohmann/json.hpp>
#include "common/aicore_util_attr_define.h"
#include "graph/utils/node_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/utils/op_desc_utils.h"

namespace fe {
std::map<uint32_t, FFTSPlusFeUpdate::procFunc> FFTSPlusFeUpdate::func_tbl_;
std::map<CACHE_OPERATION, std::tuple<string, rtFftsPlusContextType_t, bool>> g_cmo_operation_info;

REGISTER_FFTS_PLUS_CTX_UPDATER(kCoreTypeAIC, FFTSPlusFeUpdate);
REGISTER_FFTS_PLUS_CTX_UPDATER(kCoreTypeAIV, FFTSPlusFeUpdate);
FFTSPlusFeUpdate::FFTSPlusFeUpdate():input_num_(0), input_output_num_(0), task_param_offset_(0),
                                     slice_info_ptr_(nullptr)
{
  g_cmo_operation_info[CACHE_OPERATION::PREFETCH] = std::make_tuple(kPrefetchEnableBm, RT_CTX_TYPE_FLUSH_DATA, true);
  g_cmo_operation_info[CACHE_OPERATION::INVALIDATE] = std::make_tuple(kInvalidateBm, RT_CTX_TYPE_INVALIDATE_DATA,
                                                                      false);
  g_cmo_operation_info[CACHE_OPERATION::WRITE_BACK] = std::make_tuple(kWriteBackBm, RT_CTX_TYPE_WRITEBACK_DATA, false);
  func_tbl_[RT_CTX_TYPE_AICORE] = &FFTSPlusFeUpdate::UpdateAicAivCtx;
  func_tbl_[RT_CTX_TYPE_AIV] = &FFTSPlusFeUpdate::UpdateAicAivCtx;
  func_tbl_[RT_CTX_TYPE_MIX_AIC] = &FFTSPlusFeUpdate::UpdateMixAicAivCtx;
  func_tbl_[RT_CTX_TYPE_MIX_AIV] = &FFTSPlusFeUpdate::UpdateMixAicAivCtx;
  func_tbl_[RT_CTX_TYPE_AT_START] = &FFTSPlusFeUpdate::UpdateStartCtx;
  func_tbl_[RT_CTX_TYPE_LABEL] = &FFTSPlusFeUpdate::UpdateLabelCtx;
}
FFTSPlusFeUpdate::~FFTSPlusFeUpdate() {}

Status FFTSPlusFeUpdate::CalcAutoThreadWorkspace(const vector<optiling::utils::OpRunInfo> &op_run_info,
                                                 ge::NodePtr node, uint32_t thread_num,
                                                 AutoThreadParam &args_para)
{
  (void)node;
  (void)thread_num;
  if (op_run_info.empty()) {
    REPORT_FE_ERROR("Op run info is empty.");
    return FAILED;
  }
  vector<int64_t> non_tail_workspace;
  auto run_info = op_run_info[0];
  run_info.GetAllWorkspaces(non_tail_workspace);
  size_t non_tail_size = non_tail_workspace.size();
  for (size_t j = 0; j < non_tail_size; j++) {
    args_para.task_addr_offset.emplace_back(static_cast<uint64_t>(non_tail_workspace[j]));
  }
  return SUCCESS;
}

inline Status GetDataTypeSize(const ge::DataType &data_type, uint32_t &data_type_size) {
  int res = ge::GetSizeByDataType(data_type);
  if (res == -1) {
    return FAILED;
  }
  data_type_size = static_cast<uint32_t>(res);
  return SUCCESS;
}

Status CalcThreadOffset(vector<ffts::DimRange> &tensor_range, int64_t &thread_offset, const uint32_t &data_type_size,
                        vector<int64_t> &ori_dims)
{
  bool is_ori_shape = true;
  thread_offset = 1;
  for (size_t i = 0; i <  tensor_range.size(); ++i) {
    auto &dim_range = tensor_range[i];
    auto &ori_dim = ori_dims[i];
    if (dim_range.higher < dim_range.lower) {
      return FAILED;
    }
    int64_t range = dim_range.higher - dim_range.lower;
    if (ori_dim != range) {
      is_ori_shape = false;
    }
    thread_offset *= range;
  }
  if (is_ori_shape) {
    thread_offset = 0;
  } else {
    thread_offset = thread_offset * data_type_size;
  }
  return SUCCESS;
}

Status FFTSPlusFeUpdate::CalcAutoThreadInput(const ge::NodePtr &node,
                                             vector<vector<vector<ffts::DimRange>>> &tensor_slice,
                                             AutoThreadParam &argsPara)
{
  if (tensor_slice.empty()) {
    FE_LOGE("Input tensor slice is empty.");
    return FAILED;
  }
  uint32_t anchor_index = 0;
  uint32_t slice_num = tensor_slice[0].size();
  int64_t thread_offset = 0;
  for (auto const &anchor : node->GetAllInDataAnchors()) {
    if (ge::AnchorUtils::GetStatus(anchor) == ge::ANCHOR_SUSPEND) {
      anchor_index++;
      continue;
    }
    ge::GeTensorDescPtr tensor_desc_ptr = node->GetOpDesc()->MutableInputDesc(anchor->GetIdx());
    if (tensor_desc_ptr == nullptr) {
      anchor_index++;
      continue;
    }
    uint32_t data_type_size = 0;
    (void)GetDataTypeSize(tensor_desc_ptr->GetDataType(), data_type_size);
    if (anchor_index >= slice_num) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][CalcAutoThreadInput]Anchor index:%u over slice num:%u.",
                      anchor_index, slice_num);
      return FAILED;
    }
    auto &tensor_range = tensor_slice[0][anchor_index];
    auto ori_dims = tensor_desc_ptr->MutableShape().GetDims();
    if (tensor_range.size() != ori_dims.size()) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][CalcAutoThreadInput]Shape size not equal:%zu != %zu.",
                      tensor_range.size(), ori_dims.size());
      return FAILED;
    }
    if (CalcThreadOffset(tensor_range, thread_offset, data_type_size, ori_dims) != SUCCESS) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][CalcAutoThreadInput]Calculate Thread Offset failed.");
      return FAILED;
    }
    FE_LOGD("Input anchorIndex: %u, thread_offset: %d.", anchor_index, thread_offset);
    argsPara.task_addr_offset.emplace_back(thread_offset);
    anchor_index++;
  }
  return SUCCESS;
}

Status FFTSPlusFeUpdate::CalcAutoThreadOutput(const ge::NodePtr &node,
                                              vector<vector<vector<ffts::DimRange>>> &tensor_slice,
                                              AutoThreadParam &argsPara)
{
  if (tensor_slice.empty()) {
    FE_LOGE("Output tensor slice is empty.");
    return FAILED;
  }
  uint32_t anchor_index = 0;
  uint32_t slice_num = tensor_slice[0].size();
  int64_t thread_offset = 0;
  for (auto const &anchor : node->GetAllOutDataAnchors()) {
    FE_CHECK_NOTNULL(node->GetOpDesc()->GetOutputDescPtr(anchor->GetIdx()));
    ge::GeTensorDescPtr tensor_desc_ptr = node->GetOpDesc()->MutableOutputDesc(anchor->GetIdx());
    if (tensor_desc_ptr == nullptr) {
      anchor_index++;
      continue;
    }
    uint32_t data_type_size = 0;
    (void)GetDataTypeSize(tensor_desc_ptr->GetDataType(), data_type_size);
    if (anchor_index >= slice_num) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][CalcAutoThreadOutput]Anchor index:%u over slice num:%u.",
                      anchor_index, slice_num);
      return FAILED;
    }
    auto &tensor_range = tensor_slice[0][anchor_index];
    auto ori_dims = tensor_desc_ptr->MutableShape().GetDims();
    if (tensor_range.size() != ori_dims.size()) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][CalcAutoThreadOutput]Out Shape size not equal:%zu != %zu.",
                      tensor_range.size(), ori_dims.size());
      return FAILED;
    }
    if (CalcThreadOffset(tensor_range, thread_offset, data_type_size, ori_dims) != SUCCESS) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][CalcAutoThreadOutput]Calculate Thread Offset failed.");
      return FAILED;
    }
    FE_LOGD("Output Index: %u, thread_offset: %d.", anchor_index, thread_offset);
    argsPara.task_addr_offset.emplace_back(thread_offset);
    anchor_index++;
  }
  return SUCCESS;
}

Status FFTSPlusFeUpdate::GetAutoThreadParam(const ge::NodePtr &node,
                                            const vector<optiling::utils::OpRunInfo> &op_run_info,
                                            AutoThreadParam &auto_thread_param)
{
  FE_LOGD("Get node:%s AutoThreadParam.", node->GetName().c_str());
  ffts::ThreadSliceMapPtr slice_info_ptr = nullptr;
  slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(ffts::kAttrSgtStructInfo, slice_info_ptr);
  FE_CHECK_NOTNULL(slice_info_ptr);
  slice_info_ptr_ = slice_info_ptr;
  uint32_t thread_num = slice_info_ptr->slice_instance_num;
  if (thread_num == 0) {
    REPORT_FE_ERROR("thread num zero.");
    return FAILED;
  }
  Status ret = CalcAutoThreadInput(node, slice_info_ptr->input_tensor_slice, auto_thread_param);
  if (ret != SUCCESS) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FFTSPlusGetArgsParam]Calculate thread input failed.");
    return FAILED;
  }
  input_num_ = auto_thread_param.task_addr_offset.size();
  ret = CalcAutoThreadOutput(node, slice_info_ptr->output_tensor_slice, auto_thread_param);
  if (ret != SUCCESS) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FFTSPlusGetArgsParam]Calculate thread output failed.");
    return FAILED;
  }
  input_output_num_ = auto_thread_param.task_addr_offset.size();
  CalcAutoThreadWorkspace(op_run_info, node, thread_num, auto_thread_param);
  auto_thread_param.thread_dim = thread_num;
  auto_thread_param.input_output_num = input_output_num_;
  size_t args_num = auto_thread_param.task_addr_offset.size();
  task_param_offset_ = args_num * sizeof(uint64_t);
  FE_LOGD("AutoThreadParam input_output_num:%u, args_num:%zu.", input_output_num_, args_num);
  return SUCCESS;
}

// 各类context表驱动刷新
Status FFTSPlusFeUpdate::UpdateStartCtx(rtFftsPlusComCtx_t *comCtx, const rtFftsPlusTaskInfo_t &task_info,
                                        const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const
{
  FE_CHECK_NOTNULL(comCtx);
  (void)flush_data;
  (void)task_info;
  (void)node;
  rtFftsPlusAtStartCtx_t* ctx = reinterpret_cast<rtFftsPlusAtStartCtx_t*>(comCtx);
  ctx->threadDim = slice_info_ptr_->slice_instance_num;
  ctx->threadWindowSize = slice_info_ptr_->parallel_window_size;
  FE_LOGD("StartCtx thread_dim[%u] windowSize[%u].", ctx->threadDim, ctx->threadWindowSize);
  return SUCCESS;
}

Status FFTSPlusFeUpdate::UpdateLabelCtx(rtFftsPlusComCtx_t *comCtx, const rtFftsPlusTaskInfo_t &task_info,
                                        const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const
{
  FE_CHECK_NOTNULL(comCtx);
  (void)flush_data;
  (void)node;
  rtFftsPlusLabelCtx_t* ctx = reinterpret_cast<rtFftsPlusLabelCtx_t*>(comCtx);
  uint16_t thread_dim = slice_info_ptr_->slice_instance_num;
  if (ctx->successorNum != 0) {
    // if is in_label, succ_list[0] will be at-start
    uint16_t succ_ctx_id = ctx->successorList[0];
    if (succ_ctx_id >= task_info.fftsPlusSqe->totalContextNum) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateAicAivCtx]Lable context id(%u) over.", succ_ctx_id);
      return FAILED;
    }
    rtFftsPlusComCtx_t *context = (rtFftsPlusComCtx_t*)(task_info.descBuf);
    context += succ_ctx_id;
    FE_LOGD("Successor 0 CtxId(%u) label type[0x%x].", succ_ctx_id, context->contextType);
    if (context->contextType == RT_CTX_TYPE_AT_START) {
      // compile create num > runtime thread dim
      FE_LOGD("Thread_dim: %u, window_size: %u.", thread_dim, slice_info_ptr_->parallel_window_size);
      if (ctx->successorNum == slice_info_ptr_->parallel_window_size) {
        return SUCCESS;
      }
      FE_LOGI("Set label successor num (%d).", slice_info_ptr_->parallel_window_size);
      ctx->successorNum = slice_info_ptr_->parallel_window_size;
      return SUCCESS;
    }
  }
  ctx->predCnt = slice_info_ptr_->slice_instance_num;
  ctx->predCntInit = slice_info_ptr_->slice_instance_num;
  FE_LOGD("Update label predcnt %u.", ctx->predCnt);
  return SUCCESS;
}

inline void SetLow32FromSrc(uint32_t &dst, const uint64_t &src)
{
	dst = static_cast<uint32_t>(src & 0xFFFFFFFF);
	return;
}
inline void SetHigh32FromSrc(uint32_t &dst, const uint64_t &src)
{
	dst = static_cast<uint32_t>((src >> 32) & 0xFFFFFFFF);
	return;
}
inline void SetHigh16FromSrc(uint16_t &dst, const uint64_t &src)
{
	dst = static_cast<uint16_t>((src >> 32) & 0xFFFF);
	return;
}

Status FFTSPlusFeUpdate::UpdateAicAivCtx(rtFftsPlusComCtx_t *com_ctx, const rtFftsPlusTaskInfo_t &task_info,
                                         const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const
{
  FE_CHECK_NOTNULL(com_ctx);
  (void)node;
  (void)task_info;
  auto ctx = reinterpret_cast<rtFftsPlusAicAivCtx_t*>(com_ctx);
  if (flush_data.op_run_info.size() < kMinThreadNum) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateAicAivCtx]AicAivCtx run_info size(%zu) err.",
                    flush_data.op_run_info.size());
    return FAILED;
  }
  ctx->nonTailBlockdim = flush_data.op_run_info[0].GetBlockDim();
  ctx->tailBlockdim = flush_data.op_run_info[1].GetBlockDim();

  FE_LOGD("AicAivCtx nonTail_Blkdim[%d], tail_Blkdim[%d].", ctx->nonTailBlockdim, ctx->tailBlockdim);

  ctx->threadDim = slice_info_ptr_->slice_instance_num;
  uint64_t paraBase = reinterpret_cast<uintptr_t>(flush_data.args_base);

  SetLow32FromSrc(ctx->taskParamPtrBaseL, paraBase);
  SetHigh16FromSrc(ctx->taskParamPtrBaseH, paraBase);
  SetLow32FromSrc(ctx->nonTailTaskStartPcL, flush_data.aic_non_tail_task_start_pc);
  SetHigh16FromSrc(ctx->nonTailTaskStartPcH, flush_data.aic_non_tail_task_start_pc);
  SetLow32FromSrc(ctx->tailTaskStartPcL, flush_data.aic_tail_task_start_pc);
  SetHigh16FromSrc(ctx->tailTaskStartPcH, flush_data.aic_tail_task_start_pc);
  FE_LOGD("Ctx Base:0x%llx, PtrBaseL:0x%x, PtrBaseH:0x%x, PcL:0x%x, PcH:0x%x, PcLTail:0x%x, PcHTail:0x%x.", paraBase,
          ctx->taskParamPtrBaseL, ctx->taskParamPtrBaseH,
          ctx->nonTailTaskStartPcL, ctx->nonTailTaskStartPcH, ctx->tailTaskStartPcL, ctx->tailTaskStartPcH);
  ctx->icachePrefetchCnt = flush_data.aic_icache_prefetch_cnt;
  ctx->taskParamPtrOffset = task_param_offset_;
  return SUCCESS;
}

Status FFTSPlusFeUpdate::UpdateMixAicAivCtx(rtFftsPlusComCtx_t *com_ctx, const rtFftsPlusTaskInfo_t &task_info,
                                            const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const
{
  FE_CHECK_NOTNULL(com_ctx);
  (void)node;
  (void)task_info;
  auto ctx = reinterpret_cast<rtFftsPlusMixAicAivCtx_t*>(com_ctx);
  if (flush_data.op_run_info.size() < 1) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateMixAicAivCtx]MixAicAivCtx runinfo size(%zu) err.",
                    flush_data.op_run_info.size());
    return FAILED;
  }
  ctx->nonTailBlockdim = flush_data.op_run_info[0].GetBlockDim();
  ctx->tailBlockdim = flush_data.op_run_info[1].GetBlockDim();
  FE_LOGD("MixAicAivCtx nonTail_Block_dim:%d, tail_Block_dim:%d.", ctx->nonTailBlockdim, ctx->tailBlockdim);
  ctx->threadDim = slice_info_ptr_->slice_instance_num;
  uint64_t paraBase = reinterpret_cast<uintptr_t>(flush_data.args_base);
  SetLow32FromSrc(ctx->aicTaskParamPtrL, paraBase);
  SetHigh16FromSrc(ctx->aicTaskParamPtrH, paraBase);
  ctx->aivTaskParamPtrH = ctx->aicTaskParamPtrH;
  ctx->aivTaskParamPtrL = ctx->aicTaskParamPtrL;
  ctx->aicTaskParamPtrOffset = task_param_offset_;
  ctx->aivTaskParamPtrOffset = task_param_offset_;
  ctx->aicIcachePrefetchCnt = flush_data.aic_icache_prefetch_cnt;
  ctx->aivIcachePrefetchCnt = flush_data.aiv_icache_prefetch_cnt;
  SetLow32FromSrc(ctx->nonTailAicTaskStartPcL, flush_data.aic_non_tail_task_start_pc);
  SetHigh16FromSrc(ctx->nonTailAicTaskStartPcH, flush_data.aic_non_tail_task_start_pc);
  SetLow32FromSrc(ctx->tailAicTaskStartPcL, flush_data.aic_tail_task_start_pc);
  SetHigh16FromSrc(ctx->tailAicTaskStartPcH, flush_data.aic_tail_task_start_pc);
  SetLow32FromSrc(ctx->nonTailAivTaskStartPcL, flush_data.aiv_non_tail_task_start_pc);
  SetHigh16FromSrc(ctx->nonTailAivTaskStartPcH, flush_data.aiv_non_tail_task_start_pc);
  SetLow32FromSrc(ctx->tailAivTaskStartPcL, flush_data.aiv_tail_task_start_pc);
  SetHigh16FromSrc(ctx->tailAivTaskStartPcH, flush_data.aiv_tail_task_start_pc);
  return SUCCESS;
}

std::vector<uint32_t> GetIndexVec(const ge::NodePtr &node, bool is_input)
{
  vector<uint32_t> indices;
  ge::GeTensorDescPtr tensor_desc_ptr = nullptr;
  if (is_input) {
    for (const auto &input_anchor : node->GetAllInDataAnchors()) {
      if (ge::AnchorUtils::GetStatus(input_anchor) == ge::ANCHOR_SUSPEND) {
        FE_LOGD("Input anchor:%d status is SUSPEND.", input_anchor->GetIdx());
        continue;
      }
      tensor_desc_ptr = node->GetOpDesc()->MutableInputDesc(input_anchor->GetIdx());
      if (tensor_desc_ptr == nullptr) {
        FE_LOGD("Input tensor ptr is null.");
        continue;
      }
      if ((input_anchor->GetIdx() >= 0) && (input_anchor->GetIdx() < kAnchorMaxIdxNum)) {
        indices.emplace_back(input_anchor->GetIdx());
      }
    }
    return indices;
  }
  for (const auto &output_anchor : node->GetAllOutDataAnchors()) {
    tensor_desc_ptr = node->GetOpDesc()->MutableOutputDesc(output_anchor->GetIdx());
    if (tensor_desc_ptr == nullptr) {
      FE_LOGD("Output tensor ptr is null.");
      continue;
    }
    if ((output_anchor->GetIdx() >= 0) && (output_anchor->GetIdx() < kAnchorMaxIdxNum)) {
      indices.emplace_back(output_anchor->GetIdx());
    }
  }
  return indices;
}

Status FFTSPlusFeUpdate::GetInputSliceList(const ge::NodePtr &node, uint32_t index,
                                           vector<vector<ffts::DimRange>> &slice_list, ge::GeTensorDescPtr &tensor)
{
  uint32_t thread_id = 0;
  ge::NodePtr peer_out_node;
  int peer_index;
  if (fe::GetPeerNodeAndOutIdx(node, index, peer_out_node, peer_index) != SUCCESS) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetInputSliceList]Get peer node failed.");
    return FAILED;
  }
  FE_LOGI("Peer node[%s], index[%d].", peer_out_node->GetName().c_str(), peer_index);
  auto& input_tensor_slice = slice_info_ptr_->input_tensor_slice;
  if (input_tensor_slice.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetInputSliceList]Input tensor slice size err.");
    return FAILED;
  }
  size_t slice_size = input_tensor_slice.at(thread_id).size();
  if (slice_size <= static_cast<size_t>(index)) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetInputSliceList]Size of input thread %u of node %s is %zu <= %d.",
                    thread_id, node->GetName().c_str(), slice_size, index);
    return FAILED;
  }
  tensor = peer_out_node->GetOpDesc()->MutableOutputDesc(static_cast<uint32_t>(peer_index));
  slice_list.emplace_back(input_tensor_slice.at(thread_id)[index]);
  thread_id = input_tensor_slice.size() - 1;
  slice_size = input_tensor_slice.at(thread_id).size();
  if (slice_size <= static_cast<size_t>(index)) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetInputSliceList]Input thread %u size of node %s is %zu, <= %d.",
                    thread_id, node->GetName().c_str(), slice_size, index);
    return FAILED;
  }
  slice_list.emplace_back(input_tensor_slice.at(thread_id)[index]);
  return SUCCESS;
}

Status FFTSPlusFeUpdate::GetOutputSliceList(const ge::NodePtr &node, uint32_t index,
                                            vector<std::vector<ffts::DimRange>> &slice_list,
                                            ge::GeTensorDescPtr &tensor)
{
  uint32_t thread_id = 0;
  auto& output_tensor_slice = slice_info_ptr_->output_tensor_slice;
  if (output_tensor_slice.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetOutputSliceList]Output tensor slice size err.");
    return FAILED;
  }
  size_t slice_size = output_tensor_slice.at(thread_id).size();
  if (slice_size <= static_cast<size_t>(index)) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetOutputSliceList]Output thread %u size of node %s is %zu, <= %d",
                    thread_id, node->GetName().c_str(), slice_size, index);
    return FAILED;
  }
  tensor = node->GetOpDesc()->MutableOutputDesc(static_cast<uint32_t>(index));
  slice_list.emplace_back(output_tensor_slice.at(thread_id)[index]);
  thread_id = output_tensor_slice.size() - 1;
  slice_size = output_tensor_slice.at(thread_id).size();
  if (slice_size <= static_cast<size_t>(index)) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GetOutputSliceList]Size of out thread %u of node %s is %zu <= %d.",
                    thread_id, node->GetName().c_str(), slice_size, index);
    return FAILED;
  }
  slice_list.emplace_back(output_tensor_slice.at(thread_id)[index]);
  return SUCCESS;
}

Status FFTSPlusFeUpdate::GenerateDataCmoParam(const ge::NodePtr &node, int real_index, uint32_t index, bool is_input,
                                              std::vector<DataContextParam> &data_param)
{
  ge::GeTensorDescPtr tensor = nullptr;
  vector<std::vector<ffts::DimRange>> slice_list;
  FE_CHECK_NOTNULL(slice_info_ptr_);
  data_param.clear();
  Status ret;
  if (is_input) {
    ret = GetInputSliceList(node, index, slice_list, tensor);
  } else {
    ret = GetOutputSliceList(node, index, slice_list, tensor);
  }
  if (ret != SUCCESS || slice_list.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GenerateDataCmoParam]Get %s slice list err.",
                    is_input ? "inout" : "output");
    return FAILED;
  }
  FE_CHECK_NOTNULL(tensor);
  std::vector<DataContextParam> data_ctx;
  ret = MemorySlice::GenerateDataCtxParam(tensor, slice_list[0], BURST_LEN, data_ctx, real_index);
  if (ret != SUCCESS || data_ctx.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GenerateDataCmoParam]Generate notail failed.");
    return FAILED;
  }
  data_param.emplace_back(data_ctx[0]);
  if (slice_info_ptr_->slice_instance_num < kMinThreadNum) {
    return SUCCESS;
  }
  data_ctx.clear();
  ret = MemorySlice::GenerateDataCtxParam(tensor, slice_list[slice_list.size() - 1], BURST_LEN, data_ctx, real_index);
  if (ret != SUCCESS || data_ctx.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][GenerateDataCmoParam]Generate notail failed.");
    return FAILED;
  }
  data_param.emplace_back(data_ctx[0]);
  return SUCCESS;
}

inline void FillDataCtxDmu(rtFftsPlusDataCtx_t* ctx, const DataContextParam& data_para,
                           const DataContextParam& tail_data_para)
{
  ctx->nonTailLengthInner = data_para.len_inner;
  ctx->nonTailNumInner = data_para.num_inner;
  ctx->nonTailNumOutter = data_para.num_outter;
  ctx->nonTailStrideInner = data_para.stride_inner;
  ctx->nonTailStrideOutter = data_para.stride_outter;
  ctx->tailLengthInner = tail_data_para.len_inner;
  ctx->tailNumInner = tail_data_para.num_inner;
  ctx->tailNumOutter = tail_data_para.num_outter;
  ctx->tailStrideInner = tail_data_para.stride_inner;
  ctx->tailStrideOutter = tail_data_para.stride_outter;
  return;
}

Status FFTSPlusFeUpdate::FillPrefetchCtxParam(rtFftsPlusDataCtx_t* ctx,
                                              const AutoThreadSubTaskFlush &flush_data, size_t index) const
{
  if (data_param_vec_[index].size() == 0) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FillPrefetchCtxParam]Data size0(%zu) err.",
                    data_param_vec_[index].size());
    return FAILED;
  }
  const auto &data_para = data_param_vec_[index][0];
  size_t  tmp = data_param_vec_[index].size() -1;
  const auto &tail_data_para = data_param_vec_[index][tmp];
  FillDataCtxDmu(ctx, data_para, tail_data_para);
  if (data_para.index >= input_num_) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FillPrefetchCtxParam]Index(%u) over input_num(%u).",
                    data_para.index, input_num_);
    return FAILED;
  }
  if (data_para.index >= flush_data.input_addr_base.size()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FillPrefetchCtxParam]Index(%u) over addrs(%zu).",
                    data_para.index, flush_data.input_addr_base.size());
    return FAILED;
  }
  uint64_t para_base = flush_data.input_addr_base[data_para.index];
  SetLow32FromSrc(ctx->addressBaseL, para_base);
  SetHigh32FromSrc(ctx->addressBaseH, para_base);
  uint32_t offset = tail_data_para.base_addr_offset;
  if (slice_info_ptr_->slice_instance_num > 1) {
    offset = tail_data_para.base_addr_offset / (slice_info_ptr_->slice_instance_num - 1);
  }
  ctx->addressOffset = offset;
  ctx->threadDim = slice_info_ptr_->slice_instance_num;
  FE_LOGD("Prefetch DataCtx index[%u], base(0x%x), Offset[%u], oriOffset:%u.", data_para.index, para_base,
          offset, tail_data_para.base_addr_offset);
  return SUCCESS;
}

Status FFTSPlusFeUpdate::FillInvAndWriCtxParam(rtFftsPlusDataCtx_t* ctx,
                                               const AutoThreadSubTaskFlush &flush_data, size_t index) const
{
  if (data_param_vec_[index].size() == 0) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FillInvAndWriCtxParam]Data size err.");
    return FAILED;
  }
  const auto &data_para = data_param_vec_[index][0];
  size_t  tmp = data_param_vec_[index].size() -1;
  const auto &tail_data_para = data_param_vec_[index][tmp];
  FillDataCtxDmu(ctx, data_para, tail_data_para);
  // output_offset = input_size + index
  uint32_t out_num = input_output_num_ - input_num_;
  FE_LOGI("out_num(%u), outIndex(%u), total_num(%u).", out_num, data_para.index, input_output_num_);
  if (data_para.index >= out_num) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FillInvAndWriCtxParam]Output index over.");
    return FAILED;
  }
  if (data_para.index >= flush_data.output_addr_base.size()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][FillPrefetchCtxParam]Index(%u) over addrs(%zu).",
                    data_para.index, flush_data.output_addr_base.size());
    return FAILED;
  }
  uint64_t para_base = flush_data.output_addr_base[data_para.index];
  SetLow32FromSrc(ctx->addressBaseL, para_base);
  SetHigh32FromSrc(ctx->addressBaseH, para_base);
  ctx->addressOffset = data_para.base_addr_offset;
  ctx->threadDim = slice_info_ptr_->slice_instance_num;
  FE_LOGD("Invalid DataCtx addressOffset[%u], paraBase(0x%x).", data_para.base_addr_offset, para_base);
  return SUCCESS;
}

void GetNodeCMOCtxList(ge::OpDescPtr op_desc, int type, std::vector<std::vector<int64_t>> &ctx_id_vec)
{
	if (op_desc == nullptr) {
	  return;
	}
	if (type == RT_CTX_TYPE_FLUSH_DATA) {
	  (void)ge::AttrUtils::GetListListInt(op_desc, "_data_prefetch_ctx_id_list", ctx_id_vec);
	} else if (type == RT_CTX_TYPE_INVALIDATE_DATA) {
	  (void)ge::AttrUtils::GetListListInt(op_desc, "_invalid_ctx_id_list", ctx_id_vec);
	} else if (type == RT_CTX_TYPE_WRITEBACK_DATA) {
	  (void)ge::AttrUtils::GetListListInt(op_desc, "_write_back_ctx_id_list", ctx_id_vec);
	}
	FE_LOGD("Get CMO type(%d) context list num(%zu).", type, ctx_id_vec.size());
	return;
}

/*
 * |                thread0              |               thread1               |              thread2                |
 * |                OP_Ctx_0             |               OP_Ctx_1              |              OP_Ctx_2               |
 * | Data_Ctx_00,Data_Ctx_10,Data_Ctx_20 | Data_Ctx_01,Data_Ctx_11,Data_Ctx_21 |  Data_Ctx_02,Data_Ctx_12,Data_Ctx_22|
 * */
Status FFTSPlusFeUpdate::UpdateCmoCtxProc(const rtFftsPlusTaskInfo_t &task_info, const ge::NodePtr &node,
                                          const AutoThreadSubTaskFlush &flush_data, int type)
{
  auto op_desc = node->GetOpDesc();
  uint16_t window_size = slice_info_ptr_->parallel_window_size;
  size_t data_num = data_param_vec_.size();
  std::vector<std::vector<int64_t>> ctx_id_vec;
  GetNodeCMOCtxList(op_desc, type, ctx_id_vec);
  FE_LOGD("Context num(%zu), param_size(%zu).", ctx_id_vec.size(), data_num);
  if (ctx_id_vec.empty()) {
    return SUCCESS;
  }
  rtFftsPlusComCtx_t *context_head = (rtFftsPlusComCtx_t*)(task_info.descBuf);
  rtFftsPlusComCtx_t *com_ctx = nullptr;
  uint16_t ctx_Id;
  Status ret = FAILED;
  uint16_t real_size = window_size <= slice_info_ptr_->slice_instance_num ?
                       window_size : slice_info_ptr_->slice_instance_num;
  if (ctx_id_vec.size() < real_size) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateCmoCtxProc]Data context num less than windowsize.");
    return FAILED;
  }
  for (size_t i = 0; i < data_num; ++i) {
    for (uint16_t j = 0; j < real_size; ++j) {
      ctx_Id = ctx_id_vec[j][i];
      FE_LOGD("Data ctxId(%u).", ctx_Id);
      if (ctx_Id >= task_info.fftsPlusSqe->totalContextNum) {
        REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateCmoCtxProc]Data(%u) ctx Id err.", ctx_Id);
        return FAILED;
      }
      com_ctx = context_head + ctx_Id;
      auto data_ctx = reinterpret_cast<rtFftsPlusDataCtx_t*>(com_ctx);
      if (data_ctx->contextType == RT_CTX_TYPE_FLUSH_DATA) {
        ret = FillPrefetchCtxParam(data_ctx, flush_data, i);
      } else if (data_ctx->contextType == RT_CTX_TYPE_INVALIDATE_DATA ||
             data_ctx->contextType == RT_CTX_TYPE_WRITEBACK_DATA){
        ret = FillInvAndWriCtxParam(data_ctx, flush_data, i);
      }
      if (ret != SUCCESS) {
        REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateCmoCtxProc]Fill CTX type(%d) Index(%zu) thread(%u) err.",
                        data_ctx->contextType, i, j);
        return FAILED;
      }
    }
  }
  return SUCCESS;
}

Status FFTSPlusFeUpdate::UpdateCmoProc(const rtFftsPlusTaskInfo_t &task_info, const ge::NodePtr &node,
                                       const AutoThreadSubTaskFlush &flush_data)
{
  ge::OpDescPtr op_desc = node->GetOpDesc();
  for (size_t i = 0; i < g_cmo_operation_info.size(); ++i) {
    const auto &operation = g_cmo_operation_info.at((CACHE_OPERATION)i);
    string bm_name = std::get<BIT_MAP_OP>(operation);
    rtFftsPlusContextType_t hw_type = std::get<CTX_TYPE_OP>(operation);
    bool is_input = std::get<INPUT_FLAG_OP>(operation);
    int64_t bit_map = 0;
    if (!ge::AttrUtils::GetInt(op_desc, bm_name, bit_map) || bit_map == 0) {
      FE_LOGD("Node %s does not need type %d context.", node->GetName().c_str(), hw_type);
      continue;
    }
    auto index_vec = GetIndexVec(node, is_input);
    FE_LOGD("Get %s's index num %zu.", is_input == true ? "input" : "output", index_vec.size());
    uint64_t curr_bm = 0;
    int real_index = 0;
    Status ret;
    data_param_vec_.clear();
    std::vector<DataContextParam> data_param;
    for (auto index : index_vec) {
      curr_bm = 0;
      SetBitOne(static_cast<uint32_t>(index), curr_bm);
      if (!(bit_map & curr_bm)) {
        continue;
      }
      data_param.clear();
      // one to one
      ret = GenerateDataCmoParam(node, real_index, index, is_input, data_param);
      if (ret != SUCCESS) {
        FE_LOGW("[Executor][FFTS_plus_update][UpdateCmoProc]Calculate data param err.");
        return SUCCESS;
      }
      real_index++;
      data_param_vec_.emplace_back(data_param);
      FE_LOGD("data_param_vec_.size(%zu).", data_param_vec_.size());
      // get prefetch max num
      if (is_input && data_param_vec_.size() == MAX_PREFETCH_NUM) {
        break;
      }
    }
    if (UpdateCmoCtxProc(task_info, node, flush_data, hw_type) != SUCCESS) {
      FE_LOGW("[Executor][FFTS_plus_update][UpdateCmoProc]Update data context err.");
      return SUCCESS;
    }
  }
  return SUCCESS;
}

Status FFTSPlusFeUpdate::UpdateContextByType(const ge::NodePtr node, const AutoThreadSubTaskFlush &flush_data,
                                             const rtFftsPlusTaskInfo_t &task_info, const vector<int32_t> &ctx_id_vec)
{
  void *ctx_buf = const_cast<void*>(task_info.descBuf);
  rtFftsPlusComCtx_t *context_head = reinterpret_cast<rtFftsPlusComCtx_t*>(ctx_buf);
  rtFftsPlusComCtx_t *context = nullptr;
  for (auto idx = ctx_id_vec.begin(); idx != ctx_id_vec.end(); ++idx) {
    if ((*idx) > task_info.fftsPlusSqe->totalContextNum) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateSubtaskAndCache]Context Id(%d) over err.", (*idx));
      return FAILED;
    }
    context = context_head + (*idx);
    FE_LOGD("Context Id[%d] , type[0x%x].", *idx, context->contextType);
    auto itr = func_tbl_.find(context->contextType);
    if (itr == func_tbl_.end()) {
      continue;
    }
    if (((*this).*(itr->second))(context, task_info, flush_data, node) != SUCCESS) {
      REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateSubtaskAndCache]Update context failed.");
      return FAILED;
    }
  }
  return SUCCESS;
}

Status FFTSPlusFeUpdate::UpdateMixL2AicAivCtx(const rtFftsPlusTaskInfo_t &task_info,
                                              const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const
{
  void *ctx_buf = const_cast<void*>(task_info.descBuf);
  rtFftsPlusComCtx_t *context_head = reinterpret_cast<rtFftsPlusComCtx_t*>(ctx_buf);
  rtFftsPlusMixAicAivCtx_t *ctx = nullptr;
  uint32_t contextId;
  (void)ge::AttrUtils::GetInt(node->GetOpDesc(), kContextId, contextId);
  if (contextId > task_info.fftsPlusSqe->totalContextNum) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateMixL2AicAivCtx]Context Id(%d) over err.", contextId);
    return FAILED;
  }
  ctx = reinterpret_cast<rtFftsPlusMixAicAivCtx_t*>(context_head + contextId);
  FE_CHECK_NOTNULL(ctx);
  if (flush_data.op_run_info.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateMixL2AicAivCtx]MixAicAivCtx runinfo size(%zu) err.",
                    flush_data.op_run_info.size());
    return FAILED;
  }
  ctx->nonTailBlockdim = flush_data.op_run_info[0].GetBlockDim();
  ctx->tailBlockdim = flush_data.op_run_info[0].GetBlockDim();
  FE_LOGD("MixL2AicAivCtx nonTail_Block_dim:%d, tail_Block_dim:%d", ctx->nonTailBlockdim, ctx->tailBlockdim);
  uint64_t paraBase = reinterpret_cast<uintptr_t>(flush_data.args_base);
  SetLow32FromSrc(ctx->aicTaskParamPtrL, paraBase);
  SetHigh16FromSrc(ctx->aicTaskParamPtrH, paraBase);
  ctx->aicTaskParamPtrOffset = task_param_offset_;
  ctx->aivTaskParamPtrH = ctx->aicTaskParamPtrH;
  ctx->aivTaskParamPtrL = ctx->aicTaskParamPtrL;
  ctx->aivTaskParamPtrOffset = task_param_offset_;
  ctx->aicIcachePrefetchCnt = flush_data.aic_icache_prefetch_cnt;
  SetLow32FromSrc(ctx->nonTailAicTaskStartPcL, flush_data.aic_non_tail_task_start_pc);
  SetHigh16FromSrc(ctx->nonTailAicTaskStartPcH, flush_data.aic_non_tail_task_start_pc);
  SetLow32FromSrc(ctx->tailAicTaskStartPcL, flush_data.aic_tail_task_start_pc);
  SetHigh16FromSrc(ctx->tailAicTaskStartPcH, flush_data.aic_tail_task_start_pc);
  ctx->aivIcachePrefetchCnt = flush_data.aiv_icache_prefetch_cnt;
  SetLow32FromSrc(ctx->nonTailAivTaskStartPcL, flush_data.aiv_non_tail_task_start_pc);
  SetHigh16FromSrc(ctx->nonTailAivTaskStartPcH, flush_data.aiv_non_tail_task_start_pc);
  SetLow32FromSrc(ctx->tailAivTaskStartPcL, flush_data.aiv_tail_task_start_pc);
  SetHigh16FromSrc(ctx->tailAivTaskStartPcH, flush_data.aiv_tail_task_start_pc);
  return SUCCESS;
}

Status FFTSPlusFeUpdate::UpdateSubTaskAndCache(const ge::NodePtr &node, const AutoThreadSubTaskFlush &flush_data,
                                               rtFftsPlusTaskInfo_t &task_info)
{
  FE_CHECK_NOTNULL(node);
  ge::OpDescPtr op_desc = node->GetOpDesc();
  if (op_desc->HasAttr(ATTR_NAME_FFTS_PLUS_MIX_L2)) {
    return UpdateMixL2AicAivCtx(task_info, flush_data, node);
  }
  FE_CHECK_NOTNULL(task_info.descBuf);
  FE_CHECK_NOTNULL(slice_info_ptr_);
  FE_LOGI("FFTS plus update, node name(%s), ctxNum(%u), mode(%u), threadDim(%u), windowSize(%u).",
          node->GetName().c_str(), task_info.fftsPlusSqe->totalContextNum, slice_info_ptr_->thread_mode,
          slice_info_ptr_->slice_instance_num, slice_info_ptr_->parallel_window_size);

  // get node contextId list
  std::vector<int32_t> ctx_id_vec;
  (void)ge::AttrUtils::GetListInt(op_desc, kAutoCtxIdList, ctx_id_vec);
  if (ctx_id_vec.empty()) {
    REPORT_FE_ERROR("[Executor][FFTS_plus_update][UpdateSubtaskAndCache]Node context list empty.");
    return FAILED;
  }
  if (UpdateCmoProc(task_info, node, flush_data) != SUCCESS) {
    FE_LOGW("[Executor][FFTS_plus_update][UpdateSubtaskAndCache]Update CMO fail.");
  }
  if (UpdateContextByType(node, flush_data, task_info, ctx_id_vec) != SUCCESS) {
    return FAILED;
  }
  // get nop contextId list
  std::vector<int32_t> ext_id_vec;
  (void)ge::AttrUtils::GetListInt(op_desc, "_all_ctx_id_list", ext_id_vec);
  FE_LOGD("Context list size[%zu] , ext size[%zu].", ctx_id_vec.size(), ext_id_vec.size());
  if (!ext_id_vec.empty()) {
    if (UpdateContextByType(node, flush_data, task_info, ext_id_vec) != SUCCESS) {
      return FAILED;
    }
  }
  FE_LOGD("FFTS+ node context update success.");
  return SUCCESS;
}
}