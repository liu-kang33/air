/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef FFTS_PLUS_FFTS_PLUS_UPDATE_H_
#define FFTS_PLUS_FFTS_PLUS_UPDATE_H_
#include "register/ffts_plus_task_update.h"
#include "register/ffts_plus_update_manager.h"
#include "graph/utils/graph_utils.h"
#include "graph/utils/tensor_utils.h"
#include "common/sgt_slice_type.h"
#include "common/fe_log.h"
#include "common/fe_error_code.h"
#include "common/optimizer/graph_optimizer_types.h"
#include "common/memory_slice.h"
#include "common/ffts_plus_type.h"
#include "runtime/rt_ffts_plus_define.h"
#include "runtime/rt_ffts_plus.h"

namespace fe {

  const int MAX_PREFETCH_NUM = 4;
  const int64_t BURST_LEN = 0xFFFFFF;
  using ge::AutoThreadParam;
  using ge::AutoThreadSubTaskFlush;

class FFTSPlusFeUpdate : public ge::FFTSPlusTaskUpdate {
 public:
  FFTSPlusFeUpdate();
  ~FFTSPlusFeUpdate() override;
  // Interface to calculate auto thread param
  Status GetAutoThreadParam(const ge::NodePtr &node, const vector<optiling::utils::OpRunInfo> &op_run_info,
                            AutoThreadParam &auto_thread_param) override;
  // Interface to update AICore context
  Status UpdateSubTaskAndCache(const ge::NodePtr &node, const AutoThreadSubTaskFlush &flush_data,
                               rtFftsPlusTaskInfo_t &task_info) override;
 private:
  Status CalcAutoThreadInput(const ge::NodePtr &node, vector<vector<vector<ffts::DimRange>>> &tensor_slice,
                             AutoThreadParam &argsPara);
  Status CalcAutoThreadOutput(const ge::NodePtr &node, vector<vector<vector<ffts::DimRange>>> &tensor_slice,
                              AutoThreadParam &argsPara);
  Status CalcAutoThreadWorkspace(const vector<optiling::utils::OpRunInfo> &op_run_info, ge::NodePtr node,
                                 uint32_t thread_num, AutoThreadParam &args_para);

  // update context function
  Status UpdateStartCtx(rtFftsPlusComCtx_t *comCtx, const rtFftsPlusTaskInfo_t &task_info,
                        const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const;
  Status UpdateLabelCtx(rtFftsPlusComCtx_t *comCtx, const rtFftsPlusTaskInfo_t &task_info,
                        const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const;
  Status UpdateAicAivCtx(rtFftsPlusComCtx_t *com_ctx, const rtFftsPlusTaskInfo_t &task_info,
                         const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const;
  Status UpdateMixAicAivCtx(rtFftsPlusComCtx_t *com_ctx, const rtFftsPlusTaskInfo_t &task_info,
                            const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const;
  Status UpdateMixL2AicAivCtx(const rtFftsPlusTaskInfo_t &task_info,
                              const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const;
  Status UpdateContextByType(const ge::NodePtr node, const AutoThreadSubTaskFlush &flush_data,
                             const rtFftsPlusTaskInfo_t &task_info, const vector<int32_t> &ctx_id_vec);

  Status FillInvAndWriCtxParam(rtFftsPlusDataCtx_t* ctx, const AutoThreadSubTaskFlush &flush_data,
                               size_t index) const;
	Status FillPrefetchCtxParam(rtFftsPlusDataCtx_t* ctx, const AutoThreadSubTaskFlush &flush_data, size_t index) const;
  Status UpdateCmoCtxProc(const rtFftsPlusTaskInfo_t &task_info, const ge::NodePtr &node,
                          const AutoThreadSubTaskFlush &flush_data, int type);
  Status UpdateCmoProc(const rtFftsPlusTaskInfo_t &task_info, const ge::NodePtr &node,
                       const AutoThreadSubTaskFlush &flush_data);
  Status GetInputSliceList(const ge::NodePtr &node, uint32_t index, vector<std::vector<ffts::DimRange>> &slice_list,
                           ge::GeTensorDescPtr &tensor);
  Status GetOutputSliceList(const ge::NodePtr &node, uint32_t index, vector<std::vector<ffts::DimRange>> &slice_list,
                            ge::GeTensorDescPtr &tensor);
  Status GenerateDataCmoParam(const ge::NodePtr &node, int real_index, uint32_t index, bool is_input,
                              std::vector<DataContextParam> &data_ctx);
  uint32_t input_num_;
  uint32_t input_output_num_;
  uint64_t task_param_offset_;
  ffts::ThreadSliceMapPtr slice_info_ptr_;
  std::vector<std::vector<DataContextParam>> data_param_vec_;

  using procFunc = Status(FFTSPlusFeUpdate::*)(rtFftsPlusComCtx_t *ctx, const rtFftsPlusTaskInfo_t &task_info,
                                               const AutoThreadSubTaskFlush &flush_data, const ge::NodePtr node) const;
  static std::map<uint32_t, procFunc> func_tbl_;
};

}

#endif
