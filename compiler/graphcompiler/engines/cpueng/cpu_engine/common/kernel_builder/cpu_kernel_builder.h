/*
 * Copyright 2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AICPU_CPU_KERNEL_BUILDER_H
#define AICPU_CPU_KERNEL_BUILDER_H

#include "common/aicpu_ops_kernel_builder/kernel_builder.h"

namespace aicpu {
class CpuKernelBuilder : public KernelBuilder {
public:
  /**
   * constructor
   * @param void
   */
  CpuKernelBuilder() = default;
  /**
   * Destructor
   */
  virtual ~CpuKernelBuilder() = default;

  /**
   * Calc the running size of Operator,then GE will alloc the memsize from
   * runtime The size is consist of the part as follow: 1.kernel so
   * name; 2.kernel func name; 3.Kernel param.
   * @param node Node information, return task_memsize in node's attr
   * @return status whether this operation successful
   */
  ge::Status CalcOpRunningParam(const ge::Node &node) const override;

  /**
   * Copy the data from host to device, then address is alloced by GE, then
   * invoked the runtime's interface to generate the task
   * @param node Node information
   * @param run_context
   * @return status whether operation successful
   */
  ge::Status GenerateTask(const ge::Node &node,
                          const ge::RunContext &run_context,
                          std::vector<domi::TaskDef> &tasks) override;

  ge::Status GenerateMemCopyTask(uint64_t data_info_size,
                                 const ge::RunContext &run_context,
                                 std::vector<domi::TaskDef> &tasks);
  ge::Status BuildMemCopyInfo(const ge::OpDescPtr &op_desc_ptr,
                              const ge::RunContext &run_context,
                              domi::KernelDef *&kernel_def) const;

private:
  /**
   * Build the struct AicpuKernelFixedParam, then launch the task
   * @param node Op description 
   * @param run_context GE's run_context
   * @param kernel_def Kernel task info
   * @return status whether operation successful
   */
  ge::Status BuildAndLaunchKernel(const ge::Node &node,
                                  const ge::RunContext &run_context,
                                  domi::KernelDef *&kernel_def) const;

  /**
   * Make aicpu kernel task extend info
   * @param op_desc_ptr Ge op description pointer
   * @param task_ext_info task extend info
   * @return whether handle success
   */
  ge::Status MakeAicpuKernelExtInfo(const ge::OpDescPtr &op_desc_ptr,
                                    std::vector<char> &task_ext_info,
                                    const FftsPlusInfo &ffts_info) const;

  /**
   * Set attr queue resource
   * @param node_name Ge op mode name
   * @param op_desc_ptr op desc
   * @param resource_list attr result
   * @return whether handle success
   */
  ge::Status SetAttrQueueResource(
      const std::string &node_name, std::shared_ptr<ge::OpDesc> &op_desc_ptr,
      std::vector<ge::GeAttrValue::NAMED_ATTRS> &resource_list) const;

  /**
   * Generate ffts plus task node info
   * @param node Ge node description
   * @return status whether handle success
   */
  ge::Status GenerateFftsPlusTask(const ge::Node &node);

  /**
   * get ffts info from sgt and node info
   * @param op_desc_ptr Ge node description
   * @param ffts_info ffts info
   */
  ge::Status BuildFftsInfo(const ge::OpDescPtr &op_desc_ptr,
                           FftsPlusInfo &ffts_info) const;

  /**
   * build aicpu ctx for fftsdef
   * @param op_desc_ptr Ge node description
   * @param ffts_info ffts info
   * @param ctx aicpu ctx for fftsdef
   * @return status whether handle success
   */
  ge::Status BuildAiCpuCtx(const ge::OpDescPtr &op_desc_ptr,
                           const FftsPlusInfo &ffts_info,
                           domi::FftsPlusAicpuCtxDef *ctx) const;

  /**
   * build aicpu kerneldef for ffts
   * @param node Ge node description
   * @param ffts_info ffts info
   * @param aicpu_ctx aicpu ctx for fftsdef
   * @return status whether handle success
   */
  ge::Status BuildAiCpuFftsKernelDef(
      const ge::Node &node, FftsPlusInfo &ffts_info,
      domi::FftsPlusAicpuCtxDef *aicpu_ctx) const;
  /**
   * Set attr resource
   * @param node_name Ge op mode name
   * @param op_desc_ptr op desc
   * @return whether handle success
   */
  ge::Status SetAttrResource(const std::string &node_name,
                             std::shared_ptr<ge::OpDesc> &op_desc_ptr) const;
  uint32_t GetOpBlockDim(const ge::OpDescPtr &op_desc_ptr) const;
  int64_t CeilDivisor(const int64_t x, const int64_t base) const;
  uint32_t GetOpBlockDimForFftsPlus(const ge::OpDescPtr &op_desc_ptr,
                                    const FftsPlusInfo &ffts_info,
                                    const uint32_t thread_index) const;
  bool IsSupportBlockDim(const ge::OpDescPtr &op_desc_ptr) const;
  uint32_t CalcBlockDimByShapeSize(const int64_t total) const;
};
}  // namespace aicpu
#endif  // AICPU_CPU_KERNEL_BUILDER_H
