/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fftsplus_ops_kernel_builder.h"
#include "fftsplus_task_builder.h"
#include <runtime/rt.h>
#include "inc/ffts_log.h"
#include "common/sgt_slice_type.h"
#include "inc/ffts_type.h"
#include "inc/ffts_utils.h"
#include "graph/utils/attr_utils.h"
#include "inc/graph/debug/ge_attr_define.h"
#include "param_calculate/tensorsize_calculator.h"
#include "register/ops_kernel_builder_registry.h"

namespace ffts {
REGISTER_OPS_KERNEL_BUILDER(kFFTSPlusCoreName, FFTSPlusOpsKernelBuilder);

FFTSPlusOpsKernelBuilder::FFTSPlusOpsKernelBuilder() {}

FFTSPlusOpsKernelBuilder::~FFTSPlusOpsKernelBuilder() {}

Status FFTSPlusOpsKernelBuilder::Initialize(const std::map<std::string, std::string> &options) {
  FFTS_MAKE_SHARED(manual_thread_task_builder_ptr_ =
          std::make_shared<ManualTheadTaskBuilder>(), return FAILED);
  FFTS_MAKE_SHARED(auto_thread_task_builder_ptr_ =
          std::make_shared<AutoTheadTaskBuilder>(), return FAILED);
  FFTS_MAKE_SHARED(dynamic_thread_task_builder_ptr_ =
          std::make_shared<DynamicTheadTaskBuilder>(), return FAILED);
  return SUCCESS;
}

Status FFTSPlusOpsKernelBuilder::Finalize() {return SUCCESS;}

Status FFTSPlusOpsKernelBuilder::CalcOpRunningParam(ge::Node &node) {
  return SUCCESS;
}

Status FFTSPlusOpsKernelBuilder::GenerateTask(const ge::Node &node, ge::RunContext &context,
                                              std::vector<domi::TaskDef> &task_defs) {
  FFTS_LOGD("FFTSPlusOpsKernelBuilder Gen Task node name:%s, type:%s",node.GetName().c_str(), node.GetType().c_str());
  ge::OpDescPtr op_desc = node.GetOpDesc();
  std::string sub_graph_name = op_desc->GetSubgraphInstanceName(0);
  if (sub_graph_name.empty()) {
    return FAILED;
  }
  auto ai_graph = node.GetOwnerComputeGraph();
  FFTS_CHECK_NOTNULL(ai_graph);
  auto root_graph = ge::GraphUtils::FindRootGraph(ai_graph);
  FFTS_CHECK_NOTNULL(root_graph);
  auto sgt_graph = root_graph->GetSubgraph(sub_graph_name);
  FFTS_CHECK_NOTNULL(sgt_graph);

  uint64_t ready_context_num = 0;
  uint64_t total_context_number = 0;
  domi::TaskDef task_def;
  Status status = GenPersistentContext(node, ready_context_num, total_context_number, task_def);
  if (status != SUCCESS) {
    return status;
  }

  TheadTaskBuilderPtr base_mode_ptr = GetFftsPlusMode(*sgt_graph);
  if (base_mode_ptr->Initialize() != SUCCESS) {
    return FAILED;
  }

  std::vector<ge::NodePtr> sub_graph_nodes;
  status = base_mode_ptr->GenFftsPlusContextId(*sgt_graph, sub_graph_nodes, ready_context_num, total_context_number);
  if (status != SUCCESS) {
    return status;
  }

  FFTS_LOGD("FFTSPlusOpsKernelBuilder GenerateTask node name:%s, node type:%s, readynum:%lu, totalnumber:%lu",
            node.GetName().c_str(), node.GetType().c_str(), ready_context_num, total_context_number);

  status = base_mode_ptr->GenSubGraphTaskDef(sub_graph_nodes, task_def);
  if (status != SUCCESS) {
    FFTS_LOGD("GenSubGraphTaskDef failed, node name:%s, node type:%s, errno:%u.",
              node.GetName().c_str(), node.GetType().c_str(), status);
    return status;
  }

  if (GenSubGraphSqeDef(task_def, ready_context_num, total_context_number) != SUCCESS) {
    FFTS_LOGD("GenSubGraphSqeDef failed, node name:%s, node type:%s, errno:%u.",
              node.GetName().c_str(), node.GetType().c_str(), status);
    return FAILED;
  }
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  ffts_plus_task_def->set_op_index(op_desc->GetId());
  task_defs.push_back(task_def);
  FFTS_LOGD("FFTSPlusOpsKernelBuilder GenerateTask node name:%s, node type:%s , id:%ld success",
            node.GetName().c_str(), node.GetType().c_str(), op_desc->GetId());
  return SUCCESS;
}

Status FFTSPlusOpsKernelBuilder::GenPersistentContext(const ge::Node &node, uint64_t &ready_context_num,
                                                      uint64_t &total_context_number, domi::TaskDef &task_def) {
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  CachePersistTaskBuilder cp;
  if (cp.GenContextDef(node, ffts_plus_task_def) == SUCCESS) {
    ready_context_num++;
    total_context_number++;
  }
  return SUCCESS;
}

TheadTaskBuilderPtr FFTSPlusOpsKernelBuilder::GetFftsPlusMode(const ge::ComputeGraph &sgt_graph) {
  ModeType mode_type = ModeType::MANUAL_MODE_TYPE;

  for (const auto &node : sgt_graph.GetDirectNode()) {
    if (IsUnKnownShapeOp(*(node->GetOpDesc().get()))) {
      // Dynamic shape, auto thread
      mode_type = ModeType::DYNAMIC_MODE_TYPE;
      break;
    } else {
      ThreadSliceMapPtr slice_info_ptr = nullptr;
      slice_info_ptr = node->GetOpDesc()->TryGetExtAttr(kAttrSgtStructInfo, slice_info_ptr);
      if (slice_info_ptr != nullptr && slice_info_ptr->thread_mode == 1) {
        mode_type = ModeType::AUTO_MODE_TYPE;
        break;
      }
    }
  }

  switch (mode_type) {
    case ModeType::MANUAL_MODE_TYPE:
      return manual_thread_task_builder_ptr_;
    case ModeType::AUTO_MODE_TYPE:
      return auto_thread_task_builder_ptr_;
    case ModeType::DYNAMIC_MODE_TYPE:
      return dynamic_thread_task_builder_ptr_;
    default:
      return nullptr;
  }
}

Status FFTSPlusOpsKernelBuilder::GenSubGraphSqeDef(domi::TaskDef &task_def,
                                                   uint64_t &ready_context_num, uint64_t &total_context_number) {
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  uint64_t gen_ctx_num = ffts_plus_task_def->ffts_plus_ctx_size();

  domi::FftsPlusSqeDef* ffts_plus_sqe = ffts_plus_task_def->mutable_ffts_plus_sqe();
  for (size_t i = 0; i < gen_ctx_num; i++) {
    FFTS_LOGD("Gen subGraph sqe def :%s.",
              ffts_plus_task_def->mutable_ffts_plus_ctx(static_cast<int>(i))->DebugString().c_str());
  }

  ffts_plus_sqe->set_ready_context_num(ready_context_num);
  ffts_plus_sqe->set_total_context_num(gen_ctx_num);

  return SUCCESS;
}
}  // namespace ffts
