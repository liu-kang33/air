/**
 * @file cce_opiddef.h
 *
 * Copyright(C), 2017 - 2017, Huawei Tech. Co., Ltd. ALL RIGHTS RESERVED.
 *
 * @brief op id define
 *
 * @version 1.0
 *
 */

#ifndef _CCE_OPIDDEF_H_
#define _CCE_OPIDDEF_H_

/* op ID: DNN area: 1~100 */
#define CCE_DNN_OP_DEFAULT                          (uint32_t(0))
#define CCE_DNN_OP_START                            (uint32_t(1))
#define CCE_DNN_OP_RELU                             (uint32_t(CCE_DNN_OP_START + 0))
#define CCE_DNN_OP_BATCHNORM                        (uint32_t(CCE_DNN_OP_START + 1))
#define CCE_DNN_OP_CONCAT                           (uint32_t(CCE_DNN_OP_START + 2))
#define CCE_DNN_OP_CONV                             (uint32_t(CCE_DNN_OP_START + 3))
#define CCE_DNN_OP_DEPTHWISE                        (uint32_t(CCE_DNN_OP_START + 4))
#define CCE_DNN_OP_ELTWISE                          (uint32_t(CCE_DNN_OP_START + 5))
#define CCE_DNN_OP_FC                               (uint32_t(CCE_DNN_OP_START + 6))
#define CCE_DNN_OP_POOLING                          (uint32_t(CCE_DNN_OP_START + 7))
#define CCE_DNN_OP_SCALE                            (uint32_t(CCE_DNN_OP_START + 8))
#define CCE_DNN_OP_SOFTMAX                          (uint32_t(CCE_DNN_OP_START + 9))
#define CCE_DNN_OP_UT                               (uint32_t(CCE_DNN_OP_START + 10))
#define CCE_DNN_OP_DECONV                           (uint32_t(CCE_DNN_OP_START + 11))
#define CCE_DNN_OP_SIGMOID                          (uint32_t(CCE_DNN_OP_START + 12))
#define CCE_DNN_OP_ELU                              (uint32_t(CCE_DNN_OP_START + 13))
#define CCE_DNN_OP_LEAKY_RELU                       (uint32_t(CCE_DNN_OP_START + 14))
#define CCE_DNN_OP_CORRELATION                      (uint32_t(CCE_DNN_OP_START + 15))
#define CCE_DNN_OP_CROP                             (uint32_t(CCE_DNN_OP_START + 16))
#define CCE_DNN_OP_RESHAPE                          (uint32_t(CCE_DNN_OP_START + 17))
#define CCE_DNN_OP_FREESPACE_EXTRACT                (uint32_t(CCE_DNN_OP_START + 18))
#define CCE_DNN_OP_AFFINE_GRID_GENERATOR            (uint32_t(CCE_DNN_OP_START + 19))
#define CCE_DNN_OP_SHIFT_GRID_GENERATOR             (uint32_t(CCE_DNN_OP_START + 20))
#define CCE_DNN_OP_SPATIAL_TRANSFORMER_SAMPLER      (uint32_t(CCE_DNN_OP_START + 21))
#define CCE_DNN_OP_SHUFFLE_CHANNEL                  (uint32_t(CCE_DNN_OP_START + 22))
#define CCE_DNN_OP_PERMUTE                          (uint32_t(CCE_DNN_OP_START + 23))
#define CCE_DNN_OP_SPLIT                            (uint32_t(CCE_DNN_OP_START + 24))
#define CCE_DNN_OP_YOLO2_REGION                     (uint32_t(CCE_DNN_OP_START + 25))
#define CCE_DNN_OP_SSD_PRIORBOX                     (uint32_t(CCE_DNN_OP_START + 26))
#define CCE_DNN_OP_NMS                              (uint32_t(CCE_DNN_OP_START + 27))
#define CCE_DNN_OP_MSCNN_BOXOUTPUT                  (uint32_t(CCE_DNN_OP_START + 28))
#define CCE_DNN_OP_FASTER_RCNN_DETECTIONOUTPUT      (uint32_t(CCE_DNN_OP_START + 29))
#define CCE_DNN_OP_INTERP                           (uint32_t(CCE_DNN_OP_START + 30))
#define CCE_DNN_OP_PRELU                            (uint32_t(CCE_DNN_OP_START + 31))
#define CCE_DNN_OP_YOLO_DETECTION_OUTPUT            (uint32_t(CCE_DNN_OP_START + 32))
#define CCE_DNN_OP_YOLO2_DETECTION_OUTPUT           (uint32_t(CCE_DNN_OP_START + 33))
#define CCE_DNN_OP_SSD_DETECTION_OUTPUT             (uint32_t(CCE_DNN_OP_START + 34))
#define CCE_DNN_OP_FASTER_RCNN_PROPOSAL             (uint32_t(CCE_DNN_OP_START + 35))
#define CCE_DNN_OP_L2_NORMALIZATION_CHANNEL         (uint32_t(CCE_DNN_OP_START + 36))
#define CCE_DNN_OP_L2_NORMALIZATION_INSTANCE        (uint32_t(CCE_DNN_OP_START + 37))
#define CCE_DNN_OP_L2_NORMALIZATION_SPATIAL         (uint32_t(CCE_DNN_OP_START + 38))
#define CCE_DNN_OP_LRN                              (uint32_t(CCE_DNN_OP_START + 39))
#define CCE_DNN_OP_DETECTPOSTPROCESS                (uint32_t(CCE_DNN_OP_START + 40))
#define CCE_DNN_OP_SSD_NORMALIZE                    (uint32_t(CCE_DNN_OP_START + 41))
#define CCE_DNN_OP_SSD_NORMALIZE_INSTANCE           (uint32_t(CCE_DNN_OP_START + 42))
#define CCE_DNN_OP_YOLO2_REORG                      (uint32_t(CCE_DNN_OP_START + 43))
#define CCE_DNN_OP_POWER                            (uint32_t(CCE_DNN_OP_START + 44))
#define CCE_DNN_OP_REDUCTION                        (uint32_t(CCE_DNN_OP_START + 45))
#define CCE_DNN_OP_ROIPOOLING                       (uint32_t(CCE_DNN_OP_START + 46))
#define CCE_DNN_OP_PSROIPOOLING                     (uint32_t(CCE_DNN_OP_START + 47))
#define CCE_DNN_OP_SHIFT_TRANSFORMER_SAMPLER        (uint32_t(CCE_DNN_OP_START + 48))
#define CCE_DNN_OP_RSQRT                            (uint32_t(CCE_DNN_OP_START + 49))
#define CCE_DNN_OP_FOUR2FIVE                        (uint32_t(CCE_DNN_OP_START + 50))
#define CCE_DNN_OP_BIASADD                          (uint32_t(CCE_DNN_OP_START + 51))
#define CCE_DNN_OP_ADDLIMITED                       (uint32_t(CCE_DNN_OP_START + 52))
#define CCE_DNN_OP_MULLIMITED                       (uint32_t(CCE_DNN_OP_START + 53))
#define CCE_DNN_OP_ADD                              (uint32_t(CCE_DNN_OP_START + 54))
#define CCE_DNN_OP_MUL                              (uint32_t(CCE_DNN_OP_START + 55))
#define CCE_DNN_OP_FIVE2FOUR                        (uint32_t(CCE_DNN_OP_START + 56))
#define CCE_DNN_OP_SUB                              (uint32_t(CCE_DNN_OP_START + 57))
#define CCE_DNN_OP_TANH                             (uint32_t(CCE_DNN_OP_START + 58))
#define CCE_DNN_OP_ABS                              (uint32_t(CCE_DNN_OP_START + 59))
#define CCE_DNN_OP_CLIPPEDRELU                      (uint32_t(CCE_DNN_OP_START + 67))
#define CCE_DNN_OP_MSR_CLIPBOXES                    (uint32_t(CCE_DNN_OP_START + 68))
#define CCE_DNN_OP_GATHER_ND                        (uint32_t(CCE_DNN_OP_START + 69))
#define CCE_DNN_OP_STRIDED_SLICE                    (uint32_t(CCE_DNN_OP_START + 70))
#define CCE_DNN_OP_SLICE                            (uint32_t(CCE_DNN_OP_START + 71))
#define CCE_DNN_OP_STACK                            (uint32_t(CCE_DNN_OP_START + 72))
#define CCE_DNN_OP_TILE                             (uint32_t(CCE_DNN_OP_START + 73))
#define CCE_DNN_OP_REALDIV                          (uint32_t(CCE_DNN_OP_START + 74))
#define CCE_DNN_OP_RANGE                            (uint32_t(CCE_DNN_OP_START + 75))
#define CCE_DNN_OP_PERMUTE_AICPU                    (uint32_t(CCE_DNN_OP_START + 76))
#define CCE_DNN_OP_FILL                             (uint32_t(CCE_DNN_OP_START + 77))
#define CCE_DNN_OP_GATHER                           (uint32_t(CCE_DNN_OP_START + 78))
#define CCE_DNN_OP_ARGMAX                           (uint32_t(CCE_DNN_OP_START + 79))
#define CCE_DNN_OP_CAST                             (uint32_t(CCE_DNN_OP_START + 80))
#define CCE_DNN_OP_EXP                              (uint32_t(CCE_DNN_OP_START + 81))
#define CCE_DNN_OP_LOG                              (uint32_t(CCE_DNN_OP_START + 82))
#define CCE_DNN_OP_POW                              (uint32_t(CCE_DNN_OP_START + 83))
#define CCE_DNN_OP_MASK_RCNN_FASTRCNNPREDICTIONS    (uint32_t(CCE_DNN_OP_START + 84))
#define CCE_DNN_OP_MASK_RCNN_GENERATERPNPROPOSALS   (uint32_t(CCE_DNN_OP_START + 85))
#define CCE_DNN_OP_SUB5D                            (uint32_t(CCE_DNN_OP_START + 86))
#define CCE_DNN_OP_MUL_5D                           (uint32_t(CCE_DNN_OP_START + 87))
#define CCE_DNN_OP_ADD_5D                           (uint32_t(CCE_DNN_OP_START + 88))
#define CCE_DNN_OP_CHANNELAXPY                      (uint32_t(CCE_DNN_OP_START + 89))
#define CCE_DNN_OP_ROIALIGN                         (uint32_t(CCE_DNN_OP_START + 90))
#define CCE_DNN_OP_MSR_DECODEBBOX                   (uint32_t(CCE_DNN_OP_START + 91))
#define CCE_DNN_OP_MULTICLASSNMS                    (uint32_t(CCE_DNN_OP_START + 92))
#define CCE_DNN_OP_YOLO_GET_REGION_BOX              (uint32_t(CCE_DNN_OP_START + 93))
#define CCE_DNN_OP_YOLO_CORRECT_BOXES               (uint32_t(CCE_DNN_OP_START + 94))
#define CCE_DNN_OP_YOLO_CLS_PROB                    (uint32_t(CCE_DNN_OP_START + 95))
#define CCE_DNN_OP_SUB_5D                           (uint32_t(CCE_DNN_OP_START + 96))
#define CCE_DNN_OP_RELU1                            (uint32_t(CCE_DNN_OP_START + 98))

#define CCE_DNN_OP_YOLO_COORD_PROCESS               (uint32_t(CCE_DNN_OP_START + 97))
#define CCE_DNN_OP_8BIT_QUANTIZE                    (uint32_t(CCE_DNN_OP_START + 99))
#define CCE_DNN_OP_CROP_AND_RESIZE                  (uint32_t(CCE_DNN_OP_START + 100))
#define CCE_DNN_OP_CONCAT_AICPU                     (uint32_t(CCE_DNN_OP_START + 101))
#define CCE_DNN_OP_RESIZE_BILINEAR                  (uint32_t(CCE_DNN_OP_START + 102))
#define CCE_DNN_OP_FLOORDIV                         (uint32_t(CCE_DNN_OP_START + 103))
#define CCE_DNN_OP_GREATER                          (uint32_t(CCE_DNN_OP_START + 104))
#define CCE_DNN_OP_LESS                             (uint32_t(CCE_DNN_OP_START + 105))
#define CCE_DNN_OP_FLOORMOD                         (uint32_t(CCE_DNN_OP_START + 106))
#define CCE_DNN_OP_MAXIMUM                          (uint32_t(CCE_DNN_OP_START + 107))
#define CCE_DNN_OP_MINIMUM                          (uint32_t(CCE_DNN_OP_START + 108))
#define CCE_DNN_OP_REDUCE_AICPU                     (uint32_t(CCE_DNN_OP_START + 109))
#define CCE_DNN_OP_WHERE                            (uint32_t(CCE_DNN_OP_START + 110))
#define CCE_DNN_OP_SELECT                           (uint32_t(CCE_DNN_OP_START + 111))
#define CCE_DNN_OP_ROUND                            (uint32_t(CCE_DNN_OP_START + 112))
#define CCE_DNN_OP_RINT                             (uint32_t(CCE_DNN_OP_START + 113))
#define CCE_DNN_OP_SQRT                             (uint32_t(CCE_DNN_OP_START + 114))
#define CCE_DNN_OP_REVERSE                          (uint32_t(CCE_DNN_OP_START + 115))
#define CCE_DNN_OP_FLOOR                            (uint32_t(CCE_DNN_OP_START + 116))
#define CCE_DNN_OP_CEIL                             (uint32_t(CCE_DNN_OP_START + 117))
#define CCE_DNN_OP_TRUNCATEMOD                      (uint32_t(CCE_DNN_OP_START + 118))
#define CCE_DNN_OP_MOVE                             (uint32_t(CCE_DNN_OP_START + 119))
#define CCE_DNN_OP_SWISH                            (uint32_t(CCE_DNN_OP_START + 120))
#define CCE_DNN_OP_BNLL                             (uint32_t(CCE_DNN_OP_START + 121))
#define CCE_DNN_OP_BIAS                             (uint32_t(CCE_DNN_OP_START + 122))
#define CCE_DNN_OP_THRESHOLD                        (uint32_t(CCE_DNN_OP_START + 123))
#define CCE_DNN_OP_SPP                              (uint32_t(CCE_DNN_OP_START + 124))
#define CCE_DNN_OP_SHUFFLECHANNEL                   (uint32_t(CCE_DNN_OP_START + 125))
#define CCE_DNN_OP_MVN                              (uint32_t(CCE_DNN_OP_START + 126))
#define CCE_DNN_OP_HEATMAP2COORD                    (uint32_t(CCE_DNN_OP_START + 127))
#define CCE_DNN_OP_SPLIT_4D                         (uint32_t(CCE_DNN_OP_START + 128))
#define CCE_DNN_OP_SSD_DETECTIONOUTPUT_PERF         (uint32_t(CCE_DNN_OP_START + 129))
#define CCE_DNN_OP_TRANSDATA                        (uint32_t(CCE_DNN_OP_START + 130))
#define CCE_DNN_OP_SOFTPLUS                         (uint32_t(CCE_DNN_OP_START + 131))
#define CCE_DNN_OP_EMBEDDING_LOOKUP                 (uint32_t(CCE_DNN_OP_START + 132))
#define CCE_DNN_OP_SVDF                             (uint32_t(CCE_DNN_OP_START + 133))
#define CCE_DNN_OP_CORE_FLOOR                       (uint32_t(CCE_DNN_OP_START + 134))
#define CCE_DNN_OP_EMBEDDING_ATTN_DECODER           (uint32_t(CCE_DNN_OP_START + 135))
#define CCE_DNN_OP_LOGICALNOT                       (uint32_t(CCE_DNN_OP_START + 136))
#define CCE_DNN_OP_HIGHWAY                          (uint32_t(CCE_DNN_OP_START + 137))
#define CCE_DNN_OP_EQUAL                            (uint32_t(CCE_DNN_OP_START + 138))
#define CCE_DNN_OP_LOGICALAND                       (uint32_t(CCE_DNN_OP_START + 139))
#define CCE_DNN_OP_SOFTSIGN                         (uint32_t(CCE_DNN_OP_START + 140))
#define CCE_DNN_OP_SQUARE                           (uint32_t(CCE_DNN_OP_START + 142))
#define CCE_DNN_OP_MAXIMUM_AICORE                   (uint32_t(CCE_DNN_OP_START + 143))
#define CCE_DNN_OP_REDUCE_AICORE_SUM                (uint32_t(CCE_DNN_OP_START + 144))
#define CCE_DNN_OP_TOP_K_V2                         (uint32_t(CCE_DNN_OP_START + 145))
#define CCE_DNN_OP_UNSORTED_SEGMENT_REDUCTION       (uint32_t(CCE_DNN_OP_START + 146))
#define CCE_DNN_OP_LOGICALOR                        (uint32_t(CCE_DNN_OP_START + 149))
#define CCE_DNN_OP_DATADUMP                         (uint32_t(CCE_DNN_OP_START + 151))
#define CCE_DNN_OP_INVERT_PERMUTATION               (uint32_t(CCE_DNN_OP_START + 155))
#define CCE_DNN_OP_NONMAXSUPPRESSION                (uint32_t(CCE_DNN_OP_START + 156))
#define CCE_DNN_OP_MULTINOMIAL                      (uint32_t(CCE_DNN_OP_START + 157))
#define CCE_DNN_OP_REVERSE_SEQUENCE                 (uint32_t(CCE_DNN_OP_START + 158))
#define CCE_DNN_OP_DEQUANTIZE                       (uint32_t(CCE_DNN_OP_START + 159))
#define CCE_DNN_OP_QUANTIZE                         (uint32_t(CCE_DNN_OP_START + 160))
#define CCE_DNN_OP_ND2FIVE                          (uint32_t(CCE_DNN_OP_START + 161))
#define CCE_DNN_OP_SINH                             (uint32_t(CCE_DNN_OP_START + 162))
#define CCE_DNN_OP_SQUARED_DIFFERENCE               (uint32_t(CCE_DNN_OP_START + 163))
#define CCE_DNN_OP_THRESHOLD_RELU                   (uint32_t(CCE_DNN_OP_START + 164))
#define CCE_DNN_OP_LINEAR                           (uint32_t(CCE_DNN_OP_START + 165))
#define CCE_DNN_OP_COSH                             (uint32_t(CCE_DNN_OP_START + 166))
#define CCE_DNN_OP_SELU                             (uint32_t(CCE_DNN_OP_START + 167))
#define CCE_DNN_OP_ASSIGNOP                         (uint32_t(CCE_DNN_OP_START + 168))
#define CCE_DNN_OP_ASINH                            (uint32_t(CCE_DNN_OP_START + 169))
#define CCE_DNN_OP_ACOSH                            (uint32_t(CCE_DNN_OP_START + 170))
#define CCE_DNN_OP_RECIPROCAL                       (uint32_t(CCE_DNN_OP_START + 171))
#define CCE_DNN_OP_PAD                              (uint32_t(CCE_DNN_OP_START + 172))
#define CCE_DNN_OP_HARDSIGMOID                      (uint32_t(CCE_DNN_OP_START + 173))
#define CCE_DNN_OP_ATANH                            (uint32_t(CCE_DNN_OP_START + 174))
#define CCE_DNN_OP_CLIP                             (uint32_t(CCE_DNN_OP_START + 175))
#define CCE_DNN_OP_RANDOM_NORMAL                    (uint32_t(CCE_DNN_OP_START + 176))
#define CCE_DNN_OP_RANDOM_UNIFORM                   (uint32_t(CCE_DNN_OP_START + 177))
#define CCE_DNN_OP_ONE_HOT                          (uint32_t(CCE_DNN_OP_START + 178))
#define CCE_DNN_OP_SIN                              (uint32_t(CCE_DNN_OP_START + 179))
#define CCE_DNN_OP_TAN                              (uint32_t(CCE_DNN_OP_START + 181))
#define CCE_DNN_OP_MAX_MULTITENSOR                  (uint32_t(CCE_DNN_OP_START + 182))
#define CCE_DNN_OP_MIN_MULTITENSOR                  (uint32_t(CCE_DNN_OP_START + 183))
#define CCE_DNN_OP_UNSTACK                          (uint32_t(CCE_DNN_OP_START + 184))
#define CCE_DNN_OP_RESIZE_NEAREST_NEIGHBOR          (uint32_t(CCE_DNN_OP_START + 185))
#define CCE_DNN_OP_RANDOM_SHUFFLE                   (uint32_t(CCE_DNN_OP_START + 186))
#define CCE_DNN_OP_BATCH_TO_SPACE                   (uint32_t(CCE_DNN_OP_START + 187))
#define CCE_DNN_OP_SPACE_TO_BATCH                   (uint32_t(CCE_DNN_OP_START + 188))
#define CCE_DNN_OP_PAD_AICPU                        (uint32_t(CCE_DNN_OP_START + 189))
#define CCE_DNN_OP_COS                              (uint32_t(CCE_DNN_OP_START + 190))
#define CCE_DNN_OP_CUM                              (uint32_t(CCE_DNN_OP_START + 191))
#define CCE_DNN_OP_EXTRACT_IMAGE_PATCHES            (uint32_t(CCE_DNN_OP_START + 192))
#define CCE_DNN_OP_ARGMAXMIN                        (uint32_t(CCE_DNN_OP_START + 193))
#define CCE_DNN_OP_DEPTH_TO_SPACE                   (uint32_t(CCE_DNN_OP_START + 194))
#define CCE_DNN_OP_SPACE_TO_DEPTH                   (uint32_t(CCE_DNN_OP_START + 195))
#define CCE_DNN_OP_EXPM1                            (uint32_t(CCE_DNN_OP_START + 196))
#define CCE_DNN_OP_LOG1P                            (uint32_t(CCE_DNN_OP_START + 197))
#define CCE_DNN_OP_LSH_PROJECTION                   (uint32_t(CCE_DNN_OP_START + 198))
#define CCE_DNN_OP_UPSAMPLE                         (uint32_t(CCE_DNN_OP_START + 199))
#define CCE_DNN_OP_HASH_TABLE_LOOKUP                (uint32_t(CCE_DNN_OP_START + 200))
#define CCE_DNN_OP_DIV_5D                           (uint32_t(CCE_DNN_OP_START + 201))
#define CCE_DNN_OP_ARCTAN                           (uint32_t(CCE_DNN_OP_START + 202))
#define CCE_DNN_OP_LAYERNORM                        (uint32_t(CCE_DNN_OP_START + 203))
#define CCE_DNN_OP_REDUCE_AICORE_MAX                (uint32_t(CCE_DNN_OP_START + 204))
#define CCE_DNN_OP_REDUCE_AICORE_MIN                (uint32_t(CCE_DNN_OP_START + 205))
#define CCE_DNN_OP_MINIMUM_AICORE                   (uint32_t(CCE_DNN_OP_START + 206))
#define CCE_DNN_OP_NEG                              (uint32_t(CCE_DNN_OP_START + 207))
#define CCE_DNN_OP_COMPARE                          (uint32_t(CCE_DNN_OP_START + 208))
#define CCE_DNN_OP_INSTANCENORM                     (uint32_t(CCE_DNN_OP_START + 209))
#define CCE_DNN_OP_LOGICAL_AND_OR_XOR               (uint32_t(CCE_DNN_OP_START + 210))
#define CCE_DNN_OP_LOGICALNOT_AICORE                (uint32_t(CCE_DNN_OP_START + 211))
#define CCE_DNN_OP_FAKEQUANTVARS                    (uint32_t(CCE_DNN_OP_START + 212))
#define CCE_DNN_OP_ARCSINCOS                        (uint32_t(CCE_DNN_OP_START + 213))
#define CCE_DNN_OP_SHAPE_CLASSIFY                   (uint32_t(CCE_DNN_OP_START + 214))
#define CCE_DNN_OP_FSR_FIRST_STAGE_POSTPROCESSOR    (uint32_t(CCE_DNN_OP_START + 215))
#define CCE_DNN_OP_FSR_SECOND_STAGE_POSTPROCESSOR   (uint32_t(CCE_DNN_OP_START + 216))
#define CCE_DNN_OP_ROIINTERPPOOLING                 (uint32_t(CCE_DNN_OP_START + 217))
#define CCE_DNN_OP_SSD_POST_PROCESSOR               (uint32_t(CCE_DNN_OP_START + 218))
#define CCE_DNN_OP_RETINA_POST_PROCESSOR            (uint32_t(CCE_DNN_OP_START + 219))
#define CCE_DNN_OP_HUBERLOSS                        (uint32_t(CCE_DNN_OP_START + 220))
#define CCE_DNN_OP_CONCAT_FIVE2FOUR                 (uint32_t(CCE_DNN_OP_START + 221))
#define CCE_DNN_OP_SPARSE_SOFTMAX_CROSS_ENTROPY_BACKWARD (uint32_t(CCE_DNN_OP_START + 223))
#define CCE_DNN_OP_BATCH_MAT_MUL                    (uint32_t(CCE_DNN_OP_START + 222))
#define CCE_DNN_OP_RESIZE_NEAREST_NEIGHBOR_AICORE   (uint32_t(CCE_DNN_OP_START + 224))
#define CCE_DNN_OP_REFINEDET_DETECTIONOUTPUT_PERF   (uint32_t(CCE_DNN_OP_START + 225))
#define CCE_DNN_OP_ISFINITE                         (uint32_t(CCE_DNN_OP_START + 226))
#define CCE_DNN_OP_ALLOCFLOATSTATUS                 (uint32_t(CCE_DNN_OP_START + 227))
#define CCE_DNN_OP_GETFLOATSTATUS                   (uint32_t(CCE_DNN_OP_START + 228))
#define CCE_DNN_OP_CLEARFLOATSTATUS                 (uint32_t(CCE_DNN_OP_START + 229))
#define CCE_DNN_OP_GATHER_V2                        (uint32_t(CCE_DNN_OP_START + 230))
#define CCE_DNN_OP_MEMORY_CLEAR                     (uint32_t(CCE_DNN_OP_START + 231))
#define CCE_DNN_OP_CONCAT_FOUR2FIVE                 (uint32_t(CCE_DNN_OP_START + 250))
#define CCE_DNN_OP_VIRTUAL                          (uint32_t(CCE_DNN_OP_START + 1000))

//for conv+Ub
#define CCE_DNN_OP_NORMALVEC                        ((uint32_t)((CCE_DNN_OP_START) + 61))
#define CCE_DNN_OP_CONV_DEPTH                       ((uint32_t)((CCE_DNN_OP_START) + 62))

#define CCE_DNN_OP_CONVTENSORTOFILTER               ((uint32_t)((CCE_DNN_OP_START) + 63))
#define CCE_DNN_OP_DECONV_AICPU                     ((uint32_t)((CCE_DNN_OP_START) + 64))
#define CCE_DNN_OP_CONV_UBFUSION                    ((uint32_t)((CCE_DNN_OP_START) + 65))
#define CCE_DNN_OP_CONV_UB                          CCE_DNN_OP_CONV_UBFUSION
//id for te aicore
#define CCE_DNN_OP_CONV_TE                          ((uint32_t)((CCE_DNN_OP_START) + 66))

//id for te aicpu
#define CCE_DNN_OP_CONV_TE_AICPU                    ((uint32_t)((CCE_DNN_OP_START) + 145))

//id for cpu customize
#define CCE_DNN_OP_CPU_CUSTOMIZE                    ((uint32_t)((CCE_DNN_OP_START) + 141))

//for train area: 300 ~
//conv id for backward
#define CCE_DNN_OP_CONV_BW_DX                       ((uint32_t)((CCE_DNN_OP_START) + 300))  // opId for conv dX
#define CCE_DNN_OP_CONV_BW_DW                       ((uint32_t)((CCE_DNN_OP_START) + 301))  // opId for conv dW
#define CCE_DNN_OP_BATCHNORM_TRAIN_FORWARD          (uint32_t(CCE_DNN_OP_START + 302))
#define CCE_DNN_OP_BATCHNORM_TRAIN_BACKWARD         (uint32_t(CCE_DNN_OP_START + 303))
#define CCE_DNN_OP_RELU_TRAIN_BACKWARD              (uint32_t(CCE_DNN_OP_START + 304))
#define CCE_DNN_OP_SOFTMAX_TRAIN_FORWARD            (uint32_t(CCE_DNN_OP_START + 305))
#define CCE_DNN_OP_SOFTMAX_CROSS_ENTROPY            (uint32_t(CCE_DNN_OP_START + 306))
#define CCE_DNN_OP_POOLING_TRAIN_FORWARD            (uint32_t(CCE_DNN_OP_START + 307))
#define CCE_DNN_OP_BIASADD_TRAIN_BACKWARD           (uint32_t(CCE_DNN_OP_START + 308))
#define CCE_DNN_OP_MOMENTUM                         (uint32_t(CCE_DNN_OP_START + 309))
#define CCE_DNN_OP_HWCK2FRACZ                       (uint32_t(CCE_DNN_OP_START + 310))
#define CCE_DNN_OP_FRACZ2HWCK                       (uint32_t(CCE_DNN_OP_START + 311))
#define CCE_DNN_OP_L2_LOSS_FORWARD                  (uint32_t(CCE_DNN_OP_START + 312))
#define CCE_DNN_OP_POOLING_TRAIN_BACKWARD            (uint32_t(CCE_DNN_OP_START + 313))
#define CCE_DNN_OP_AVG_POOLING_BACKWARD             (uint32_t(CCE_DNN_OP_START + 314))
#define CCE_DNN_OP_ADDN                             (uint32_t(CCE_DNN_OP_START + 315))
#define CCE_DNN_OP_MATH_TRAIN_FORWARD               (uint32_t(CCE_DNN_OP_START + 316))
#define CCE_DNN_OP_ASSIGN_OP_FP32                   (uint32_t(CCE_DNN_OP_START + 317))
#define CCE_DNN_OP_FC_BWD_DW                        (uint32_t(CCE_DNN_OP_START + 318))

#define CCE_DNN_OP_HUBERLOSS_BACKWARD               (uint32_t(CCE_DNN_OP_START + 319))
#define CCE_DNN_OP_SPARSE_SOFTMAX_CROSS_ENTROPY_FWD (uint32_t(CCE_DNN_OP_START + 320))
#define CCE_DNN_OP_FC_DX                            (uint32_t(CCE_DNN_OP_START + 321))
#define CCE_DNN_OP_RNN_TRAIN_FORWARD                (uint32_t(CCE_DNN_OP_START + 322))
#define CCE_DNN_OP_FOUR2FIVE_TRAIN                  (uint32_t(CCE_DNN_OP_START + 323))
#define CCE_DNN_OP_LARS                             (uint32_t(CCE_DNN_OP_START + 324))

#define CCE_DNN_OP_UBFUSION_CONV_BN1                (uint32_t(CCE_DNN_OP_START + 330))
#define CCE_DNN_OP_UBFUSION_BN2_RELU_CONV_BN1       (uint32_t(CCE_DNN_OP_START + 331))
#define CCE_DNN_OP_UBFUSION_BN2_RELU                (uint32_t(CCE_DNN_OP_START + 332))
#define CCE_DNN_OP_UBFUSION_TWO_BN2_ADD_RELU        (uint32_t(CCE_DNN_OP_START + 333))
#define CCE_DNN_OP_UBFUSION_BN2_ADD_RELU            (uint32_t(CCE_DNN_OP_START + 334))
#define CCE_DNN_OP_UBFUSION_DBN2_CONVDW             (uint32_t(CCE_DNN_OP_START + 335))
#define CCE_DNN_OP_UBFUSION_CONV_DX_ELTWISE_DRELU   (uint32_t(CCE_DNN_OP_START + 336))
#define CCE_DNN_OP_UBFUSION_CONV_DX_ELTWISE         (uint32_t(CCE_DNN_OP_START + 337))
#define CCE_DNN_OP_UBFUSION_CONV_DX_DRELU           (uint32_t(CCE_DNN_OP_START + 338))
#define CCE_DNN_OP_UBFUSION_FC_DX_DAVGPOOL_DRELU    (uint32_t(CCE_DNN_OP_START + 339))
#define CCE_DNN_OP_UBFUSION_DMAXPOOL_DRELU          (uint32_t(CCE_DNN_OP_START + 340))
#define CCE_DNN_OP_MOMENTUM_UBFUSION                (uint32_t(CCE_DNN_OP_START + 341))
#define CCE_DNN_OP_UBFUSION_L2LOSSGRAD_ADDN_MOMENTUM          CCE_DNN_OP_MOMENTUM_UBFUSION
#define CCE_DNN_OP_CLIPGLOBAL                       (uint32_t(CCE_DNN_OP_START + 350))
#define CCE_DNN_OP_PROJECTION_LOSS                  (uint32_t(CCE_DNN_OP_START + 351))
#define CCE_DNN_OP_ADAM_FORWARD                     (uint32_t(CCE_DNN_OP_START + 352))
#define CCE_DNN_OP_RNN_ATTN_DECODER_TRAIN_FWD       (uint32_t(CCE_DNN_OP_START + 353))
#define CCE_DNN_OP_SSD_REALDIVTILEMUL_FUSION_FWD    (uint32_t(CCE_DNN_OP_START + 354))
#define CCE_DNN_OP_SSD_SUMMULSUMDIVMEAN_FUSION_FWD  (uint32_t(CCE_DNN_OP_START + 355))
#define CCE_DNN_OP_SSD_CMMSTLC_FUS_TRAIN_FWD        (uint32_t(CCE_DNN_OP_START + 356))
#define CCE_DNN_OP_CLASSIFYLOSS_PRE_TRAIN_FORWARD   (uint32_t(CCE_DNN_OP_START + 357))
#define CCE_DNN_OP_NEGATIVE_FWD                     (uint32_t(CCE_DNN_OP_START + 358))
#define CCE_DNN_OP_SSD_TOPKNEGTOPK_FUSION_FWD       (uint32_t(CCE_DNN_OP_START + 359))


//for rnn
#define CCE_DNN_OP_RNN                              ((uint32_t)((CCE_DNN_OP_START) + 180))

/* op ID: Blas area: 1001~2000 */
#define CCE_BLAS_OP_START  uint32_t(CCE_DNN_OP_START + 1001)
#define CCE_BLAS_OP_AMAX   uint32_t(CCE_BLAS_OP_START + 0)
#define CCE_BLAS_OP_IAMIN   uint32_t(CCE_BLAS_OP_START + 1)
#define CCE_BLAS_OP_ASUM   uint32_t(CCE_BLAS_OP_START + 2)
#define CCE_BLAS_OP_AXPY   uint32_t(CCE_BLAS_OP_START + 3)
#define CCE_BLAS_OP_COPY   uint32_t(CCE_BLAS_OP_START + 4)
#define CCE_BLAS_OP_DOT    uint32_t(CCE_BLAS_OP_START + 5)
#define CCE_BLAS_OP_NRM2   uint32_t(CCE_BLAS_OP_START + 6)
#define CCE_BLAS_OP_SCAL   uint32_t(CCE_BLAS_OP_START + 7)
#define CCE_BLAS_OP_SWAP   uint32_t(CCE_BLAS_OP_START + 8)
#define CCE_BLAS_OP_ROTG   uint32_t(CCE_BLAS_OP_START + 9)
#define CCE_BLAS_OP_ROTMG  uint32_t(CCE_BLAS_OP_START + 10)
#define CCE_BLAS_OP_GEMV   uint32_t(CCE_BLAS_OP_START + 11)
#define CCE_BLAS_OP_GER    uint32_t(CCE_BLAS_OP_START + 12)
#define CCE_BLAS_OP_TBSV   uint32_t(CCE_BLAS_OP_START + 13)
#define CCE_BLAS_OP_TPSV   uint32_t(CCE_BLAS_OP_START + 14)
#define CCE_BLAS_OP_TRSV   uint32_t(CCE_BLAS_OP_START + 15)
#define CCE_BLAS_OP_GEMM   uint32_t(CCE_BLAS_OP_START + 16)


/* op ID: HCCL area: 2001*/
#define CCE_HCCL_OP_START uint32_t(CCE_DNN_OP_START + 2001)
#define CCE_HCCL_OP_REDUCE uint32_t(CCE_HCCL_OP_START + 0)


/*AttrList Id*/
//以下是给TILING和融合算子使用的ID
#define OP_DATA_START           (0x0)
#define OP_DATA_TENSOR          (OP_DATA_START+0) //data tensor: (kernel::kTensor_t)
#define OP_FILTER_TENSOR        (OP_DATA_START+1) //conv param :(kernel::kFilter_t)
#define OP_BIAS_TENSOR          (OP_DATA_START+2) //conv param :(kernel::kTensor_t)
#define OP_ALGO_PARAM           (OP_DATA_START+3) //(conv: kernel::kConvParam_t), pooling(kernel::kPoolingParam_t)

//以下为算子自己使用的ID， 用于分解为单算子graph解析
#define OP_TENSOR_INPUT         (OP_DATA_START+4)
#define OP_TENSOR_INPUT_2         (OP_DATA_START+5)
#define OP_TENSOR_INPUT_3         (OP_DATA_START+6)
#define OP_TENSOR_INPUT_4         (OP_DATA_START+7)
#define OP_TENSOR_INPUT_5         (OP_DATA_START+8)
#define OP_TENSOR_INPUT_6         (OP_DATA_START+9)
//中间的编号，已被预留，不要使用

#define OP_FILTER_TENSOR_2        (OP_DATA_START+10) //svdf param :(kernel::kFilter_t)

#define OP_TENSOR_INPUT_1000         (OP_DATA_START+1003)

#define OP_TENSOR_OUTPUT        (OP_TENSOR_INPUT_1000+1)
#define OP_TENSOR_OUTPUT_2        (OP_TENSOR_INPUT_1000+2)
#define OP_TENSOR_OUTPUT_3        (OP_TENSOR_INPUT_1000+3)
#define OP_TENSOR_OUTPUT_4        (OP_TENSOR_INPUT_1000+4)
#define OP_TENSOR_OUTPUT_5        (OP_TENSOR_INPUT_1000+5)
#define OP_TENSOR_OUTPUT_6        (OP_TENSOR_INPUT_1000+6)
//中间的编号，已被预留，不要使用
#define OP_TENSOR_OUTPUT_1000        (OP_TENSOR_INPUT_1000+1000)

#define OP_DATA_INPUT_ADDR       (OP_TENSOR_OUTPUT_1000+1)
#define OP_DATA_INPUT_ADDR_2      (OP_TENSOR_OUTPUT_1000+2)
#define OP_DATA_INPUT_ADDR_3      (OP_TENSOR_OUTPUT_1000+3)
#define OP_DATA_INPUT_ADDR_4      (OP_TENSOR_OUTPUT_1000+4)
#define OP_DATA_INPUT_ADDR_5      (OP_TENSOR_OUTPUT_1000+5)
#define OP_DATA_INPUT_ADDR_6      (OP_TENSOR_OUTPUT_1000+6)
//中间的编号，已被预留，不要使用
#define OP_DATA_INPUT_ADDR_1000      (OP_TENSOR_OUTPUT_1000+1000)

#define OP_DATA_OUTPUT_ADDR      (OP_DATA_INPUT_ADDR_1000+1)
#define OP_DATA_OUTPUT_ADDR_2     (OP_DATA_INPUT_ADDR_1000+2)
#define OP_DATA_OUTPUT_ADDR_3     (OP_DATA_INPUT_ADDR_1000+3)
#define OP_DATA_OUTPUT_ADDR_4     (OP_DATA_INPUT_ADDR_1000+4)
#define OP_DATA_OUTPUT_ADDR_5     (OP_DATA_INPUT_ADDR_1000+5)
#define OP_DATA_OUTPUT_ADDR_6     (OP_DATA_INPUT_ADDR_1000+6)
//中间的编号，已被预留，不要使用
#define OP_DATA_OUTPUT_ADDR_1000     (OP_DATA_INPUT_ADDR_1000+1000)

#define CONV_KERNEL_RELU        (OP_DATA_OUTPUT_ADDR_1000+1)
#define OP_DATA_FILTER_ADDR     (OP_DATA_OUTPUT_ADDR_1000+2)
#define MULTI_OP_NUM            (OP_DATA_OUTPUT_ADDR_1000+3)
#define TASK_PARA_L2_DATA_ALLOC (OP_DATA_OUTPUT_ADDR_1000+4)
#define TASK_PARA_L2_MAX_PAGENUM (OP_DATA_OUTPUT_ADDR_1000+5)
#define TILING_PLAN_INFO       (OP_DATA_OUTPUT_ADDR_1000 + 6)
#define CONCAT_BATCH_SIZE       (OP_DATA_OUTPUT_ADDR_1000+7)
#define OP_DATA_FILTER_ADDR_2     (OP_DATA_OUTPUT_ADDR_1000+8)
#define TASK_PARA_OP_INDEX      (OP_DATA_OUTPUT_ADDR_1000+9)
#define OP_DATA_END             (OP_DATA_OUTPUT_ADDR_1000+10)

// for cce op
#define POOLING_KERNEL_PARAM        OP_ALGO_PARAM
#define SOFTMAX_KERNEL_PARAM          (OP_DATA_END+0)
#define SOFTMAX_KERNEL_TENSOR_INPUT   OP_TENSOR_INPUT
#define CONCAT_KERNEL_PARAM         ((uint32_t)(OP_DATA_END + 2))
#define LRN_KERNEL_PARAM            (OP_DATA_END + 0)
#define ROIALIGN_KERNEL_TENSOR_INPUT  ((uint32_t)(OP_TENSOR_INPUT))
#define ELTWISE_KERNEL_TENSOR_OUTPUT    OP_TENSOR_OUTPUT
#define FC_KERNEL_UNZIP_PARAM        (OP_DATA_END + 7)
#define RESIZE_NEAREST_NEIGHBOR_COMMON_KERNEL_TENSOR_INPUT  OP_TENSOR_INPUT
#define RESIZE_NEAREST_NEIGHBOR_COMMON_KERNEL_TENSOR_OUTPUT OP_TENSOR_OUTPUT
#define RESIZE_NEAREST_NEIGHBOR_COMMON_KERNEL_DATA_PARAM    (OP_DATA_END + 0)
#define CONV_KERNEL_PARAM          OP_ALGO_PARAM


#define FusionOpIdJudge(opId) \
    ((CCE_DNN_OP_CONV == opId)||(CCE_DNN_OP_FC == opId)||(CCE_DNN_OP_DECONV == opId)||(CCE_DNN_OP_CONV_DEPTH == opId)||(CCE_DNN_OP_SVDF == opId)||(CCE_DNN_OP_CONV_BW_DX == opId)||(CCE_DNN_OP_CONV_BW_DW == opId)||(CCE_DNN_OP_FC_DX == opId))

#define GetFusionOpIdMacro(opId) \
    (((CCE_DNN_OP_CONV == opId)||(CCE_DNN_OP_CONV_BW_DX == opId)||(CCE_DNN_OP_CONV_BW_DW == opId)) ? CCE_DNN_OP_CONV_UBFUSION : opId)

#define SplitSubGraphJudge(opId) \
    ((CCE_DNN_OP_BATCHNORM_TRAIN_FORWARD == opId)||(CCE_DNN_OP_BATCHNORM_TRAIN_BACKWARD == opId)||(CCE_DNN_OP_TRANSDATA == opId))

/*  */
#endif
