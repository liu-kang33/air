/**
 *
 * @file ffts_plus_ops_kernel_builder_unittest.cc
 *
 * @brief
 *
 * @version 1.0
 *
 */
#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "common/sgt_slice_type.h"
#include "inc/ffts_log.h"
#include "inc/ffts_error_codes.h"
#include "inc/ffts_type.h"
#include "task_builder/fftsplus_ops_kernel_builder.h"
#include "graph/node.h"
#include "graph_builder_utils.h"
#include "graph/utils/tensor_utils.h"
#include "graph/compute_graph.h"
#include "graph/op_kernel_bin.h"
#include "graph/debug/ge_attr_define.h"
#include "runtime/context.h"
#include "runtime/stream.h"
#include "runtime/rt_model.h"
#include "runtime/kernel.h"
#include "runtime/mem.h"
#include "../test_utils.h"

using namespace std;
using namespace ffts;
using namespace ge;

#define SET_SIZE 1000

using AICAIVTaskBuilderPtr = shared_ptr<AICAIVTaskBuilder>;
class FFTSPlusAICAIVTaskBuilderTest : public testing::Test
{
protected:
	void SetUp()
	{
		aic_aiv_task_builder_ptr_ = make_shared<AICAIVTaskBuilder>();
		ffts_plus_def_ptr_ = make_shared<domi::FftsPlusCtxDef>();
		node_ = CreateNode();
	}
	void TearDown() {

	}
	static void SetOpDecSize(NodePtr& node) {
		OpDesc::Vistor<GeTensorDesc> tensors = node->GetOpDesc()->GetAllInputsDesc();
		for (int i = 0; i < node->GetOpDesc()->GetAllInputsDesc().size(); i++) {
			ge::GeTensorDesc tensor = node->GetOpDesc()->GetAllInputsDesc().at(i);
			ge::TensorUtils::SetSize(tensor, SET_SIZE);
			node->GetOpDesc()->UpdateInputDesc(i, tensor);
		}
		OpDesc::Vistor<GeTensorDesc> tensorsOutput = node->GetOpDesc()->GetAllOutputsDesc();
		for (int i = 0; i < tensorsOutput.size(); i++) {
			ge::GeTensorDesc tensorOutput = tensorsOutput.at(i);
			ge::TensorUtils::SetSize(tensorOutput, SET_SIZE);
			node->GetOpDesc()->UpdateOutputDesc(i, tensorOutput);
		}
	}

  Status GenManualAICAIVCtxDef() {
    domi::FftsPlusAicAivCtxDef *aic_aiv_ctx_def = ffts_plus_def_ptr_->mutable_aic_aiv_ctx();
    FFTS_CHECK_NOTNULL(aic_aiv_ctx_def);

    // cache managemet will do at GenerateDataTaskDef()
    aic_aiv_ctx_def->set_prefetch_once_bitmap(0);
    aic_aiv_ctx_def->set_prefetch_enable_bitmap(0);
    aic_aiv_ctx_def->set_atm(ffts::kManualMode);
    aic_aiv_ctx_def->set_thread_dim(1);

    int32_t block_dim = 1;
    //(void) ge::AttrUtils::GetInt(op_desc, ge::TVM_ATTR_NAME_BLOCKDIM, block_dim);
    aic_aiv_ctx_def->set_tail_block_dim(static_cast<uint32_t>(block_dim));
    aic_aiv_ctx_def->set_non_tail_block_dim(static_cast<uint32_t>(block_dim));

    //input
    aic_aiv_ctx_def->add_task_addr(111);
    aic_aiv_ctx_def->add_task_addr(222);

    //output
    aic_aiv_ctx_def->add_task_addr(333);

    //workspace
    aic_aiv_ctx_def->add_task_addr(444);

    auto op_desc = node_->GetOpDesc();
    string attr_key_kernel_name = op_desc->GetName() + kKernelName;
    string attr_kernel_name;
    (void) ge::AttrUtils::GetStr(op_desc, attr_key_kernel_name, attr_kernel_name);
    aic_aiv_ctx_def->add_kernel_name(attr_kernel_name);
    (void)ge::AttrUtils::SetInt(op_desc, ffts::kAttrAICoreCtxType, static_cast<int64_t>(ffts::TaskBuilderType::EN_TASK_TYPE_AIC_AIV));
    (void)op_desc->SetExtAttr(ffts::kAttrAICoreCtxDef, ffts_plus_def_ptr_);
    return ffts::SUCCESS;
  }

	static NodePtr CreateNode()
	{
		FeTestOpDescBuilder builder;
		builder.SetName("test_tvm");
		builder.SetType("conv");
		builder.SetInputs({ 1 });
		builder.SetOutputs({ 1 });
		builder.AddInputDesc({ 2,4,4,4 }, ge::FORMAT_NCHW, ge::DT_FLOAT);
		builder.AddOutputDesc({ 2,4,4,4 }, ge::FORMAT_NCHW, ge::DT_FLOAT);
		auto node = builder.Finish();

		const char tbeBin[] = "tbe_bin";
		vector<char> buffer(tbeBin, tbeBin + strlen(tbeBin));
		OpKernelBinPtr tbeKernelPtr = std::make_shared<OpKernelBin>("test_tvm", std::move(buffer));
		node->GetOpDesc()->SetExtAttr(OP_EXTATTR_NAME_TBE_KERNEL, tbeKernelPtr);
		ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadScopeId, 0);

		ge::AttrUtils::SetInt(node->GetOpDesc(), "_fe_imply_type", (int64_t)2);
		ge::AttrUtils::SetStr(node->GetOpDesc(), "tvm_magic", "RT_DEV_BINARY_MAGIC_ELF");
		ge::AttrUtils::SetBool(node->GetOpDesc(), "is_first_node", true);
		ge::AttrUtils::SetBool(node->GetOpDesc(), "is_last_node", true);
		ge::AttrUtils::SetStr(node->GetOpDesc(), node->GetOpDesc()->GetName() + "_kernelname", "kernelname");
    (void)ge::AttrUtils::SetStr(node->GetOpDesc(), "_cube_vector_core_type", "AIC");

		SetOpDecSize(node);
		ThreadSliceMapPtr tsmp_ptr = make_shared<ThreadSliceMap>();
		tsmp_ptr->slice_instance_num = 4;
		node->GetOpDesc()->SetExtAttr("_sgt_struct_info", tsmp_ptr);
		return node;
	}

public:
	AICAIVTaskBuilderPtr aic_aiv_task_builder_ptr_;
	std::shared_ptr<domi::FftsPlusCtxDef> ffts_plus_def_ptr_;
	NodePtr node_{ nullptr };
};

TEST_F(FFTSPlusAICAIVTaskBuilderTest, GenContextDef_SUCCESS)
{
  (void)GenManualAICAIVCtxDef();
  domi::FftsPlusTaskDef taskdef;
	Status ret = aic_aiv_task_builder_ptr_->GenContextDef(node_, &taskdef);
	EXPECT_EQ(ffts::SUCCESS, ret);
}