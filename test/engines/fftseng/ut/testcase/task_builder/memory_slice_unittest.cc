/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "task_builder/mode/memory_slice.h"
#undef private
#undef protected

using namespace std;
using namespace ffts;
using namespace ge;

class MemorySliceUTest : public testing::Test {
 protected:
	void SetUp() {
	}

	void TearDown() {
	}
};

TEST_F(MemorySliceUTest, GenerateDataCtxParam_failed) {
  std::vector<int64_t> shape = {1, 1};
  std::vector<DimRange> slice;
  ge::DataType dtype;
  int64_t burst_len;
  std::vector<DataContextParam> data_ctx;
  Status ret = MemorySlice::GenerateDataCtxParam(shape, slice, dtype, burst_len, data_ctx);
  EXPECT_EQ(ffts::FAILED, ret);
}