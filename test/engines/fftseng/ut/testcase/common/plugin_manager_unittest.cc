/**
 * Copyright 2022-2023 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "common/plugin_manager.h"
#undef private
#undef protected

using namespace std;
using namespace ffts;

class PluginManagerUTEST : public testing::Test {
 protected:
	void SetUp() {
	}

	void TearDown() {
	}
};

TEST_F(PluginManagerUTEST, Close_Handle_Fail) {
  PluginManager pm("so_name");
  Status ret = pm.CloseHandle();
  EXPECT_EQ(ret, FAILED);
}

TEST_F(PluginManagerUTEST, Close_Handle_Fail_2) {
  PluginManager pm("so_name");
	int a = 1;
	void *h = &a;
	pm.handle = h;
  Status ret = pm.CloseHandle();
}
