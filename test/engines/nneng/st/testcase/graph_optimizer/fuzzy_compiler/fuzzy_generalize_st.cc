/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>


#define protected public
#define private public
#include "graph_optimizer/fe_graph_optimizer.h"
#include "graph/utils/graph_utils.h"
#include "graph/utils/op_desc_utils.h"
#include "common/configuration.h"
#include "graph/ge_tensor.h"
#include "graph/op_desc.h"
#include "graph/compute_graph.h"
#include "graph/utils/attr_utils.h"
#include "graph/utils/tensor_utils.h"
// #include "all_ops.h"
#include "array_ops.h"
#include "selection_ops.h"
#include "matrix_calculation_ops.h"
#include "elewise_calculation_ops.h"
#include "reduce_ops.h"
#include "common/util/op_info_util.h"
#include "adapter/tbe_adapter/tbe_op_store_adapter.h"
#include <graph_optimizer/fe_graph_optimizer.h>
#include "ops_kernel_store/fe_ops_kernel_info_store.h"
#include "ops_store/sub_op_info_store.h"
#include "ops_store/ops_kernel_manager.h"
#include <fusion_rule_manager/fusion_rule_data/fusion_rule_pattern.h>
#include "graph_optimizer/graph_fusion/graph_replace.h"
#include "./ge_context.h"
#include "./ge_local_context.h"

#include "graph_optimizer/heavy_format_propagation/heavy_format_propagation.h"
#include "ge/ge_api_types.h"
#include "common/lxfusion_json_util.h"
#include "adapter/common/op_store_adapter_manager.h"
#include "graph_optimizer/ffts/ffts_pass.h"

#include "ops_kernel_store/fe_ops_kernel_info_store.h"
#include "common/graph/fe_graph_utils.h"
#include "common/configuration.h"
#include <vector>
#include "common/fe_inner_attr_define.h"
#include "ge/ge_api_types.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/tuning_utils.h"
#include "graph/node.h"
#include "graph_optimizer/ffts_plus/cache_manager.h"
#include "graph_optimizer/fuzzy_compiler/fuzzy_generalize.h"
#include "../../../../graph_constructor/graph_constructor.h"

#undef protected
#undef private

using namespace testing;
using namespace ge;
using namespace fe;

using TbeOpStoreAdapterPtr = std::shared_ptr<TbeOpStoreAdapter>;
using FEGraphOptimizerPtr = std::shared_ptr<fe::FEGraphOptimizer>;
using OpStoreAdapterPtr = std::shared_ptr<fe::OpStoreAdapter>;
using FFTSGraphOptimizerPtr = std::shared_ptr<fe::FFTSPlusGraphOptimizer>;

class OptimizeUtilityStubST: public ge::OptimizeUtility {
 public:
  OptimizeUtilityStubST() {}
  virtual ~OptimizeUtilityStubST() override {}

  ge::Status InferShape(ComputeGraph &compute_graph) override{
    return ge::SUCCESS;
  }
};

bool CheckIsRegistered(const te::TbeOpInfo &op_info, bool &val) {
  val = true;
  return true;
}

bool CheckIsNotRegistered(const te::TbeOpInfo &op_info, bool &val) {
  val = false;
  return true;
}

bool CheckIsRegisteredException(const te::TbeOpInfo &op_info, bool &val) {
  val = false;
  return false;
}

bool TeGeneralizeStub(const te::TbeOpInfo &op_info,
    const te::TE_GENERALIZE_TYPE &general_type, ge::NodePtr &node) {
  std::vector<int64_t> shape_vec;
  auto op_desc = node->GetOpDesc();
  auto tensor_desc_x = op_desc->MutableInputDesc("x");
  shape_vec = tensor_desc_x->GetShape().GetDims();
  if (general_type == te::REGISTER_FUNC) {
    for (auto &i : shape_vec) {
      i = -1;
    }
  } else if (general_type == te::DEFAULT_TBE_OP_INFO) {
    for (int i = 0; i < shape_vec.size()-1; ++i) {
      shape_vec[i] = -1;
    }
  } else {
    shape_vec[0] = -1;
  }
  tensor_desc_x->SetShape(ge::GeShape(shape_vec));
  tensor_desc_x->SetOriginShape(ge::GeShape(shape_vec));
  return true;
}

bool TeGeneralizeTrue(const te::TbeOpInfo &op_info,
    const te::TE_GENERALIZE_TYPE &general_type, ge::NodePtr &node) {
  return true;
}

bool TeGeneralizeException(const te::TbeOpInfo &op_info,
    const te::TE_GENERALIZE_TYPE &general_type, ge::NodePtr &node) {
  return false;
}

bool GetOpSpecificInfoLimitStep(const te::TbeOpInfo &tbeOpInfo, std::string &opSpecificInfo) {
  opSpecificInfo = "limited";
  return true;
}

bool GetOpSpecificInfoUnlimitStep(const te::TbeOpInfo &tbeOpInfo, std::string &opSpecificInfo) {
  opSpecificInfo = "unlimited";
  return true;
}

bool GetOpSpecificInfoDynamicStep(const te::TbeOpInfo &tbeOpInfo, std::string &opSpecificInfo) {
  opSpecificInfo = "dynamic";
  return true;
}

bool GetOpSpecificInfoFailStep(const te::TbeOpInfo &tbeOpInfo, std::string &opSpecificInfo) {
  return false;
}

bool DynamicShapeRangeCheckNotSupportUpperStep(const te::TbeOpInfo &tbeOpInfo, bool &isSupported,
    std::vector<size_t> &upperLimitedInputIndexs,
    std::vector<size_t> &lowerLimitedInputIndexs) {
  upperLimitedInputIndexs.emplace_back(0);
  isSupported = false;
  return true;
}

bool DynamicShapeRangeCheckNotSupportLowerStep(const te::TbeOpInfo &tbeOpInfo, bool &isSupported,
    std::vector<size_t> &upperLimitedInputIndexs,
    std::vector<size_t> &lowerLimitedInputIndexs) {
  lowerLimitedInputIndexs.emplace_back(0);
  isSupported = false;
  return true;
}

bool DynamicShapeRangeCheckSupportStep(const te::TbeOpInfo &tbeOpInfo, bool &isSupported,
    std::vector<size_t> &upperLimitedInputIndexs,
    std::vector<size_t> &lowerLimitedInputIndexs) {
  isSupported = true;
  return true;
}

class STEST_fusion_engine_fuzzy_generalize : public testing::Test {
public:
  OpStoreAdapterManagerPtr op_store_adapter_manager_ptr_;
  FEOpsKernelInfoStorePtr ops_kernel_info_store_ptr_;
  FuzzyGeneralizePtr fuzzy_ptr;
  SplitOptimizer split_optimizer;
  ConcatOptimizer concat_optimizer;
  RefRelationsPtr reflection_builder_ptr_;
  FEGraphOptimizerPtr fe_graph_optimizer_;
  TbeOpStoreAdapterPtr tbe_op_store_adapter;
  shared_ptr<fe::SubOpInfoStore> sub_ops_kernel_ptr;
  shared_ptr<fe::SubOpsStore> sub_ops_store_ptr;
  GraphFusionPtr graph_fusion_ptr_;
  NodePtr MakeNode(const ComputeGraphPtr &graph, uint32_t in_num, uint32_t out_num, string name, string type) {
    GeTensorDesc test_desc(GeShape(), FORMAT_NCHW, DT_FLOAT);
    auto op_desc = std::make_shared<OpDesc>(name, type);
    for (auto i = 0; i < in_num; ++i) {
      op_desc->AddInputDesc(test_desc);
    }
    for (auto i = 0; i < out_num; ++i) {
      op_desc->AddOutputDesc(test_desc);
    }
    return graph->AddNode(op_desc);
  }
protected:
  void SetUp()
  {
    tbe_op_store_adapter = std::make_shared<TbeOpStoreAdapter>();
    op_store_adapter_manager_ptr_ = std::make_shared<OpStoreAdapterManager>();
    op_store_adapter_manager_ptr_->map_all_op_store_adapter_.emplace(std::make_pair("tbe_op_adapter", tbe_op_store_adapter));
    ops_kernel_info_store_ptr_ = std::make_shared<FEOpsKernelInfoStore>(op_store_adapter_manager_ptr_, fe::AI_CORE_NAME);
    reflection_builder_ptr_ = std::make_shared<ge::RefRelations>();
    RuleMgrPtr fusion_rule_mgr_ptr_ = std::make_shared<FusionRuleManager>(ops_kernel_info_store_ptr_);
    PassMgrPtr fusion_pass_mgr_ptr_ = std::make_shared<FusionPassManager>();
    FusionPriorityMgrPtr fusion_priority_mgr_ptr_ = std::make_shared<FusionPriorityManager>(
              fe::AI_CORE_NAME, fusion_pass_mgr_ptr_, fusion_rule_mgr_ptr_);
     graph_fusion_ptr_ = std::make_shared<GraphFusion>(fusion_rule_mgr_ptr_,
        ops_kernel_info_store_ptr_, fusion_pass_mgr_ptr_, fusion_priority_mgr_ptr_);
    graph_fusion_ptr_->SetEngineName(fe::AI_CORE_NAME);
    fe_graph_optimizer_ = make_shared<FEGraphOptimizer>(ops_kernel_info_store_ptr_,
        op_store_adapter_manager_ptr_, fe::AI_CORE_NAME);
    fe_graph_optimizer_->graph_fusion_ptr_ = graph_fusion_ptr_;
    OptimizeUtilityStubST *optimize_utility_stub = new OptimizeUtilityStubST();
    fe_graph_optimizer_->optimize_utility_ = optimize_utility_stub;
    fuzzy_ptr = std::make_shared<fe::FuzzyGeneralize>(nullptr, ops_kernel_info_store_ptr_, op_store_adapter_manager_ptr_);
    fuzzy_ptr->optimize_utility_ = optimize_utility_stub;

    FEOpsStoreInfo TBE_OPINFO_STUB = {
            6,
            "tbe-builtin",
            EN_IMPL_HW_TBE,
            "./air/test/engines/nneng/st/stub/fe_config/tbe_opinfo",
            ""
    };

    sub_ops_store_ptr = make_shared<fe::SubOpsStore>(op_store_adapter_manager_ptr_);
    sub_ops_store_ptr->SetSubStoreType("tbe-builtin");
    sub_ops_store_ptr->SetSubStoreInfo(TBE_OPINFO_STUB);
    sub_ops_store_ptr->InitializeSubStore(fe::AI_CORE_NAME);

    vector<FEOpsStoreInfo> store_info;
    store_info.emplace_back(TBE_OPINFO_STUB);
    Configuration::Instance(fe::AI_CORE_NAME).ops_store_info_vector_ = (store_info);

    sub_ops_kernel_ptr = std::make_shared<fe::SubOpInfoStore>(TBE_OPINFO_STUB);
    sub_ops_kernel_ptr->Initialize(fe::AI_CORE_NAME);
    OpsKernelManager::Instance(fe::AI_CORE_NAME).sub_ops_kernel_map_.emplace("tbe-builtin", sub_ops_kernel_ptr);

    std::map<std::string, std::string> options;
    options.insert(std::pair<std::string, std::string>("ge.shape_generalized_build_mode", SHAPE_GENERALIZED));
    ge::GetThreadLocalContext().SetGlobalOption(options);

    std::map<std::string, std::string> options1;
    OpsKernelManager::Instance(fe::AI_CORE_NAME).Finalize();
    ops_kernel_info_store_ptr_->Initialize(options1);
  }

  void TearDown() {
    sub_ops_store_ptr->FinalizeSubStore();
    sub_ops_store_ptr.reset();
    sub_ops_kernel_ptr->Finalize();
    sub_ops_kernel_ptr.reset();
    ops_kernel_info_store_ptr_->Finalize();
  }

  static void CreateBatchNormGraph(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);

    // add descriptor
    vector<int64_t> dims = {1, 2, 3, 32};
    vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}, {1, 200}, {1, 200}});
    GeShape shape(dims);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_ND);
    in_desc2.SetOriginFormat(FORMAT_ND);
    in_desc2.SetDataType(DT_FLOAT16);
    in_desc2.SetShapeRange(range);
    in_desc2.SetOriginShape(shape);
    in_desc2.SetOriginShapeRange(range);
    bn_op->AddInputDesc("x", in_desc2);
    data->AddOutputDesc("x", in_desc2);
    data->AddInputDesc(in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetOriginFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    out_desc2.SetShapeRange(range);
    out_desc2.SetOriginShapeRange(range);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE,
                          static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetBool(bn_op, ge::ATTR_NAME_NOTASK, true);
    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), bn_node->GetInDataAnchor(0));
  }

  static void CreateBatchNormGraph1(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);

    // add descriptor
    vector<int64_t> dims = {1, 2};
    vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}});
    GeShape shape(dims);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_NCHW);
    in_desc2.SetOriginFormat(FORMAT_NCHW);
    in_desc2.SetDataType(DT_FLOAT16);
    in_desc2.SetShapeRange(range);
    in_desc2.SetOriginShape(shape);
    in_desc2.SetOriginShapeRange(range);
    bn_op->AddInputDesc("x", in_desc2);
    data->AddOutputDesc("x", in_desc2);
    data->AddInputDesc(in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetOriginFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    out_desc2.SetShapeRange(range);
    out_desc2.SetOriginShapeRange(range);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE,
                          static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetBool(bn_op, ge::ATTR_NAME_NOTASK, true);
    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), bn_node->GetInDataAnchor(0));
  }

  static void CreateBatchNormGeneralizedGraph(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);

    // add descriptor
    vector<int64_t> dims = {-1, -1, -1, -1};
    vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}, {1, 200}, {1, 200}});
    GeShape shape(dims);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_ND);
    in_desc2.SetOriginFormat(FORMAT_ND);
    in_desc2.SetDataType(DT_FLOAT16);
    in_desc2.SetShapeRange(range);
    in_desc2.SetOriginShape(shape);
    in_desc2.SetOriginShapeRange(range);
    AttrUtils::SetInt(in_desc2, ge::ATTR_NAME_STORAGE_FORMAT, static_cast<int64_t>(ge::FORMAT_NC1HWC0));
    bn_op->AddInputDesc("x", in_desc2);
    data->AddOutputDesc("x", in_desc2);
    data->AddInputDesc(in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetOriginFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE,
                          static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetBool(bn_op, ge::ATTR_NAME_NOTASK, true);
    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), bn_node->GetInDataAnchor(0));
  }

  static void CreateBatchNormGeneralizedGraph1(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);

    // add descriptor
    vector<int64_t> dims = {-1, -1};
    vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}});
    GeShape shape(dims);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_NCHW);
    in_desc2.SetOriginFormat(FORMAT_NCHW);
    in_desc2.SetDataType(DT_FLOAT16);
    in_desc2.SetShapeRange(range);
    in_desc2.SetOriginShape(shape);
    in_desc2.SetOriginShapeRange(range);
    AttrUtils::SetInt(in_desc2, ge::ATTR_NAME_STORAGE_FORMAT, static_cast<int64_t>(ge::FORMAT_NC1HWC0));
    bn_op->AddInputDesc("x", in_desc2);
    data->AddOutputDesc("x", in_desc2);
    data->AddInputDesc(in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetOriginFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE,
                          static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetBool(bn_op, ge::ATTR_NAME_NOTASK, true);
    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), bn_node->GetInDataAnchor(0));
  }

  static void CreateDynamicShapeGraph(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);

    // add descriptor
    vector<int64_t> dims = {-1, 2, 3, 32};
    vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}, {1, 200}, {1, 200}});
    GeShape shape(dims);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_ND);
    in_desc2.SetOriginFormat(FORMAT_ND);
    in_desc2.SetDataType(DT_FLOAT16);
    in_desc2.SetShapeRange(range);
    in_desc2.SetOriginShapeRange(range);
    bn_op->AddInputDesc("x", in_desc2);
    data->AddOutputDesc("x", in_desc2);
    data->AddInputDesc(in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetOriginFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    out_desc2.SetShapeRange(range);
    out_desc2.SetOriginShapeRange(range);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE,
                          static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetBool(bn_op, ge::ATTR_NAME_NOTASK, true);
    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), bn_node->GetInDataAnchor(0));
  }

  static void CreateDynamicShapeGraphWithoutRange(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);

    // add descriptor
    vector<int64_t> dims = {-1, 2, 3, 32};
    GeShape shape(dims);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_ND);
    in_desc2.SetOriginFormat(FORMAT_ND);
    in_desc2.SetDataType(DT_FLOAT16);
    bn_op->AddInputDesc("x", in_desc2);
    data->AddOutputDesc("x", in_desc2);
    data->AddInputDesc(in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetOriginFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE,
                          static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetBool(bn_op, ge::ATTR_NAME_NOTASK, true);
    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), bn_node->GetInDataAnchor(0));
  }

  static void CreateSingleNodeGraph(ComputeGraphPtr graph) {
    OpDescPtr relu_op = std::make_shared<OpDesc>("relu", "Activation");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);
    vector<int64_t> dims = {1, 2, 3, 4};
    GeShape shape(dims);

    GeTensorDesc in_desc1(shape);
    in_desc1.SetFormat(FORMAT_NCHW);
    in_desc1.SetOriginFormat(FORMAT_NCHW);
    in_desc1.SetDataType(DT_FLOAT16);
    relu_op->AddInputDesc("x", in_desc1);
    data->AddOutputDesc("x", in_desc1);

    GeTensorDesc out_desc1(shape);
    out_desc1.SetFormat(FORMAT_HWCN);
    out_desc1.SetOriginFormat(FORMAT_HWCN);
    out_desc1.SetDataType(DT_FLOAT16);
    relu_op->AddOutputDesc("y", out_desc1);

    ge::AttrUtils::SetInt(relu_op, FE_IMPLY_TYPE, static_cast<int>(EN_IMPL_HW_GENERAL_CCE));
    NodePtr relu_node = graph->AddNode(relu_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), relu_node->GetInDataAnchor(0));
  }

  static void CreateSingleNodeGraph2(ComputeGraphPtr graph) {
    OpDescPtr max_pool_op = std::make_shared<OpDesc>("maxpool", "MaxPoolV3");
    OpDescPtr data = std::make_shared<OpDesc>("DATA0", fe::DATA);
    vector<int64_t> dims = {1, 2, 3, 4};
    GeShape shape(dims);

    GeTensorDesc in_desc1(shape);
    in_desc1.SetFormat(FORMAT_NCHW);
    in_desc1.SetOriginFormat(FORMAT_NCHW);
    in_desc1.SetDataType(DT_FLOAT16);
    max_pool_op->AddInputDesc("x", in_desc1);
    data->AddOutputDesc("x", in_desc1);

    GeTensorDesc out_desc1(shape);
    out_desc1.SetFormat(FORMAT_HWCN);
    out_desc1.SetOriginFormat(FORMAT_HWCN);
    out_desc1.SetDataType(DT_FLOAT16);
    max_pool_op->AddOutputDesc("y", out_desc1);

    ge::AttrUtils::SetInt(max_pool_op, FE_IMPLY_TYPE, static_cast<int>(EN_IMPL_HW_GENERAL_CCE));
    NodePtr relu_node = graph->AddNode(max_pool_op);
    NodePtr data_node = graph->AddNode(data);
    GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), relu_node->GetInDataAnchor(0));
  }

  static void CreateComplexGraph(ComputeGraphPtr graph, string data_type) {
    shared_ptr<ge::OpDesc> op_desc_ptr = make_shared<ge::OpDesc>("tbe_conv2d", "Convolution");

    vector<ge::GeTensorPtr> weights;
    vector<int64_t> dim_weight = {256, 256, 512};
    uint32_t data_size = 256 * 256 * 512;
    GeShape shape_weight(dim_weight);
    GeTensorDesc weight_desc(shape_weight);
    GeTensor tensor(weight_desc);
    if (data_type == "float") {
      weight_desc.SetDataType(DT_FLOAT);
      vector<float> weight(data_size, 0.01);
      vector<uint8_t> buffer(data_size * 4);
      memcpy(buffer.data(), weight.data(), data_size * 4);
      tensor.SetData(std::move(buffer));
    } else if (data_type == "int32") {
      weight_desc.SetDataType(DT_INT32);
      vector<int32_t> weight(data_size, 1);
      vector<uint8_t> buffer(data_size * 4);
      memcpy(buffer.data(), weight.data(), data_size * 4);
      tensor.SetData(std::move(buffer));
    }
    GeTensorPtr tensor_ptr = make_shared<GeTensor>(tensor);
    weights.emplace_back(tensor_ptr);

    OpDescPtr weight_op_desc1 = std::make_shared<OpDesc>("w1", "Const");
    weight_op_desc1->AddInputDesc("x", weight_desc);
    weight_op_desc1->AddOutputDesc("z", weight_desc);
    std::vector<bool> w_input_const;
    w_input_const.emplace_back(false);
    weight_op_desc1->SetIsInputConst(w_input_const);

    int64_t int_value = 1;
    float float_value = 2.0;
    bool bool_value = false;
    string str_value = "abc";
    vector<int64_t> int_vec{1, 2, 3};
    vector<int64_t> rint_vec;
    vector<float> float_vec{4.0, 5.0, 6.0};
    vector<float> rfloat_vec;
    vector<bool> bool_vec{false, true, true};
    vector<bool> rbool_vec;
    std::vector<string> str_vec{"a", "b", "c"};
    AttrUtils::SetInt(op_desc_ptr, "transposX", int_value);
    AttrUtils::SetFloat(op_desc_ptr, "transposY", float_value);
    AttrUtils::SetBool(op_desc_ptr, "attrBool", bool_value);
    AttrUtils::SetStr(op_desc_ptr, "attrStr", str_value);
    AttrUtils::SetListInt(op_desc_ptr, "attrListInt", int_vec);
    AttrUtils::SetListFloat(op_desc_ptr, "attrListFloat", float_vec);
    AttrUtils::SetListBool(op_desc_ptr, "attrListBool", bool_vec);
    AttrUtils::SetListStr(op_desc_ptr, "attrListStr", str_vec);

    AttrUtils::SetInt(weight_op_desc1, "transposX", int_value);
    AttrUtils::SetFloat(weight_op_desc1, "transposY", float_value);
    AttrUtils::SetBool(weight_op_desc1, "attrBool", bool_value);
    AttrUtils::SetStr(weight_op_desc1, "attrStr", str_value);
    AttrUtils::SetListInt(weight_op_desc1, "attrListInt", int_vec);
    AttrUtils::SetListFloat(weight_op_desc1, "attrListFloat", float_vec);
    AttrUtils::SetListBool(weight_op_desc1, "attrListBool", bool_vec);
    AttrUtils::SetListStr(weight_op_desc1, "attrListStr", str_vec);

    ge::DataType set_dtype = ge::DT_FLOAT;
    std::vector<int64_t> shape_vec{256, 256, 512};
    ge::GeShape shape_desc = ge::GeShape(shape_vec);

    shared_ptr<ge::GeTensorDesc> input0_desc_ptr = make_shared<ge::GeTensorDesc>();
    input0_desc_ptr->SetDataType(set_dtype);
    input0_desc_ptr->SetShape(shape_desc);
    op_desc_ptr->AddInputDesc("x", input0_desc_ptr->Clone());

    shared_ptr<ge::GeTensorDesc> input1_desc_ptr = make_shared<ge::GeTensorDesc>();
    if (data_type == "float") {
      input1_desc_ptr->SetDataType(DT_FLOAT);
    } else if (data_type == "int32") {
      input1_desc_ptr->SetDataType(DT_INT32);
    }
    input1_desc_ptr->SetShape(shape_desc);
    op_desc_ptr->AddInputDesc("y", input1_desc_ptr->Clone());

    std::vector<bool> is_input_const;
    is_input_const.emplace_back(false);
    is_input_const.emplace_back(true);
    op_desc_ptr->SetIsInputConst(is_input_const);

    shared_ptr<ge::GeTensorDesc> output_desc_ptr = make_shared<ge::GeTensorDesc>();
    output_desc_ptr->SetDataType(set_dtype);
    output_desc_ptr->SetShape(shape_desc);
    op_desc_ptr->AddOutputDesc("z", output_desc_ptr->Clone());

    AttrUtils::SetInt(op_desc_ptr, "imply_type", EN_IMPL_HW_TBE);
    NodePtr weight_node1 = graph->AddNode(weight_op_desc1);
    NodePtr conv_node = graph->AddNode(op_desc_ptr);
    op_desc_ptr->SetName("conv2");
    NodePtr conv_next_node = graph->AddNode(op_desc_ptr);
    GraphUtils::AddEdge(weight_node1->GetOutDataAnchor(0), conv_node->GetInDataAnchor(1));
    GraphUtils::AddEdge(weight_node1->GetOutDataAnchor(0), conv_next_node->GetInDataAnchor(1));
    GraphUtils::AddEdge(conv_node->GetOutDataAnchor(0), conv_next_node->GetInDataAnchor(0));
    OpDescUtils::SetWeights(*conv_node, weights);
    OpDescUtils::SetWeights(*conv_next_node, weights);
  }

  static void CreateSimpleGraph(ComputeGraphPtr graph) {
    shared_ptr<ge::OpDesc> op_desc_ptr = make_shared<ge::OpDesc>("tbe_conv2d", "conv");

    int64_t int_value = 1;
    float float_value = 2.0;
    bool bool_value = false;
    string str_value = "abc";
    vector<int64_t> int_vec{1, 2, 3};
    vector<int64_t> rint_vec;
    vector<float> float_vec{4.0, 5.0, 6.0};
    vector<float> rfloat_vec;
    vector<bool> bool_vec{false, true, true};
    vector<bool> rbool_vec;
    std::vector<string> str_vec{"a", "b", "c"};
    AttrUtils::SetInt(op_desc_ptr, "transposX", int_value);
    AttrUtils::SetFloat(op_desc_ptr, "transposY", float_value);
    AttrUtils::SetBool(op_desc_ptr, "attrBool", bool_value);
    AttrUtils::SetStr(op_desc_ptr, "attrStr", str_value);
    AttrUtils::SetListInt(op_desc_ptr, "attrListInt", int_vec);
    AttrUtils::SetListFloat(op_desc_ptr, "attrListFloat", float_vec);
    AttrUtils::SetListBool(op_desc_ptr, "attrListBool", bool_vec);
    AttrUtils::SetListStr(op_desc_ptr, "attrListStr", str_vec);

    ge::DataType set_dtype = ge::DT_FLOAT16;
    std::vector<int64_t> shape_vec{256, 256, 512};
    ge::GeShape shape_desc = ge::GeShape(shape_vec);

    shared_ptr<ge::GeTensorDesc> input0_desc_ptr = make_shared<ge::GeTensorDesc>();
    input0_desc_ptr->SetDataType(set_dtype);
    input0_desc_ptr->SetShape(shape_desc);
    op_desc_ptr->AddInputDesc("x", input0_desc_ptr->Clone());

    shared_ptr<ge::GeTensorDesc> input1_desc_ptr = make_shared<ge::GeTensorDesc>();
    input1_desc_ptr->SetDataType(set_dtype);
    input1_desc_ptr->SetShape(shape_desc);
    op_desc_ptr->AddInputDesc("y", input1_desc_ptr->Clone());

    std::vector<bool> is_input_const;
    is_input_const.emplace_back(false);
    is_input_const.emplace_back(true);
    op_desc_ptr->SetIsInputConst(is_input_const);

    shared_ptr<ge::GeTensorDesc> output_desc_ptr = make_shared<ge::GeTensorDesc>();
    output_desc_ptr->SetDataType(set_dtype);
    output_desc_ptr->SetShape(shape_desc);
    op_desc_ptr->AddOutputDesc("z", output_desc_ptr->Clone());

    AttrUtils::SetInt(op_desc_ptr, "imply_type", EN_IMPL_HW_TBE);
    NodePtr conv_node = graph->AddNode(op_desc_ptr);
    op_desc_ptr->SetName("conv2");
    NodePtr conv_next_node = graph->AddNode(op_desc_ptr);
    GraphUtils::AddEdge(conv_node->GetOutDataAnchor(0), conv_next_node->GetInDataAnchor(0));
  }

  static void CreateTwoOpDescGraph(ComputeGraphPtr graph) {
    OpDescPtr bn_op = std::make_shared<OpDesc>("batchnormal", "BatchNorm");
    OpDescPtr relu_op = std::make_shared<OpDesc>("relu", "Activation");

    // add descriptor
    vector<int64_t> dims = {1, 2, 3, 4};
    GeShape shape(dims);

    GeTensorDesc in_desc1(shape);
    in_desc1.SetFormat(FORMAT_NCHW);
    in_desc1.SetDataType(DT_FLOAT16);
    relu_op->AddInputDesc("x", in_desc1);

    GeTensorDesc out_desc1(shape);
    out_desc1.SetFormat(FORMAT_HWCN);
    out_desc1.SetDataType(DT_FLOAT16);
    relu_op->AddOutputDesc("y", out_desc1);

    GeTensorDesc in_desc2(shape);
    in_desc2.SetFormat(FORMAT_FRACTAL_Z);
    in_desc2.SetDataType(DT_FLOAT16);
    bn_op->AddInputDesc("x", in_desc2);

    GeTensorDesc out_desc2(shape);
    out_desc2.SetFormat(FORMAT_NHWC);
    out_desc2.SetDataType(DT_FLOAT16);
    bn_op->AddOutputDesc("y", out_desc2);
    std::vector<bool> is_in_const_vec = {false};
    bn_op->SetIsInputConst(is_in_const_vec);

    GeTensorDesc in_desc3(shape);
    in_desc3.SetFormat(FORMAT_FRACTAL_Z);
    in_desc3.SetDataType(DT_FLOAT16);

    GeTensorDesc in_desc4(shape);
    in_desc4.SetFormat(FORMAT_FRACTAL_Z);
    in_desc4.SetDataType(DT_FLOAT16);

    GeTensorDesc out_desc3(shape);
    out_desc3.SetFormat(FORMAT_NHWC);
    out_desc3.SetDataType(DT_FLOAT16);

    ge::AttrUtils::SetInt(bn_op, FE_IMPLY_TYPE, static_cast<int>(EN_IMPL_HW_TBE));
    ge::AttrUtils::SetInt(relu_op, FE_IMPLY_TYPE, static_cast<int>(EN_IMPL_HW_GENERAL_CCE));

    NodePtr bn_node = graph->AddNode(bn_op);
    NodePtr relu_node = graph->AddNode(relu_op);
    GraphUtils::AddEdge(bn_node->GetOutDataAnchor(0), relu_node->GetInDataAnchor(0));
  }
};

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_feedInputsRootSet) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->unset_dynamic_shape_counts_ = 0;

  NodeGeneralInfoPtr node_info = std::make_shared<NodeGeneralInfo>();
  NodeGeneralInfoPtr node_info_data = std::make_shared<NodeGeneralInfo>();
  node_info_data->disjoint_root_set.emplace(data_node);
  fuzzy_ptr->node_info_map_.insert(std::make_pair(data_node, node_info_data));
  fuzzy_ptr->FeedInputsRootSet(bn_node, node_info);
  EXPECT_EQ(node_info->disjoint_root_set, node_info_data->disjoint_root_set);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_check_is_external) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "batchnormal") {
      EXPECT_EQ(fuzzy_ptr->CheckIsExternalNode(node), false);
    } else {
      EXPECT_EQ(fuzzy_ptr->CheckIsExternalNode(node), true);
    }
  }
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_check_and_update_limited_nodes) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateDynamicShapeGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->unset_dynamic_shape_counts_ = 0;
  fuzzy_ptr->original_input_nodes_.emplace("DATA0", data_node);

  bool flag = false;
  vector<int64_t> dims = {1, 2, 3, 32};
  vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}, {1, 200}, {1, 200}});
  const std::vector<NodePtr> nodes = {bn_node};
  NodeGeneralInfoPtr node_info_bn = std::make_shared<NodeGeneralInfo>();;
  node_info_bn->disjoint_root_set.emplace(data_node);
  node_info_bn->inputs_root_map.insert(std::make_pair(bn_node->GetOpDesc()->MutableInputDesc(0),
      node_info_bn->disjoint_root_set));
  fuzzy_ptr->node_info_map_.insert(std::make_pair(bn_node, node_info_bn));

  tbe_op_store_adapter->DynamicShapeRangeCheck = DynamicShapeRangeCheckSupportStep;
  (void)fuzzy_ptr->CheckAndUpdateLimitedNodes(tbe_op_store_adapter, nodes, flag);
  vector<pair<int64_t, int64_t>> range_temp;
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_temp);
  EXPECT_EQ(range_temp, range);

  fuzzy_ptr->node_info_map_.insert(std::make_pair(bn_node, node_info_bn));
  fuzzy_ptr->decent_times_count_.insert(make_pair(data_node->GetName(), 1));
  tbe_op_store_adapter->DynamicShapeRangeCheck = DynamicShapeRangeCheckNotSupportUpperStep;
  (void)fuzzy_ptr->CheckAndUpdateLimitedNodes(tbe_op_store_adapter, nodes, flag);
  range_temp.clear();
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_temp);
  EXPECT_EQ(range_temp, range);

  tbe_op_store_adapter->DynamicShapeRangeCheck = DynamicShapeRangeCheckNotSupportLowerStep;
  (void)fuzzy_ptr->CheckAndUpdateLimitedNodes(tbe_op_store_adapter, nodes, flag);
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_temp);
  EXPECT_NE(range_temp, range);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_update_dynamic_shape_to_original) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateDynamicShapeGraph(graph);
  ge::NodePtr data_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    }
  }

  ge::ComputeGraphPtr graph_1 = std::make_shared<ComputeGraph>("test1");
  CreateDynamicShapeGraph(graph_1);
  ge::NodePtr data1_node;
  for (auto &node : graph_1->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data1_node = node;
    }
  }

  vector<std::pair<int64_t, int64_t>> range1({{1, 100}, {1, 100}, {1, 100}, {1, 100}});
  data1_node->GetOpDesc()->MutableOutputDesc(0)->SetOriginShapeRange(range1);

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->unset_dynamic_shape_counts_ = 0;

  fuzzy_ptr->external_input_nodes_.emplace(data1_node);
  fuzzy_ptr->UpdateDynamicShapeToNewBakGraph(*graph_1);
  std::vector<std::pair<int64_t, int64_t>> src_shape_range;
  data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(src_shape_range);
  EXPECT_NE(range1, src_shape_range);

  ge::ComputeGraphPtr graph_2 = std::make_shared<ComputeGraph>("test2");
  CreateDynamicShapeGraphWithoutRange(graph_2);
  ge::NodePtr data2_node;
  for (auto &node : graph_2->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data2_node = node;
    }
  }
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data2_node);
  fuzzy_ptr->UpdateDynamicShapeToNewBakGraph(*graph_2);

  ge::ComputeGraphPtr graph_3 = std::make_shared<ComputeGraph>("test2");
  CreateBatchNormGraph(graph_3);
  ge::NodePtr data3_node;
  for (auto &node : graph_3->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data3_node = node;
    }
  }
  fuzzy_ptr->external_input_nodes_.emplace(data3_node);
  fuzzy_ptr->UpdateDynamicShapeToNewBakGraph(*graph_3);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_graph_preprocessing) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->unset_dynamic_shape_counts_ = 0;

  tbe_op_store_adapter->GetOpSpecificInfo = GetOpSpecificInfoLimitStep;
  fe::Status res = fuzzy_ptr->GraphPreprocessing(*graph, tbe_op_store_adapter);
  EXPECT_EQ(fuzzy_ptr->external_input_nodes_.empty(), false);
  EXPECT_EQ(fuzzy_ptr->limited_range_nodes_.empty(), false);
  EXPECT_EQ(res, fe::SUCCESS);

  tbe_op_store_adapter->GetOpSpecificInfo = GetOpSpecificInfoUnlimitStep;
  res = fuzzy_ptr->GraphPreprocessing(*graph, tbe_op_store_adapter);
  EXPECT_EQ(res, fe::SUCCESS);

  tbe_op_store_adapter->GetOpSpecificInfo = GetOpSpecificInfoDynamicStep;
  res = fuzzy_ptr->GraphPreprocessing(*graph, tbe_op_store_adapter);
  EXPECT_EQ(res, fe::SUCCESS);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_update_input_nodes) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateDynamicShapeGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  vector<std::pair<int64_t, int64_t>> range({{1, 200}, {1, 200}, {1, 200}, {1, 200}});
  vector<std::pair<int64_t, int64_t>> range_last;
  vector<std::pair<int64_t, int64_t>> range_cur;
  fuzzy_ptr->original_input_nodes_.emplace("DATA0", data_node);
  vector<size_t> limited_input_indexs = {0};
  NodeGeneralInfoPtr node_info_bn = std::make_shared<NodeGeneralInfo>();;
  std::unordered_set<ge::NodePtr> root_set{data_node};
  node_info_bn->inputs_root_map.insert(std::make_pair(bn_node->GetOpDesc()->MutableInputDesc(0), root_set));
  fuzzy_ptr->node_info_map_.insert(std::make_pair(bn_node, node_info_bn));

  fuzzy_ptr->decent_times_count_.insert(std::make_pair("DATA0", 1));
  fuzzy_ptr->unset_dynamic_shape_counts_ = 2;
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_cur);
  fe::Status res = fuzzy_ptr->Downgrades(bn_node, true, limited_input_indexs);
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_last);
  EXPECT_NE(range_last, range_cur);
  range_last.clear();
  range_cur.clear();

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->unset_dynamic_shape_counts_ = 0;

  fuzzy_ptr->decent_times_count_["DATA0"] = 2;
  fuzzy_ptr->decent_steps_.insert(pair<string, vector<double>>("DATA0", {5.0, 5.0, 5.0, 5.0}));
  fuzzy_ptr->unset_dynamic_shape_counts_ = 2;
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_cur);
  res = fuzzy_ptr->Downgrades(bn_node, true, limited_input_indexs);
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_last);
  
  EXPECT_NE(range_last, range_cur);
  range_last.clear();
  range_cur.clear();

  fuzzy_ptr->unset_dynamic_shape_counts_ = 5;
  fuzzy_ptr->decent_times_count_["DATA0"] = 5;
  res = fuzzy_ptr->Downgrades(bn_node, true, limited_input_indexs);
  EXPECT_EQ(res, fe::FAILED);

  fuzzy_ptr->unset_dynamic_shape_counts_ = 2;
  fuzzy_ptr->decent_times_count_["DATA0"] = 5;
  res = fuzzy_ptr->Downgrades(bn_node, true, limited_input_indexs);
  EXPECT_EQ(res, fe::SUCCESS);

  fuzzy_ptr->unset_dynamic_shape_counts_ = 2;
  fuzzy_ptr->decent_times_count_["DATA0"] = 5;
  res = fuzzy_ptr->Downgrades(bn_node, false, limited_input_indexs);
  EXPECT_EQ(fuzzy_ptr->unset_dynamic_shape_counts_ , 3);

  vector<size_t> limited_input_indexs1 = {2};
  fuzzy_ptr->unset_dynamic_shape_counts_ = 2;
  fuzzy_ptr->decent_times_count_["DATA0"] = 5;
  res = fuzzy_ptr->Downgrades(bn_node, true, limited_input_indexs1);
  EXPECT_EQ(res, fe::FAILED);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_calculate_decent_steps) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateDynamicShapeGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  vector<std::pair<int64_t, int64_t>> range_last;
  vector<std::pair<int64_t, int64_t>> range_cur;
  NodeGeneralInfoPtr node_info_bn = std::make_shared<NodeGeneralInfo>();;
  std::unordered_set<ge::NodePtr> root_set{data_node};
  node_info_bn->inputs_root_map.insert(std::make_pair(bn_node->GetOpDesc()->MutableInputDesc(0), root_set));

  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_cur);
  fuzzy_ptr->CalDecentSteps(data_node, data_node->GetOpDesc());
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->GetShapeRange(range_last);
  EXPECT_EQ(fuzzy_ptr->decent_steps_.empty(), false);

  vector<std::pair<int64_t, int64_t>> range({{1024, -1}, {1, 200}, {1, 200}, {1, 200}});
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->SetShapeRange(range);
  (void)data_node->GetOpDesc()->MutableOutputDesc(0)->SetOriginShapeRange(range);
  fuzzy_ptr->CalDecentSteps(data_node, data_node->GetOpDesc());
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_init_range_decentinfos) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateDynamicShapeGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->InitOriginalGraphInfos(*graph);
  EXPECT_EQ(fuzzy_ptr->decent_steps_.empty(), false);
  EXPECT_EQ(fuzzy_ptr->decent_times_count_.empty(), false);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_generalize_graph) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  fuzzy_ptr->decent_steps_.clear();
  fuzzy_ptr->decent_times_count_.clear();
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->unset_dynamic_shape_counts_ = 0;
  fuzzy_ptr->original_input_nodes_.emplace("DATA0", data_node);

  tbe_op_store_adapter->GetOpSpecificInfo = GetOpSpecificInfoDynamicStep;
  tbe_op_store_adapter->DynamicShapeRangeCheck = DynamicShapeRangeCheckSupportStep;
  tbe_op_store_adapter->CheckIsTbeGeneralizeFuncRegistered = CheckIsRegistered;
  tbe_op_store_adapter->TeGeneralize = TeGeneralizeStub;
  Status res = fuzzy_ptr->GeneralizeGraph(*graph);
  EXPECT_EQ(res, fe::SUCCESS);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_C_axis_corrent_001) {
  ge::ComputeGraphPtr generalized_graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGeneralizedGraph(generalized_graph);
  ge::ComputeGraphPtr original_graph = std::make_shared<ComputeGraph>("test1");
  CreateBatchNormGraph(original_graph);
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->original_input_nodes_.clear();
  ge::NodePtr node;
  for (auto &cur_node : generalized_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->external_input_nodes_.emplace(cur_node);
      node = cur_node;
    }
    if (cur_node->GetType() == "BatchNorm") {
        NodeGeneralInfoPtr node_info_ptr = std::make_shared<NodeGeneralInfo>();
        node_info_ptr->op_kernel = OpsKernelManager::Instance(AI_CORE_NAME).GetOpKernelInfoByOpType("tbe-builtin", "BatchNorm");
        fuzzy_ptr->node_info_map_.insert(std::make_pair(cur_node, node_info_ptr));
    }
  }
  for (auto &cur_node : original_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->original_input_nodes_.emplace(std::make_pair(cur_node->GetName(), cur_node));
    }
  }

  Status res = fuzzy_ptr->UpdateDynamicShapeToOriginalGraph(*generalized_graph);
  EXPECT_EQ(res, fe::SUCCESS);

  std::vector<int64_t> shape = node->GetOpDesc()->MutableInputDesc(0)->GetOriginShape().GetDims();
  EXPECT_EQ(shape, (std::vector<int64_t>{ -1, 2, -1, -1 }));
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_C_axis_corrent_002) {
  ge::ComputeGraphPtr generalized_graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGeneralizedGraph1(generalized_graph);
  ge::ComputeGraphPtr original_graph = std::make_shared<ComputeGraph>("test1");
  CreateBatchNormGraph1(original_graph);
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->original_input_nodes_.clear();
  ge::NodePtr node;
  for (auto &cur_node : generalized_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->external_input_nodes_.emplace(cur_node);
      node = cur_node;
    }
    if (cur_node->GetType() == "BatchNorm") {
      NodeGeneralInfoPtr node_info_ptr = std::make_shared<NodeGeneralInfo>();
      node_info_ptr->op_kernel = OpsKernelManager::Instance(AI_CORE_NAME).GetOpKernelInfoByOpType("tbe-builtin", "BatchNorm");
      fuzzy_ptr->node_info_map_.insert(std::make_pair(cur_node, node_info_ptr));
    }
  }
  for (auto &cur_node : original_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->original_input_nodes_.emplace(std::make_pair(cur_node->GetName(), cur_node));
    }
  }

  Status res = fuzzy_ptr->UpdateDynamicShapeToOriginalGraph(*generalized_graph);
  EXPECT_EQ(res, fe::SUCCESS);

  std::vector<int64_t> shape = node->GetOpDesc()->MutableInputDesc(0)->GetOriginShape().GetDims();
  EXPECT_EQ(shape, (std::vector<int64_t>{ -1, 2}));
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_C_axis_corrent_003) {
  ge::ComputeGraphPtr generalized_graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGeneralizedGraph1(generalized_graph);
  ge::ComputeGraphPtr original_graph = std::make_shared<ComputeGraph>("test1");
  CreateBatchNormGraph1(original_graph);
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->original_input_nodes_.clear();
  ge::NodePtr node;
  for (auto &cur_node : generalized_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->external_input_nodes_.emplace(cur_node);
      node = cur_node;
    }
    if (cur_node->GetType() == "BatchNorm") {
      cur_node->GetOpDesc()->SetType("Cosh");
      cur_node->GetOpDesc()->SetName("cosh");
      NodeGeneralInfoPtr node_info_ptr = std::make_shared<NodeGeneralInfo>();
      node_info_ptr->op_kernel = OpsKernelManager::Instance(AI_CORE_NAME).GetOpKernelInfoByOpType("tbe-builtin", "BatchNorm");
      fuzzy_ptr->node_info_map_.insert(std::make_pair(cur_node, node_info_ptr));
    }
  }
  for (auto &cur_node : original_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->original_input_nodes_.emplace(std::make_pair(cur_node->GetName(), cur_node));
    }
    if (cur_node->GetType() == "BatchNorm") {
      cur_node->GetOpDesc()->SetType("Cosh");
      cur_node->GetOpDesc()->SetName("cosh");
    }
  }

  Status res = fuzzy_ptr->UpdateDynamicShapeToOriginalGraph(*generalized_graph);
  EXPECT_EQ(res, fe::SUCCESS);

  std::vector<int64_t> shape = node->GetOpDesc()->MutableInputDesc(0)->GetOriginShape().GetDims();
  EXPECT_EQ(shape, (std::vector<int64_t>{-1, 2}));
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, fuzzy_generalize_C_axis_corrent_004) {
  ge::ComputeGraphPtr generalized_graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGeneralizedGraph(generalized_graph);
  ge::ComputeGraphPtr original_graph = std::make_shared<ComputeGraph>("test1");
  CreateBatchNormGraph(original_graph);
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->original_input_nodes_.clear();
  ge::NodePtr node;
  for (auto &cur_node : generalized_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->external_input_nodes_.emplace(cur_node);
      node = cur_node;
    }
  }
  for (auto &cur_node : original_graph->GetDirectNode()) {
    if (cur_node->GetType() == fe::DATA) {
      fuzzy_ptr->original_input_nodes_.emplace(std::make_pair(cur_node->GetName(), cur_node));
    }
  }
  Status res = fuzzy_ptr->UpdateDynamicShapeToOriginalGraph(*generalized_graph);
  EXPECT_EQ(res, fe::FAILED);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, get_subgraphs_by_curnode_001) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->is_range_limited_graph_ = true;
  fuzzy_ptr->node_info_map_.clear();
  InputNodeGeneralize input_node_generalize(fuzzy_ptr->external_input_nodes_, fuzzy_ptr->is_range_limited_graph_,
      fuzzy_ptr->node_info_map_, tbe_op_store_adapter);
  std::vector<ge::ComputeGraphPtr> cur_node_subgraph = input_node_generalize.GetSubgraphsByCurNode(data_node);
  EXPECT_EQ(cur_node_subgraph.empty(), true);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, get_subgraphs_by_curnode_002) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }

  vector<NodePtr> node_vec;
  node_vec.emplace_back(bn_node);

  FFTSGraphOptimizerPtr ffts_graph_opt_ptr = make_shared<FFTSPlusGraphOptimizer>();
  ffts_graph_opt_ptr->graph_comm_ptr_ = make_shared<GraphComm>("engineName");
  ffts_graph_opt_ptr->graph_comm_ptr_->Initialize();

  ComputeGraphPtr src_graph = std::make_shared<ComputeGraph>("src_graph");
  ge::GeShape original_shape = GeShape({3, 12, 5, 6});
  GraphConstructor test2(src_graph, "", ge::FORMAT_NCHW, ge::DT_FLOAT, original_shape);
  ge::AttrUtils::SetStr(graph, ge::ATTR_NAME_SESSION_GRAPH_ID, "0_1");
  test2.AddOpDesc("const", "Const")
       .AddOpDesc("constant", "Constant");

  graph->SetExtAttr("part_src_graph", src_graph);
  graph->SetParentGraph(src_graph);
  auto sub_graph = src_graph->GetSubgraph("test_sgt_graph_0");

  Status ret = ffts_graph_opt_ptr->TransSingleSubGraph(*graph, node_vec);

  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->is_range_limited_graph_ = true;
  fuzzy_ptr->node_info_map_.clear();
  InputNodeGeneralize input_node_generalize(fuzzy_ptr->external_input_nodes_, fuzzy_ptr->is_range_limited_graph_,
      fuzzy_ptr->node_info_map_, tbe_op_store_adapter);
  std::vector<ge::ComputeGraphPtr> cur_node_subgraph = input_node_generalize.GetSubgraphsByCurNode(data_node);
  //EXPECT_EQ(cur_node_subgraph.empty(), false);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, merge_range) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->is_range_limited_graph_ = true;
  fuzzy_ptr->node_info_map_.clear();
  InputNodeGeneralize input_node_generalize(fuzzy_ptr->external_input_nodes_, fuzzy_ptr->is_range_limited_graph_,
      fuzzy_ptr->node_info_map_, tbe_op_store_adapter);

  std::pair<int64_t, int64_t> upper_limit_max_range{1, 3};
  std::pair<int64_t, int64_t> range{1, 2};
  size_t dim_index = 0;
  std::vector<std::pair<int64_t, int64_t>> dst_shape_range;
  Status res = input_node_generalize.MergeRangeWithUpperLimitMax(upper_limit_max_range, range, dim_index, dst_shape_range);
  EXPECT_EQ(res, fe::FAILED);
  dst_shape_range.emplace_back(upper_limit_max_range);
  dim_index = 3;
  res = input_node_generalize.MergeRangeWithUpperLimitMax(upper_limit_max_range, range, dim_index, dst_shape_range);
  EXPECT_EQ(res, fe::FAILED);

  dst_shape_range.emplace_back(range);
  std::pair<int64_t, int64_t> upper_limit_max_range1{6, 31};
  std::pair<int64_t, int64_t> range1{2, 18};
  dim_index = 1;
  res = input_node_generalize.MergeRangeWithUpperLimitMax(upper_limit_max_range1, range1, dim_index, dst_shape_range);
  EXPECT_EQ(res, fe::SUCCESS);

  std::pair<int64_t, int64_t> upper_limit_max_range2{3, 4};
  res = input_node_generalize.MergeRangeWithUpperLimitMax(upper_limit_max_range1, range, dim_index, dst_shape_range);
  EXPECT_EQ(res, fe::FAILED);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, unlimited_node_generalize) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->is_range_limited_graph_ = true;
  fuzzy_ptr->node_info_map_.clear();
  InputNodeGeneralize input_node_generalize(fuzzy_ptr->external_input_nodes_, fuzzy_ptr->is_range_limited_graph_,
      fuzzy_ptr->node_info_map_, tbe_op_store_adapter);

  NodeGeneralInfoPtr node_info_bn = std::make_shared<NodeGeneralInfo>();;
  std::unordered_set<ge::NodePtr> root_set{data_node};
  node_info_bn->inputs_root_map.insert(std::make_pair(bn_node->GetOpDesc()->MutableInputDesc(0), root_set));
  node_info_bn->is_found_in_opstore = true;
  tbe_op_store_adapter->TeGeneralize = TeGeneralizeException;
  Status res = input_node_generalize.UnlimitedNodeGeneralize(bn_node, node_info_bn);
  EXPECT_EQ(res, fe::FAILED);

  node_info_bn->is_found_in_opstore = false;
  res = input_node_generalize.UnlimitedNodeGeneralize(bn_node, node_info_bn);
  EXPECT_EQ(res, fe::FAILED);

  tbe_op_store_adapter->TeGeneralize = TeGeneralizeTrue;
  res = input_node_generalize.UnlimitedNodeGeneralize(bn_node, node_info_bn);
  EXPECT_EQ(res, fe::SUCCESS);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, GeneralizeFirstNodeOfGraph) {
  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->is_range_limited_graph_ = false;
  fuzzy_ptr->node_info_map_.clear();

  NodeGeneralInfoPtr node_info_bn = std::make_shared<NodeGeneralInfo>();;
  std::unordered_set<ge::NodePtr> root_set{data_node};
  node_info_bn->inputs_root_map.insert(std::make_pair(bn_node->GetOpDesc()->MutableInputDesc(0), root_set));
  node_info_bn->is_found_in_opstore = false;
  fuzzy_ptr->node_info_map_.insert(std::make_pair(bn_node, node_info_bn));
  InputNodeGeneralize input_node_generalize(fuzzy_ptr->external_input_nodes_, fuzzy_ptr->is_range_limited_graph_,
      fuzzy_ptr->node_info_map_, tbe_op_store_adapter);
  tbe_op_store_adapter->TeGeneralize = TeGeneralizeException;
  Status res = input_node_generalize.GeneralizeFirstNodeOfGraph(bn_node);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, downgrades_test) {
  bool is_upper_limited;
  std::vector<size_t> limited_input_indexs;
  limited_input_indexs.emplace_back(0);
  limited_input_indexs.emplace_back(1);
  limited_input_indexs.emplace_back(2);
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  OpDescPtr op1 = std::make_shared<OpDesc>("test", "test");
  vector<int64_t> dim_input({4, 33, 12, 16, 64});
  GeShape shape(dim_input);
  GeTensorDesc tensor_desc(shape);
  tensor_desc.SetOriginFormat(FORMAT_ND);
  tensor_desc.SetOriginShape(shape);
  tensor_desc.SetFormat(FORMAT_ND);
  tensor_desc.SetDataType(DT_FLOAT);
  op1->AddInputDesc("x", tensor_desc);
  op1->AddOutputDesc("y", tensor_desc);
  ge::NodePtr cur_node = graph->AddNode(op1);
  std::map<ge::NodePtr, NodeGeneralInfoPtr> node_info_map_;
  NodeGeneralInfoPtr node_info = std::make_shared<NodeGeneralInfo>();
  fuzzy_ptr->node_info_map_.insert(std::make_pair(cur_node, node_info));
  Status ret = fuzzy_ptr->Downgrades(cur_node, is_upper_limited, limited_input_indexs);
  EXPECT_EQ(ret, fe::FAILED);
}

TEST_F(STEST_fusion_engine_fuzzy_generalize, merge_range_with_upper_limit_max_test) {
  std::pair<int64_t, int64_t> upper_limit_max_range{3, 4};
  std::pair<int64_t, int64_t> range{1, 2};
  size_t dim_index;
  std::vector<std::pair<int64_t, int64_t>> dst_shape_range;

  ge::ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  CreateBatchNormGraph(graph);
  ge::NodePtr data_node;
  ge::NodePtr bn_node;
  for (auto &node : graph->GetDirectNode()) {
    if (node->GetName() == "DATA0") {
      data_node = node;
    } else {
      bn_node = node;
    }
  }
  fuzzy_ptr->external_input_nodes_.clear();
  fuzzy_ptr->external_input_nodes_.emplace(data_node);
  fuzzy_ptr->is_range_limited_graph_ = true;
  fuzzy_ptr->node_info_map_.clear();
  InputNodeGeneralize input_node_generalize(fuzzy_ptr->external_input_nodes_, fuzzy_ptr->is_range_limited_graph_,
      fuzzy_ptr->node_info_map_, tbe_op_store_adapter);
      
  Status ret = input_node_generalize.MergeRangeWithUpperLimitMax(upper_limit_max_range, range, dim_index, dst_shape_range);
  EXPECT_EQ(ret, fe::FAILED);
}