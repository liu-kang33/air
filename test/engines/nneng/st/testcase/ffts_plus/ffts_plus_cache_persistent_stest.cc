/**
 * Copyright 2020-2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "ffts_plus_task_builder/ffts_plus_ops_kernel_builder.h"
#include "ffts_plus_task_builder/data/cache_persistent_task_builder.h"
#include "task_builder/task_builder.h"
#include "graph/node.h"
#include "graph_builder_utils.h"
#include "graph/utils/tensor_utils.h"
#include "graph/compute_graph.h"
#include "graph/op_kernel_bin.h"
#include "common/util/op_info_util.h"
#include "common/constants_define.h"
#include "common/aicore_util_attr_define.h"
#include "runtime/context.h"
#include "runtime/stream.h"
#include "runtime/rt_model.h"
#include "runtime/kernel.h"
#include "adapter/tbe_adapter/tbe_op_store_adapter.h"
#include "ops_store/ops_kernel_manager.h"
#include "common/configuration.h"
#include "graph_optimizer/ffts_plus/ffts_plus_graph_optimizer.h"
#include "graph_optimizer/ffts_plus/cache_manager.h"
#include "../../../graph_constructor/graph_constructor.h"
#include "graph_optimizer/fe_graph_optimizer.h"

using namespace std;
using namespace fe;
using namespace ge;
using TbeOpStoreAdapterPtr = std::shared_ptr<TbeOpStoreAdapter>;
using FEGraphOptimizerPtr = std::shared_ptr<FEGraphOptimizer>;
class STEST_CACHE_PERSISTETNT : public testing::Test {
 protected:

  void SetUp() {
    op_store_adapter_manager_ptr_ = std::make_shared<OpStoreAdapterManager>();
    TbeOpStoreAdapterPtr tbe_adapter_ptr = std::make_shared<TbeOpStoreAdapter>();
    op_store_adapter_manager_ptr_->map_all_op_store_adapter_.emplace(
        std::make_pair("tbe_op_adapter", tbe_adapter_ptr));
    std::map<std::string, std::string> options;
    fe_ops_kernel_info_store_ptr_ = make_shared<fe::FEOpsKernelInfoStore>(
        op_store_adapter_manager_ptr_);
    FEOpsStoreInfo heavy_op_info{
        6,
        "tbe-builtin",
        EN_IMPL_HW_TBE,
        "./air/test/engines/nneng/ut/testcase/fusion_engine/ops_kernel_store/fe_config/heavy_opinfo",
        "",
        false,
        false};

    vector<FEOpsStoreInfo> store_info;
    store_info.emplace_back(heavy_op_info);
    Configuration::Instance(fe::AI_CORE_NAME).ops_store_info_vector_ = (store_info);
    OpsKernelManager::Instance(AI_CORE_NAME).Finalize();

    fe_ops_kernel_info_store_ptr_->Initialize(options);

    reflection_builder_ptr_ = std::make_shared<ge::RefRelations>();

    graph_optimizer_ptr_ = std::make_shared<FEGraphOptimizer>(fe_ops_kernel_info_store_ptr_,
                                                              op_store_adapter_manager_ptr_,
                                                              fe::AI_CORE_NAME);
    FFTSPlusGraphOptimizer ffts_plus;
    ffts_plus.Initialize(options, nullptr);
  }

  void TearDown() {

  }
  shared_ptr<fe::FEOpsKernelInfoStore> fe_ops_kernel_info_store_ptr_;
  OpStoreAdapterManagerPtr op_store_adapter_manager_ptr_;
  RefRelationsPtr reflection_builder_ptr_;
  FEGraphOptimizerPtr graph_optimizer_ptr_;
};

void SecondGraphPartition(ComputeGraphPtr &sub_graph, ge::NodePtr &place_holder0, ge::NodePtr &place_holder1,
                          bool set_thread_scope=true) {
  /* Do the second time partition for sub graph optimization for FE.
   * Create three new placeholder and set there parent node as these three data. */
  ge::NodePtr data0;
  ge::NodePtr data1;
  ge::NodePtr data2;
  for (const auto &node : sub_graph->GetAllNodes()) {
    auto name = node->GetName();
    if (name == "test_sgt_graph_0/Data_0") {
      data0 = node;
      ge::GraphUtils::IsolateNode(node, {0});
      continue;
    }
    if (name == "test_sgt_graph_0/Data_1") {
      data1 = node;
      ge::GraphUtils::IsolateNode(node, {0});
      continue;
    }
    if (name == "test_sgt_graph_0/Data_2") {
      data2 = node;
      ge::GraphUtils::IsolateNode(node, {0});
      continue;
    }
  }

  ge::GeShape original_shape = GeShape({3, 12, 5, 6});
  GraphConstructor test3(sub_graph, "", ge::FORMAT_NCHW, ge::DT_FLOAT, original_shape);
  test3.SetInput("test_sgt_graph_0/am1:0", ge::FORMAT_NC1HWC0, "PlaceHolder_2:0", ge::FORMAT_NC1HWC0)
       .SetInput("test_sgt_graph_0/am2:0", "PlaceHolder_3:0")
       .SetInput("test_sgt_graph_0/am3:0", ge::FORMAT_FRACTAL_Z, "PlaceHolder_4:0", ge::FORMAT_FRACTAL_Z);

  ge::NodePtr place_holder2;
  ge::NodePtr place_holder3;
  ge::NodePtr place_holder4;

  test3.GetNodeByName("PlaceHolder_2", place_holder2);
  test3.GetNodeByName("PlaceHolder_3", place_holder3);
  test3.GetNodeByName("PlaceHolder_4", place_holder4);
  place_holder2->GetOpDesc()->SetExtAttr(ATTR_NAME_PARENT_NODE, data0);
  place_holder3->GetOpDesc()->SetExtAttr(ATTR_NAME_PARENT_NODE, data1);
  place_holder4->GetOpDesc()->SetExtAttr(ATTR_NAME_PARENT_NODE, data2);

  auto op_desc_plhd0 = place_holder0->GetOpDesc();
  auto op_desc_plhd1 = place_holder1->GetOpDesc();
  op_desc_plhd0->SetType("Const");
  op_desc_plhd0->SetName("const_pld0");
  if (set_thread_scope) {
    (void)ge::AttrUtils::SetListInt(op_desc_plhd0, kCachePersistScopeIds, {13});
  }

  op_desc_plhd1->SetType("Const");
  op_desc_plhd1->SetName("const_pld1");
}

Status CreateGraphOfFunctionOp1(ComputeGraphPtr graph, ge::NodePtr &const_node,
                                ge::NodePtr &place_holder0, ge::NodePtr &place_holder1,
                                bool set_thread_scope=true) {
  /* In this graph we will create a function op case
   * which have three inputs and three outputs.
   * Graph will be like:
   * parent node of placeholder:
   *                   (Const                    Constant)
   *                     |                          |
   *                PlaceHolder0               PlaceHolder1
   *                     |      \                   |
   *                     |       \                  |
   *                     |        \                 |
   *                     |         \                |
   *                  am1(5HD)     am2(NCHW)      am3(Fz)
   *                     |           |              |
   *                     |           |              |
   *                  x1(5HD)     x2(NCHW)      x3(Fz)
   *                     |           |              |
   *                     |           |              |
   *                 Conv2D(5HD)  am4(NCHW)  Conv2D(Fz)
   *                     \           |         /
   *                       \         |        /
   *                         \       |       /
   *                           \     |      /
   *                             \   |     /
   *                              NetOutput
   *
   * am1, am2, am3, x1, x2, x3 are six sgt slicing op, they will be merged into a function op.
   * After merging {am1, am2, am3, x1, x2, x3}, the graph will look like:
   *                   (Const                    Constant)
   *                     |                          |
   *                PlaceHolder0               PlaceHolder1
   *                     |      \                   |
   *                     |       \                  |
   *                     |        \                 |
   *                     |         \                |
   *                     |          \              /
   *                     \           |            /
   *                      \          |          /
   *                        \        |        /
   *                         FunctionOp(5HD,NCHW,Fz)
   *                            /   |     \
   *                          /     |      \
   *                        /       |       \
   *                 Conv2D(5HD)  am4(NCHW)  Conv2D(Fz)
   *                     \           |         /
   *                       \         |        /
   *                         \       |       /
   *                           \     |      /
   *                             \   |     /
   *                              NetOutput
   *
   * Inside the case operator, the subgraph look like:
   *                      Data    Data   Data
   *                         \     |     /
   *                          \    |    /
   *                         am1  am2 am3
   *                            \  |  /
   *                             X X X (three formatAgnosticOp)
   *                             / | \
   *                           NetOutput
   *
   */

  ge::GeShape original_shape = GeShape({3, 12, 5, 6});
  GraphConstructor test(graph, "", ge::FORMAT_NCHW, ge::DT_FLOAT, original_shape);
  string fa = "FormatAgnosticOp";
  test.AddOpDesc("conv2d", fe::CONV2D, 3, 1)
      .AddOpDesc("conv2d1", fe::CONV2D, 2, 1)
      .AddOpDesc("x1", fa)
      .AddOpDesc("x2", fa)
      .AddOpDesc("x3", fa)
      .AddOpDesc("am1", fa, 1, 1)
      .AddOpDesc("am2", fa, 1, 1)
      .AddOpDesc("am3", fa, 1, 1)
      .AddOpDesc("am4", fa, 1, 1)

      .SetInput("conv2d:0", "", ge::FORMAT_NC1HWC0)
      .SetInput("conv2d:1", "", ge::FORMAT_FRACTAL_Z, ge::FORMAT_HWCN)
      .SetInput("conv2d:2", "", {12})

      .SetInput("conv2d1:0", "", ge::FORMAT_NC1HWC0)
      .SetInput("conv2d1:1", "", ge::FORMAT_FRACTAL_Z);

  test.SetInput("am1:0", ge::FORMAT_NC1HWC0, "PlaceHolder_0:0", ge::FORMAT_NC1HWC0)
      .SetInput("am2:0", "PlaceHolder_0:0")
      .SetInput("am3:0", ge::FORMAT_FRACTAL_Z, "PlaceHolder_1:0", ge::FORMAT_FRACTAL_Z);

  test.SetInput("x1:0", ge::FORMAT_NC1HWC0, "am1:0", ge::FORMAT_NC1HWC0)
      .SetInput("x2", "am2")
      .SetInput("x3", ge::FORMAT_FRACTAL_Z, "am3", ge::FORMAT_FRACTAL_Z);

  test.SetInput("conv2d:0", ge::FORMAT_NC1HWC0, "x1:0", ge::FORMAT_NC1HWC0);
  test.SetInput("am4:0", "x2:0");
  test.SetInput("conv2d1:1", ge::FORMAT_FRACTAL_Z, "x3:0", ge::FORMAT_FRACTAL_Z);
  ge::NodePtr am1;
  ge::NodePtr am2;
  ge::NodePtr am3;
  test.GetNodeByName("am1", am1);
  test.GetNodeByName("am2", am2);
  test.GetNodeByName("am3", am3);

  ge::NodePtr x1;
  ge::NodePtr x2;
  ge::NodePtr x3;
  test.GetNodeByName("x1", x1);
  test.GetNodeByName("x2", x2);
  test.GetNodeByName("x3", x3);

  ge::AttrUtils::SetInt(x1->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 13);
  ge::AttrUtils::SetInt(x2->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 13);
  ge::AttrUtils::SetInt(x3->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 13);
  if (set_thread_scope) {
    ge::AttrUtils::SetInt(am1->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 13);
  }
  ge::AttrUtils::SetInt(am2->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 13);
  ge::AttrUtils::SetInt(am3->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 13);

  vector<NodePtr> nodes = {am1, am2, am3, x1, x2, x3};
  FFTSPlusGraphOptimizer ffts_plus;
  std::shared_ptr<GraphComm> graph_comm_ptr = nullptr;
  FE_MAKE_SHARED(graph_comm_ptr = std::make_shared<GraphComm>(kFFTSPlusCoreName),
  return fe::FAILED);
  FE_CHECK(graph_comm_ptr == nullptr, FE_LOGE("graphCommPtr is nullptr."), return fe::FAILED);
  ffts_plus.graph_comm_ptr_ = graph_comm_ptr;

  ge::AttrUtils::SetStr(graph, ge::ATTR_NAME_SESSION_GRAPH_ID, "0_1");

  // Create source graph(the graph above is the sub graph with placeholder and end)
  // In the source graph the input node of am1/am2 is const and am3 is constant
  ComputeGraphPtr src_graph = std::make_shared<ComputeGraph>("src_graph");
  GraphConstructor test2(src_graph, "", ge::FORMAT_NCHW, ge::DT_FLOAT, original_shape);
  test2.AddOpDesc("const", "Const")
       .AddOpDesc("constatnt", "Constant");
  ge::NodePtr constant_node;
  test2.GetNodeByName("const", const_node);
  test2.GetNodeByName("constant", constant_node);

  graph->SetExtAttr("part_src_graph", src_graph);
  graph->SetParentGraph(src_graph);

  test.GetNodeByName("PlaceHolder_0", place_holder0);
  test.GetNodeByName("PlaceHolder_1", place_holder1);
  place_holder0->GetOpDesc()->SetExtAttr(ATTR_NAME_PARENT_NODE, const_node);
  place_holder1->GetOpDesc()->SetExtAttr(ATTR_NAME_PARENT_NODE, constant_node);
  CacheManager::SetPersistWeightForGraph(*graph);

  ffts_plus.TransSingleSubGraph(*graph.get(), nodes);

  GraphConstructor::DumpGraph(graph);
  auto sub_graph = src_graph->GetSubgraph("test_sgt_graph_0");
  GraphConstructor::DumpGraph(sub_graph);

  SecondGraphPartition(sub_graph, place_holder0, place_holder1, set_thread_scope);

  GraphConstructor::DumpGraph(sub_graph);
  return fe::SUCCESS;
}

TEST_F(STEST_CACHE_PERSISTETNT, converage_01) {
  std::unordered_set<string> types = {"a", "ab", "cd"};
  bool matched = false;
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  ge::OpDescPtr op = std::make_shared<OpDesc>("test", "cd");
  ge::GeTensorDesc t;
  op->AddInputDesc(t);
  ge::NodePtr node = graph->AddNode(op);

  ge::OpDescPtr const_op = std::make_shared<OpDesc>("const", "Const");
  const_op->AddOutputDesc(t);
  ge::NodePtr const_node = graph->AddNode(const_op);

  ge::GraphUtils::AddEdge(const_node->GetOutDataAnchor(0), node->GetInDataAnchor(0));

  FeGraphUtils::IsNodeSpecificType(types, node, matched);
  EXPECT_EQ(matched, true);

  ge::NodePtr peer_out_node;
  bool ret = FeGraphUtils::IsPeerOutConst(peer_out_node.get(), 0, peer_out_node);
  EXPECT_EQ(ret, false);


  ret = FeGraphUtils::IsPeerOutConst(node.get(), 0, peer_out_node);
  EXPECT_EQ(ret, true);
}
