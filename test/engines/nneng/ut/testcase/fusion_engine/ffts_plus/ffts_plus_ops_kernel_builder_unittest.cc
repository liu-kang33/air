/**
 *
 * @file ffts_plus_ops_kernel_builder_unittest.cc
 *
 * @brief
 *
 * @version 1.0
 *
 */
#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "ffts_plus_task_builder/ffts_plus_ops_kernel_builder.h"
#include "ffts_plus_task_builder/mode/ffts_plus_manual_mode.h"
#include "ffts_plus_task_builder/mode/ffts_plus_auto_mode.h"
#include "task_builder/task_builder.h"
#include "graph/node.h"
#include "graph_builder_utils.h"
#include "graph/utils/tensor_utils.h"
#include "graph/compute_graph.h"
#include "common/util/op_info_util.h"
#include "common/constants_define.h"
#include "common/aicore_util_attr_define.h"
#include "runtime/context.h"
#include "runtime/stream.h"
#include "runtime/rt_model.h"
#include "runtime/kernel.h"
#include "runtime/mem.h"

using namespace std;
using namespace fe;
using namespace ge;
using FFTSPlusOpsKernelBuilderPtr = shared_ptr<FFTSPlusOpsKernelBuilder>;

#define SET_SIZE 10000

void DestroyContext(RunContext& context)
{
  assert(rtModelUnbindStream(context.model, context.stream) == RT_ERROR_NONE);
  assert(rtModelDestroy(context.model) == RT_ERROR_NONE);
  assert(rtStreamDestroy(context.stream) == RT_ERROR_NONE);
}

class FFTSPlusOpsKernelBuilderTest : public testing::Test{
 protected:
  static void SetUpTestCase() {
    cout << "FFTSPlusOpsKernelBuilder SetUpTestCase" << endl;
  }
  static void TearDownTestCase() {
    cout << "FFTSPlusOpsKernelBuilder TearDownTestCase" << endl;
  }

  // Some expensive resource shared by all tests.
  virtual void SetUp(){
    ffts_plus_ops_kernel_builder_ptr = make_shared<FFTSPlusOpsKernelBuilder>();
    ffts_plus_manual_mode_ptr = make_shared<FFTSPlusManualMode>();
    ffts_plus_auto_mode_ptr = make_shared<FFTSPlusAutoMode>();
    std::map<std::string, std::string> options;
    ffts_plus_ops_kernel_builder_ptr->Initialize(options);
    context_ = CreateContext();
  }
  virtual void TearDown(){
    cout << "a test Tear Down" << endl;
    ffts_plus_ops_kernel_builder_ptr->Finalize();
    DestroyContext(context_);

  }

  static RunContext CreateContext()
  {
    rtStream_t stream = nullptr;
    rtModel_t model = nullptr;

    assert(rtStreamCreate(&stream, 0) == RT_ERROR_NONE);
    assert(rtModelCreate(&model, 0) == RT_ERROR_NONE);
    assert(rtModelBindStream(model, stream, 0) == RT_ERROR_NONE);

    RunContext context;
    context.model = model;
    context.stream = stream;
    context.dataMemSize = 101;
    context.dataMemBase = (uint8_t *) (intptr_t) 1000;
    context.weightMemSize = 200;
    context.weightMemBase = (uint8_t *) (intptr_t) 1101;
    context.weightsBuffer = Buffer(20);

    return context;
  }
 public:
  FFTSPlusOpsKernelBuilderPtr ffts_plus_ops_kernel_builder_ptr;
  FFTSPlusManualModePtr ffts_plus_manual_mode_ptr;
  FFTSPlusAutoModePtr ffts_plus_auto_mode_ptr;
  RunContext context_;
};

void SetOpDecSize(NodePtr& node) {
  OpDesc::Vistor<GeTensorDesc> tensors = node->GetOpDesc()->GetAllInputsDesc();
  for (int i = 0; i < node->GetOpDesc()->GetAllInputsDesc().size(); i++) {
    ge::GeTensorDesc tensor = node->GetOpDesc()->GetAllInputsDesc().at(i);
    ge::TensorUtils::SetSize(tensor, SET_SIZE);
    node->GetOpDesc()->UpdateInputDesc(i, tensor);
  }
  OpDesc::Vistor<GeTensorDesc> tensorsOutput = node->GetOpDesc()->GetAllOutputsDesc();
  for (int i = 0; i < tensorsOutput.size(); i++) {
    ge::GeTensorDesc tensorOutput = tensorsOutput.at(i);
    ge::TensorUtils::SetSize(tensorOutput, SET_SIZE);
    node->GetOpDesc()->UpdateOutputDesc(i, tensorOutput);
  }
}

/*
 * Data -cast - netoutput
 */
ComputeGraphPtr BuildGraph_Readonly_Subgraph(const string &subraph_name, const bool &thread_mode){
  auto sub_builder = ut::ComputeGraphBuilder(subraph_name);
  auto data1 = sub_builder.AddNode("sdma1", "HcomAllReduce", 0,1);
  auto cast = sub_builder.AddNode("sdma2", "HcomAllReduce", 1,1);
  auto netoutput = sub_builder.AddNode("sdma3","HcomAllReduce", 1,1);
  AttrUtils::SetInt(data1->GetOpDesc(),ATTR_NAME_PARENT_NODE_INDEX, 1);
  AttrUtils::SetInt(netoutput->GetOpDesc(),ATTR_NAME_PARENT_NODE_INDEX,0);

  ffts::ThreadSliceMap thread_slice_map;
  thread_slice_map.thread_mode = thread_mode;
  ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
  data1->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  cast->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  netoutput->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);

  sub_builder.AddDataEdge(data1,0,cast,0);
  sub_builder.AddDataEdge(cast,0,netoutput,0);
  return sub_builder.GetGraph();
}

/*
 *      const - allreduce
 *            \ if
 *         insert identity
 */
ComputeGraphPtr BuildGraph_Readonly_ScopeWrite() {
  auto builder = ut::ComputeGraphBuilder("test");
  auto const1 = builder.AddNode("const1", fe::CONSTANT, 0, 1);
  auto ctrl_const = builder.AddNode("ctrl_const", fe::CONSTANT, 0, 1);
  auto allreduce = builder.AddNode("allreduce", "allreduce", 1, 1);
  auto if_node = builder.AddNode("if", "if", 1,0);

  builder.AddDataEdge(const1, 0, allreduce, 0);
  builder.AddDataEdge(const1, 0, if_node, 0);
  builder.AddControlEdge(ctrl_const, allreduce);

  auto root_graph = builder.GetGraph();
  string subgraph_name = "then_branch";
  ComputeGraphPtr then_branch_graph = BuildGraph_Readonly_Subgraph(subgraph_name, false);
  then_branch_graph->SetParentNode(if_node);
  then_branch_graph->SetParentGraph(root_graph);
  if_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  if_node->GetOpDesc()->SetSubgraphInstanceName(0,subgraph_name);
  root_graph->AddSubgraph(subgraph_name, then_branch_graph);
  return root_graph;
}

ComputeGraphPtr BuildGraph_Mix_Subgraph(const string &subraph_name) {
  auto builder = ut::ComputeGraphBuilder(subraph_name);
  auto lstm = builder.AddNode("LSTM", "LSTM", 2, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  ge::AttrUtils::SetInt(lstm->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  ge::AttrUtils::SetInt(lstm->GetOpDesc(), kModeInArgsFirstField, 1);
  ge::AttrUtils::SetInt(lstm->GetOpDesc(), kTaskRadio, 2);
  ge::AttrUtils::SetStr(lstm->GetOpDesc(), "_cube_vector_core_type", "MIX_AIC");
  ge::AttrUtils::SetStr(lstm->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_MIX_AIC");
  ge::AttrUtils::SetInt(lstm->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);

  ffts::ThreadSliceMapPtr threadSliceMap = std::make_shared<ffts::ThreadSliceMap>();
  lstm->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  SetOpDecSize(lstm);

  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

/*
 *
 *            \
 *         insert identity
 */
ComputeGraphPtr BuildGraph_Mix_ScopeWrite() {
  auto builder = ut::ComputeGraphBuilder("test");
  auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
  auto root_graph = builder.GetGraph();
  string subgraph_name = "then_branch";
  ComputeGraphPtr then_branch_graph = BuildGraph_Mix_Subgraph(subgraph_name);
  then_branch_graph->SetParentNode(sub_node);
  then_branch_graph->SetParentGraph(root_graph);
  sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
  root_graph->AddSubgraph(subgraph_name, then_branch_graph);
  return root_graph;
}

/*
 *     data1     data2
 *       |        |
 *      add - - - -
 *       |
 *     relu
 *       |
 *    netoutput
 */
ComputeGraphPtr BuilGraph_Sgt_Subgraph_1(const string &subgraph_name, const uint32_t &thread_mode,
                                        const uint32_t &window_size, uint32_t slice_num = 4) {
  auto builder = ut::ComputeGraphBuilder(subgraph_name);
  auto data1 = builder.AddNode("data1", fe::DATA, 0, 1);
  auto data2 = builder.AddNode("data2", fe::DATA, 0, 1);
  auto add = builder.AddNode("add", "Add", 2, 1, FORMAT_NCHW, DT_FLOAT, {4, 4, 4, 4});
  auto relu = builder.AddNode("relu", fe::RELU, 1, 1, FORMAT_NCHW, DT_FLOAT, {4, 4, 4, 4});
  auto netoutput = builder.AddNode("netoutput", fe::NETOUTPUT, 1, 1);

  for (auto anchor : add->GetAllInDataAnchors()) {
    ge::AnchorUtils::SetStatus(anchor, ge::ANCHOR_DATA);
  }
  for (auto anchor : relu->GetAllInDataAnchors()) {
    ge::AnchorUtils::SetStatus(anchor, ge::ANCHOR_DATA);
  }

  AttrUtils::SetInt(data1->GetOpDesc(),ATTR_NAME_PARENT_NODE_INDEX, 1);
  AttrUtils::SetInt(data2->GetOpDesc(),ATTR_NAME_PARENT_NODE_INDEX, 1);

  ffts::ThreadSliceMap thread_slice_map;
  thread_slice_map.thread_mode = thread_mode;
  thread_slice_map.parallel_window_size = window_size;
  thread_slice_map.slice_instance_num = slice_num;
  ffts::DimRange dim_rang;
  dim_rang.higher = 1;
  dim_rang.lower = 0;


  vector<vector<ffts::DimRange>> input_tensor_slice_vv;
  vector<ffts::DimRange> input_tensor_slice_v;
  input_tensor_slice_v.push_back(dim_rang);
  dim_rang.higher = 0;
  dim_rang.lower = 0;
  input_tensor_slice_v.push_back(dim_rang);
  input_tensor_slice_v.push_back(dim_rang);
  input_tensor_slice_v.push_back(dim_rang);
  input_tensor_slice_vv.push_back(input_tensor_slice_v);

  vector<vector<vector<ffts::DimRange>>> output_tensor_slice_vvv = {input_tensor_slice_vv, input_tensor_slice_vv,
                                                              input_tensor_slice_vv, input_tensor_slice_vv};
  input_tensor_slice_v.clear();
  dim_rang.higher = 1;
  dim_rang.lower = 1;
  input_tensor_slice_v.push_back(dim_rang);
  dim_rang.higher = 3;
  dim_rang.lower = 0;
  input_tensor_slice_v.push_back(dim_rang);
  input_tensor_slice_v.push_back(dim_rang);
  input_tensor_slice_v.push_back(dim_rang);
  input_tensor_slice_vv.push_back(input_tensor_slice_v);

  vector<vector<vector<ffts::DimRange>>> input_tensor_slice_vvv = {input_tensor_slice_vv, input_tensor_slice_vv,
                                                             input_tensor_slice_vv, input_tensor_slice_vv};
  thread_slice_map.input_tensor_slice = input_tensor_slice_vvv;
  thread_slice_map.output_tensor_slice = output_tensor_slice_vvv;
  ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
  thread_slice_map.input_tensor_slice = output_tensor_slice_vvv;
  ffts::ThreadSliceMapPtr thread_slice_map_ptr_relu = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
  add->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  relu->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr_relu);
  ge::AttrUtils::SetInt(add->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);
  ge::AttrUtils::SetInt(relu->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);

  int64_t ge_impl_type = static_cast<int>(domi::ImplyType::BUILDIN);
  ge::AttrUtils::SetInt(add->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, ge_impl_type);
  ge::AttrUtils::SetInt(relu->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, ge_impl_type);
  if (thread_mode == 1) {
    vector<string> thread_core_type = {"AIC", "AIC"};
    (void)ge::AttrUtils::SetListStr(add->GetOpDesc(), "_thread_cube_vector_core_type", thread_core_type);
    (void)ge::AttrUtils::SetListStr(relu->GetOpDesc(), "_thread_cube_vector_core_type", thread_core_type);
  } else {
    ge::AttrUtils::SetStr(add->GetOpDesc(), "_cube_vector_core_type", "AIC");
    ge::AttrUtils::SetStr(relu->GetOpDesc(), "_cube_vector_core_type", "AIC");
  }

  auto op_desc = netoutput->GetOpDesc();
  for (auto &tensor : op_desc->GetAllInputsDescPtr()) {
    ge::AttrUtils::SetInt(tensor, ATTR_NAME_PARENT_NODE_INDEX, 0);
  }

  builder.AddDataEdge(data1, 0, add, 0);
  builder.AddDataEdge(data2, 0, add, 1);
  builder.AddDataEdge(add, 0, relu, 0);
  builder.AddDataEdge(relu, 0, netoutput, 0);
  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

/*
 *         data1     data2
 *           |        |
 *          add <- - -
 *           |  \
 *         relu  \
 *       /        \
 * netoutput2   netoutput1
 */
ComputeGraphPtr BuilGraph_Sgt_Subgraph_2(const string &subgraph_name, const bool &thread_mode,
                                       const uint32_t &window_size) {
  auto builder = ut::ComputeGraphBuilder(subgraph_name);
  auto data1 = builder.AddNode("data1", fe::DATA, 0, 1);
  auto data2 = builder.AddNode("data2", fe::DATA, 0, 1);
  auto add = builder.AddNode("add", "Add", 2, 1);
  auto relu = builder.AddNode("relu", fe::RELU, 1, 1);
  auto netoutput1 = builder.AddNode("netoutput1", fe::NETOUTPUT, 1, 1);
  auto netoutput2 = builder.AddNode("netoutput2", fe::NETOUTPUT, 1, 1);

  AttrUtils::SetInt(data1->GetOpDesc(),ATTR_NAME_PARENT_NODE_INDEX, 1);
  AttrUtils::SetInt(data2->GetOpDesc(),ATTR_NAME_PARENT_NODE_INDEX, 1);

  ffts::ThreadSliceMap thread_slice_map;
  thread_slice_map.thread_mode = thread_mode;
  thread_slice_map.parallel_window_size = window_size;
  ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
  add->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  relu->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);

  auto op_desc1 = netoutput1->GetOpDesc();
  for (auto &tensor1 : op_desc1->GetAllInputsDescPtr()) {
    ge::AttrUtils::SetInt(tensor1, ATTR_NAME_PARENT_NODE_INDEX, 0);
  }
  auto op_desc2 = netoutput2->GetOpDesc();
  for (auto &tensor2 : op_desc2->GetAllInputsDescPtr()) {
    ge::AttrUtils::SetInt(tensor2, ATTR_NAME_PARENT_NODE_INDEX, 0);
  }

  builder.AddDataEdge(data1, 0, add, 0);
  builder.AddDataEdge(data2, 0, add, 1);
  builder.AddDataEdge(add, 0, relu, 0);
  builder.AddDataEdge(add, 0, netoutput1, 0);
  builder.AddDataEdge(relu, 0, netoutput2, 0);
  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

ComputeGraphPtr BuildGraph_SubGraph_Greater26(const string &subraph_name) {
  auto builder = ut::ComputeGraphBuilder(subraph_name);

  ffts::ThreadSliceMap thread_slice_map;
  thread_slice_map.thread_mode = 0;
  ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);

  auto input = builder.AddNode("input", "input", 0, 30, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  (void)ge::AttrUtils::SetInt(input->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  (void)ge::AttrUtils::SetStr(input->GetOpDesc(), "_cube_vector_core_type", "AIC");
  (void)ge::AttrUtils::SetStr(input->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AICUBE");
  (void)ge::AttrUtils::SetBool(input->GetOpDesc(), kTypeFFTSPlus, true);
  (void)ge::AttrUtils::SetInt(input->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 1);
  input->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  SetOpDecSize(input);

  auto output = builder.AddNode("output", "output", 30, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  (void)ge::AttrUtils::SetInt(output->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  (void)ge::AttrUtils::SetStr(output->GetOpDesc(), "_cube_vector_core_type", "AIC");
  (void)ge::AttrUtils::SetStr(output->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AICUBE");
  (void)ge::AttrUtils::SetBool(output->GetOpDesc(), kTypeFFTSPlus, true);
  (void)ge::AttrUtils::SetInt(output->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 1);
  output->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  SetOpDecSize(output);
  for (auto i = 0; i < 30; i++) {
    string node_name = "conv2d";
    node_name += to_string(i);
    auto conv2d = builder.AddNode(node_name, "conv2d", 1, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
    (void)ge::AttrUtils::SetInt(conv2d->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
    (void)ge::AttrUtils::SetStr(conv2d->GetOpDesc(), "_cube_vector_core_type", "AIC");
    (void)ge::AttrUtils::SetStr(conv2d->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AICUBE");
    (void)ge::AttrUtils::SetBool(conv2d->GetOpDesc(), kTypeFFTSPlus, true);
    (void)ge::AttrUtils::SetInt(conv2d->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 1);
    conv2d->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
    SetOpDecSize(conv2d);
    builder.AddDataEdge(input, i, conv2d, 0);
    builder.AddDataEdge(conv2d, 0, output, i);
  }

  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

ComputeGraphPtr BuildGraph_SubGraph_Greater60(const string &subraph_name) {
  auto builder = ut::ComputeGraphBuilder(subraph_name);

  ffts::ThreadSliceMap thread_slice_map;
  thread_slice_map.thread_mode = 0;
  ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);

  auto input = builder.AddNode("input", "input", 0, 60, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  (void)ge::AttrUtils::SetInt(input->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  (void)ge::AttrUtils::SetStr(input->GetOpDesc(), "_cube_vector_core_type", "AIV");
  (void)ge::AttrUtils::SetStr(input->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AIVEC");
  (void)ge::AttrUtils::SetBool(input->GetOpDesc(), kTypeFFTSPlus, true);
  (void)ge::AttrUtils::SetInt(input->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 1);
  input->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  SetOpDecSize(input);

  auto output = builder.AddNode("output", kTypePhonyConcat, 60, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  (void)ge::AttrUtils::SetInt(output->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  (void)ge::AttrUtils::SetStr(output->GetOpDesc(), "_cube_vector_core_type", "AIV");
  (void)ge::AttrUtils::SetStr(output->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AIVEC");
  (void)ge::AttrUtils::SetBool(output->GetOpDesc(), kTypeFFTSPlus, true);
  (void)ge::AttrUtils::SetInt(output->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 1);
  output->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  SetOpDecSize(output);

  auto output1 = builder.AddNode("output1", "output1", 1, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  (void)ge::AttrUtils::SetInt(output1->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  (void)ge::AttrUtils::SetStr(output1->GetOpDesc(), "_cube_vector_core_type", "AIV");
  (void)ge::AttrUtils::SetStr(output1->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AIVEC");
  (void)ge::AttrUtils::SetBool(output1->GetOpDesc(), kTypeFFTSPlus, true);
  (void)ge::AttrUtils::SetInt(output1->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 2);
  output1->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
  SetOpDecSize(output1);

  for (auto i = 0; i < 60; i++) {
    string node_name = "conv2d";
    node_name += to_string(i);
    auto conv2d = builder.AddNode(node_name, "conv2d", 1, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
    (void)ge::AttrUtils::SetInt(conv2d->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
    (void)ge::AttrUtils::SetStr(conv2d->GetOpDesc(), "_cube_vector_core_type", "AIV");
    (void)ge::AttrUtils::SetStr(conv2d->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AIVEC");
    (void)ge::AttrUtils::SetBool(conv2d->GetOpDesc(), kTypeFFTSPlus, true);
    (void)ge::AttrUtils::SetInt(conv2d->GetOpDesc(), ATTR_NAME_THREAD_SCOPE_ID, 1);
    conv2d->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
    SetOpDecSize(conv2d);
    builder.AddDataEdge(input, i, conv2d, 0);
    builder.AddDataEdge(conv2d, 0, output, i);
  }
  builder.AddDataEdge(output, 0, output1, 0);
  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

ComputeGraphPtr BuildGraph_Greater26() {
  auto builder = ut::ComputeGraphBuilder("test");
  auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
  auto root_graph = builder.GetGraph();
  string subgraph_name = "then_branch";
  ComputeGraphPtr then_branch_graph = BuildGraph_SubGraph_Greater26(subgraph_name);
  then_branch_graph->SetParentNode(sub_node);
  then_branch_graph->SetParentGraph(root_graph);
  sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
  root_graph->AddSubgraph(subgraph_name, then_branch_graph);
  return root_graph;
}

ComputeGraphPtr BuildGraph_Greater60() {
  auto builder = ut::ComputeGraphBuilder("test");
  auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
  auto root_graph = builder.GetGraph();
  string subgraph_name = "then_branch";
  ComputeGraphPtr then_branch_graph = BuildGraph_SubGraph_Greater60(subgraph_name);
  then_branch_graph->SetParentNode(sub_node);
  then_branch_graph->SetParentGraph(root_graph);
  sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
  root_graph->AddSubgraph(subgraph_name, then_branch_graph);
  return root_graph;
}

ComputeGraphPtr BuildGraph_AICPU_Subgraph(const string &subraph_name) {
  auto builder = ut::ComputeGraphBuilder(subraph_name);
  auto lstm = builder.AddNode("add", "Add", 2, 1);
  FftsPlusCtxDefPtr ctxDefPtr = std::make_shared<domi::FftsPlusCtxDef>();
  lstm->GetOpDesc()->SetExtAttr("_ffts_plus_aicpu_ctx_def", ctxDefPtr);
  ffts::ThreadSliceMapPtr threadSliceMap = std::make_shared<ffts::ThreadSliceMap>();
  lstm->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  ge::AttrUtils::SetInt(lstm->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);

  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

ComputeGraphPtr BuildGraph_HCCL_Subgraph(const string &subraph_name) {
  auto builder = ut::ComputeGraphBuilder(subraph_name);
  auto hccl_mode = builder.AddNode("HcomAllReduce", "HcomAllReduce", 1, 1);
  auto up_mode = builder.AddNode("upmode", "reduce", 1, 1, ge::FORMAT_NCHW, ge::DT_FLOAT, {2, 4, 4, 4});
  builder.AddDataEdge(up_mode, 0, hccl_mode, 0);
  std::vector<domi::FftsPlusCtxDef> hccl_sub_tasks;
  domi::FftsPlusCtxDef hccl_sub_task;
  hccl_sub_task.set_context_type(RT_CTX_TYPE_SDMA);
  hccl_sub_tasks.push_back(hccl_sub_task);
  hccl_sub_task.set_context_type(RT_CTX_TYPE_NOTIFY_WAIT);
  hccl_sub_tasks.push_back(hccl_sub_task);
  hccl_sub_task.set_context_type(RT_CTX_TYPE_WRITE_VALUE);
  hccl_sub_tasks.push_back(hccl_sub_task);

  hccl_mode->GetOpDesc()->SetExtAttr(kHcclSubTasks, hccl_sub_tasks);
  ffts::ThreadSliceMapPtr threadSliceMap = std::make_shared<ffts::ThreadSliceMap>();
  hccl_mode->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  up_mode->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  ge::AttrUtils::SetInt(hccl_mode->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 2);
  ge::AttrUtils::SetInt(up_mode->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 2);
  ge::AttrUtils::SetInt(up_mode->GetOpDesc(), ge::ATTR_NAME_IMPLY_TYPE, static_cast<int>(domi::ImplyType::TVM));
  AttrUtils::SetStr(up_mode->GetOpDesc(), "_cube_vector_core_type", "AIV");
  (void)ge::AttrUtils::SetStr(up_mode->GetOpDesc(), ge::TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF_AIVEC");
  SetOpDecSize(up_mode);
  std::vector<std::vector<int64_t>> adjacency_list = {{1, 2}, {3}, {}};
  (void)ge::AttrUtils::SetListListInt(hccl_mode->GetOpDesc(), kAdjacencyList, adjacency_list);

  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

ComputeGraphPtr BuildGraph_AICPU_ScopeWrite() {
  auto builder = ut::ComputeGraphBuilder("test");
  auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
  auto root_graph = builder.GetGraph();
  string subgraph_name = "then_branch";
  ComputeGraphPtr then_branch_graph = BuildGraph_AICPU_Subgraph(subgraph_name);
  then_branch_graph->SetParentNode(sub_node);
  then_branch_graph->SetParentGraph(root_graph);
  sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
  root_graph->AddSubgraph(subgraph_name, then_branch_graph);
  return root_graph;
}

ComputeGraphPtr BuildGraph_HCCL_Graph() {
  auto builder = ut::ComputeGraphBuilder("test");
  auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
  auto root_graph = builder.GetGraph();
  string subgraph_name = "then_branch";
  ComputeGraphPtr then_branch_graph = BuildGraph_HCCL_Subgraph(subgraph_name);
  then_branch_graph->SetParentNode(sub_node);
  then_branch_graph->SetParentGraph(root_graph);
  sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
  root_graph->AddSubgraph(subgraph_name, then_branch_graph);
  return root_graph;
}


TEST_F(FFTSPlusOpsKernelBuilderTest, GenerateTask_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_Readonly_ScopeWrite();
  auto ifnode = graph->FindNode("if");

  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*ifnode, context_, tasks);

  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, GenerateTask_Greater26_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_Greater26();
  auto sub_node = graph->FindNode("sub_node");
  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context_, tasks);
  domi::FftsPlusTaskDef *ffts_plus_task_def = tasks[0].mutable_ffts_plus_task();
  EXPECT_EQ(fe::SUCCESS, ret);
  ASSERT_EQ(ffts_plus_task_def->ffts_plus_ctx_size(), 33);

  domi::FftsPlusCtxDef* label_ctx_def =
          ffts_plus_task_def->mutable_ffts_plus_ctx(static_cast<int>(32));
  domi::FftsPlusLabelCtxDef* label_ctx = label_ctx_def->mutable_label_ctx();
  EXPECT_EQ(label_ctx_def->context_type(), RT_CTX_TYPE_LABEL);
  EXPECT_EQ(label_ctx->pred_cnt(), 1);
  EXPECT_EQ(label_ctx->successor_num(), 5);
  EXPECT_EQ(label_ctx->successor_list(0), 5);

  domi::FftsPlusCtxDef* second_ctx_first =
          ffts_plus_task_def->mutable_ffts_plus_ctx(static_cast<int>(RT_CTX_SUCCESSOR_NUM - 1));
  EXPECT_NE(second_ctx_first->context_type(), RT_CTX_TYPE_LABEL);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, GenerateTask_Greater60_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_Greater60();
  auto sub_node = graph->FindNode("sub_node");
  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context_, tasks);
  domi::FftsPlusTaskDef *ffts_plus_task_def = tasks[0].mutable_ffts_plus_task();
  EXPECT_EQ(fe::SUCCESS, ret);
  ASSERT_EQ(ffts_plus_task_def->ffts_plus_ctx_size(), 64);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, Mix_GenerateTask_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_Mix_ScopeWrite();
  cout << "========================MIX AIC/AIV GENTASK BEGIN========================" << endl;
  auto sub_node = graph->FindNode("sub_node");
  if (sub_node == nullptr) {
    cout << "[ERROR] FE:sub node is nullptr";
  }
  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context_, tasks);

  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, Case_Gen_AutoThread_Context_Id)
{
  std::vector<ge::NodePtr> sub_graph_nodes;
  uint32_t thread_mode = true;
  uint32_t window_size = 4;
  ComputeGraphPtr sgt_graph = BuilGraph_Sgt_Subgraph_1("sgt_graph", thread_mode, window_size);
  uint64_t ready_context_num = 0;
  uint64_t total_context_number = 0;
  Status ret = ffts_plus_auto_mode_ptr->GenFftsPlusContextId(*sgt_graph, sub_graph_nodes, ready_context_num,
                                                                      total_context_number);
  // check result
  int total_count = 0;
  int flag_attr_count = 0;
  for (const auto &node : sub_graph_nodes) {
    total_count++;
    if (node->GetName() == "add") {
      uint32_t in_label_ctx_id = 1;
      vector<uint32_t> at_start_ctx_id_list;
      vector<uint32_t> context_id_list;
      if (ge::AttrUtils::GetInt(node->GetOpDesc(), kAutoInlabelCtxId, in_label_ctx_id) &&
          ge::AttrUtils::GetListInt(node->GetOpDesc(), kAutoAtStartCtxIdList, at_start_ctx_id_list) &&
          ge::AttrUtils::GetListInt(node->GetOpDesc(), kAutoCtxIdList, context_id_list)) {
        flag_attr_count++;
      }
    } else if (node->GetName() == "relu") {
      uint32_t at_end_pre_cnt = 0;
      uint32_t out_label_ctx_id = 0;
      vector<uint32_t> at_end_ctx_id_list;
      vector<uint32_t> context_id_list;
      if (ge::AttrUtils::GetInt(node->GetOpDesc(), kAutoOutlabelCtxId, out_label_ctx_id) &&
          ge::AttrUtils::GetListInt(node->GetOpDesc(), kAutoAtEndCtxIdList, at_end_ctx_id_list) &&
          ge::AttrUtils::GetInt(node->GetOpDesc(), kAutoAtEndPreCnt, at_end_pre_cnt) &&
          ge::AttrUtils::GetListInt(node->GetOpDesc(), kAutoCtxIdList, context_id_list)) {
        flag_attr_count++;
      }
    }
  }
  EXPECT_EQ(2, total_count);
  EXPECT_EQ(2, flag_attr_count);
  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, Case_Gen_AutoThread_AtEnd_PreCnt)
{
  std::vector<ge::NodePtr> sub_graph_nodes;
  bool thread_mode = 1;
  uint32_t window_size = 4;
  ComputeGraphPtr sgt_graph = BuilGraph_Sgt_Subgraph_2("sgt_graph", thread_mode, window_size);
  uint64_t ready_context_num = 0;
  uint64_t total_context_number = 0;
  Status ret = ffts_plus_auto_mode_ptr->GenFftsPlusContextId(*sgt_graph, sub_graph_nodes, ready_context_num,
                                                                      total_context_number);
  // check result
  bool status_pre_cnt = false;
  for (const auto &node : sub_graph_nodes) {
    if (node->GetName() == "relu") {
      uint32_t at_end_pre_cnt = 0;
      (void)ge::AttrUtils::GetInt(node->GetOpDesc(), kAutoAtEndPreCnt, at_end_pre_cnt);
      if (at_end_pre_cnt == 2) {
        status_pre_cnt = true;
      }
    }
  }
  EXPECT_EQ(true, status_pre_cnt);
  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, Case_Gen_Sub_Graph_Task_Def)
{
  std::vector<ge::NodePtr> sub_graph_nodes;
  uint32_t thread_mode = 1;
  uint32_t window_size = 4;
  ComputeGraphPtr sgt_graph = BuilGraph_Sgt_Subgraph_1("sgt_graph", thread_mode, window_size);
  uint64_t ready_context_num = 0;
  uint64_t total_context_number = 0;
  ffts_plus_auto_mode_ptr->Initialize();
  Status ret = ffts_plus_auto_mode_ptr->GenFftsPlusContextId(*sgt_graph, sub_graph_nodes, ready_context_num,
                                                                      total_context_number);
  RunContext context = CreateContext();
  domi::TaskDef task_def;
  ret = ffts_plus_auto_mode_ptr->GenSubGraphTaskDef(sub_graph_nodes, context, task_def);
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  uint64_t gen_ctx_num = ffts_plus_task_def->ffts_plus_ctx_size();

  EXPECT_EQ(18, gen_ctx_num);
  EXPECT_EQ(fe::SUCCESS, ret);
}
/*
 *         data1
 *           |
 *       labelswitch
 *           |
 *         labelset
 *           |
 *        identity
 *           |
 *        output
 */
ComputeGraphPtr BuildGraph_RuntimeOp_Subgraph(const string &subraph_name) {
  auto builder = ut::ComputeGraphBuilder(subraph_name);
  auto data = builder.AddNode("data", fe::DATA, 0, 1);
  auto labelswitch = builder.AddNode("labelswitch", "LabelSwitchByIndex", 1, 1);
  auto labelset = builder.AddNode("labelset", "LabelSet", 1, 1);
  auto identity = builder.AddNode("identity", "Identity", 1, 1);
  auto output = builder.AddNode("output", fe::NETOUTPUT, 1, 1);
  FftsPlusCtxDefPtr ctxDefPtr = std::make_shared<domi::FftsPlusCtxDef>();
  labelswitch->GetOpDesc()->SetExtAttr("FFTS_PLUS_TASK_DEF", ctxDefPtr);
  labelset->GetOpDesc()->SetExtAttr("FFTS_PLUS_TASK_DEF", ctxDefPtr);
  labelswitch->GetOpDesc()->SetExtAttr("FFTS_PLUS_TASK_DEF", ctxDefPtr);
  identity->GetOpDesc()->SetExtAttr("FFTS_PLUS_TASK_DEF", ctxDefPtr);
  ffts::ThreadSliceMapPtr threadSliceMap = std::make_shared<ffts::ThreadSliceMap>();
  labelswitch->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  ge::AttrUtils::SetInt(labelswitch->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);
  labelset->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  ge::AttrUtils::SetInt(labelset->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);
  identity->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, threadSliceMap);
  ge::AttrUtils::SetInt(identity->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);

 
  builder.AddDataEdge(data, 0, labelswitch, 0);
  builder.AddDataEdge(labelswitch, 0, labelswitch, 0);
  builder.AddDataEdge(labelswitch, 0, labelset, 0);
  builder.AddDataEdge(labelset, 0, identity, 0);
  builder.AddDataEdge(identity, 0, output, 0);

  auto sub_graph = builder.GetGraph();
  return sub_graph;
}

ComputeGraphPtr BuildGraph_RuntimeOp_ScopeWrite() {
  auto builder = ut::ComputeGraphBuilder("rts_root_graph");
  auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
  auto root_graph = builder.GetGraph();
  string subgraph_name = "func_op_branch";
  ComputeGraphPtr func_op_branch_graph = BuildGraph_RuntimeOp_Subgraph(subgraph_name);
  func_op_branch_graph->SetParentNode(sub_node);
  func_op_branch_graph->SetParentGraph(root_graph);
  sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
  sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
  root_graph->AddSubgraph(subgraph_name, func_op_branch_graph);
  return root_graph;
}

TEST_F(FFTSPlusOpsKernelBuilderTest, RTSOP_GenerateTask_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_RuntimeOp_ScopeWrite();
  cout << "========================RTSOP GENTASK BEGIN========================" << endl;
  auto sub_node = graph->FindNode("sub_node");

  if (sub_node == nullptr) {
    cout << "[ERROR] FE:sub node is nullptr";
  }
  RunContext context = CreateContext();
  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context, tasks);

  EXPECT_EQ(fe::SUCCESS, ret);
}
TEST_F(FFTSPlusOpsKernelBuilderTest, AICPU_GenerateTask_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_AICPU_ScopeWrite();
  cout << "========================aicpu GENTASK BEGIN========================" << endl;
  auto sub_node = graph->FindNode("sub_node");

  if (sub_node == nullptr) {
    cout << "[ERROR] FE:sub node is nullptr";
  }
  RunContext context = CreateContext();
  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context, tasks);

  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, HCCL_GenerateTask_SUCCESS)
{
  ComputeGraphPtr graph = BuildGraph_HCCL_Graph();
  cout << "========================hccl GENTASK BEGIN========================" << endl;
  auto sub_node = graph->FindNode("sub_node");
  if (sub_node == nullptr) {
    cout << "[ERROR] FE:sub node is nullptr";
  }
  RunContext context = CreateContext();
  vector<domi::TaskDef> tasks;
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context, tasks);

  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSPlusOpsKernelBuilderTest, AutoFillContextSuccList)
{
  FFTSPlusAutoModePtr ffts_plus_auto_mode_ptr = std::make_shared<FFTSPlusAutoMode>();
  FftsPlusComCtx sub_ffts_plus_context = {0};
  auto builder = ut::ComputeGraphBuilder("test");
  auto input = builder.AddNode("test", "test", 0, 1);
  auto out_node = builder.AddNode("test1", "NetOutput", 1, 0);
  builder.AddDataEdge(input, 0, out_node, 0);
  vector<uint32_t> context_id_list = {3, 4};
  ge::AttrUtils::SetListInt(out_node->GetOpDesc(), kAutoCtxIdList, context_id_list);
  domi::FftsPlusTaskDef taskdef;
  domi::FftsPlusCtxDef *ffts_plus_ctx_def = taskdef.add_ffts_plus_ctx();
  ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_AICPU);
  ffts_plus_ctx_def->set_context_id(0);
  ffts_plus_ctx_def = taskdef.add_ffts_plus_ctx();
  ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_SDMA);
  ffts_plus_ctx_def->set_context_id(1);
  ffts_plus_ctx_def = taskdef.add_ffts_plus_ctx();
  ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_NOTIFY_WAIT);
  ffts_plus_ctx_def->set_context_id(2);
  ffts_plus_ctx_def = taskdef.add_ffts_plus_ctx();
  ffts_plus_ctx_def->set_context_type(RT_CTX_TYPE_WRITE_VALUE);
  ffts_plus_ctx_def->set_context_id(3);
  FFTSPlusTaskBuilderPtr task_builder = nullptr;
  vector<uint32_t> context_list;
  context_list.push_back(0);
  context_list.push_back(1);
  context_list.push_back(2);
  context_list.push_back(3);
  Status ret = ffts_plus_auto_mode_ptr->FillContextSuccList(input, &taskdef, task_builder, context_list,
                                                            context_list);
  EXPECT_EQ(fe::SUCCESS, ret);
}
TEST_F(FFTSPlusOpsKernelBuilderTest, MIX_L2_GenerateTask_SUCCESS)
{
  RunContext context = CreateContext();
  vector<domi::TaskDef> tasks;
  auto builder = ut::ComputeGraphBuilder("test");
  auto sub_node = builder.AddNode("mix_node", "mix_node", 1, 0);
  (void)ge::AttrUtils::SetStr(sub_node->GetOpDesc(), ATTR_NAME_FFTS_PLUS_MIX_L2, "ffts_plus");
  FftsPlusCtxDefPtr ffts_plus_ctx_def = nullptr;
  ffts_plus_ctx_def = std::make_shared<domi::FftsPlusCtxDef>();
  domi::FftsPlusMixAicAivCtxDef *mix_l2_ctx = ffts_plus_ctx_def->mutable_mix_aic_aiv_ctx();
  mix_l2_ctx->set_ns(1);
  mix_l2_ctx->set_atm(1);
  mix_l2_ctx->set_thread_dim(1);
  mix_l2_ctx->set_tail_block_ratio_n(2);
  mix_l2_ctx->set_non_tail_block_ratio_n(2);
  mix_l2_ctx->add_task_addr(0x1114);
  mix_l2_ctx->add_task_addr(0x1111);
  mix_l2_ctx->add_task_addr(0x1112);
  mix_l2_ctx->add_kernel_name("mix1");
  mix_l2_ctx->add_kernel_name("mix2");
  (void)sub_node->GetOpDesc()->SetExtAttr(kMixL2CtxDef, ffts_plus_ctx_def);
  (void) ge::AttrUtils::SetInt(sub_node->GetOpDesc(), kModeInArgsFirstField, 1);
  Status ret = ffts_plus_ops_kernel_builder_ptr->GenerateTask(*sub_node, context, tasks);
  EXPECT_EQ(fe::SUCCESS, ret);
}
