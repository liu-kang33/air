/**
 *
 * @file ffts_plus_ops_kernel_builder_unittest.cc
 *
 * @brief
 *
 * @version 1.0
 *
 */
#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "ffts_plus_task_builder/ffts_plus_ops_kernel_builder.h"
#include "task_builder/task_builder.h"
#include "graph/node.h"
#include "graph_builder_utils.h"
#include "graph/utils/tensor_utils.h"
#include "graph/compute_graph.h"
#include "graph/op_kernel_bin.h"
#include "common/util/op_info_util.h"
#include "common/constants_define.h"
#include "common/aicore_util_attr_define.h"
#include "runtime/context.h"
#include "runtime/stream.h"
#include "runtime/rt_model.h"
#include "runtime/kernel.h"
#include "runtime/mem.h"
#include "../fe_test_utils.h"

using namespace std;
using namespace fe;
using namespace ge;

#define SET_SIZE 1000

using MixMixAICAIVAutoTaskBuilderPtr = shared_ptr<MixAICAIVAutoTaskBuilder>;
class FFTSPlusMixAICAIVAutoTaskBuilderUTest : public testing::Test
{
protected:
	void SetUp()
	{
		mix_aic_aiv_auto_task_builder_ptr_ = make_shared<MixAICAIVAutoTaskBuilder>();
		ffts_plus_def_ptr_ = new domi::FftsPlusTaskDef;
		node_ = CreateNode();
	}
	void TearDown() {
		delete ffts_plus_def_ptr_;
	}
	static void SetOpDecSize(NodePtr& node) {
		OpDesc::Vistor<GeTensorDesc> tensors = node->GetOpDesc()->GetAllInputsDesc();
		for (int i = 0; i < node->GetOpDesc()->GetAllInputsDesc().size(); i++) {
			ge::GeTensorDesc tensor = node->GetOpDesc()->GetAllInputsDesc().at(i);
			ge::TensorUtils::SetSize(tensor, SET_SIZE);
			node->GetOpDesc()->UpdateInputDesc(i, tensor);
		}
		OpDesc::Vistor<GeTensorDesc> tensorsOutput = node->GetOpDesc()->GetAllOutputsDesc();
		for (int i = 0; i < tensorsOutput.size(); i++) {
			ge::GeTensorDesc tensorOutput = tensorsOutput.at(i);
			ge::TensorUtils::SetSize(tensorOutput, SET_SIZE);
			node->GetOpDesc()->UpdateOutputDesc(i, tensorOutput);
		}
	}
	static NodePtr CreateNode()
	{
		FeTestOpDescBuilder builder;
		builder.SetName("test_tvm");
		builder.SetType("conv");
		builder.SetInputs({ 1 });
		builder.SetOutputs({ 1 });
		builder.AddInputDesc({ 2, 4, 4, 4 }, ge::FORMAT_NCHW, ge::DT_FLOAT);
		builder.AddOutputDesc({ 2, 4, 4, 4 }, ge::FORMAT_NCHW, ge::DT_FLOAT);
		auto node = builder.Finish();

		const char tbeBin[] = "tbe_bin";
		vector<char> buffer(tbeBin, tbeBin + strlen(tbeBin));
		OpKernelBinPtr tbeKernelPtr = std::make_shared<OpKernelBin>("test_tvm", std::move(buffer));
		node->GetOpDesc()->SetExtAttr(OP_EXTATTR_NAME_TBE_KERNEL, tbeKernelPtr);
		ge::AttrUtils::SetInt(node->GetOpDesc(), ge::ATTR_NAME_THREAD_SCOPE_ID, 0);
		ge::AttrUtils::SetInt(node->GetOpDesc(), "_fe_imply_type", (int64_t)EN_IMPL_CUSTOM_TBE);
		ge::AttrUtils::SetStr(node->GetOpDesc(), "tvm_magic", "RT_DEV_BINARY_MAGIC_ELF");
		ge::AttrUtils::SetBool(node->GetOpDesc(), "is_first_node", true);
		ge::AttrUtils::SetBool(node->GetOpDesc(), "is_last_node", true);
		ge::AttrUtils::SetStr(node->GetOpDesc(), node->GetOpDesc()->GetName() + "_kernelname", "kernelname");

    vector<string> thread_core_type = {"MIX_AIC", "MIX_AIC"};
    (void)ge::AttrUtils::SetListStr(node->GetOpDesc(), "_thread_cube_vector_core_type", thread_core_type);
		vector<uint32_t> context_id_list = {3, 4};
		ge::AttrUtils::SetListInt(node->GetOpDesc(), kAutoCtxIdList, context_id_list);

		SetOpDecSize(node);
		ffts::ThreadSliceMapPtr tsmp_ptr = make_shared<ffts::ThreadSliceMap>();
		tsmp_ptr->slice_instance_num = 1;
    tsmp_ptr->parallel_window_size = 1;
    tsmp_ptr->thread_mode = 1;

    ffts::DimRange dim_rang;
		dim_rang.higher = 3;
		dim_rang.lower = 0;
		vector<ffts::DimRange> input_tensor_slice_v;
		input_tensor_slice_v.push_back(dim_rang);
		dim_rang.higher = 3;
		dim_rang.lower = 0;
		input_tensor_slice_v.push_back(dim_rang);
		input_tensor_slice_v.push_back(dim_rang);
		input_tensor_slice_v.push_back(dim_rang);
		vector<vector<ffts::DimRange>> input_tensor_slice_vv;
		input_tensor_slice_vv.push_back(input_tensor_slice_v);
		vector<vector<vector<ffts::DimRange>>> input_tensor_slice_vvv = { input_tensor_slice_vv };
    vector<vector<vector<ffts::DimRange>>> output_tensor_slice = { input_tensor_slice_vv };
		tsmp_ptr->input_tensor_slice = input_tensor_slice_vvv;
		tsmp_ptr->output_tensor_slice = output_tensor_slice;
		node->GetOpDesc()->SetExtAttr("_sgt_struct_info", tsmp_ptr);
		return node;
	}

public:
	MixMixAICAIVAutoTaskBuilderPtr mix_aic_aiv_auto_task_builder_ptr_;
	domi::FftsPlusTaskDef *ffts_plus_def_ptr_;
	NodePtr node_{ nullptr };
};

TEST_F(FFTSPlusMixAICAIVAutoTaskBuilderUTest, Gen_Mix_AICAIV_AUTO_ContextDef_SUCCESS)
{

	Status ret = mix_aic_aiv_auto_task_builder_ptr_->GenContextDef(node_, ffts_plus_def_ptr_);
	EXPECT_EQ(fe::SUCCESS, ret);
}