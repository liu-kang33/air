/**
 *
 * @file ffts_pass_unittest.cc
 *
 * @brief
 *
 * @version 1.0
 *
 */
#include <gtest/gtest.h>
#include <iostream>

#define private public
#define protected public
#include "graph_optimizer/ffts/ffts_pass.h"
#include "../../fe_test_utils.h"

#undef private
#undef protected

using namespace std;
using namespace fe;
using namespace ge;
using FftsPassPtr = shared_ptr<FftsPass>;

class FftsPassUTest : public testing::Test {
 protected:
  static void SetUpTestCase() {
    cout << "FFTSTaskBuilderUTest SetUpTestCase" << endl;
  }
  static void TearDownTestCase() {
    cout << "FFTSTaskBuilderUTest TearDownTestCase" << endl;
  }

  virtual void SetUp() {
    ffts_pass_ptr = make_shared<FftsPass>();
  }

  virtual void TearDown() {
    cout << "a test Tear Down" << endl;
  }

  void GenerateTensorSlice(vector<vector<vector<ffts::DimRange>>> &tensor_slice, const vector<vector<int64_t>> &shape) {
    for (size_t i = 0; i < 2; i++) {
      for (size_t j = 0; j < shape.size(); j++) {
        vector<int64_t> temp = shape[j];
        vector<ffts::DimRange> vdr;
        for (size_t z = 0; z < temp.size() - 1;) {
            ffts::DimRange dr;
            dr.lower = temp[z];
            dr.higher = temp[z + 1];
            vdr.push_back(dr);
            z = z + 2;
        }
        vector<vector<ffts::DimRange>> threadSlice;
        threadSlice.push_back(vdr);
        tensor_slice.push_back(threadSlice);
      }
    }
    return ;
  }

  NodePtr CreateNode() {
    FeTestOpDescBuilder builder;
    builder.SetName("test_tvm");
    builder.SetType("test_tvm");
    builder.AddInputDesc({288,32,16,16}, ge::FORMAT_NCHW, ge::DT_FLOAT);
    builder.AddOutputDesc({288,32,16,16}, ge::FORMAT_NCHW, ge::DT_FLOAT);
    NodePtr node =  builder.Finish();
    ge::AttrUtils::SetListInt(node->GetOpDesc(), ge::TVM_ATTR_NAME_THREAD_BLOCKDIM, {1, 1});

    vector<vector<vector<ffts::DimRange>>> tensor_slice_;
    vector<int64_t> shape_input = {0, 288, 0, 32, 0, 16, 0, 16};
    vector<vector<int64_t>> shape_;
    shape_.push_back(shape_input);
    GenerateTensorSlice(tensor_slice_, shape_);
    ffts::ThreadSliceMap thread_slice_map;
    thread_slice_map.thread_mode = 0;
    thread_slice_map.input_tensor_slice = tensor_slice_;
    thread_slice_map.output_tensor_slice = tensor_slice_;
    ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
    std::string str_slice_info;
    (void)ge::AttrUtils::SetStr(node->GetOpDesc(), ffts::kAttrSgtJsonInfo, str_slice_info);
    return node;
  }

  void CreateGraph(ComputeGraphPtr& graph) {
    NodePtr tile = CreateNode();
    NodePtr tile_node = graph->AddNode(tile);
  }

 public:
  FftsPassPtr ffts_pass_ptr;
};

TEST_F(FftsPassUTest, Get_slice_info_failed) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  ffts_pass_ptr->GetSliceInfo(*graph);
  int32_t thread_scopt_id = -1;
  for (auto &node : graph->GetDirectNode()) {
    (void)ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadScopeId, thread_scopt_id);
  }
  EXPECT_EQ(-1, thread_scopt_id); 
}

TEST_F(FftsPassUTest, Set_Attr_Of_Nodes_In_Sub_Graph_SUCCESS) {
  NodePtr node = CreateNode();
  vector<NodePtr> node_vec;
  node_vec.push_back(node);
  Status ret = ffts_pass_ptr->SetAttrOfNodesInSubGraph(node_vec);
  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FftsPassUTest, Trans_Sub_Graph_To_Function_Op_FAILED) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  for (auto &node : graph->GetDirectNode()) {
    (void)ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadScopeId, 1);
    ge::AttrUtils::SetInt(node->GetOpDesc(), FE_IMPLY_TYPE, 6);
  }
  Status ret = ffts_pass_ptr->TransSubGraphToFunctionOp(*graph);
  EXPECT_EQ(fe::FAILED, ret);
}

TEST_F(FftsPassUTest, Remove_Isolate_Node_FAILED) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  std::vector<ge::NodePtr> node_vec = {nullptr};
  Status ret = ffts_pass_ptr->RemoveIsolateNode(graph, node_vec);
  EXPECT_EQ(fe::FAILED, ret);
}