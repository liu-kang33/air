/**
 *
 * @file ffts_plus_graph_optimizer_unittest.cc
 *
 * @brief
 *
 * @version 1.0
 *
 */
#include <gtest/gtest.h>
#include <iostream>

#define private public
#define protected public
#include "graph_optimizer/ffts_plus/ffts_plus_graph_optimizer.h"
#include "platform_info.h"
#include "../../fe_test_utils.h"
#undef private
#undef protected

using namespace testing;
using namespace std;
using namespace fe;
using namespace ge;
using FFTSPlusGraphOptimizerPtr = shared_ptr<FFTSPlusGraphOptimizer>;

class FfftsPlusGraphOptimizerUTest : public testing::Test {
 protected:
  static void SetUpTestCase() {
    cout << "FfftsPlusGraphOptimizerUTest SetUpTestCase" << endl;
  }
  static void TearDownTestCase() {
    cout << "FfftsPlusGraphOptimizerUTest TearDownTestCase" << endl;
  }

  virtual void SetUp() {
    ffts_plus_graph_optimizer_ptr = make_shared<FFTSPlusGraphOptimizer>();
  }

  virtual void TearDown() {
    cout << "a test Tear Down" << endl;
  }

  NodePtr CreateNode() {
    FeTestOpDescBuilder builder;
    builder.SetName("test_tvm");
    builder.SetType("test_tvm");
    builder.AddInputDesc({288,32,16,16}, ge::FORMAT_NCHW, ge::DT_FLOAT);
    builder.AddOutputDesc({288,32,16,16}, ge::FORMAT_NCHW, ge::DT_FLOAT);
    NodePtr node =  builder.Finish();
    ge::AttrUtils::SetListInt(node->GetOpDesc(), ge::TVM_ATTR_NAME_THREAD_BLOCKDIM, {1, 1});
    ffts::ThreadSliceMap thread_slice_map;
    thread_slice_map.thread_mode = 0;
    ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
    std::string str_slice_info;
    (void)ge::AttrUtils::SetStr(node->GetOpDesc(), ffts::kAttrSgtJsonInfo, str_slice_info);
    return node;
  }

  void CreateGraph(ComputeGraphPtr& graph) {
    NodePtr tile = CreateNode();
    NodePtr tile_node = graph->AddNode(tile);
  }

 public:
  FFTSPlusGraphOptimizerPtr ffts_plus_graph_optimizer_ptr;
};

TEST_F(FfftsPlusGraphOptimizerUTest, Initialize_SUCCESS) {
  map<string, string> options;
  Status ret = ffts_plus_graph_optimizer_ptr->Initialize(options, nullptr);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FfftsPlusGraphOptimizerUTest, Finalize_SUCCESS) {
  Status ret = ffts_plus_graph_optimizer_ptr->Finalize();
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FfftsPlusGraphOptimizerUTest, OptimizeOriginalGraph_FAILED) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  Status ret = ffts_plus_graph_optimizer_ptr->OptimizeOriginalGraph(*graph);
  EXPECT_EQ(fe::FAILED, ret); 
}

TEST_F(FfftsPlusGraphOptimizerUTest, OptimizeOriginalGraph_SUCCESS) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  ffts_plus_graph_optimizer_ptr->init_flag_ = true;
  Status ret = ffts_plus_graph_optimizer_ptr->OptimizeOriginalGraph(*graph);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FfftsPlusGraphOptimizerUTest, OptimizeFusedGraph_FAILED) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  ffts_plus_graph_optimizer_ptr->init_flag_ = true;
  Status ret = ffts_plus_graph_optimizer_ptr->OptimizeFusedGraph(*graph);
  EXPECT_EQ(fe::FAILED, ret); 
}

tune::Status FFTSOptimizer_TEST(ge::ComputeGraph &graph, bool flag)
{
  return tune::SUCCESS;
}
TEST_F(FfftsPlusGraphOptimizerUTest, OptimizeFusedGraph_SUCCESS) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  ffts_plus_graph_optimizer_ptr->init_flag_ = true;
  ffts_plus_graph_optimizer_ptr->FFTSOptimizer_ = FFTSOptimizer_TEST;
  Status ret = ffts_plus_graph_optimizer_ptr->OptimizeFusedGraph(*graph);
  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FfftsPlusGraphOptimizerUTest, OptimizeGraphBeforeBuild_FAILED) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  Status ret = ffts_plus_graph_optimizer_ptr->OptimizeGraphBeforeBuild(*graph);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FfftsPlusGraphOptimizerUTest, TransSubGraphToFunctionOp_FAILED) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
  CreateGraph(graph);
  for(auto node : graph->GetDirectNode()) {
    (void)ge::AttrUtils::SetInt(node->GetOpDesc(), kThreadScopeId, 1);
  }
  Status ret = ffts_plus_graph_optimizer_ptr->TransSubGraphToFunctionOp(*graph);
  EXPECT_EQ(fe::FAILED, ret); 
}

