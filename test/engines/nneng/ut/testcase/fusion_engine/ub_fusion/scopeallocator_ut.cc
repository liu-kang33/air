/**
 * Copyright 2019-2020 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>


#define private public
#define protected public
#include "common/scope_allocator.h"
#include "graph/compute_graph.h"
#include "graph/op_desc.h"
#include "graph/utils/attr_utils.h"
#undef private
#undef protected

using namespace ge;
using namespace fe;

class UTTEST_scope_allocator : public testing::Test
{
protected:
    static void SetUpTestCase()
    {
        std::cout << "fusion engine scope allocator UT SetUp" << std::endl;
    }
    static void TearDownTestCase()
    {
        std::cout << "fusion engine scope allocator UT TearDown" << std::endl;
    }

    virtual void SetUp()
    {
    }

    virtual void TearDown()
    {

    }
};

TEST_F(UTTEST_scope_allocator, has_scope_attr)
{
    shared_ptr<fe::ScopeAllocator> scope_allocator(new fe::ScopeAllocator());
    scope_allocator->Init();

    ge::OpDescPtr opdef = nullptr;
    bool check_value1 = scope_allocator->HasScopeAttr(opdef);

    int64_t scope_id = 1;
    bool check_value2 = scope_allocator->GetScopeAttr(opdef,scope_id);
    bool check_value3 = scope_allocator->SetScopeAttr(opdef,2);

    bool acutal_value = (check_value1== false)&&(check_value2== false)&&(check_value3== false);

    EXPECT_EQ(acutal_value,true);
}

TEST_F(UTTEST_scope_allocator, set_scope_attr)
{
    shared_ptr<fe::ScopeAllocator> scope_allocator(new fe::ScopeAllocator());

    ge::OpDescPtr opdef = std::make_shared<OpDesc>("node", "node");
    int64_t scope_id = 1;

    bool result = scope_allocator->SetScopeAttr(opdef, scope_id);
    EXPECT_EQ(result, true);
}
