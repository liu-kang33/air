/**
 *
 * @file ffts_task_builder_unittest.cc
 *
 * @brief
 *
 * @version 1.0
 *
 */
#include <gtest/gtest.h>
#include <iostream>

#include <list>

#define private public
#define protected public
#include "ffts/auto_ffts_task_builder.h"
#include "graph_builder_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "common/aicore_util_attr_define.h"
#include "common/util/op_info_util.h"
#include "common/configuration.h"
#include "common/plugin_manager.h"
#include "common/util/platform_info.h"
#include "graph_optimizer/ffts/ffts_pass.h"
#include "../../../../graph_constructor/graph_constructor.h"
#include "../fe_test_utils.h"

using namespace std;
using namespace fe;
using namespace ge;
using FftsTaskBuilderAdapterPtr = std::shared_ptr<FftsTaskBuilderAdapter>;
using AutoFftsTaskBuilderPtr = shared_ptr<AutoFftsTaskBuilder>;

void FFTSDestroyContext(RunContext& context)
{
  assert(rtModelUnbindStream(context.model, context.stream) == RT_ERROR_NONE);
  assert(rtModelDestroy(context.model) == RT_ERROR_NONE);
  assert(rtStreamDestroy(context.stream) == RT_ERROR_NONE);
}

class FFTSTaskBuilderUTest : public testing::Test {
 protected:
  static void SetUpTestCase() {
    cout << "FFTSTaskBuilderUTest SetUpTestCase" << endl;
  }
  static void TearDownTestCase() {
    cout << "FFTSTaskBuilderUTest TearDownTestCase" << endl;
  }

  virtual void SetUp() {
    NodePtr node = CreateNode();
    TaskBuilderContext context_tbc;
    ffts_task_builder_adapter_ptr = std::make_shared<FftsTaskBuilderAdapter>(*node, context_tbc);
    auto_ffts_task_builder_ptr = make_shared<AutoFftsTaskBuilder>();
    context_ = CreateContext();
  }

  virtual void TearDown() {
    cout << "a test Tear Down" << endl;
    FFTSDestroyContext(context_);
  }

  static RunContext CreateContext()
  {
    rtStream_t stream = nullptr;
    rtModel_t model = nullptr;

    assert(rtStreamCreate(&stream, 0) == RT_ERROR_NONE);
    assert(rtModelCreate(&model, 0) == RT_ERROR_NONE);
    assert(rtModelBindStream(model, stream, 0) == RT_ERROR_NONE);

    RunContext context;
    context.model = model;
    context.stream = stream;
    context.dataMemSize = 101;
    context.dataMemBase = (uint8_t *) (intptr_t) 1000;
    context.weightMemSize = 200;
    context.weightMemBase = (uint8_t *) (intptr_t) 1101;
    context.weightsBuffer = Buffer(20);

    return context;
  }

  void GenerateTensorSlice(vector<vector<vector<ffts::DimRange>>> &tensor_slice, const vector<vector<int64_t>> &shape) {
    for (size_t i = 0; i < 2; i++) {
      for (size_t j = 0; j < shape.size(); j++) {
        vector<int64_t> temp = shape[j];
        vector<ffts::DimRange> vdr;
        for (size_t z = 0; z < temp.size() - 1;) {
            ffts::DimRange dr;
            dr.lower = temp[z];
            dr.higher = temp[z + 1];
            vdr.push_back(dr);
            z = z + 2;
        }
        vector<vector<ffts::DimRange>> threadSlice;
        threadSlice.push_back(vdr);
        tensor_slice.push_back(threadSlice);
      }
    }
    return ;
  }

  NodePtr CreateNode()
  {
    FeTestOpDescBuilder builder;
    builder.SetName("test_tvm");
    builder.SetType("test_tvm");
    builder.AddInputDesc({288,32,16,16}, ge::FORMAT_NCHW, ge::DT_FLOAT);
    builder.AddOutputDesc({288,32,16,16}, ge::FORMAT_NCHW, ge::DT_FLOAT);
    NodePtr node =  builder.Finish();
    ge::AttrUtils::SetListInt(node->GetOpDesc(), ge::TVM_ATTR_NAME_THREAD_BLOCKDIM, {1, 1});

    vector<vector<vector<ffts::DimRange>>> tensor_slice_;
    vector<int64_t> shape_input = {0, 288, 0, 32, 0, 16, 0, 16};
    vector<vector<int64_t>> shape_;
    shape_.push_back(shape_input);
    GenerateTensorSlice(tensor_slice_, shape_);
    ffts::ThreadSliceMap thread_slice_map;
    thread_slice_map.thread_mode = 0;
    thread_slice_map.input_tensor_slice = tensor_slice_;
    thread_slice_map.output_tensor_slice = tensor_slice_;
    ffts::ThreadSliceMapPtr thread_slice_map_ptr = std::make_shared<ffts::ThreadSliceMap>(thread_slice_map);
    node->GetOpDesc()->SetExtAttr(ffts::kAttrSgtStructInfo, thread_slice_map_ptr);
    return node;
  }

  void BuildGraph_SubGraph(const ComputeGraphPtr& graph) {
    NodePtr tile = CreateNode();
    NodePtr tile_node = graph->AddNode(tile);
  }

  ComputeGraphPtr BuildGraph() {
    auto builder = ut::ComputeGraphBuilder("test");
    auto sub_node = builder.AddNode("sub_node", "sub_node", 1, 0);
    auto root_graph = builder.GetGraph();
    string subgraph_name = "_sgt_sub_graph";
    ComputeGraphPtr then_branch_graph = std::make_shared<ComputeGraph>("_sgt_sub_graph");
    BuildGraph_SubGraph(then_branch_graph);
    then_branch_graph->SetParentNode(sub_node);
    then_branch_graph->SetParentGraph(root_graph);
    sub_node->GetOpDesc()->AddSubgraphName(subgraph_name);
    sub_node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph_name);
    (void)ge::AttrUtils::SetGraph(sub_node->GetOpDesc(), "_sgt_sub_graph", then_branch_graph);
    (void)ge::AttrUtils::SetInt(sub_node->GetOpDesc(), "_graph_sub_task_num", 1);
    (void)ge::AttrUtils::SetInt(sub_node->GetOpDesc(), "_graph_tick_cache_num", 1);
    root_graph->AddSubgraph(subgraph_name, then_branch_graph);
    return root_graph;
  }

 public:
  FftsTaskBuilderAdapterPtr ffts_task_builder_adapter_ptr;
  AutoFftsTaskBuilderPtr auto_ffts_task_builder_ptr;
  RunContext context_;
};


TEST_F(FFTSTaskBuilderUTest, GenerateTask_SUCCESS) {
  ComputeGraphPtr graph = BuildGraph();
  auto sub_node = graph->FindNode("sub_node");
  vector<domi::TaskDef> task_defs;
  Status ret = auto_ffts_task_builder_ptr->GenerateTask(*sub_node, context_, task_defs);
  EXPECT_EQ(fe::SUCCESS, ret);
}

TEST_F(FFTSTaskBuilderUTest, Debug_task_info_SUCCESS) {
  rtFftsTaskInfo_t task_info;
  auto_ffts_task_builder_ptr->DebugTaskInfo(task_info);
  bool flag = false;
  if (task_info.subTaskNum == 0) {
    flag = true;
  }
  EXPECT_EQ(true, flag);
}

TEST_F(FFTSTaskBuilderUTest, Gen_Sub_Ffts_Task_Common_Info_SUCCESS) {
  NodePtr node_ptr = CreateNode();
  rtFftsSubTaskInfo_t sub_ffts_task_info;
  Status ret = auto_ffts_task_builder_ptr->GenSubFftsTaskCommonInfo(node_ptr, sub_ffts_task_info);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FFTSTaskBuilderUTest, Do_Gen_sub_Node_Task_SUCCESS) {
  NodePtr node_ptr = CreateNode();
  rtFftsSubTaskInfo_t sub_ffts_task_info;
  Status ret = auto_ffts_task_builder_ptr->DoGenerateSubNodeTask(node_ptr, sub_ffts_task_info);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FFTSTaskBuilderUTest, GenTicketCacheInfo_0_SUCCESS) {
  NodePtr node_ptr = CreateNode();
  TickCacheMap tick_cache_table;
  map<int32_t, uint8_t> mp_i;
  map<int32_t, uint8_t> mp_o;
  mp_i[1] = 1;
  mp_o[2] = 2;
  tick_cache_table.input_cache_table= mp_i;
  tick_cache_table.output_cache_table= mp_o;
  node_ptr->GetOpDesc()->SetExtAttr("_tick_cache_map", tick_cache_table);
  rtFftsTaskInfo_t task_info;
  task_info.ticketCache[1].custom.autoThreadCache.ticketCacheRefCnt = 0;
  vector<void *> thread_input_addrs_v = {nullptr};
  vector<vector<void*>> thread_input_addrs;
  thread_input_addrs.push_back(thread_input_addrs_v);
  vector<int64_t> input_tensor_sizes = {1};
  vector<uint64_t> thread_addr_offset = {1};
  ffts_task_builder_adapter_ptr->thread_input_addrs_ = thread_input_addrs;
  ffts_task_builder_adapter_ptr->input_tensor_sizes_ = input_tensor_sizes;
  ffts_task_builder_adapter_ptr->output_tensor_sizes_ = input_tensor_sizes;
  ffts_task_builder_adapter_ptr->thread_addr_offset_ = thread_addr_offset;
  ffts_task_builder_adapter_ptr->first_thread_input_addrs_ = thread_input_addrs_v;
  ffts_task_builder_adapter_ptr->first_thread_output_addrs_ = thread_input_addrs_v;
  ffts_task_builder_adapter_ptr->thread_dim_ = 2;

  auto_ffts_task_builder_ptr->task_builder_adapter_ptr_map_["test_tvm"] = ffts_task_builder_adapter_ptr;
  Status ret = auto_ffts_task_builder_ptr->GenTicketCacheInfo(node_ptr, task_info);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FFTSTaskBuilderUTest, GenTicketCacheInfo_1_SUCCESS) {
  NodePtr node_ptr = CreateNode();
  TickCacheMap tick_cache_table;
  map<int32_t, uint8_t> mp_o;
  mp_o[2] = 2;
  tick_cache_table.output_cache_table= mp_o;
  node_ptr->GetOpDesc()->SetExtAttr("_tick_cache_map", tick_cache_table);
  rtFftsTaskInfo_t task_info;
  task_info.ticketCache[2].custom.autoThreadCache.ticketCacheRefCnt = 0;
  vector<void *> thread_input_addrs_v = {nullptr};
  vector<vector<void*>> thread_input_addrs;
  thread_input_addrs.push_back(thread_input_addrs_v);
  vector<int64_t> input_tensor_sizes = {1};
  vector<uint64_t> thread_addr_offset = {1};
  ffts_task_builder_adapter_ptr->thread_input_addrs_ = thread_input_addrs;
  ffts_task_builder_adapter_ptr->input_tensor_sizes_ = input_tensor_sizes;
  ffts_task_builder_adapter_ptr->output_tensor_sizes_ = input_tensor_sizes;
  ffts_task_builder_adapter_ptr->thread_addr_offset_ = thread_addr_offset;
  ffts_task_builder_adapter_ptr->first_thread_input_addrs_ = thread_input_addrs_v;
  ffts_task_builder_adapter_ptr->first_thread_output_addrs_ = thread_input_addrs_v;
  ffts_task_builder_adapter_ptr->thread_dim_ = 2;

  auto_ffts_task_builder_ptr->task_builder_adapter_ptr_map_["test_tvm"] = ffts_task_builder_adapter_ptr;
  Status ret = auto_ffts_task_builder_ptr->GenTicketCacheInfo(node_ptr, task_info);
  EXPECT_EQ(fe::SUCCESS, ret); 
}

TEST_F(FFTSTaskBuilderUTest, GenSubFftsTaskInfo_Faield) {
  NodePtr node_ptr = CreateNode();
  rtFftsSubTaskInfo_t sub_ffts_task_info;
  vector<vector<void *>> thread_workspace_addrs;
  ffts_task_builder_adapter_ptr->thread_workspace_addrs_ = thread_workspace_addrs;

  auto_ffts_task_builder_ptr->task_builder_adapter_ptr_map_["test_tvm"] = ffts_task_builder_adapter_ptr;
  Status ret = auto_ffts_task_builder_ptr->GenSubFftsTaskInfo(node_ptr, sub_ffts_task_info);
  EXPECT_EQ(fe::FAILED, ret); 
}
