/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "ge_graph_dsl/graph_dsl.h"

#define private public
#define protected public
#include "hybrid/executor/subgraph_context.h"
#include "hybrid/node_executor/ffts_plus/ffts_plus_sub_task.h"
#include "common/model/ge_root_model.h"
#include "graph/load/model_manager/tbe_handle_store.h"
#include "ut/ge/ffts_plus_proto_tools.h"
#include "register/op_tiling_registry.h"

using namespace std;
using namespace testing;

namespace ge {
using namespace hybrid;

void SetPartitionedCallSubgraph(const ComputeGraphPtr &root, const NodePtr &node, const ComputeGraphPtr &subgraph) {
  subgraph->SetParentNode(node);
  subgraph->SetParentGraph(node->GetOwnerComputeGraph());
  subgraph->SetGraphUnknownFlag(true);

  root->AddSubgraph(subgraph);
  node->GetOpDesc()->AddSubgraphName("f");
  node->GetOpDesc()->SetSubgraphInstanceName(0, subgraph->GetName());
}

class FFTSPlusTaskUpdateMix : public FFTSPlusTaskUpdate {
 public:
  Status GetAutoThreadParam(const NodePtr &node, const std::vector<optiling::utils::OpRunInfo> &op_run_info,
                            AutoThreadParam &auto_thread_param) {
    auto_thread_param.thread_dim = 32U;
    auto_thread_param.input_output_num = node->GetAllInDataAnchorsSize() + node->GetAllOutDataAnchorsSize();

    std::vector<int64_t> workspaces = node->GetOpDesc()->GetWorkspaceBytes();
    auto_thread_param.input_output_num += workspaces.size();

    auto_thread_param.task_addr_offset.resize(auto_thread_param.input_output_num, 32);

    return SUCCESS;
  }
};

class FFTSPlusTaskInvalid : public FFTSPlusTaskUpdate {
 public:
  Status GetAutoThreadParam(const NodePtr &node, const std::vector<optiling::utils::OpRunInfo> &op_run_info,
                            AutoThreadParam &auto_thread_param) {
    auto_thread_param.thread_dim = 0U;
    return SUCCESS;
  }
};

class UtestFftsPlusSubTask : public testing::Test {
 protected:
  void SetUp() {
    // Register from FE, set stub here.
    FftsPlusUpdateManager::FftsPlusUpdateRegistrar aic_001("AIC", [](){ return MakeShared<FFTSPlusTaskUpdateMix>(); });
    FftsPlusUpdateManager::FftsPlusUpdateRegistrar aiv_001("MIX_AIV", [](){ return MakeShared<FFTSPlusTaskUpdateMix>(); });
    FftsPlusUpdateManager::FftsPlusUpdateRegistrar cpu_001("AICPU", [](){ return MakeShared<FFTSPlusTaskUpdateMix>(); });

    const auto op_tiling_func = [](const Operator &, const optiling::OpCompileInfoV2 &, optiling::OpRunInfoV2 &) { return true; };
    REGISTER_OP_TILING_UNIQ_V2(ReLU, op_tiling_func, 201);
    optiling::OpTilingRegistryInterf_V2("ReLU", op_tiling_func);
    REGISTER_OP_TILING_UNIQ_V2(Conv2D, op_tiling_func, 201);
    optiling::OpTilingRegistryInterf_V2("Conv2D", op_tiling_func);
  }
  void TearDown() {
    TBEHandleStore::GetInstance().kernels_.clear();
    FftsPlusUpdateManager::Instance().creators_.clear();
  }
};

TEST_F(UtestFftsPlusSubTask, ffts_plus_aic_task) {
  DEF_GRAPH(g1) {
    CHAIN(NODE("_arg_0", DATA)->NODE("PartitionedCall_0", PARTITIONEDCALL)->NODE("Node_Output", NETOUTPUT));
    CHAIN(NODE("_arg_1", DATA)->NODE("PartitionedCall_0"));
  };
  ComputeGraphPtr sgt_graph = ToComputeGraph(g1);
  const auto sgt_node = sgt_graph->FindNode("PartitionedCall_0");
  EXPECT_NE(sgt_node, nullptr);

  DEF_GRAPH(g2) {
    CHAIN(NODE("sgt/_arg_0", DATA)->NODE("sgt/conv2d", CONV2D)->NODE("sgt/Node_Output", NETOUTPUT));
    CHAIN(NODE("sgt/_arg_1", DATA)->NODE("sgt/conv2d"));
  };
  ComputeGraphPtr sub_graph = ToComputeGraph(g2);
  const auto sub_node = sub_graph->FindNode("sgt/conv2d");
  EXPECT_NE(sub_node, nullptr);

  SetPartitionedCallSubgraph(sgt_graph, sgt_node, sub_graph);
  AttrUtils::SetBool(sgt_node->GetOpDesc(), ATTR_NAME_FFTS_PLUS_SUB_GRAPH, true);
  AttrUtils::SetStr(sub_node->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "AIC");
  SetNodeAnchorStatus(sub_node);
  InitFftsThreadSliceMap(sub_node->GetOpDesc());

  //////////////////////////////////////////////////////////////////////////////
  // Build FftsPlusTaskDef.
  domi::TaskDef task_def;
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  ffts_plus_task_def->set_op_index(sgt_node->GetOpDesc()->GetId());
  InitTaskSQEInfo(ffts_plus_task_def);
  domi::FftsPlusCtxDef *ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  ctx_def->set_op_index(sub_node->GetOpDesc()->GetId());
  ctx_def->set_context_type(static_cast<uint32_t>(RT_CTX_TYPE_AIV));
  domi::FftsPlusAicAivCtxDef *aic_aiv_def = ctx_def->mutable_aic_aiv_ctx();
  InitAicAivCtx(aic_aiv_def);
  aic_aiv_def->clear_kernel_name();

  GeRootModelPtr ge_root_model = MakeShared<GeRootModel>(sgt_graph);
  HybridModel hybrid_model(ge_root_model);
  hybrid_model.task_defs_[sgt_node].emplace_back(task_def);

  auto unique_graph_item = std::unique_ptr<GraphItem>(new(std::nothrow)GraphItem());
  auto graph_item = unique_graph_item.get();
  graph_item->total_inputs_ = 2;
  graph_item->total_outputs_ = 1;
  graph_item->SetName(sub_graph->GetName());
  hybrid_model.subgraph_items_.emplace(sub_graph->GetName(), std::move(unique_graph_item));

  {
    std::unique_ptr<NodeItem> new_node;
    ASSERT_EQ(NodeItem::Create(sgt_node, new_node), SUCCESS);
    NodeItem *node_item = new_node.get();
    hybrid_model.node_items_[sgt_node] = std::move(new_node);
    node_item->input_start = 0;
    node_item->output_start = 0;
    ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
    ASSERT_NE(node_item->node_executor, nullptr);
    ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sgt_node, node_item->kernel_task), SUCCESS);
  }

  std::unique_ptr<NodeItem> new_node;
  ASSERT_EQ(NodeItem::Create(sub_node, new_node), SUCCESS);
  NodeItem *node_item = new_node.get();
  hybrid_model.node_items_[sub_node] = std::move(new_node);
  graph_item->node_items_.emplace_back(node_item);
  node_item->input_start = 0;
  node_item->output_start = 0;
  ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
  ASSERT_NE(node_item->node_executor, nullptr);
  ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sub_node, node_item->kernel_task), SUCCESS);

  GraphExecutionContext graph_context;
  graph_context.allocator = NpuMemoryAllocator::GetAllocator();
  SubgraphContext subgraph_context(graph_item, &graph_context);
  ASSERT_EQ(subgraph_context.Init(), SUCCESS);

  auto node_state = subgraph_context.GetNodeState(node_item);
  ASSERT_NE(node_state, nullptr);
  ASSERT_NE(node_state->GetTaskContext(), nullptr);
  auto &task_context = *node_state->GetTaskContext();

  // GetTaskDefs is null.
  FftsPlusAiCoreTask sub_task;
  ASSERT_EQ(sub_task.Load(hybrid_model, sub_node), SUCCESS);
  ASSERT_NE(sub_task.ffts_node_item_, nullptr);

  // SUCCESS for default function.
  ASSERT_EQ(sub_task.UpdateArgs(task_context), SUCCESS);
  ASSERT_EQ(sub_task.UpdateTilingData(task_context), SUCCESS);

  // SUCCESS for default function.
  optiling::utils::OpRunInfo run_info;
  AutoThreadSubTaskFlush sub_task_flush;
  sub_task_flush.op_run_info.emplace_back(run_info);
  sub_task_flush.op_run_info.emplace_back(run_info);
  ASSERT_EQ(sub_task.UpdateAddrAndPrefCnt(*sub_node->GetOpDesc(), sub_task_flush), SUCCESS);

  // SUCCESS for default function.
  AutoThreadParam params;
  params.thread_dim = 1;
  params.input_output_num = 2;
  std::vector<uintptr_t> io_addrs((params.input_output_num + 2) * params.thread_dim);
  sub_task.InitOpTiling(params, 0, 0, io_addrs);

  TBEHandleStore::GetInstance().StoreTBEHandle(sub_node->GetOpDesc()->GetName(), nullptr, nullptr);
  auto aic_aiv_task = reinterpret_cast<FftsPlusAicAivTask *>(node_item->kernel_task.get());
  ASSERT_NE(aic_aiv_task, nullptr);

  // GetAutoThreadParam failed, thread_dim is 0.
  aic_aiv_task->ffts_plus_ctx_update_ = MakeShared<FFTSPlusTaskInvalid>();
  ASSERT_EQ(aic_aiv_task->UpdateTilingData(task_context), FAILED);

  // tune::FFTSNodeThread failed.
  auto empty_graph = MakeShared<ComputeGraph>("empty_graph");
  node_item->node->SetOwnerComputeGraph(empty_graph);
  ASSERT_EQ(aic_aiv_task->UpdateTilingData(task_context), INTERNAL_ERROR);
  ASSERT_NE(aic_aiv_task->ffts_node_task_, nullptr);
  ASSERT_EQ(aic_aiv_task->UpdateTilingData(task_context), INTERNAL_ERROR);

  aic_aiv_task->tiling_offset_ = {0, 24};
  aic_aiv_task->InitOpTiling(params, 0, 0, io_addrs);
  ASSERT_EQ(aic_aiv_task->UpdateAddrAndPrefCnt(*sub_node->GetOpDesc(), sub_task_flush), SUCCESS);
}

TEST_F(UtestFftsPlusSubTask, ffts_plus_mix_task) {
  DEF_GRAPH(g1) {
    CHAIN(NODE("_arg_0", DATA)->NODE("PartitionedCall_0", PARTITIONEDCALL)->NODE("Node_Output", NETOUTPUT));
    CHAIN(NODE("_arg_1", DATA)->NODE("PartitionedCall_0"));
  };
  ComputeGraphPtr sgt_graph = ToComputeGraph(g1);
  const auto sgt_node = sgt_graph->FindNode("PartitionedCall_0");
  EXPECT_NE(sgt_node, nullptr);

  DEF_GRAPH(g2) {
    CHAIN(NODE("sgt/_arg_0", DATA)->NODE("sgt/conv2d", CONV2D)->NODE("sgt/Node_Output", NETOUTPUT));
    CHAIN(NODE("sgt/_arg_1", DATA)->NODE("sgt/conv2d"));
  };
  ComputeGraphPtr sub_graph = ToComputeGraph(g2);
  const auto sub_node = sub_graph->FindNode("sgt/conv2d");
  EXPECT_NE(sub_node, nullptr);

  SetPartitionedCallSubgraph(sgt_graph, sgt_node, sub_graph);
  AttrUtils::SetBool(sgt_node->GetOpDesc(), ATTR_NAME_FFTS_PLUS_SUB_GRAPH, true);
  AttrUtils::SetStr(sub_node->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "MIX_AIV");

  //////////////////////////////////////////////////////////////////////////////
  // Build FftsPlusTaskDef.
  domi::TaskDef task_def;
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  ffts_plus_task_def->set_op_index(sgt_node->GetOpDesc()->GetId());
  InitTaskSQEInfo(ffts_plus_task_def);
  domi::FftsPlusCtxDef *ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  ctx_def->set_op_index(sub_node->GetOpDesc()->GetId());
  ctx_def->set_context_type(static_cast<uint32_t>(RT_CTX_TYPE_MIX_AIV));
  domi::FftsPlusMixAicAivCtxDef *aic_aiv_def = ctx_def->mutable_mix_aic_aiv_ctx();
  InitMixAicAivCtx(aic_aiv_def);
  aic_aiv_def->clear_kernel_name();

  GeRootModelPtr ge_root_model = MakeShared<GeRootModel>(sgt_graph);
  HybridModel hybrid_model(ge_root_model);
  hybrid_model.task_defs_[sgt_node].emplace_back(task_def);

  auto unique_graph_item = std::unique_ptr<GraphItem>(new(std::nothrow)GraphItem());
  auto graph_item = unique_graph_item.get();
  graph_item->total_inputs_ = 2;
  graph_item->total_outputs_ = 1;
  graph_item->SetName(sub_graph->GetName());
  hybrid_model.subgraph_items_.emplace(sub_graph->GetName(), std::move(unique_graph_item));

  {
    std::unique_ptr<NodeItem> new_node;
    ASSERT_EQ(NodeItem::Create(sgt_node, new_node), SUCCESS);
    NodeItem *node_item = new_node.get();
    hybrid_model.node_items_[sgt_node] = std::move(new_node);
    node_item->input_start = 0;
    node_item->output_start = 0;
    ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
    ASSERT_NE(node_item->node_executor, nullptr);
    ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sgt_node, node_item->kernel_task), SUCCESS);
  }

  std::unique_ptr<NodeItem> new_node;
  ASSERT_EQ(NodeItem::Create(sub_node, new_node), SUCCESS);
  NodeItem *node_item = new_node.get();
  hybrid_model.node_items_[sub_node] = std::move(new_node);
  graph_item->node_items_.emplace_back(node_item);
  node_item->input_start = 0;
  node_item->output_start = 0;
  ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
  ASSERT_NE(node_item->node_executor, nullptr);
  ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sub_node, node_item->kernel_task), SUCCESS);

  GraphExecutionContext graph_context;
  SubgraphContext subgraph_context(graph_item, &graph_context);
  ASSERT_EQ(subgraph_context.Init(), SUCCESS);

  auto node_state = subgraph_context.GetNodeState(node_item);
  ASSERT_NE(node_state, nullptr);
  ASSERT_NE(node_state->GetTaskContext(), nullptr);
  auto &task_context = *node_state->GetTaskContext();

  // GetTaskDefs is null.
  FftsPlusAiCoreTask sub_task;
  ASSERT_EQ(sub_task.Load(hybrid_model, sub_node), SUCCESS);
  ASSERT_NE(sub_task.ffts_node_item_, nullptr);

  // SUCCESS for default function.
  ASSERT_EQ(sub_task.UpdateArgs(task_context), SUCCESS);

  // SUCCESS for default function.
  optiling::utils::OpRunInfo run_info;
  AutoThreadSubTaskFlush sub_task_flush;
  sub_task_flush.op_run_info.emplace_back(run_info);
  sub_task_flush.op_run_info.emplace_back(run_info);
  sub_task_flush.op_run_info.emplace_back(run_info);
  sub_task_flush.op_run_info.emplace_back(run_info);
  ASSERT_EQ(sub_task.UpdateAddrAndPrefCnt(*sub_node->GetOpDesc(), sub_task_flush), SUCCESS);

  // SUCCESS for default function.
  AutoThreadParam params;
  params.thread_dim = 1;
  params.input_output_num = 2;
  std::vector<uintptr_t> io_addrs((params.input_output_num + 4U) * params.thread_dim);
  sub_task.InitOpTiling(params, 0, 0, io_addrs);

  TBEHandleStore::GetInstance().StoreTBEHandle(sub_node->GetOpDesc()->GetName(), nullptr, nullptr);
  auto aic_aiv_task = reinterpret_cast<FftsPlusMixAicAivTask *>(node_item->kernel_task.get());
  ASSERT_NE(aic_aiv_task, nullptr);

  // tune::FFTSNodeThread failed.
  auto empty_graph = MakeShared<ComputeGraph>("empty_graph");
  node_item->node->SetOwnerComputeGraph(empty_graph);
  ASSERT_EQ(aic_aiv_task->UpdateTilingData(task_context), INTERNAL_ERROR);
  ASSERT_NE(aic_aiv_task->ffts_node_task_, nullptr);
  ASSERT_EQ(aic_aiv_task->UpdateTilingData(task_context), INTERNAL_ERROR);

  aic_aiv_task->tiling_offset_ = {0, 24, 64, 88};
  aic_aiv_task->InitOpTiling(params, 0, 0, io_addrs);
  ASSERT_EQ(aic_aiv_task->UpdateAddrAndPrefCnt(*sub_node->GetOpDesc(), sub_task_flush), SUCCESS);
}

TEST_F(UtestFftsPlusSubTask, ffts_plus_cpu_task) {
  DEF_GRAPH(g1) {
    CHAIN(NODE("_arg_0", DATA)->NODE("PartitionedCall_0", PARTITIONEDCALL)->NODE("Node_Output", NETOUTPUT));
    CHAIN(NODE("_arg_1", DATA)->NODE("PartitionedCall_0"));
  };
  ComputeGraphPtr sgt_graph = ToComputeGraph(g1);
  const auto sgt_node = sgt_graph->FindNode("PartitionedCall_0");
  EXPECT_NE(sgt_node, nullptr);

  DEF_GRAPH(g2) {
    CHAIN(NODE("sgt/_arg_0", DATA)->NODE("sgt/conv2d", CONV2D)->NODE("sgt/Node_Output", NETOUTPUT));
    CHAIN(NODE("sgt/_arg_1", DATA)->NODE("sgt/conv2d"));
  };
  ComputeGraphPtr sub_graph = ToComputeGraph(g2);
  const auto sub_node = sub_graph->FindNode("sgt/conv2d");
  EXPECT_NE(sub_node, nullptr);

  SetPartitionedCallSubgraph(sgt_graph, sgt_node, sub_graph);
  AttrUtils::SetBool(sgt_node->GetOpDesc(), ATTR_NAME_FFTS_PLUS_SUB_GRAPH, true);
  AttrUtils::SetStr(sub_node->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "AICPU");

  //////////////////////////////////////////////////////////////////////////////
  // Build FftsPlusTaskDef.
  domi::TaskDef task_def;
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  ffts_plus_task_def->set_op_index(sgt_node->GetOpDesc()->GetId());
  InitTaskSQEInfo(ffts_plus_task_def);
  domi::FftsPlusCtxDef *ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  ctx_def->set_op_index(sub_node->GetOpDesc()->GetId());
  ctx_def->set_context_type(static_cast<uint32_t>(RT_CTX_TYPE_AICPU));
  domi::FftsPlusAicpuCtxDef *ai_cpu_def = ctx_def->mutable_aicpu_ctx();
  InitAicpuCtxCtx(ai_cpu_def);

  GeRootModelPtr ge_root_model = MakeShared<GeRootModel>(sgt_graph);
  HybridModel hybrid_model(ge_root_model);
  hybrid_model.task_defs_[sgt_node].emplace_back(task_def);

  auto unique_graph_item = std::unique_ptr<GraphItem>(new(std::nothrow)GraphItem());
  auto graph_item = unique_graph_item.get();
  graph_item->total_inputs_ = 2;
  graph_item->total_outputs_ = 1;
  graph_item->SetName(sub_graph->GetName());
  hybrid_model.subgraph_items_.emplace(sub_graph->GetName(), std::move(unique_graph_item));

  {
    std::unique_ptr<NodeItem> new_node;
    ASSERT_EQ(NodeItem::Create(sgt_node, new_node), SUCCESS);
    NodeItem *node_item = new_node.get();
    hybrid_model.node_items_[sgt_node] = std::move(new_node);
    node_item->input_start = 0;
    node_item->output_start = 0;
    ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
    ASSERT_NE(node_item->node_executor, nullptr);
    ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sgt_node, node_item->kernel_task), SUCCESS);
  }

  std::unique_ptr<NodeItem> new_node;
  ASSERT_EQ(NodeItem::Create(sub_node, new_node), SUCCESS);
  NodeItem *node_item = new_node.get();
  hybrid_model.node_items_[sub_node] = std::move(new_node);
  graph_item->node_items_.emplace_back(node_item);
  node_item->input_start = 0;
  node_item->output_start = 0;
  ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
  ASSERT_NE(node_item->node_executor, nullptr);
  ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sub_node, node_item->kernel_task), SUCCESS);

  GraphExecutionContext graph_context;
  SubgraphContext subgraph_context(graph_item, &graph_context);
  ASSERT_EQ(subgraph_context.Init(), SUCCESS);

  auto node_state = subgraph_context.GetNodeState(node_item);
  ASSERT_NE(node_state, nullptr);
  ASSERT_NE(node_state->GetTaskContext(), nullptr);
  auto &task_context = *node_state->GetTaskContext();

  // GetTaskDefs is null.
  FftsPlusAiCpuTask sub_task;
  ASSERT_EQ(sub_task.Load(hybrid_model, sub_node), SUCCESS);
  ASSERT_NE(sub_task.ffts_node_item_, nullptr);

  // SUCCESS for default function.
  ASSERT_EQ(sub_task.UpdateArgs(task_context), SUCCESS);

  ASSERT_EQ(sub_task.UpdateTilingData(task_context), SUCCESS);

  TBEHandleStore::GetInstance().StoreTBEHandle(sub_node->GetOpDesc()->GetName(), nullptr, nullptr);
  auto aic_aiv_task = reinterpret_cast<FftsPlusAiCpuTask *>(node_item->kernel_task.get());
  ASSERT_NE(aic_aiv_task, nullptr);
}

TEST_F(UtestFftsPlusSubTask, ffts_plus_mix_l2_task) {
  DEF_GRAPH(g1) {
    CHAIN(NODE("_arg_0", DATA)->NODE("PartitionedCall_0", PARTITIONEDCALL)->NODE("Node_Output", NETOUTPUT));
    CHAIN(NODE("_arg_1", DATA)->NODE("PartitionedCall_0"));
  };
  ComputeGraphPtr sgt_graph = ToComputeGraph(g1);
  const auto sgt_node = sgt_graph->FindNode("PartitionedCall_0");
  EXPECT_NE(sgt_node, nullptr);

  DEF_GRAPH(g2) {
    CHAIN(NODE("sgt/_arg_0", DATA)->NODE("sgt/conv2d", CONV2D)->NODE("sgt/Node_Output", NETOUTPUT));
    CHAIN(NODE("sgt/_arg_1", DATA)->NODE("sgt/conv2d"));
  };
  ComputeGraphPtr sub_graph = ToComputeGraph(g2);
  const auto sub_node = sub_graph->FindNode("sgt/conv2d");
  EXPECT_NE(sub_node, nullptr);

  SetPartitionedCallSubgraph(sgt_graph, sgt_node, sub_graph);
  AttrUtils::SetStr(sub_node->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "MIX_AIV");
  AttrUtils::SetStr(sub_node->GetOpDesc(), "_alias_engine_name", "ffts_plus");
  // set tbe kernel
  AttrUtils::SetStr(sub_node->GetOpDesc(), TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF");
  std::vector<char> conv_bin(64, '\0');
  TBEKernelPtr conv_kernel = MakeShared<ge::OpKernelBin>("sgt/conv2d", std::move(conv_bin));
  sub_node->GetOpDesc()->SetExtAttr(OP_EXTATTR_NAME_TBE_KERNEL, conv_kernel);
  AttrUtils::SetStr(sub_node->GetOpDesc(), sub_node->GetName() + "_kernelname", "sgt/conv2d");
  //////////////////////////////////////////////////////////////////////////////
  // Build FftsPlusTaskDef.
  domi::TaskDef task_def;
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  ffts_plus_task_def->set_op_index(sub_node->GetOpDesc()->GetId());
  InitTaskSQEInfo(ffts_plus_task_def);
  domi::FftsPlusCtxDef *ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  ctx_def->set_op_index(sub_node->GetOpDesc()->GetId());
  ctx_def->set_context_type(static_cast<uint32_t>(RT_CTX_TYPE_MIX_AIV));
  domi::FftsPlusMixAicAivCtxDef *aic_aiv_def = ctx_def->mutable_mix_aic_aiv_ctx();
  InitMixAicAivCtx(aic_aiv_def);
  aic_aiv_def->clear_kernel_name();

  GeRootModelPtr ge_root_model = MakeShared<GeRootModel>(sgt_graph);
  HybridModel hybrid_model(ge_root_model);
  hybrid_model.task_defs_[sub_node].emplace_back(task_def);

  auto unique_graph_item = std::unique_ptr<GraphItem>(new (std::nothrow) GraphItem());
  auto graph_item = unique_graph_item.get();
  graph_item->total_inputs_ = 2;
  graph_item->total_outputs_ = 1;
  graph_item->SetName(sub_graph->GetName());
  hybrid_model.subgraph_items_.emplace(sub_graph->GetName(), std::move(unique_graph_item));

  {
    std::unique_ptr<NodeItem> new_node;
    ASSERT_EQ(NodeItem::Create(sub_node, new_node), SUCCESS);
    NodeItem *node_item = new_node.get();
    hybrid_model.node_items_[sub_node] = std::move(new_node);
    node_item->input_start = 0;
    node_item->output_start = 0;
    ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
    ASSERT_NE(node_item->node_executor, nullptr);
    ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sub_node, node_item->kernel_task), SUCCESS);
  }

  std::unique_ptr<NodeItem> new_node;
  ASSERT_EQ(NodeItem::Create(sub_node, new_node), SUCCESS);
  NodeItem *node_item = new_node.get();
  hybrid_model.node_items_[sub_node] = std::move(new_node);
  graph_item->node_items_.emplace_back(node_item);
  node_item->input_start = 0;
  node_item->output_start = 0;
  ASSERT_EQ(NodeExecutorManager::GetInstance().GetExecutor(*node_item, node_item->node_executor), SUCCESS);
  ASSERT_NE(node_item->node_executor, nullptr);
  ASSERT_EQ(node_item->node_executor->LoadTask(hybrid_model, sub_node, node_item->kernel_task), SUCCESS);

  GraphExecutionContext graph_context;
  SubgraphContext subgraph_context(graph_item, &graph_context);
  ASSERT_EQ(subgraph_context.Init(), SUCCESS);
  graph_context.callback_manager = new (std::nothrow) CallbackManager();
  graph_context.allocator = new (std::nothrow) NpuMemoryAllocator(0, nullptr);
  graph_context.own_callback_manager = true;

  auto node_state = subgraph_context.GetNodeState(node_item);
  ASSERT_NE(node_state, nullptr);
  ASSERT_NE(node_state->GetTaskContext(), nullptr);
  auto &task_context = *node_state->GetTaskContext();

  // GetTaskDefs is null.
  FftsPlusMixL2Task sub_task;
  ASSERT_EQ(sub_task.Load(hybrid_model, sub_node), SUCCESS);
  ASSERT_EQ(sub_task.ffts_node_item_, nullptr);

  // SUCCESS for default function.
  ASSERT_EQ(sub_task.UpdateArgs(task_context), SUCCESS);

  // SUCCESS for default function.
  optiling::OpTilingFuncV2 op_tiling_func = [](const Operator &, const optiling::OpCompileInfoV2 &,
                                               optiling::OpRunInfoV2 &op_run_info) -> bool {
    op_run_info.SetWorkspaces({100, 100});
    return true;
  };
  optiling::OpTilingRegistryInterf_V2(CONV2D, op_tiling_func);
  REGISTER_OP_TILING_UNIQ_V2(CONV2D, op_tiling_func, 100);
  AttrUtils::SetStr(sub_node->GetOpDesc(), "compile_info_json", "stub_json");
  AttrUtils::SetStr(sub_node->GetOpDesc(), "compile_info_key", "stub_key");
  ASSERT_EQ(sub_task.UpdateTilingData(task_context), SUCCESS);
  auto call_back = []() { return; };

  TBEHandleStore::GetInstance().StoreTBEHandle(sub_node->GetOpDesc()->GetName(), nullptr, nullptr);
  auto mixl2_task = reinterpret_cast<FftsPlusMixL2Task *>(node_item->kernel_task.get());
  ASSERT_NE(mixl2_task, nullptr);

  ASSERT_EQ(sub_task.ExecuteAsync(task_context, call_back), SUCCESS);
}
}  // namespace ge
