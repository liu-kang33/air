/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#define private public
#define protected public
#include "hybrid/node_executor/node_executor.h"
#include "hybrid/model/hybrid_model.h"

#include "register/ffts_plus_update_manager.h"
#include "register/op_tiling_registry.h"
#include "graph/utils/graph_utils.h"
#include "graph/utils/tensor_utils.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/load/model_manager/tbe_handle_store.h"
#include "ut/ge/ffts_plus_proto_tools.h"
#include "framework/common/types.h"
#include "common/kernel_store.h"
#include "common/model/ge_root_model.h"
#include "hybrid/hybrid_davinci_model.h"

using namespace std;
using namespace testing;
using namespace optiling;

namespace ge {
using namespace hybrid;
class FFTSPlusTaskUpdateStub : public FFTSPlusTaskUpdate {
 public:
  Status GetAutoThreadParam(const NodePtr &node, const std::vector<optiling::utils::OpRunInfo> &op_run_info,
                            AutoThreadParam &auto_thread_param) {
    auto_thread_param.thread_dim = 32U;
    auto_thread_param.input_output_num = node->GetAllInDataAnchorsSize() + node->GetAllOutDataAnchorsSize();

    std::vector<int64_t> workspaces = node->GetOpDesc()->GetWorkspaceBytes();
    auto_thread_param.input_output_num += workspaces.size();

    auto_thread_param.task_addr_offset.resize(auto_thread_param.input_output_num, 32U);

    std::string core_type;
    (void)AttrUtils::GetStr(node->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, core_type);
    if (core_type == "AICPU") {
      auto_thread_param.args_size = 64U;
      auto_thread_param.extinfo_size = 32U;
    }
    return SUCCESS;
  }

  Status UpdateSubTaskAndCache(const NodePtr &node, const AutoThreadSubTaskFlush &sub_task_flush,
                               rtFftsPlusTaskInfo_t &ffts_plus_task_info) {
    return SUCCESS;
  }
};

class UtestFftsPlusSubgraphExecutor : public testing::Test {
 protected:
  void SetUp() {
    auto &engine_mapping = NodeExecutorManager::GetInstance().engine_mapping_;
    engine_mapping.emplace("DNN_VM_RTS_OP_STORE", NodeExecutorManager::ExecutorType::RTS);
    engine_mapping.emplace("DNN_VM_GE_LOCAL_OP_STORE", NodeExecutorManager::ExecutorType::GE_LOCAL);

    // Register from FE, set stub here.
    const std::string kCoreTypeAIC = "AIC";     // FftsPlusUpdateManager::FftsPlusUpdateRegistrar
    const std::string kCoreTypeAIV = "MIX_AIV"; // FftsPlusUpdateManager::FftsPlusUpdateRegistrar
    const std::string kCoreTypeCPU = "AICPU";   // FftsPlusUpdateManager::FftsPlusUpdateRegistrar
    REGISTER_FFTS_PLUS_CTX_UPDATER(kCoreTypeAIC, FFTSPlusTaskUpdateStub);
    REGISTER_FFTS_PLUS_CTX_UPDATER(kCoreTypeAIV, FFTSPlusTaskUpdateStub);
    REGISTER_FFTS_PLUS_CTX_UPDATER(kCoreTypeCPU, FFTSPlusTaskUpdateStub);

    OpTilingFuncV2 op_tiling_func = [](const Operator &, const OpCompileInfoV2 &, OpRunInfoV2 &) -> bool {return true;};
    REGISTER_OP_TILING_UNIQ_V2(ReLU, op_tiling_func, 101);
    OpTilingRegistryInterf_V2("ReLU", op_tiling_func);
    REGISTER_OP_TILING_UNIQ_V2(Conv2D, op_tiling_func, 101);
    OpTilingRegistryInterf_V2("Conv2D", op_tiling_func);
  }

  void TearDown() {
    TBEHandleStore::GetInstance().kernels_.clear();
    FftsPlusUpdateManager::Instance().creators_.clear();
  }
};

/***********************************************************************************************************************
 *                                      Data             Data
 * Data           Data                   |                 |
 *   \             /                     |                 |
 *    \           /                  TransData         TransData
 *     \         /                       \                 /                    Data      Data
 *      \       /                         \               /                       \        /
 *   PartitionedCall                       \             /                         \      /
 *          |                               \           /                           Conv2D
 *          |                              PartitionedCall                             |
 *          |                                     |                                    |
 *          |                                     |                                  Relu
 *      NetOutput                                 |                                    |
 *                                                |                                    |
 *                                            TransData                            NetOutput
 *                                                |
 *                                                |
 *                                            NetOutput
***********************************************************************************************************************/
static void BuildFftsDynamicGraph(ComputeGraphPtr &root_graph, ComputeGraphPtr &dsp_graph, ComputeGraphPtr &ffts_graph) {
  root_graph = MakeShared<ComputeGraph>("ffts_plus_root_graph");
  const auto input_data0 = CreateNode(*root_graph, "input_0", DATA, 1, 1);
  const auto input_data1 = CreateNode(*root_graph, "input_1", DATA, 1, 1);
  const auto root_call1 = CreateNode(*root_graph, "unknown_1", PARTITIONEDCALL, 2, 1);
  const auto root_output = CreateNode(*root_graph, "root_output", NETOUTPUT, 1, 0);
  AttrUtils::SetInt(root_graph, "globleworkspace_status", 1);
  AttrUtils::SetInt(root_graph, "globleworkspace_status_bytes", 1);
  ASSERT_EQ(GraphUtils::AddEdge(input_data0->GetOutDataAnchor(0), root_call1->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(input_data1->GetOutDataAnchor(0), root_call1->GetInDataAnchor(1)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(root_call1->GetOutDataAnchor(0), root_output->GetInDataAnchor(0)), GRAPH_SUCCESS);

  dsp_graph = MakeShared<ComputeGraph>("ffts_plus_dsp_graph");
  dsp_graph->SetGraphUnknownFlag(true);
  dsp_graph->SetParentNode(root_call1);
  dsp_graph->SetParentGraph(root_graph);
  root_call1->GetOpDesc()->AddSubgraphName("f");
  root_call1->GetOpDesc()->SetSubgraphInstanceName(0, dsp_graph->GetName());
  root_graph->AddSubgraph(dsp_graph);

  const auto unknown_data0 = CreateNode(*dsp_graph, "data0", DATA, 1, 1);
  const auto unknown_data1 = CreateNode(*dsp_graph, "data1", DATA, 1, 1);
  const auto unknown_trans0 = CreateNode(*dsp_graph, "TransData0", IDENTITY, 1, 1);
  const auto unknown_trans1 = CreateNode(*dsp_graph, "TransData1", IDENTITY, 1, 1);
  const auto unknown_call0 = CreateNode(*dsp_graph, "sgt_graph", PARTITIONEDCALL, 2, 1);
  const auto unknown_trans2 = CreateNode(*dsp_graph, "TransData2", IDENTITY, 1, 1);
  const auto unknown_output = CreateNode(*dsp_graph, "unknown_output", NETOUTPUT, 1, 0);
  ASSERT_EQ(GraphUtils::AddEdge(unknown_data0->GetOutDataAnchor(0), unknown_trans0->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(unknown_data1->GetOutDataAnchor(0), unknown_trans1->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(unknown_trans0->GetOutDataAnchor(0), unknown_call0->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(unknown_trans1->GetOutDataAnchor(0), unknown_call0->GetInDataAnchor(1)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(unknown_call0->GetOutDataAnchor(0), unknown_trans2->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(unknown_trans2->GetOutDataAnchor(0), unknown_output->GetInDataAnchor(0)), GRAPH_SUCCESS);

  AttrUtils::SetBool(unknown_call0->GetOpDesc(), ATTR_NAME_FFTS_PLUS_SUB_GRAPH, true);
  AttrUtils::SetInt(unknown_data0->GetOpDesc(), ATTR_NAME_PARENT_NODE_INDEX, 0);
  AttrUtils::SetInt(unknown_data1->GetOpDesc(), ATTR_NAME_PARENT_NODE_INDEX, 1);
  const auto &all_nodes = dsp_graph->GetDirectNode();
  std::for_each(all_nodes.begin(), all_nodes.end(), [](const NodePtr &n) {
    (void)AttrUtils::SetBool(n->GetOpDesc(), "OwnerGraphIsUnknown", true);
  });

  ffts_graph = MakeShared<ComputeGraph>("ffts_plus_graph");
  ffts_graph->SetGraphUnknownFlag(true);
  ffts_graph->SetParentNode(unknown_call0);
  ffts_graph->SetParentGraph(dsp_graph);
  unknown_call0->GetOpDesc()->AddSubgraphName("f");
  unknown_call0->GetOpDesc()->SetSubgraphInstanceName(0, ffts_graph->GetName());
  root_graph->AddSubgraph(ffts_graph);

  const auto ffts_data0 = CreateNode(*ffts_graph, "ffts_data0", DATA, 1, 1);
  const auto ffts_data1 = CreateNode(*ffts_graph, "ffts_data1", DATA, 1, 1);
  const auto ffts_conv0 = CreateNode(*ffts_graph, "ffts_conv0", CONV2D, 2, 1);
  const auto ffts_relu0 = CreateNode(*ffts_graph, "ffts_relu1", RELU, 1, 1);
  const auto ffts_gather = CreateNode(*ffts_graph, "ffts_gather", GATHERV2, 1, 1);
  const auto ffts_output = CreateNode(*ffts_graph, "ffts_output", NETOUTPUT, 1, 0);
  ASSERT_EQ(GraphUtils::AddEdge(ffts_data0->GetOutDataAnchor(0), ffts_conv0->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(ffts_data1->GetOutDataAnchor(0), ffts_conv0->GetInDataAnchor(1)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(ffts_conv0->GetOutDataAnchor(0), ffts_relu0->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(ffts_relu0->GetOutDataAnchor(0), ffts_gather->GetInDataAnchor(0)), GRAPH_SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(ffts_gather->GetOutDataAnchor(0), ffts_output->GetInDataAnchor(0)), GRAPH_SUCCESS);

  AttrUtils::SetInt(ffts_data0->GetOpDesc(), ATTR_NAME_PARENT_NODE_INDEX, 0);
  AttrUtils::SetInt(ffts_data1->GetOpDesc(), ATTR_NAME_PARENT_NODE_INDEX, 1);

  AttrUtils::SetStr(ffts_conv0->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "AIC");
  AttrUtils::SetInt(ffts_conv0->GetOpDesc(), ATTR_NAME_IMPLY_TYPE, static_cast<int64_t>(domi::ImplyType::TVM));
  AttrUtils::SetStr(ffts_conv0->GetOpDesc(), TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF");

  AttrUtils::SetStr(ffts_relu0->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "MIX_AIV");
  AttrUtils::SetInt(ffts_relu0->GetOpDesc(), ATTR_NAME_IMPLY_TYPE, static_cast<int64_t>(domi::ImplyType::TVM));
  AttrUtils::SetStr(ffts_relu0->GetOpDesc(), TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF");

  AttrUtils::SetStr(ffts_gather->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, "AICPU");
  AttrUtils::SetInt(ffts_gather->GetOpDesc(), ATTR_NAME_IMPLY_TYPE, static_cast<int64_t>(domi::ImplyType::AI_CPU));
  AttrUtils::SetStr(ffts_gather->GetOpDesc(), TVM_ATTR_NAME_MAGIC, "RT_DEV_BINARY_MAGIC_ELF");

  InitFftsThreadSliceMap(ffts_conv0->GetOpDesc());
  InitFftsThreadSliceMap(ffts_relu0->GetOpDesc());

  std::vector<char> conv_bin(64, '\0');
  TBEKernelPtr conv_kernel = MakeShared<ge::OpKernelBin>("sgt/conv", std::move(conv_bin));
  ffts_conv0->GetOpDesc()->SetExtAttr(OP_EXTATTR_NAME_TBE_KERNEL, conv_kernel);
  AttrUtils::SetStr(ffts_conv0->GetOpDesc(), ffts_conv0->GetName() + "_kernelname", "sgt/conv");

  std::vector<char> relu_bin(64, '\0');
  TBEKernelPtr relu_kernel = MakeShared<ge::OpKernelBin>("sgt/relu", std::move(relu_bin));
  ffts_relu0->GetOpDesc()->SetExtAttr(OP_EXTATTR_NAME_TBE_KERNEL, relu_kernel);
  AttrUtils::SetStr(ffts_relu0->GetOpDesc(), ffts_relu0->GetName() + "_kernelname", "sgt/relu");

  const auto &sgt_nodes = ffts_graph->GetDirectNode();
  std::for_each(sgt_nodes.begin(), sgt_nodes.end(), [](const NodePtr &n) {
    (void)AttrUtils::SetBool(n->GetOpDesc(), "OwnerGraphIsUnknown", true);
  });
}

TEST_F(UtestFftsPlusSubgraphExecutor, simple_ffts_plus_dynamic_graph) {
  ComputeGraphPtr root_graph;
  ComputeGraphPtr dsp_graph;
  ComputeGraphPtr ffts_graph;
  BuildFftsDynamicGraph(root_graph, dsp_graph, ffts_graph);

  // Build FftsPlusTaskDef.
  std::shared_ptr<domi::ModelTaskDef> model_def = MakeShared<domi::ModelTaskDef>();
  domi::TaskDef &task_def = *model_def->add_task();
  task_def.set_type(RT_MODEL_TASK_FFTS_PLUS_TASK);
  domi::FftsPlusTaskDef *ffts_plus_task_def = task_def.mutable_ffts_plus_task();
  ffts_plus_task_def->set_op_index(ffts_graph->GetParentNode()->GetOpDesc()->GetId());
  InitTaskSQEInfo(ffts_plus_task_def);
  domi::FftsPlusCtxDef *ctx_def = ffts_plus_task_def->add_ffts_plus_ctx();
  ctx_def->set_op_index(0);
  ctx_def->set_context_type(static_cast<uint32_t>(RT_CTX_TYPE_AIV));
  domi::FftsPlusAicAivCtxDef *aic_aiv_def = ctx_def->mutable_aic_aiv_ctx();
  InitAicAivCtx(aic_aiv_def);
  aic_aiv_def->clear_kernel_name();

  // Build GeModel.
  GeRootModelPtr ge_root_model = MakeShared<GeRootModel>(root_graph);
  ge_root_model->SetModelName(root_graph->GetName());
  GeModelPtr ge_sub_model = MakeShared<GeModel>();
  ge_sub_model->SetModelTaskDef(model_def);

  ge_sub_model->SetGraph(GraphUtils::CreateGraphFromComputeGraph(dsp_graph));
  ge_root_model->SetSubgraphInstanceNameToModel(dsp_graph->GetName(), ge_sub_model);

  // Test for Load.
  auto hybrid_model = hybrid::HybridDavinciModel::Create(ge_root_model);
  ASSERT_NE(hybrid_model, nullptr);
  ASSERT_EQ(hybrid_model->Init(), SUCCESS);


  // Test for Execute.
  uint64_t value_0 = 110;
  DataBuffer in_tensor0(&value_0, sizeof(value_0), false, 0);
  uint64_t value_1 = 110;
  DataBuffer in_tensor1(&value_1, sizeof(value_1), false, 0);
  const std::vector<DataBuffer> inputs{ in_tensor0, in_tensor1 };

  uint64_t value_2 = 123;
  DataBuffer out_tensor0(&value_2, sizeof(value_2), false, 0);
  std::vector<DataBuffer> outputs{ out_tensor0 };

  GeTensorDesc input_desc(GeShape(), FORMAT_ND, DT_INT64);
  TensorUtils::SetSize(input_desc, 64);
  const std::vector<GeTensorDesc> input_descs{ input_desc, input_desc };
  std::vector<GeTensorDesc> output_descs;

  ASSERT_EQ(hybrid_model->Execute(inputs, input_descs, outputs, output_descs, nullptr), SUCCESS);
}
} // namespace ge