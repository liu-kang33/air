/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <vector>

#define private public
#define protected public
#include "hybrid/executor/subgraph_context.h"
#include "common/model/ge_root_model.h"
#include "ut/ge/ffts_plus_proto_tools.h"
#include "ge_graph_dsl/graph_dsl.h"
#include "graph/utils/graph_utils.h"
#include "hybrid/executor/hybrid_resource_manager.h"
#include "hybrid/node_executor/compiledsubgraph/known_node_executor.h"
#include "graph/load/model_manager/task_info/kernel_task_info.h"

using namespace std;
using namespace testing;

namespace ge {
using namespace hybrid;

class UtestKnownNodeExecutor : public testing::Test {
 protected:
  void SetUp() {}
  void TearDown() { }
};

TEST_F(UtestKnownNodeExecutor, test_KnownNodeTask) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("test");
  GeModelPtr ge_sub_model = std::make_shared<GeModel>();
  ge_sub_model->graph_ = GraphUtils::CreateGraphFromComputeGraph(graph);

  ComputeGraphPtr root_graph = std::make_shared<ComputeGraph>("root");

  GeRootModelPtr ge_root_model = std::make_shared<GeRootModel>(root_graph);
  ge_root_model->SetModelName("test_name");
  ge_root_model->SetSubgraphInstanceNameToModel("sub", ge_sub_model);
  HybridModel hybrid_model(ge_root_model);
  uint8_t *test_buffer = new uint8_t[16];
  hybrid_model.global_step_ = TensorBuffer::Create(test_buffer, 16);

  NodePtr node = CreateNode(*graph, "mul", MATMUL, 2, 2);
  NodePtr data_node = CreateNode(*graph, "data", DATA, 1, 1);
  NodePtr const_node = CreateNode(*graph, "const", CONSTANT, 1, 1);
  NodePtr out_node = CreateNode(*graph, "netoutput", NETOUTPUT, 2, 1);

  out_node->GetOpDesc()->SetSrcName(std::vector<std::string>({"out", "out2"}));
  out_node->GetOpDesc()->SetSrcIndex(std::vector<int64_t>({0, 1}));

  ASSERT_EQ(GraphUtils::AddEdge(const_node->GetOutDataAnchor(0), node->GetInDataAnchor(0)), SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), node->GetInDataAnchor(1)), SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(node->GetOutDataAnchor(0), out_node->GetInDataAnchor(0)), SUCCESS);
  ASSERT_EQ(GraphUtils::AddEdge(data_node->GetOutDataAnchor(0), out_node->GetInDataAnchor(1)), SUCCESS);

  NodePtr root_node = CreateNode(*root_graph, "root_node", DATA, 1, 1);
  root_node->GetOpDesc()->AddSubgraphName("sub");
  NodeUtils::SetSubgraph(*root_node, 0, graph);
  graph->SetParentNode(root_node);

  graph->SetParentGraph(root_graph);
  root_graph->AddSubgraph("test", graph);

  std::unique_ptr<NodeItem> new_node;
  ASSERT_EQ(NodeItem::Create(node, new_node), SUCCESS);
  NodeItem *node_item = new_node.get();
  new_node->input_start = 2;
  new_node->output_start = 2;
  new_node->num_inputs = 2;
  new_node->num_outputs = 2;
  hybrid_model.node_items_[node] = std::move(new_node);

  std::unique_ptr<NodeItem> new_data_node;
  ASSERT_EQ(NodeItem::Create(data_node, new_data_node), SUCCESS);
  NodeItem *data_node_item = new_data_node.get();
  data_node_item->input_start = 1;
  data_node_item->output_start = 1;
  hybrid_model.node_items_[data_node] = std::move(new_data_node);

  std::unique_ptr<NodeItem> new_const_node;
  ASSERT_EQ(NodeItem::Create(const_node, new_const_node), SUCCESS);
  NodeItem *const_node_item = new_const_node.get();
  const_node_item->input_start = 1;
  const_node_item->output_start = 1;
  hybrid_model.node_items_[const_node] = std::move(new_const_node);

  std::unique_ptr<NodeItem> new_out_node;
  ASSERT_EQ(NodeItem::Create(out_node, new_out_node), SUCCESS);
  NodeItem *out_node_item = new_out_node.get();
  ASSERT_NE(out_node_item, nullptr);

  out_node_item->input_start = 2;
  out_node_item->output_start = 1;

  hybrid_model.node_items_[out_node] = std::move(new_out_node);

  GraphItem graph_item;
  graph_item.node_items_.emplace_back(node_item);
  graph_item.node_items_.emplace_back(data_node_item);
  graph_item.node_items_.emplace_back(const_node_item);
  graph_item.node_items_.emplace_back(out_node_item);
  graph_item.total_inputs_ = 1;
  graph_item.total_outputs_ = 2;

  hybrid_model.known_shape_sub_models_[node] = ge_sub_model;
  hybrid_model.root_graph_ = root_graph;

  GraphExecutionContext graph_context;
  SubgraphContext subgraph_context(&graph_item, &graph_context);
  ASSERT_EQ(subgraph_context.Init(), SUCCESS);
  graph_context.callback_manager = new (std::nothrow) CallbackManager();
  graph_context.own_callback_manager = true;

  auto node_state = subgraph_context.GetNodeState(node_item);
  ASSERT_NE(node_state, nullptr);

  NodeTaskPtr task = nullptr;
  KnownNodeExecutor node_executor;
  ASSERT_EQ(node_executor.LoadTask(hybrid_model, node, task), SUCCESS);
  ASSERT_NE(task, nullptr);

  ASSERT_NE(node_executor.PrepareTask(*task, *node_state->GetTaskContext()), SUCCESS);
  ASSERT_EQ(task->UpdateArgs(*node_state->GetTaskContext()), SUCCESS);
  std::function<void()> done = []() {};

  void *test1 = new uint8_t[1];
  void *test2 = new uint8_t[1];
  node_state->GetTaskContext()->outputs_start_ = new TensorValue[2];
  node_state->GetTaskContext()->outputs_start_->ref_buffer_ = test1;
  node_state->GetTaskContext()->outputs_start_->ref_size_ = 1;

  (node_state->GetTaskContext()->outputs_start_ + 1)->ref_buffer_ = test2;
  (node_state->GetTaskContext()->outputs_start_ + 1)->ref_size_ = 1;

  void *test3 = new uint8_t[1];
  void *test4 = new uint8_t[1];
  node_state->GetTaskContext()->inputs_start_ = new TensorValue[2];
  node_state->GetTaskContext()->inputs_start_->ref_buffer_ = test3;
  node_state->GetTaskContext()->inputs_start_->ref_size_ = 1;

  (node_state->GetTaskContext()->inputs_start_ + 1)->ref_buffer_ = test4;
  (node_state->GetTaskContext()->inputs_start_ + 1)->ref_size_ = 1;


  ASSERT_EQ(node_executor.ExecuteTask(*task, *node_state->GetTaskContext(), done), SUCCESS);

  dynamic_cast<KnownNodeTask *>(task.get())->davinci_model_->task_list_.push_back(std::make_shared<KernelTaskInfo>());
  ASSERT_EQ(node_executor.ExecuteTask(*task, *node_state->GetTaskContext(), done), SUCCESS);

  ASSERT_NE(task->UpdateArgs(*node_state->GetTaskContext()), SUCCESS);  //??

  node_state->GetTaskContext()->outputs_start_->ref_buffer_ = nullptr;
  (node_state->GetTaskContext()->outputs_start_ + 1)->ref_buffer_ = nullptr;

  ge::AttrUtils::SetInt(node->GetOpDesc()->MutableOutputDesc(0), "_memory_size_calc_type", 1);
  ge::AttrUtils::SetInt(node->GetOpDesc()->MutableOutputDesc(0), "_memory_size_calc_type", 1);

  // dynamic_cast<KnownNodeTask *>(task.get())->davinci_model_->runtime_param_.mem_size = 10;
  // void *base = new uint8_t[50];
  // dynamic_cast<KnownNodeTask *>(task.get())->davinci_model_->runtime_param_.mem_base = PtrToValue(base);

  // ASSERT_EQ(dynamic_cast<KnownNodeTask *>(task.get())->Init(*node_state->GetTaskContext()), SUCCESS);
}

}