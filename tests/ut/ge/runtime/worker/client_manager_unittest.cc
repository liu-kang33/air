/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <vector>
#include <string>
#include <map>
#include "framework/common/debug/ge_log.h"
#define protected public
#define private public
#include "deploy/daemon/client_manager.h"
#undef protected public
#undef private public

using namespace std;
namespace ge {
// client manager
class UtClientManager : public testing::Test {
 public:
  UtClientManager() = default;
  ~UtClientManager() = default;
 protected:
  void SetUp() override {}
  void TearDown() override {}
};

TEST_F(UtClientManager, run_initialize) {
  ge::ClientManager clientManager;
  clientManager.Initialize();
}

TEST_F(UtClientManager, run_finalize) {
  ge::ClientManager clientManager;
  clientManager.Finalize();
}

TEST_F(UtClientManager, run_create_client) {
  ge::ClientManager clientManager;
  int64_t client_id = 0;
  auto ret = clientManager.CreateClient(client_id);
  ASSERT_NE(ret, ge::FAILED);
}

TEST_F(UtClientManager, run_close_client) {
  ge::ClientManager clientManager;
  int64_t client_id = 0;
  clientManager.CloseClient(client_id);
}

TEST_F(UtClientManager, run_get_client) {
  ge::ClientManager clientManager;
  int64_t client_id = 0;
  clientManager.GetClient(client_id);
}

// client
class UtClient : public testing::Test {
 protected:
  void SetUp() override {}
  void TearDown() override {}
};

TEST_F(UtClient, run_initialize) {
  ge::Client client;
  client.Initialize();
}

TEST_F(UtClient, run_isExpired) {
  ge::Client client;
  client.IsExpired();
}

TEST_F(UtClient, run_setIsExecuting) {
  ge::Client client;
  client.SetIsExecuting(true);
}

TEST_F(UtClient, run_IsExecuting) {
  ge::Client client;
  client.IsExecuting();
}

TEST_F(UtClient, run_OnHeartbeat) {
  ge::Client client;
  client.OnHeartbeat();
}

TEST_F(UtClient, run_finalize) {
  ge::Client client;
  client.Finalize();
}

TEST_F(UtClient, run_is_model_complete) {
  ge::Client client;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  auto res = client.LoadModel(request, response);
  ASSERT_EQ(res, ge::FAILED);
}

TEST_F(UtClient, run_download_Model) {
  ge::Client client;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  auto res = client.DownloadModel(request, response);
  ASSERT_NE(res, ge::SUCCESS);
}

TEST_F(UtClient, run_pre_deploy_model) {
  deployer::DeployerRequest request;
  request.set_type(deployer::kPreDeployModel);
  auto pre_deploy_req = request.mutable_pre_deploy_model_request();
  pre_deploy_req->set_root_model_id(1);
  auto exchange_plan = pre_deploy_req->mutable_exchange_plan();
  deployer::QueueDesc queue_desc;
  queue_desc.set_name("data-1");
  queue_desc.set_depth(2);
  queue_desc.set_type(2);  // tag
  *exchange_plan->add_queues() = queue_desc;
  queue_desc.set_type(1);  // queue
  *exchange_plan->add_queues() = queue_desc;

  queue_desc.set_name("output-1");
  queue_desc.set_type(1);  // queue
  *exchange_plan->add_queues() = queue_desc;
  queue_desc.set_type(2);  // tag
  *exchange_plan->add_queues() = queue_desc;
  auto input_binding = exchange_plan->add_bindings();
  input_binding->set_src_queue_index(0);
  input_binding->set_dst_queue_index(1);
  auto output_binding = exchange_plan->add_bindings();
  output_binding->set_src_queue_index(2);
  output_binding->set_dst_queue_index(3);

  auto submodel_desc = pre_deploy_req->add_submodels();
  submodel_desc->set_model_size(128);
  submodel_desc->set_model_name("any-model");
  submodel_desc->add_input_queue_indices(1);
  submodel_desc->add_output_queue_indices(2);

  ge::Client client;
  deployer::DeployerResponse response;
  client.PreDeployModel(*pre_deploy_req, response);
  ASSERT_EQ(response.error_code(), ge::SUCCESS);
}

TEST_F(UtClient, run_unload_model) {
  ge::Client client;
  deployer::DeployerRequest request;
  request.set_type(deployer::kUnloadModel);
  deployer::DeployerResponse response;
  client.UnloadModel(request, response);
}

TEST_F(UtClient, ProcessVarManagerAndSharedInfo) {
  ge::Client client;
  deployer::DeployerResponse response;
  deployer::MultiVarManagerRequest info;
  client.ProcessMultiVarManager(info, response);
  deployer::SharedContentDescRequest shared_info;
  client.ProcessSharedContent(shared_info, response);
}

TEST_F(UtClient, GetQueuesFail) {
  ge::Client client;
  deployer::SubmodelDesc subDesc;
  client.submodel_descs_[{0, 1, 1}] = subDesc;
  std::vector<uint32_t> input_queues;
  std::vector<uint32_t> output_queues;
  auto ret = client.GetQueues({0, 1, 2}, input_queues, output_queues);
  ASSERT_EQ(ret, ge::FAILED);
}

TEST_F(UtClient, GetQueuesSuccess) {
  ge::Client client;
  deployer::SubmodelDesc sub_desc;
  client.submodel_descs_[{0, 1, 1}] = sub_desc;
  client.exchange_routes_[{0, 1, 0}] = ExchangeRoute{};
  std::vector<uint32_t> input_queues;
  std::vector<uint32_t> output_queues;
  auto ret = client.GetQueues({0, 1, 1}, input_queues, output_queues);
  ASSERT_EQ(ret, ge::SUCCESS);
}

}

