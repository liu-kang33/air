/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <vector>
#include <string>
#include "deploy/daemon/deployer_service_impl.h"
#define protected public
#define private public

using namespace std;
namespace ge {
class UtDeployerService : public testing::Test {
 protected:
  void SetUp() override {}
  void TearDown() override {}
};

TEST_F(UtDeployerService, run_register_req_processor) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  RequestContext context;
  deployerService.Process(context, request, response);
}

TEST_F(UtDeployerService, run_generate_client_id) {
  ge::DeployerServiceImpl deployerService;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
}

TEST_F(UtDeployerService, run_generate_client_id_1) {
  ge::DeployerServiceImpl deployerService;
  setenv("HELPER_RES_FILE_PATH","", 1);
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
}

TEST_F(UtDeployerService, run_generate_client_id_wrong_token) {
  ge::DeployerServiceImpl deployerService;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
}

TEST_F(UtDeployerService, run_generate_client_id_wrong_key) {
  ge::DeployerServiceImpl deployerService;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/wrong_key/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
}

TEST_F(UtDeployerService, run_generat_client_id_error_path) {
  ge::DeployerServiceImpl deployerService;
  unsetenv("HELPER_RES_FILE_PATH");
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
}


TEST_F(UtDeployerService, run_client_heartbeat_process) {
  ge::DeployerServiceImpl deployerService;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  request.set_client_id(0);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
  deployerService.ClientHeartbeatProcess(context, request, response);
}

TEST_F(UtDeployerService, run_disconnect_client) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.DisconnectClient(context, request, response);
}

TEST_F(UtDeployerService, run_disconnect_client_1) {
  ge::DeployerServiceImpl deployerService;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
  request.set_client_id(0);
  deployerService.DisconnectClient(context, request, response);
}

TEST_F(UtDeployerService, run_down_model) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  RequestContext context;
  deployerService.DownloadModelProcess(context, request, response);
}

TEST_F(UtDeployerService, load_model) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  request.set_type(deployer::kLoadModel);
  RequestContext context;
  deployerService.LoadModelProcess(context, request, response);
}

TEST_F(UtDeployerService, load_model_1) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  request.set_client_id(0);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
  deployerService.LoadModelProcess(context, request, response);
}

TEST_F(UtDeployerService, unload_model) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  RequestContext context;
  request.set_type(deployer::kUnloadModel);
  deployerService.UnloadModelProcess(context, request, response);
}

TEST_F(UtDeployerService, run_pre_down_model) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  RequestContext context;
  deployerService.PreDownloadModelProcess(context, request, response);
}

TEST_F(UtDeployerService, run_pre_down_model_1) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
  int64_t id = response.init_response().client_id();
  request.set_client_id(id);
  deployerService.PreDownloadModelProcess(context, request, response);
}

TEST_F(UtDeployerService, run_deployer_service_process) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  request.set_type(deployer::kPreDownloadModel);
  RequestContext context;
  deployerService.Process(context, request, response);
}

TEST_F(UtDeployerService, run_deployer_service_process_1) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  RequestContext context;
  request.set_type(deployer::kInitRequest);
  deployerService.Process(context, request, response);
  request.set_type(deployer::kLoadModel);
  int64_t id = response.init_response().client_id();
  request.set_client_id(id);
  deployerService.Process(context, request, response);
}

TEST_F(UtDeployerService, MultiVarManagerInfoProcess) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  request.set_client_id(0);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
  request.set_type(deployer::kDownloadVarManager);
  deployerService.MultiVarManagerInfoProcess(context, request, response);
}

TEST_F(UtDeployerService, SharedContentProcess) {
  ge::DeployerServiceImpl deployerService;
  deployer::DeployerRequest request;
  deployer::DeployerResponse response;
  char path[200];
  realpath("../tests/ut/ge/runtime/data/valid/device", path);
  Configurations::GetInstance().InitDeviceInformation(path);
  deployer::InitRequest init_request_;
  std::string token = "xxxxxxx";
  init_request_.set_token(token);
  request.set_client_id(0);
  *request.mutable_init_request() = init_request_;
  RequestContext context;
  context.peer_url = "xxx:127.0.0.1:8080";
  deployerService.GenerateClientId(context, request, response);
  request.set_type(deployer::kDownloadSharedContent);
  deployerService.SharedContentProcess(context, request, response);
}
}