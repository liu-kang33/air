/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <thread>

#define protected public
#define private public
#include "deploy/executor/engine_daemon.h"
#undef private
#undef protected

#include "graph/build/graph_builder.h"
#include "runtime/deploy/stub_models.h"
#include "ge_graph_dsl/graph_dsl.h"
#include "generator/ge_generator.h"
#include "ge/ge_api.h"

using namespace std;

namespace ge {
class EngineDaemonTest : public testing::Test {
 protected:
  void SetUp() override {
  }
  void TearDown() override {
  }

};

TEST_F(EngineDaemonTest, TestEngineDaemon) {
  EngineDaemon engine_daemon;
  auto device_id = std::to_string(0);
  auto event_group_id = std::to_string(1);
  const std::string process_name = "npu_executor";
  const char_t *argv[] = {
      process_name.c_str(),
      "BufferGroupName",
      "MsgQueueName",
      device_id.c_str(),
      event_group_id.c_str(),
  };
  EXPECT_EQ(engine_daemon.InitializeWithArgs(5, (char **)argv), SUCCESS);
}
}  // namespace ge


