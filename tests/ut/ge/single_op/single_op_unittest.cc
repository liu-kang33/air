/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <vector>

#include "runtime/rt.h"

#define protected public
#define private public
#include "hybrid/model/graph_item.h"
#include "hybrid/model/node_item.h"
#include "single_op/single_op.h"
#include "single_op/single_op_manager.h"
#include "single_op/task/build_task_utils.h"
#include "common/dump/dump_manager.h"
#undef private
#undef protected

using namespace std;
using namespace ge;
using namespace hybrid;

class UtestSingleOp : public testing::Test {
 protected:
  void SetUp() {}
  void TearDown() {}
  ObjectPool<GeTensor> tensor_pool_;
};

TEST_F(UtestSingleOp, test_dynamic_singleop_execute_async) {
  uintptr_t resource_id = 0;
  std::mutex stream_mu;
  rtStream_t stream = nullptr;
  rtStreamCreate(&stream, 0);
  DynamicSingleOp dynamic_single_op(&tensor_pool_, resource_id, &stream_mu, stream);

  vector<int64_t> dims_vec_0 = {2};
  vector<GeTensorDesc> input_desc;
  GeTensorDesc tensor_desc_0(GeShape(dims_vec_0), FORMAT_NCHW, DT_INT32);
  // input data from device
  AttrUtils::SetInt(tensor_desc_0, ATTR_NAME_PLACEMENT, 0);
  input_desc.emplace_back(tensor_desc_0);

  vector<DataBuffer> input_buffers;
  ge::DataBuffer data_buffer;
  data_buffer.data = new char[4];
  data_buffer.length = 4;
  input_buffers.emplace_back(data_buffer);

  vector<GeTensorDesc> output_desc;
  vector<DataBuffer> output_buffers;

  // UpdateRunInfo failed
  EXPECT_EQ(dynamic_single_op.ExecuteAsync(input_desc, input_buffers, output_desc, output_buffers), ACL_ERROR_GE_PARAM_INVALID);
}

TEST_F(UtestSingleOp, test_dynamic_singleop_execute_async1) {
  uintptr_t resource_id = 0;
  std::mutex stream_mu;
  rtStream_t stream = nullptr;
  rtStreamCreate(&stream, 0);
  DynamicSingleOp dynamic_single_op(&tensor_pool_, resource_id, &stream_mu, stream);
  dynamic_single_op.num_inputs_ = 1;

  vector<int64_t> dims_vec_0 = {2};
  vector<GeTensorDesc> input_desc;
  GeTensorDesc tensor_desc_0(GeShape(dims_vec_0), FORMAT_NCHW, DT_INT32);
  // input data from host
  AttrUtils::SetInt(tensor_desc_0, ATTR_NAME_PLACEMENT, 1);
  input_desc.emplace_back(tensor_desc_0);

  int64_t input_size = 0;
  EXPECT_EQ(TensorUtils::GetTensorMemorySizeInBytes(tensor_desc_0, input_size), SUCCESS);
  EXPECT_EQ(input_size, 64);
  EXPECT_NE(SingleOpManager::GetInstance().GetResource(resource_id, stream), nullptr);

  vector<DataBuffer> input_buffers;
  ge::DataBuffer data_buffer;
  data_buffer.data = new char[4];
  data_buffer.length = 4;
  input_buffers.emplace_back(data_buffer);

  vector<GeTensorDesc> output_desc;
  vector<DataBuffer> output_buffers;

  auto *tbe_task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  tbe_task->node_ = node;

  dynamic_single_op.op_task_.reset((OpTask *)(tbe_task));

  OpDescPtr desc_ptr = MakeShared<OpDesc>("name1", "type1");
  EXPECT_EQ(desc_ptr->AddInputDesc("x", GeTensorDesc(GeShape({2}), FORMAT_NCHW)), GRAPH_SUCCESS);
  dynamic_single_op.op_task_->op_desc_ = desc_ptr;
  // UpdateRunInfo failed
  EXPECT_EQ(dynamic_single_op.ExecuteAsync(input_desc, input_buffers, output_desc, output_buffers), ACL_ERROR_GE_PARAM_INVALID);
}

TEST_F(UtestSingleOp, test_stream_resource) {
  StreamResource *res = new (std::nothrow) StreamResource(1);
  uint8_t *ttt = new uint8_t[10];
  res->weight_list_.push_back(ttt);

  res->stream_ = ValueToPtr(11);
  EXPECT_EQ(res->GetStream(), res->stream_);

  uint8_t *allocated = new uint8_t[10];
  size_t max_allocated = 555;
  std::vector<uint8_t *> allocated_vec = { allocated };
  EXPECT_NE(res->DoMallocMemory("test_purpose", 123, max_allocated, allocated_vec), nullptr);
  max_allocated = 0;
  allocated_vec.clear();
  EXPECT_EQ(res->DoMallocMemory("test_purpose", 123, max_allocated, allocated_vec), nullptr);

  allocated = new uint8_t[10];
  EXPECT_EQ(res->DoMallocMemory("test_purpose", 321, max_allocated, allocated_vec), nullptr);

  EXPECT_NE(res->MallocWeight("test_purpose", 10), nullptr);
  EXPECT_EQ(res->MallocWeight("test_purpose", 123), nullptr);

  string model_data_str = "123456789";
  ModelData model_data;
  model_data.model_data = (void *)(const_cast<char *>(model_data_str.c_str()));
  model_data.model_len = model_data_str.size();
  DynamicSingleOp *single_op;

  ObjectPool<GeTensor> *tensor_pool = nullptr;
  uintptr_t resource_id = 2;
  std::mutex *stream_mutex;
  rtStream_t stream;
  res->dynamic_op_map_[10] = std::unique_ptr<DynamicSingleOp>(new DynamicSingleOp(tensor_pool, resource_id, stream_mutex, stream));

  EXPECT_EQ(res->BuildDynamicOperator(model_data, &single_op, 10), SUCCESS);
  EXPECT_NE(res->BuildDynamicOperator(model_data, &single_op, 9), SUCCESS);
}

TEST_F(UtestSingleOp, test_singleop_execute_async1) {
  StreamResource *res = new (std::nothrow) StreamResource(1);
  std::mutex stream_mu;
  rtStream_t stream = nullptr;
  rtStreamCreate(&stream, 0);
  SingleOp single_op(res, &stream_mu, stream);

  vector<DataBuffer> input_buffers;
  ge::DataBuffer data_buffer;
  data_buffer.data = new char[4];
  data_buffer.length = 4;
  data_buffer.placement = 1;
  input_buffers.emplace_back(data_buffer);
  vector<DataBuffer> output_buffers;

  single_op.input_sizes_.emplace_back(4);
  SingleOpModelParam model_params;
  single_op.running_param_.reset(new (std::nothrow)SingleOpModelParam(model_params));
  single_op.args_.resize(1);

  auto tbe_task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  EXPECT_EQ(op_desc->AddInputDesc("x", GeTensorDesc(GeShape({2}), FORMAT_NCHW)), GRAPH_SUCCESS);
  EXPECT_EQ(op_desc->AddOutputDesc("x", GeTensorDesc(GeShape({2}), FORMAT_NCHW)), GRAPH_SUCCESS);
  EXPECT_NE(BuildTaskUtils::GetTaskInfo(op_desc), "");
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  tbe_task->node_ = node;
  tbe_task->op_desc_ = op_desc;
  std::unique_ptr<OpTask> op_task;
  op_task.reset(tbe_task);
  single_op.tasks_.push_back(std::move(op_task));
  EXPECT_EQ(single_op.hybrid_model_executor_, nullptr);
  EXPECT_EQ(single_op.running_param_->mem_base, nullptr);
  EXPECT_EQ(single_op.ExecuteAsync(input_buffers, output_buffers), SUCCESS);
}

namespace {
GraphItem *MakeGraphItem() {
  const auto graph = make_shared<ComputeGraph>("graph");
  OpDescPtr op_desc = MakeShared<OpDesc>("add", "add");
  GeShape shape({2, 16});
  GeTensorDesc tensor_desc(shape);
  op_desc->AddInputDesc(tensor_desc);
  op_desc->AddInputDesc(tensor_desc);
  op_desc->AddOutputDesc(tensor_desc);
  const auto node = graph->AddNode(op_desc);

  std::unique_ptr<NodeItem> node_item;
  NodeItem::Create(node, node_item);
  node_item->input_start = 0;
  node_item->output_start = 0;

  GraphItem *graph_item = new GraphItem;
  graph_item->node_items_.emplace_back(node_item.get());
  graph_item->total_inputs_ = node->GetAllInAnchors().size();
  graph_item->total_outputs_ = node->GetAllOutAnchors().size();
  node_item.release();
  return graph_item;
}
}  // namespace

TEST_F(UtestSingleOp, test_singleop_execute_async2) {
  StreamResource *res = new (std::nothrow) StreamResource(1);
  std::mutex stream_mu;
  rtStream_t stream = nullptr;
  rtStreamCreate(&stream, 0);
  SingleOp single_op(res, &stream_mu, stream);

  vector<DataBuffer> input_buffers;
  ge::DataBuffer data_buffer;
  data_buffer.data = new char[4];
  data_buffer.length = 4;
  data_buffer.placement = 1;
  input_buffers.emplace_back(data_buffer);
  vector<DataBuffer> output_buffers;

  single_op.input_sizes_.emplace_back(4);
  SingleOpModelParam model_params;
  single_op.running_param_.reset(new (std::nothrow)SingleOpModelParam(model_params));
  single_op.args_.resize(1);

  GeTensorDesc tensor_desc(GeShape({1}), FORMAT_NHWC, DT_UINT64);
  single_op.inputs_desc_.emplace_back(tensor_desc);
  std::shared_ptr<ge::GeRootModel> root_model = ge::MakeShared<ge::GeRootModel>();
  single_op.hybrid_model_.reset(new (std::nothrow)hybrid::HybridModel(root_model));
  single_op.hybrid_model_->root_graph_item_.reset(MakeGraphItem());
  single_op.hybrid_model_executor_.reset(new (std::nothrow)hybrid::HybridModelExecutor(single_op.hybrid_model_.get(), 0, stream));
  EXPECT_EQ(single_op.hybrid_model_executor_->Init(), SUCCESS);
  EXPECT_EQ(single_op.running_param_->mem_base, nullptr);
  EXPECT_EQ(single_op.tasks_.size(), 0);

  GeTensorDesc tensor;
  int64_t storage_format_val = static_cast<Format>(FORMAT_NCHW);
  AttrUtils::SetInt(tensor, "storage_format", storage_format_val);
  std::vector<int64_t> storage_shape{1, 1, 1, 1};
  AttrUtils::SetListInt(tensor, "storage_shape", storage_shape);
  single_op.inputs_desc_.emplace_back(tensor);
  single_op.hybrid_model_->root_graph_item_.release();
  EXPECT_EQ(single_op.ExecuteAsync(input_buffers, output_buffers), PARAM_INVALID);
}

TEST_F(UtestSingleOp, test_set_host_mem) {
  std::mutex stream_mu_;
  DynamicSingleOp single_op(&tensor_pool_, 0, &stream_mu_, nullptr);
  rtStream_t stream = nullptr;
  rtStreamCreate(&stream, 0);
  std::shared_ptr<ge::GeRootModel> root_model = ge::MakeShared<ge::GeRootModel>();
  single_op.hybrid_model_.reset(new (std::nothrow)hybrid::HybridModel(root_model));
  single_op.hybrid_model_->root_graph_item_.reset(MakeGraphItem());
  single_op.hybrid_model_executor_.
      reset(new (std::nothrow)hybrid::HybridModelExecutor(single_op.hybrid_model_.get(), 0, stream));
  EXPECT_EQ(single_op.hybrid_model_executor_->Init(), SUCCESS);

  vector<DataBuffer> input_buffers;
  DataBuffer data_buffer;
  input_buffers.emplace_back(data_buffer);

  vector<GeTensorDesc> input_descs;
  GeTensorDesc tensor_desc1;
  input_descs.emplace_back(tensor_desc1);

  single_op.hostmem_node_id_map_.emplace(0, 0);
  EXPECT_EQ(single_op.SetHostTensorValue(input_descs, input_buffers), SUCCESS);
}

TEST_F(UtestSingleOp, test_dynamic_singleop_execute_async_with_host_input) {
  uintptr_t resource_id = 0;
  std::mutex stream_mu;
  rtStream_t stream = nullptr;
  rtStreamCreate(&stream, 0);
  DynamicSingleOp dynamic_single_op(&tensor_pool_, resource_id, &stream_mu, stream);
  dynamic_single_op.num_inputs_ = 1;

  vector<int64_t> dims_vec_0 = {2};
  vector<GeTensorDesc> input_desc;
  GeTensorDesc tensor_desc_0(GeShape(dims_vec_0), FORMAT_NCHW, DT_INT32);
  // input data from host
  AttrUtils::SetInt(tensor_desc_0, ATTR_NAME_PLACEMENT, 1);
  input_desc.emplace_back(tensor_desc_0);

  vector<DataBuffer> input_buffers;
  ge::DataBuffer data_buffer;
  data_buffer.data = new char[64];
  data_buffer.length = 64;
  data_buffer.placement = 1;

  input_buffers.emplace_back(data_buffer);

  vector<GeTensorDesc> output_desc;
  vector<DataBuffer> output_buffers;

  auto *tbe_task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  tbe_task->node_ = node;
  tbe_task->need_tiling_ = true;
  tbe_task->need_host_mem_opt_ = false;
  //tbe_task->arg_index_ = {0};

  dynamic_single_op.hybrid_model_ = nullptr;
  dynamic_single_op.op_task_.reset((OpTask *)(tbe_task));

  OpDescPtr desc_ptr = MakeShared<OpDesc>("name1", "type1");
  EXPECT_EQ(desc_ptr->AddInputDesc("x", GeTensorDesc(GeShape({2}), FORMAT_NCHW)), GRAPH_SUCCESS);
  dynamic_single_op.op_task_->op_desc_ = desc_ptr;
  dynamic_single_op.ExecuteAsync(input_desc, input_buffers, output_desc, output_buffers);
  EXPECT_EQ(tbe_task->need_host_mem_opt_, true);
  ge::DataBuffer data_buffer_long;

  delete [] static_cast<char *>(data_buffer.data);
  data_buffer.data = new char[65];
  data_buffer.length = 65;
  data_buffer.placement = 1;
  input_buffers[0] = data_buffer;
  tbe_task->need_host_mem_opt_ = false;
  dynamic_single_op.ExecuteAsync(input_desc, input_buffers, output_desc, output_buffers);
  EXPECT_EQ(tbe_task->need_host_mem_opt_, false);
}

TEST_F(UtestSingleOp, test_OpenDump) {
  auto *tbe_task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  tbe_task->node_ = node;
  tbe_task->op_desc_ = op_desc;

  DumpManager::GetInstance().dump_properties_map_[kInferSessionId].SetDumpOpSwitch("on");

  EXPECT_NE(tbe_task->OpenDump(0), SUCCESS);

  tbe_task->args_ = std::unique_ptr<uint8_t[]>(new uint8_t[24]());
  tbe_task->arg_size_ = 24;
  tbe_task->max_tiling_size_ = 0;
  tbe_task->need_tiling_ = false;

  EXPECT_EQ(tbe_task->OpenDump(0), SUCCESS);
  delete tbe_task;
}

TEST_F(UtestSingleOp, test_SetStubFunc) {
  auto *tbe_task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  tbe_task->node_ = node;
  tbe_task->op_desc_ = op_desc;

  tbe_task->SetStubFunc("test", nullptr);

  EXPECT_EQ(tbe_task->stub_name_, "test");
  EXPECT_EQ(tbe_task->stub_func_, nullptr);
  EXPECT_EQ(tbe_task->task_name_, "test");
  delete tbe_task;
}

TEST_F(UtestSingleOp, test_OpTask_related) {
  auto *task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);

  task->node_ = node;
  task->op_desc_ = op_desc;

  EXPECT_EQ(task->OpTask::UpdateRunInfo(), UNSUPPORTED);
  std::vector<GeTensorDesc> output_desc;
  std::vector<DataBuffer> output_buffers;
  rtStream_t stream;
  EXPECT_EQ(task->OpTask::LaunchKernel(
      std::vector<GeTensorDesc>(), std::vector<DataBuffer>(), output_desc, output_buffers, stream), UNSUPPORTED);
  EXPECT_EQ(task->OpTask::GetTaskType(), kTaskTypeInvalid);
  delete task;
}

TEST_F(UtestSingleOp, test_UpdateArgTable) {
  auto *tbe_task = new (std::nothrow) TbeOpTask();
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  tbe_task->node_ = node;
  tbe_task->op_desc_ = op_desc;

  SingleOpModelParam param;
  param.memory_size = 1000;

  tbe_task->args_ = std::unique_ptr<uint8_t[]>(new uint8_t[24]());
  tbe_task->arg_size_ = 24;
  tbe_task->max_tiling_size_ = 0;
  tbe_task->need_tiling_ = false;

  tbe_task->sm_desc_ = malloc(10);

  EXPECT_EQ(tbe_task->UpdateArgTable(param), SUCCESS);

  tbe_task->arg_size_ = 16;
  EXPECT_NE(tbe_task->UpdateArgTable(param), ACL_ERROR_GE_INTERNAL_ERROR);   // ??

  free(tbe_task->sm_desc_);
  delete tbe_task;
}

// TEST_F(UtestSingleOp, test_OpTask_SetRuntimeContext) {
//   ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
//   ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
//   ge::NodePtr node = graph->AddNode(op_desc);
//   auto *task = new (std::nothrow) TbeOpTask(node);

//   OpTask *base = dynamic_cast<OpTask *>(task);

//   RuntimeInferenceContext *context = nullptr;
//   task->SetRuntimeContext(context);
//   //EXPECT_EQ(base->op_->operator_impl_.runtime_context_, nullptr);
//   delete task;
// }

TEST_F(UtestSingleOp, test_TbeOpTask_LaunchKernelFail) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) TbeOpTask(node);

  task->block_dim_ = 99;

  rtStream_t stream = nullptr;
  EXPECT_NE(task->LaunchKernel(stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_TbeOpTask_AllocateWorkspaces) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) TbeOpTask(node);
  EXPECT_NE(task->AllocateWorkspaces(std::vector<int64_t>(20)), SUCCESS);
  task->stream_resource_ = new StreamResource(1);
  EXPECT_NE(task->AllocateWorkspaces(std::vector<int64_t>(20)), SUCCESS);
  delete task->stream_resource_;
  delete task;
}

TEST_F(UtestSingleOp, test_TbeOpTask_CheckAndExecuteAtomic) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) TbeOpTask(node);
  task->node_ = node;
  task->op_desc_ = op_desc;

  std::vector<GeTensorDesc> input_desc;
  std::vector<DataBuffer> input_buffers;
  std::vector<GeTensorDesc> output_desc;
  std::vector<DataBuffer> output_buffers;
  rtStream_t stream = nullptr;

  task->clear_atomic_ = true;
  task->atomic_task_ = std::unique_ptr<AtomicAddrCleanOpTask>(new AtomicAddrCleanOpTask());
  task->atomic_task_->node_ = node;

  EXPECT_NE(task->CheckAndExecuteAtomic(input_desc, input_buffers, output_desc, output_buffers, stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AtomicAddrCleanOpTask_UpdateNodeByShape) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AtomicAddrCleanOpTask(node);

  EXPECT_EQ(task->UpdateNodeByShape(std::vector<GeTensorDesc>(), std::vector<GeTensorDesc>()), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuBaseTask_SetInputConst) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();
  task->op_desc_ = op_desc;

  EXPECT_EQ(task->SetInputConst(), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuBaseTask_UpdateExtInfo) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();
  task->op_desc_ = op_desc;
  task->aicpu_ext_handle_ = std::unique_ptr<ge::hybrid::AicpuExtInfoHandler>(
      new ge::hybrid::AicpuExtInfoHandler("Mul", 2, 1, DEPEND_IN_SHAPE));
  std::unique_ptr<AicpuShapeAndType> input_shape_and_type =
    std::unique_ptr<AicpuShapeAndType>(new AicpuShapeAndType());
  input_shape_and_type->type = 3;
  input_shape_and_type->dims[0] = 4;
  task->aicpu_ext_handle_->input_shape_and_type_.push_back(input_shape_and_type.get());
  task->aicpu_ext_handle_->input_shape_and_type_.push_back(input_shape_and_type.get());
  task->aicpu_ext_handle_->output_shape_and_type_.push_back(input_shape_and_type.get());

  std::vector<GeTensorDesc> input_desc = {GeTensorDesc(), GeTensorDesc()};
  std::vector<GeTensorDesc> output_desc = {GeTensorDesc()};
  rtStream_t stream = nullptr;
  EXPECT_EQ(task->UpdateExtInfo(input_desc, output_desc, stream), SUCCESS);

  task->num_inputs_ = 2;
  task->num_outputs_ = 1;
  task->input_is_const_ = std::vector<bool>({true});
  EXPECT_EQ(task->UpdateExtInfo(input_desc, output_desc, stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuBaseTask_UpdateOutputShape) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();
  task->op_desc_ = op_desc;
  task->aicpu_ext_handle_ = std::unique_ptr<ge::hybrid::AicpuExtInfoHandler>(
      new ge::hybrid::AicpuExtInfoHandler("Mul", 2, 1, DEPEND_IN_SHAPE));
  std::unique_ptr<AicpuShapeAndType> output_shape_and_type =
    std::unique_ptr<AicpuShapeAndType>(new AicpuShapeAndType());
  output_shape_and_type->type = 3;
  output_shape_and_type->dims[0] = 4;
  task->aicpu_ext_handle_->output_shape_and_type_.push_back(output_shape_and_type.get());

  std::vector<GeTensorDesc> input_desc = {GeTensorDesc(), GeTensorDesc()};
  std::vector<GeTensorDesc> output_desc = {GeTensorDesc()};
  EXPECT_EQ(task->UpdateOutputShape(output_desc), SUCCESS);

  task->num_inputs_ = 2;
  task->num_outputs_ = 1;
  task->input_is_const_ = std::vector<bool>({true});
  EXPECT_EQ(task->UpdateOutputShape(output_desc), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuBaseTask_UpdateShapeToOutputDesc) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();
  task->op_desc_ = op_desc;
  task->aicpu_ext_handle_ = std::unique_ptr<ge::hybrid::AicpuExtInfoHandler>(
      new ge::hybrid::AicpuExtInfoHandler("Mul", 2, 1, DEPEND_IN_SHAPE));

  GeTensorDesc output_desc = GeTensorDesc();
  GeShape shape_new;

  output_desc.SetShape(GeShape(std::vector<int64_t>{4}));
  output_desc.SetFormat(FORMAT_NHWC);
  output_desc.SetDataType(DT_INT32);
  EXPECT_NE(task->UpdateShapeToOutputDesc(shape_new, output_desc), SUCCESS);
  delete task;
}


TEST_F(UtestSingleOp, test_AiCpuBaseTask_ReadResultSummaryAndPrepareMemory) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();
  task->op_desc_ = op_desc;
  task->num_outputs_ = 1;
  void *ptr = new uint8_t[16];

  task->output_summary_host_.push_back(aicpu::FWKAdapter::ResultSummary({PtrToValue(ptr), 16,
    PtrToValue(ptr), 16}));
  void *test = new uint8_t[sizeof(aicpu::FWKAdapter::ResultSummary)];
  aicpu::FWKAdapter::ResultSummary *p = PtrToPtr<void, aicpu::FWKAdapter::ResultSummary>(test);
  p->shape_data_ptr = PtrToValue(ptr);
  p->shape_data_size = 16;
  p->raw_data_ptr = PtrToValue(ptr);
  p->raw_data_size = 16;;
  task->output_summary_.push_back(p);

  EXPECT_EQ(task->ReadResultSummaryAndPrepareMemory(), SUCCESS);
  task->output_summary_[0] = nullptr;
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuTask_CopyDataToHbm) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();
  task->op_desc_ = op_desc;
  task->num_outputs_ = 1;
  void *ptr = new uint8_t[16];
  task->output_summary_host_.push_back(aicpu::FWKAdapter::ResultSummary({PtrToValue(ptr), 16,
    PtrToValue(ptr), 16}));
  task->output_summary_.push_back(new uint8_t[sizeof(aicpu::FWKAdapter::ResultSummary)]);

  void* ptr1 = new uint8_t[16];
  task->copy_input_release_flag_dev_ = ptr1;
  void* ptr2 = new uint8_t[16];
  task->copy_input_data_size_dev_ = ptr2;
  void* ptr3 = new uint8_t[16];
  task->copy_input_src_dev_ = ptr3;
  void* ptr4 = new uint8_t[16];
  task->copy_input_dst_dev_ = ptr4;

  void *ptr5 = new uint8_t[sizeof(STR_FWK_OP_KERNEL)];
  task->copy_task_args_buf_ = ptr5;

  rtStream_t stream = nullptr;
  std::vector<DataBuffer> output;
  DataBuffer buffer;
  buffer.data = new uint8_t[16];
  buffer.length = 16;
  output.push_back(buffer);
  task->out_shape_hbm_.push_back(buffer.data);
  EXPECT_EQ(task->CopyDataToHbm(output, stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuTask_UpdateShapeAndDataByResultSummary) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();

  task->op_desc_ = op_desc;
  task->num_outputs_ = 1;
  void *ptr = new uint8_t[16];
  task->output_summary_host_.push_back(aicpu::FWKAdapter::ResultSummary({PtrToValue(ptr), 16,
    PtrToValue(ptr), 16}));
  void *test = new uint8_t[sizeof(aicpu::FWKAdapter::ResultSummary)];
  aicpu::FWKAdapter::ResultSummary *p = PtrToPtr<void, aicpu::FWKAdapter::ResultSummary>(test);
  p->shape_data_ptr = PtrToValue(ptr);
  p->shape_data_size = 16;
  p->raw_data_ptr = PtrToValue(ptr);
  p->raw_data_size = 16;;
  task->output_summary_.push_back(p);
  void* ptr1 = new uint8_t[16];
  task->copy_input_release_flag_dev_ = ptr1;
  void* ptr2 = new uint8_t[16];
  task->copy_input_data_size_dev_ = ptr2;
  void* ptr3 = new uint8_t[16];
  task->copy_input_src_dev_ = ptr3;
  void* ptr4 = new uint8_t[16];
  task->copy_input_dst_dev_ = ptr4;

  void *ptr5 = new uint8_t[sizeof(STR_FWK_OP_KERNEL)];
  task->copy_task_args_buf_ = ptr5;

  rtStream_t stream = nullptr;
  std::vector<GeTensorDesc> output_desc;
  output_desc.push_back(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  std::vector<DataBuffer> output;
  DataBuffer buffer;
  buffer.data = new uint8_t[16];
  buffer.length = 16;
  output.push_back(buffer);
  EXPECT_NE(task->UpdateShapeAndDataByResultSummary(output_desc, output, stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuTask_InitForSummaryAndCopy) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();

  task->op_desc_ = op_desc;
  task->unknown_type_ = DEPEND_COMPUTE;
  task->num_outputs_ = 1;
  void *ptr = new uint8_t[16];
  task->output_summary_host_.push_back(aicpu::FWKAdapter::ResultSummary({PtrToValue(ptr), 16,
    PtrToValue(ptr), 16}));
  void *test = new uint8_t[sizeof(aicpu::FWKAdapter::ResultSummary)];
  aicpu::FWKAdapter::ResultSummary *p = PtrToPtr<void, aicpu::FWKAdapter::ResultSummary>(test);
  p->shape_data_ptr = PtrToValue(ptr);
  p->shape_data_size = 16;
  p->raw_data_ptr = PtrToValue(ptr);
  p->raw_data_size = 16;;
  task->output_summary_.push_back(p);
  void* ptr1 = new uint8_t[16];
  task->copy_input_release_flag_dev_ = ptr1;
  void* ptr2 = new uint8_t[16];
  task->copy_input_data_size_dev_ = ptr2;
  void* ptr3 = new uint8_t[16];
  task->copy_input_src_dev_ = ptr3;
  void* ptr4 = new uint8_t[16];
  task->copy_input_dst_dev_ = ptr4;

  void *ptr5 = new uint8_t[sizeof(STR_FWK_OP_KERNEL)];
  task->copy_task_args_buf_ = ptr5;

  EXPECT_EQ(task->InitForSummaryAndCopy(), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuTask_SetMemCopyTask) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();

  task->op_desc_ = op_desc;
  task->unknown_type_ = DEPEND_COMPUTE;
  task->num_outputs_ = 1;
  void *ptr = new uint8_t[16];
  task->output_summary_host_.push_back(aicpu::FWKAdapter::ResultSummary({PtrToValue(ptr), 16,
    PtrToValue(ptr), 16}));
  void *test = new uint8_t[sizeof(aicpu::FWKAdapter::ResultSummary)];
  aicpu::FWKAdapter::ResultSummary *p = PtrToPtr<void, aicpu::FWKAdapter::ResultSummary>(test);
  p->shape_data_ptr = PtrToValue(ptr);
  p->shape_data_size = 16;
  p->raw_data_ptr = PtrToValue(ptr);
  p->raw_data_size = 16;;
  task->output_summary_.push_back(p);
  void* ptr1 = new uint8_t[16];
  task->copy_input_release_flag_dev_ = ptr1;
  void* ptr2 = new uint8_t[16];
  task->copy_input_data_size_dev_ = ptr2;
  void* ptr3 = new uint8_t[16];
  task->copy_input_src_dev_ = ptr3;
  void* ptr4 = new uint8_t[16];
  task->copy_input_dst_dev_ = ptr4;

  void *ptr5 = new uint8_t[sizeof(STR_FWK_OP_KERNEL)];
  task->copy_task_args_buf_ = ptr5;

  domi::KernelExDef kernel_def;
  std::vector<uint8_t> task_info(16, 0);
  kernel_def.set_task_info(task_info.data(), task_info.size());
  kernel_def.set_task_info_size(task_info.size());
  std::vector<uint8_t> args_info(sizeof(STR_FWK_OP_KERNEL), 0);
  kernel_def.set_args(args_info.data(), args_info.size());
  kernel_def.set_args_size(args_info.size());
  EXPECT_EQ(task->SetMemCopyTask(kernel_def), SUCCESS);

  kernel_def.set_args_size(sizeof(STR_FWK_OP_KERNEL) + 1);
  EXPECT_EQ(task->SetMemCopyTask(kernel_def), ACL_ERROR_GE_PARAM_INVALID);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuTask_LaunchKernel) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();

  task->op_desc_ = op_desc;
  task->num_outputs_ = 0;
  void *ptr = new uint8_t[16];

  GeTensorDesc tensor = GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32);
  std::vector<GeTensorDesc> input_desc = {tensor, tensor};
  std::vector<DataBuffer> input_buffers = {};
  std::vector<GeTensorDesc> output_desc;
  std::vector<DataBuffer> output_buffers;
  rtStream_t stream = nullptr;

  task->unknown_type_ = DEPEND_COMPUTE;
  EXPECT_NE(task->LaunchKernel(input_desc, input_buffers, output_desc, output_buffers, stream), SUCCESS);
  task->unknown_type_ = DEPEND_SHAPE_RANGE;
  EXPECT_NE(task->LaunchKernel(input_desc, input_buffers, output_desc, output_buffers, stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuCCTask_LaunchKernel) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuCCTask();

  task->op_desc_ = op_desc;
  task->num_outputs_ = 0;
  void *ptr = new uint8_t[16];

  GeTensorDesc tensor = GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32);
  std::vector<GeTensorDesc> input_desc = {tensor, tensor};
  std::vector<DataBuffer> input_buffers = {};
  std::vector<GeTensorDesc> output_desc;
  std::vector<DataBuffer> output_buffers;
  rtStream_t stream = nullptr;

  task->unknown_type_ = DEPEND_COMPUTE;
  EXPECT_NE(task->LaunchKernel(input_desc, input_buffers, output_desc, output_buffers, stream), SUCCESS);
  task->unknown_type_ = DEPEND_SHAPE_RANGE;
  EXPECT_NE(task->LaunchKernel(input_desc, input_buffers, output_desc, output_buffers, stream), SUCCESS);
  delete task;
}

TEST_F(UtestSingleOp, test_AiCpuBaseTask_UpdateArgTable) {
  ge::OpDescPtr op_desc = std::make_shared<OpDesc>("Mul", MATMUL);
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddInputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  op_desc->AddOutputDesc(GeTensorDesc(GeShape(std::vector<int64_t>{4}), FORMAT_NCHW, DT_INT32));
  ge::ComputeGraphPtr graph = std::make_shared<ge::ComputeGraph>("default");
  ge::NodePtr node = graph->AddNode(op_desc);
  auto *task = new (std::nothrow) AiCpuTask();

  task->op_desc_ = op_desc;
  task->num_outputs_ = 1;

  SingleOpModelParam param;
  EXPECT_EQ(task->UpdateArgTable(param), SUCCESS);
  EXPECT_EQ(task->GetTaskType(), kTaskTypeAicpu);

  void *ptr = new uint8_t[16];
  task->io_addr_host_.push_back(ptr);

  uintptr_t *arg_base;
  size_t arg_count;
  task->GetIoAddr(arg_base, arg_count);
  EXPECT_EQ(arg_count, 1);

  delete task;
}


