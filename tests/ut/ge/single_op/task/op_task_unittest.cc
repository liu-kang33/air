/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <vector>

#define protected public
#define private public
#include "single_op/task/op_task.h"
#include "graph/utils/graph_utils.h"

using namespace std;
using namespace testing;
using namespace ge;
using namespace optiling;

class UtestOpTask : public testing::Test {
 protected:
  void SetUp() {}
  void TearDown() {}
};

TEST_F(UtestOpTask, test_tbe_launch_kernel) {
  auto graph = make_shared<ComputeGraph>("graph");
  auto op_desc = make_shared<OpDesc>("Add", "Add");
  GeTensorDesc desc;
  op_desc->AddInputDesc(desc);
  op_desc->AddOutputDesc(desc);
  auto node = graph->AddNode(op_desc);

  TbeOpTask task;
  ge::DataBuffer data_buffer;
  vector<GeTensorDesc> input_desc;
  vector<DataBuffer> input_buffers = { data_buffer };
  vector<GeTensorDesc> output_desc;
  vector<DataBuffer> output_buffers = { data_buffer };
  task.op_desc_ = op_desc;
  auto op = OpDescUtils::CreateOperatorFromNode(node);
  task.op_ = std::move(std::unique_ptr<Operator>(new(std::nothrow) Operator(op)));
  task.node_ = node;
  OpTilingFuncV2 op_tiling_func = [](const ge::Operator &, const OpCompileInfoV2 &, OpRunInfoV2 &) -> bool {return true;};
  REGISTER_OP_TILING_UNIQ_V2(Add, op_tiling_func, 1);
  OpTilingRegistryInterf_V2("Add", op_tiling_func);
  ge::AttrUtils::SetStr(op_desc, "compile_info_key", "op_compile_info_key");
  ge::AttrUtils::SetStr(op_desc, "compile_info_json", "op_compile_info_json");
  char c = '0';
  char* buffer = &c;
  task.max_tiling_size_ = 64;
  task.need_tiling_ = true;
  task.tiling_data_ = "tiling_data";
  task.arg_index_ = {0};
  task.input_num_ = 1;
  task.output_num_ = 1;
  task.arg_size_ = sizeof(void *) * 3 + 64;
  task.args_.reset(new (std::nothrow) uint8_t[sizeof(void *) * 3 + 64]);
  task.args_with_tiling_.reset(new (std::nothrow) rtArgsWithTiling_t);
  task.args_with_tiling_->args = task.args_.get();

  rtStream_t stream_ = nullptr;
  ASSERT_EQ(task.LaunchKernel(input_desc, input_buffers, output_desc, output_buffers, stream_), SUCCESS);
  char *handle = "00";
  task.SetHandle(handle);
  ASSERT_EQ(task.LaunchKernel(input_desc, input_buffers, output_desc, output_buffers, stream_), SUCCESS);
}

TEST_F(UtestOpTask, test_update_args) {
  auto graph = make_shared<ComputeGraph>("graph");
  auto op_desc = make_shared<OpDesc>("Add", "Add");
  auto node = graph->AddNode(op_desc);

  TbeOpTask task;
  task.EnableDynamicSupport(node, 64);
  task.max_tiling_size_ = 64;
  task.tiling_data_ = "data";
  task.input_num_ = 1;
  task.output_num_ = 1;
  task.arg_size_ = sizeof(void *) * 2 + 64;
  task.args_.reset(new (std::nothrow) uint8_t[sizeof(void *) * 2 + 64]);
  task.args_with_tiling_.reset(new (std::nothrow) rtArgsWithTiling_t);
  task.args_with_tiling_->args = task.args_.get();
  int64_t buffer = 64;
  task.overflow_addr_ = &buffer;

  ASSERT_EQ(task.UpdateTilingArgs(nullptr), SUCCESS);
  size_t arg_count = 0;
  uintptr_t *arg_base = nullptr;
  task.GetIoAddr(arg_base, arg_count);
  ASSERT_EQ(arg_base, reinterpret_cast<uintptr_t *>(task.args_.get()));
  ASSERT_NE(arg_count, 2);

  AtomicAddrCleanOpTask atomic_task;
  atomic_task.node_ = node;
  atomic_task.need_tiling_ = true;
  atomic_task.arg_size_ = sizeof(void *) * 2 + 64;
  atomic_task.args_.reset(new (std::nothrow) uint8_t[sizeof(void *) * 2 + 64]);
  atomic_task.args_with_tiling_.reset(new (std::nothrow) rtArgsWithTiling_t);
  atomic_task.args_with_tiling_->args = task.args_.get();
  ASSERT_EQ(atomic_task.UpdateTilingArgs(nullptr), ACL_ERROR_GE_MEMORY_OPERATE_FAILED);

  atomic_task.max_tiling_size_ = 64;
  atomic_task.tiling_data_ = "atomic";
  ASSERT_EQ(atomic_task.UpdateTilingArgs(nullptr), SUCCESS);
}
