/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>

#define protected public
#define private public
#include "exec_runtime/deploy/deploy_planner.h"
#undef private
#undef protected

#include "graph/passes/graph_builder_utils.h"
#include "graph/build/graph_builder.h"
#include "runtime/deploy/stub_models.h"

using namespace std;

namespace ge {
class DeployPlannerTest : public testing::Test {
 protected:
  void SetUp() override {
  }
  void TearDown() override {
  }
};

TEST_F(DeployPlannerTest, TestFailedDueToMismatchOfQueueNames) {
  auto root_model = SubModels::BuildRootModel(SubModels::BuildGraphWithQueueBindings());
  ASSERT_TRUE(root_model != nullptr);
  root_model->model_relation_->submodel_queue_infos["subgraph-2"].input_queue_names[0] = "oops";
  DeployPlan deploy_plan;
  auto ret = DeployPlanner(root_model).BuildPlan(deploy_plan);
  ASSERT_EQ(ret, PARAM_INVALID);
}

///      NetOutput
///         |
///         |
///        PC_2
///        |  \
///       PC_1 |
///     /     \
///    |      |
///  data1  data2
TEST_F(DeployPlannerTest, TestBuildDeployPlan_WithQueueBindings) {
  auto root_model = SubModels::BuildRootModel(SubModels::BuildGraphWithQueueBindings());
  ASSERT_TRUE(root_model != nullptr);
  EXPECT_EQ(root_model->GetSubmodels().size(), 2);
  std::cout << root_model->GetSubmodels().size() << std::endl;
  auto model_relation = root_model->GetModelRelation();
  ASSERT_TRUE(model_relation != nullptr);
  ASSERT_EQ(model_relation->submodel_queue_infos.size(), 2);
  ASSERT_EQ(model_relation->root_model_queue_info.input_queue_names.size(), 2);
  ASSERT_EQ(model_relation->root_model_queue_info.output_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-1")->second.input_queue_names.size(), 2);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-1")->second.output_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-2")->second.input_queue_names.size(), 2);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-2")->second.output_queue_names.size(), 1);

  DeployPlan deploy_plan;
  auto ret = DeployPlanner(root_model).BuildPlan(deploy_plan);
  ASSERT_EQ(ret, SUCCESS);
  // data2 -> PC_1, data2 -> PC_2
  ASSERT_EQ(deploy_plan.GetQueueInfoList().size(), 10);
  ASSERT_EQ(deploy_plan.GetQueueBindings().size(), 6);
  std::map<int, std::set<int>> check;
  for (auto to_bind : deploy_plan.GetQueueBindings()) {
    check[to_bind.first].emplace(to_bind.second);
    std::cout << to_bind.first << " -> " << to_bind.second << std::endl;
  }
  ASSERT_EQ(check.size(), 3);
  ASSERT_EQ(check.begin()->second.size(), 4);
  ASSERT_TRUE(check.begin()->second.count(deploy_plan.submodels_["subgraph-1"].input_queue_indices[1]) > 0);
  ASSERT_TRUE(check.begin()->second.count(deploy_plan.submodels_["subgraph-1"].input_queue_indices[1]) > 0);

  ASSERT_EQ(deploy_plan.GetInputQueueIndices().size(), 2);
  ASSERT_EQ(deploy_plan.GetOutputQueueIndices().size(), 1);
}

///     NetOutput
///         |
///       PC_3
///      /   \
///    PC_1  PC2
///    |      |
///  data1  data2
TEST_F(DeployPlannerTest, TestBuildDeployPlan_WithoutQueueBindings) {
  auto root_model = SubModels::BuildRootModel(SubModels::BuildGraphWithoutNeedForBindingQueues());
  ASSERT_TRUE(root_model != nullptr);
  EXPECT_EQ(root_model->GetSubmodels().size(), 3);
  auto model_relation = root_model->GetModelRelation();
  ASSERT_TRUE(model_relation != nullptr);
  ASSERT_EQ(model_relation->submodel_queue_infos.size(), 3);
  ASSERT_EQ(model_relation->root_model_queue_info.input_queue_names.size(), 2);
  ASSERT_EQ(model_relation->root_model_queue_info.output_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-1")->second.input_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-1")->second.output_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-2")->second.input_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-2")->second.output_queue_names.size(), 1);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-3")->second.input_queue_names.size(), 2);
  ASSERT_EQ(model_relation->submodel_queue_infos.find("subgraph-3")->second.output_queue_names.size(), 1);

  DeployPlan deploy_plan;
  auto ret = DeployPlanner(root_model).BuildPlan(deploy_plan);
  ASSERT_EQ(ret, SUCCESS);
  ASSERT_EQ(deploy_plan.GetQueueInfoList().size(), 5);
  ASSERT_EQ(deploy_plan.GetQueueBindings().size(), 0);
}

TEST_F(DeployPlannerTest, TestGetQueueInfo) {
  DeployPlan plan;
  const DeployPlan::QueueInfo *queue_info = nullptr;
  ASSERT_EQ(plan.GetQueueInfo(0, queue_info), PARAM_INVALID);
  plan.queues_.resize(1);
  ASSERT_EQ(plan.GetQueueInfo(0, queue_info), SUCCESS);
}
}  // namespace ge