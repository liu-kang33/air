/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <memory>

#define protected public
#define private public
#include "graph/manager/trans_var_data_utils.h"
#include <framework/common/debug/log.h>
#include "framework/common/debug/ge_log.h"
#include "framework/common/types.h"
#include "graph/compute_graph.h"
#include "graph/ge_context.h"
#include "runtime/dev.h"
#include "graph/manager/graph_mem_manager.h"
#include "mmpa/mmpa_api.h"
#undef protected
#undef private

namespace ge {
class UtestTransVarDataTest : public testing::Test {
 protected:
  void SetUp() {}
  void TearDown() {}
};

namespace ut {
class GraphBuilder {
 public:
  explicit GraphBuilder(const std::string &name) { graph_ = std::make_shared<ComputeGraph>(name); }
  NodePtr AddNode(const std::string &name, const std::string &type, int in_cnt, int out_cnt,
                  Format format = FORMAT_NCHW, DataType data_type = DT_FLOAT,
                  std::vector<int64_t> shape = {1, 1, 224, 224});
  NodePtr AddNode(const std::string &name, const std::string &type,
                  std::initializer_list<std::string> input_names,
                  std::initializer_list<std::string> output_names,
                  Format format = FORMAT_NCHW, DataType data_type = DT_FLOAT,
                  std::vector<int64_t> shape = {1, 1, 224, 224});
  void AddDataEdge(const NodePtr &src_node, int src_idx, const NodePtr &dst_node, int dst_idx);
  void AddControlEdge(const NodePtr &src_node, const NodePtr &dst_node);
  ComputeGraphPtr GetGraph() {
    graph_->TopologicalSorting();
    return graph_;
  }

 private:
  ComputeGraphPtr graph_;
};
}  // namespace ut

NodePtr UtAddNode(ComputeGraphPtr &graph, std::string name, std::string type, int in_cnt, int out_cnt);

ComputeGraphPtr BuildGraphTransVarData() {
  ut::GraphBuilder builder = ut::GraphBuilder("graph");
  auto data = builder.AddNode("Data", "Data", 1, 1);
  auto const1 = builder.AddNode("const1", "Const", 1, 1);
  auto addn = builder.AddNode("addn", "AddN", 2, 1);

  builder.AddDataEdge(data, 0, addn, 0);
  builder.AddDataEdge(const1, 0, addn, 1);
  return builder.GetGraph();
}

TEST_F(UtestTransVarDataTest, CopyVarData) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("graph");
  auto node_0 = UtAddNode(graph, "data1", VARIABLE, 1, 1);
  auto node_1 = UtAddNode(graph, "data2", VARIABLE, 1, 1);
  node_0->GetInDataAnchor(0)->LinkFrom(node_1->GetOutDataAnchor(0));
  auto device_id = GetContext().DeviceId();
  uint64_t session_id = 1;
  Status ret = TransVarDataUtils::CopyVarData(graph, session_id, device_id);
  EXPECT_EQ(ret, SUCCESS);

  ComputeGraphPtr graph2 = nullptr;
  ret = TransVarDataUtils::CopyVarData(graph2, session_id, device_id);
  EXPECT_EQ(ret, FAILED);
}

TEST_F(UtestTransVarDataTest, TransAllVarData) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("graph");
  auto node_0 = UtAddNode(graph, "data1", VARIABLE, 1, 1);
  auto node_1 = UtAddNode(graph, "data2", VARIABLE, 1, 1);
  node_0->GetInDataAnchor(0)->LinkFrom(node_1->GetOutDataAnchor(0));
  std::vector<NodePtr> variable_nodes;
  variable_nodes.push_back(node_0);
  variable_nodes.push_back(node_1);
  uint64_t session_id = 1;
  rtContext_t context = nullptr;
  uint32_t graph_id = 1;
  uint32_t device_id = 1;
  Status ret = TransVarDataUtils::TransAllVarData(variable_nodes, session_id, context, graph_id, device_id);
}

TEST_F(UtestTransVarDataTest, CopyVarData_failed)
{
  ComputeGraphPtr graph = BuildGraphTransVarData();
  OpDescPtr op_desc_ptr = make_shared<OpDesc>("Variable", "Variable");
  GeTensorDesc dims_tensor_desc(GeShape({1,1,1,1}), FORMAT_NCHW, DT_FLOAT16);
  GeTensorDesc dims_tensor_desc_in(GeShape({1,1,1,1}), FORMAT_NCHW, DT_FLOAT16);
  op_desc_ptr->AddInputDesc(dims_tensor_desc_in);
  op_desc_ptr->AddOutputDesc(dims_tensor_desc);

  NodePtr src_node = graph->AddNode(op_desc_ptr);
  (void)AttrUtils::SetStr(src_node->GetOpDesc(), "_copy_from_var_node", "addn");
  (void)AttrUtils::SetBool(src_node->GetOpDesc(), "_copy_value", false);

  auto device_id = GetContext().DeviceId();
  uint64_t session_id = 1;
  Status ret = TransVarDataUtils::CopyVarData(graph, session_id, device_id);
  EXPECT_EQ(FAILED, ret);
}

TEST_F(UtestTransVarDataTest, TransAllVarData_failed) {
  ComputeGraphPtr graph = std::make_shared<ComputeGraph>("graph");
  auto node_0 = UtAddNode(graph, "data1", VARIABLE, 1, 1);
  auto node_1 = UtAddNode(graph, "data2", VARIABLE, 1, 1);
  node_0->GetInDataAnchor(0)->LinkFrom(node_1->GetOutDataAnchor(0));
  std::vector<NodePtr> variable_nodes;
  variable_nodes.push_back(node_0);
  variable_nodes.push_back(node_1);
  uint64_t session_id = 1;
  rtContext_t context = nullptr;
  uint32_t graph_id = 1;
  uint32_t device_id = 1;

  const char_t * const kEnvValue = "SET_TRANS_VAR_DATA";
  // 设置环境变量
  char_t npu_collect_path[MMPA_MAX_PATH] = {};
  mmRealPath(".", &npu_collect_path[0U], MMPA_MAX_PATH);
  const std::string fail_collect_path = (std::string(&npu_collect_path[0U]) + "/mock_fail");
  mmSetEnv(kEnvValue, fail_collect_path.c_str(), 1);

  Status ret = TransVarDataUtils::TransAllVarData(variable_nodes, session_id, context, graph_id, device_id);
  EXPECT_EQ(ret, -1);

  // 清理环境变量
  mmSetEnv(kEnvValue, "", 1);
}

} // namespace ge
