/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __INC_TESTS_FFTS_PLUS_PROTO_TOOLS_H
#define __INC_TESTS_FFTS_PLUS_PROTO_TOOLS_H

#include "proto/task.pb.h"
#include "graph/compute_graph.h"

namespace ge {
OpDescPtr CreateOpDesc(std::string name = "", std::string type = "", int in_num = 0, int out_num = 0);

NodePtr CreateNode(ComputeGraph &graph, const std::string &name, const std::string &type, int in_num, int out_num);

void SetNodeAnchorStatus(const NodePtr &node);
void InitFftsThreadSliceMap(const OpDescPtr &op_desc);

void InitTaskSQEInfo(domi::FftsPlusTaskDef *task_def);

void InitTaskAdditionalDataInfo(domi::FftsPlusTaskDef *task_def);

void InitCachePersistentCtx(domi::FftsPlusCachePersistCtxDef *ctx_def);

void InitAicAivCtx(domi::FftsPlusAicAivCtxDef *ctx_def);

void InitMixAicAivCtx(domi::FftsPlusMixAicAivCtxDef *ctx_def, bool is_auto=false);

void InitSdmaCtx(domi::FftsPlusSdmaCtxDef *ctx_def);

void InitNotifyCtx(domi::FftsPlusNotifyCtxDef *ctx_def);

void InitWriteValueCtx(domi::FftsPlusWriteValueCtxDef *ctx_def);

void InitAicpuCtxCtx(domi::FftsPlusAicpuCtxDef *ctx_def);

void InitAicpuFwkCtxCtx(domi::FftsPlusAicpuCtxDef *ctx_def);

void InitDataCtx(domi::FftsPlusDataCtxDef *ctx_def);

void InitAicpuFwkCtxAndExtInfo(domi::FftsPlusAicpuCtxDef *ctx_def);

void InitAicpuCtxAndExtInfo(domi::FftsPlusAicpuCtxDef *ctx_def);

void InitCustomAicpuCtxAndExtInfo(domi::FftsPlusAicpuCtxDef *ctx_def);

void InitAtStartCtx(domi::FftsPlusAtStartCtxDef *ctx_def);

void InitAtEndCtx(domi::FftsPlusAtEndCtxDef *ctx_def);

void InitLabelCtx(domi::FftsPlusLabelCtxDef *ctx_def);

void InitCaseSwitchCtx(domi::FftsPlusCaseSwitchCtxDef *ctx_def);

void InitCaseDefaultCtx(domi::FftsPlusCaseDefaultCtxDef *ctx_def);

void InitCondSwitchCtx(domi::FftsPlusCondSwitchCtxDef *ctx_def);
} // namespace ge
#endif // __INC_TESTS_FFTS_PLUS_PROTO_TOOLS_H
