/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/daemon/executor_manager.h"
#include <vector>
#include "framework/common/types.h"
#include "framework/common/debug/ge_log.h"
#include "runtime/rt_mem_queue.h"
#include "deploy/executor/executor_context.h"
#define private public
#include "deploy/executor/event_handler.h"
#undef private

namespace ge {
namespace {
std::map<const ExecutorProcess *, std::unique_ptr<EventHandler>> g_event_handlers;
std::mutex g_mu;

class ModelHandleMock : public ExecutorContext::ModelHandle {
 public:
  explicit ModelHandleMock(uint64_t model_size) : ModelHandle(model_size) {}
  Status DoLoadModel(const shared_ptr<GeRootModel> &root_model,
                     const vector<uint32_t> &input_queues,
                     const vector<uint32_t> &output_queues,
                     uint32_t &model_id) const override {
    static std::atomic<uint32_t> id_gen;
    model_id = id_gen++;
    bool is_dynamic = false;
    root_model->CheckIsUnknownShape(is_dynamic);
    GELOGD("Root model: %s, is_dynamic = %d", root_model->GetRootGraph()->GetName().c_str(), is_dynamic);
    return SUCCESS;
  }

  Status DoUnloadModel(const uint32_t model_id) const override {
    return SUCCESS;
  }
};

class ExecutionContextMock : public ExecutorContext {
 public:
 protected:
  std::unique_ptr<ModelHandle> CreateModelHandle(uint64_t model_size) const override {
   return MakeUnique<ModelHandleMock>(model_size);
 }
};
}  // namespace

ExecutorProcess::ExecutorProcess(int32_t pid, int32_t device_id, std::string queue_name, std::string group_name)
    : pid_(pid), device_id_(device_id), msg_queue_name_(std::move(queue_name)), group_name_(std::move(group_name)) {
}

ExecutorProcess::~ExecutorProcess() {
  (void) Finalize();
}

Status ExecutorProcess::Initialize() {
  std::lock_guard<std::mutex> lk(g_mu);
  auto &handler = g_event_handlers[this];
  handler.reset(new EventHandler());
  handler->Initialize();
  handler->context_.reset(new ExecutionContextMock());
  return SUCCESS;
}

Status ExecutorManager::LoadAndExecuteChildProcess(const int32_t device_id,
                                                   const std::string &msg_queue_name,
                                                   const std::string &group_name) {
  return SUCCESS;
}

Status ExecutorProcess::InitMessageQueue() {
  return SUCCESS;
}

Status ExecutorProcess::WaitEvent(ExecutorEventType expected_event_type) const {
  return SUCCESS;
}

Status ExecutorProcess::WaitForExecutorInitialized() {
  return SUCCESS;
}

Status ExecutorProcess::SendRequest(deployer::ExecutorRequest &request, deployer::ExecutorResponse &resp) const {
  EventHandler *handler = nullptr;
  {
    std::lock_guard<std::mutex> lk(g_mu);
    handler = g_event_handlers[this].get();
  }

  handler->HandleEvent(request, resp);
  return SUCCESS;
}

Status ExecutorProcess::SendEvent(ExecutorEventType event_type) const {
  return SUCCESS;
}

Status ExecutorProcess::Finalize() {
  return SUCCESS;
}

Status ExecutorProcess::InitEventGroup() {
  return SUCCESS;
}

Status ExecutorManager::ForAndInit(const int32_t device_id, std::unique_ptr<ExecutorProcess> &executor_process) {
  return SUCCESS;
}

Status ExecutorManager::GetOrForkExecutorProcess(const ExecutorId &context, ExecutorProcess **executor_process) {
  std::lock_guard<std::mutex> lk(mu_);
  auto it = processes_.find(context);
  if (it != processes_.end()) {
    *executor_process = it->second.get();
    return SUCCESS;
  }

  GELOGD("Executor process not exist, start to create, context_id = %ld", context.context_id);
  std::unique_ptr<ExecutorProcess> process(new ExecutorProcess(0, 0, "queue", "DM_QS_GROUP"));
  GE_CHK_STATUS_RET_NOLOG(process->Initialize());
  *executor_process = process.get();
  processes_.emplace(context, std::move(process));
  return SUCCESS;
}

Status ExecutorManager::GetExecutorProcess(const ExecutorId &context, ExecutorProcess **executor_process) const {
  std::lock_guard<std::mutex> lk(mu_);
  auto it = processes_.find(context);
  if (it != processes_.end()) {
    *executor_process = it->second.get();
    return SUCCESS;
  }

  GELOGE(FAILED, "Executor process not exist, context_id = %ld", context.context_id);
  return FAILED;
}
}  // namespace ge
