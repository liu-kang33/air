/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "deploy/master/device_mgr/deployer_client.h"
#include "framework/common/debug/ge_log.h"
#include "debug/ge_util.h"
#include "common/plugin/ge_util.h"
#include "deploy/daemon/deployer_service_impl.h"

namespace ge {
class DeployerClient::Impl {
};

DeployerClient::DeployerClient() = default;
DeployerClient::~DeployerClient() = default;

Status DeployerClient::Init(const string &address) {
  impl_ = MakeUnique<DeployerClient::Impl>();
  GE_CHECK_NOTNULL(impl_);
  return SUCCESS;
}

Status DeployerClient::SendRequest(const deployer::DeployerRequest &request, deployer::DeployerResponse &response) {
  RequestContext context;
  context.peer_url = "http://127.0.0.1:1234";
  return DeployerServiceImpl::GetInstance().Process(context, request, response);
}
}  // namespace ge