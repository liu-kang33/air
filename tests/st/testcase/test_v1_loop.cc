/**
 * Copyright 2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "utils/graph_factory.h"
#include <gtest/gtest.h>
#include <map>
#include "external/ge/ge_api.h"
#include "utils/synchronizer.h"
#include "ge_running_env/tensor_utils.h"
#include "utils/tensor_adapter.h"
#include "graph/utils/graph_utils.h"
#include "mmpa/mmpa_api.h"
#include "init_ge.h"

namespace ge {
class V1LoopSt : public testing::Test {
 protected:
  void SetUp() override {
    ReInitGe();
  }
};

TEST_F(V1LoopSt, BasicV1LoopSuccess) {
  std::map<AscendString, AscendString> options;
  Session sess(options);

  auto g1 = GraphFactory::BuildV1LoopGraph1();
  EXPECT_EQ(sess.AddGraph(1, g1), SUCCESS);

  std::vector<ge::Tensor> g1_inputs;
  g1_inputs.emplace_back(TensorAdapter::AsTensor(*GenerateTensor(DT_INT32, {})));
  g1_inputs.emplace_back(TensorAdapter::AsTensor(*GenerateTensor({8,3,224,224})));
  Synchronizer sync;
  Status run_ret;
  auto ret = sess.RunGraphAsync(1, g1_inputs, [&run_ret, &sync](Status result, std::vector<ge::Tensor> &) {
    run_ret = result;
    sync.OnDone();
  });
  EXPECT_EQ(ret, SUCCESS);
  sync.WaitFor(10);
  EXPECT_EQ(run_ret, SUCCESS);
}

TEST_F(V1LoopSt, V1LoopSuccess_CtrlEnterIn) {
  std::map<AscendString, AscendString> options;
  Session sess(options);

  auto g1 = GraphFactory::BuildV1LoopGraph2_CtrlEnterIn();

  EXPECT_EQ(sess.AddGraph(1, g1), SUCCESS);

  std::vector<ge::Tensor> g1_inputs;
  g1_inputs.emplace_back(TensorAdapter::AsTensor(*GenerateTensor(DT_INT32, {})));
  g1_inputs.emplace_back(TensorAdapter::AsTensor(*GenerateTensor({8,3,224,224})));
  Synchronizer sync;
  Status run_ret;
  auto ret = sess.RunGraphAsync(1, g1_inputs, [&run_ret, &sync](Status result, std::vector<ge::Tensor> &) {
    run_ret = result;
    sync.OnDone();
  });
  EXPECT_EQ(ret, SUCCESS);
  sync.WaitFor(10);
  EXPECT_EQ(run_ret, SUCCESS);
}

TEST_F(V1LoopSt, V1LoopSuccess_DataEnterIn_BypassMerge) {
  std::map<AscendString, AscendString> options;
  Session sess(options);

  auto g1 = GraphFactory::BuildV1LoopGraph4_DataEnterInByPassMerge();

  EXPECT_EQ(sess.AddGraph(1, g1), SUCCESS);

  std::vector<ge::Tensor> g1_inputs;
  g1_inputs.emplace_back(TensorAdapter::AsTensor(*GenerateTensor(DT_INT32, {})));
  Synchronizer sync;
  Status run_ret;
  auto ret = sess.RunGraphAsync(1, g1_inputs, [&run_ret, &sync](Status result, std::vector<ge::Tensor> &) {
    run_ret = result;
    sync.OnDone();
  });
  EXPECT_EQ(ret, SUCCESS);
  sync.WaitFor(10);
  EXPECT_EQ(run_ret, SUCCESS);
}

TEST_F(V1LoopSt, V1LoopSuccess_EnterCtrlConst) {
  std::map<AscendString, AscendString> options;
  Session sess(options);

  auto g1 = GraphFactory::BuildV1LoopGraph3_CtrlEnterIn2();

  EXPECT_EQ(sess.AddGraph(1, g1), SUCCESS);

  std::vector<ge::Tensor> g1_inputs;
  g1_inputs.emplace_back(TensorAdapter::AsTensor(*GenerateTensor(DT_INT32, {})));
  Synchronizer sync;
  Status run_ret;
  auto ret = sess.RunGraphAsync(1, g1_inputs, [&run_ret, &sync](Status result, std::vector<ge::Tensor> &) {
    run_ret = result;
    sync.OnDone();
  });
  EXPECT_EQ(ret, SUCCESS);
  sync.WaitFor(10);
  EXPECT_EQ(run_ret, SUCCESS);
}
}
