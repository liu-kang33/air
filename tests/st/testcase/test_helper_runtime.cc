/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <condition_variable>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <thread>
#include <memory>
#include <unistd.h>
#include <errno.h>
#include <string>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "generator/ge_generator.h"
#include "deploy/daemon/grpc_server.h"
#include "deploy/master/device_mgr/device_manager.h"
#include "common/config/json_parser.h"
#include "framework/common/debug/ge_log.h"
#include "ge/ge_api_error_codes.h"
#include "third_party/inc/runtime/rt_mem_queue.h"
#define protected public
#define private public
#include "executor/hybrid/node_executor/node_executor.h"
#include "common/mem_grp/memory_group_manager.h"
#include "runtime/common/data_flow/route/datagw_manager.h"
#include "runtime/common/data_flow/route/network_manager.h"
#include "graph/manager/graph_var_manager.h"
#include "graph/manager/graph_mem_manager.h"
#include "graph/utils/tensor_utils.h"
#include "deploy/daemon/client_manager.h"
#undef private
#undef protected

#include "ge_graph_dsl/graph_dsl.h"
#include "ge_graph_dsl/assert/graph_assert.h"
#include "graph/ge_local_context.h"
#include "proto/deployer.pb.h"
#include "common/data_flow/queue/helper_exchange_service.h"
#include "exec_runtime/runtime_tensor_desc.h"
#include "graph/debug/ge_attr_define.h"
#include "exec_runtime/execution_runtime.h"
#include "depends/mmpa/src/mmpa_stub.h"
#include "external/ge/ge_api.h"
#include "graph/utils/graph_utils.h"

#include "deploy/master/helper_execution_runtime.h"
#include "depends/mmpa/src/mmpa_stub.h"
#include "depends/runtime/src/runtime_stub.h"
#include "init_ge.h"


using namespace std;
using namespace ::testing;
using namespace ge;

rtError_t rtMemQueueDeQueueBuff(int32_t device, uint32_t qid, rtMemQueueBuff_t *outBuf, int32_t timeout) {
  RuntimeTensorDesc mbuf_tensor_desc;
  mbuf_tensor_desc.shape[0] = 4;
  mbuf_tensor_desc.shape[1] = 1;
  mbuf_tensor_desc.shape[2] = 1;
  mbuf_tensor_desc.shape[3] = 224;
  mbuf_tensor_desc.shape[4] = 224;
  mbuf_tensor_desc.dtype = static_cast<int64_t>(DT_INT64);
  mbuf_tensor_desc.data_addr = static_cast<int64_t>(reinterpret_cast<intptr_t>(outBuf->buffInfo->addr));
  if (memcpy_s(outBuf->buffInfo->addr, sizeof(RuntimeTensorDesc), &mbuf_tensor_desc, sizeof(RuntimeTensorDesc)) !=
      EOK) {
    printf("Failed to copy mbuf data, dst size:%zu, src size:%zu\n", outBuf->buffInfo->len, sizeof(RuntimeTensorDesc));
    return -1;
  }
  return 0;
}

rtError_t rtMemQueuePeek(int32_t device, uint32_t qid, size_t *bufLen, int32_t timeout) {
  *bufLen = sizeof(RuntimeTensorDesc) + 224U * 224U;
  return 0;
}

vector<int8_t> placeholder(224U * 224U * sizeof(int64_t) * 10);

rtError_t rtMbufAlloc(rtMbufPtr_t *mbuf, uint64_t size) {
  *mbuf = placeholder.data();
  return 0;
}

rtError_t rtMbufFree(rtMbufPtr_t mbuf) {
  return 0;
}

rtError_t rtMbufGetBuffAddr(rtMbufPtr_t mbuf, void **databuf) {
  *databuf = mbuf;
  return 0;
}

rtError_t rtMbufGetPrivInfo(rtMbufPtr_t mbuf, void **priv, uint64_t *size) {
  *priv = mbuf;
  return 0;
}

rtError_t rtMemQueueEnQueue(int32_t devId, uint32_t qid, void *mbuf) {
  return 0;
}

rtError_t rtMemQueueDeQueue(int32_t devId, uint32_t qid, void **mbuf) {
  *mbuf = placeholder.data();
  return 0;
}

rtError_t rtMbufGetBuffSize(rtMbufPtr_t mbuf, uint64_t *size) {
  *size = placeholder.size();
  return 0;
}

#define RT_QUEUE_MODE_PUSH 1

namespace ge {
void *mock_handle = nullptr;
void *mock_method = nullptr;

class MockMmpa : public MmpaStubApi {
 public:
  void *DlOpen(const char *file_name, int32_t mode) override {
    return mock_handle;
  }
  void *DlSym(void *handle, const char *func_name) override {
    return mock_method;
  }

  int32_t DlClose(void *handle) override {
    return 0;
  }
};

class ModelDeployerMock : public ModelDeployer {
 public:
  Status DeployModel(const vector<GeRootModelPtr> &models, const ModelRelation *model_relation,
                     const vector<uint32_t> &input_queue_ids, const vector<uint32_t> &output_queue_ids,
                     DeployResult &deploy_result) override {
    deploy_result.input_queue_ids = {1, 2, 3};
    deploy_result.output_queue_ids = {4};
    uint32_t input0_qid = 1;
    ExecutionRuntime::GetInstance()->GetExchangeService().CreateQueue(0, "input0", 2, 0, input0_qid);
    uint32_t input1_qid = 2;
    ExecutionRuntime::GetInstance()->GetExchangeService().CreateQueue(0, "input1", 2, 0, input1_qid);
    uint32_t input2_qid = 3;
    ExecutionRuntime::GetInstance()->GetExchangeService().CreateQueue(0, "input2", 2, 0, input2_qid);
    return SUCCESS;
  }

  Status Undeploy(uint32_t model_id) override {
    return SUCCESS;
  }
};

class ExecutionRuntimeHelperMock : public ExecutionRuntime {
 public:
  Status Initialize(const map<std::string, std::string> &options) override {
    return 0;
  }
  Status Finalize() override {
    return 0;
  }
  ModelDeployer &GetModelDeployer() override {
    return model_deployer_;
  }
  ExchangeService &GetExchangeService() override {
    return exchange_service_;
  }

 public:
  HelperExchangeService exchange_service_;
  ModelDeployerMock model_deployer_;
};

Status MockInitializeHelperRuntime(const std::map<std::string, std::string> &options) {
  ExecutionRuntime::SetExecutionRuntime(std::make_shared<ExecutionRuntimeHelperMock>());
  return SUCCESS;
}

class STEST_helper_runtime : public testing::Test {
 protected:
  void SetUp() {
    hybrid::NodeExecutorManager::GetInstance().
        engine_mapping_.emplace("AiCoreLib", hybrid::NodeExecutorManager::ExecutorType::AICORE);
    MmpaStub::GetInstance().SetImpl(std::make_shared<MockMmpa>());
  }
  void TearDown() {
    ExecutionRuntime::FinalizeExecutionRuntime();
    MmpaStub::GetInstance().Reset();
    mock_handle = nullptr;
    mock_method = nullptr;
  }
};

class MockDataGwManager : public DataGwManager {
 public:
  MockDataGwManager() : DataGwManager() {}
  MOCK_METHOD2(execv, int64_t(const char *pathname, char *const argv[]));
  MOCK_METHOD4(InitHostDataGwServer, ge::Status(const uint32_t, uint32_t, const std::string &, pid_t &));
  MOCK_METHOD3(InitDeviceDataGwServer, ge::Status(const uint32_t, const std::string &, pid_t &));
};

const char *config_path = "HELPER_RES_FILE_PATH";
using RemoteDevicePtr = std::shared_ptr<RemoteDevice>;

void StartServer(ge::GrpcServer &grpc_server) {
  auto res = grpc_server.Run();
  if (res != ge::SUCCESS) {
    std::cout<<"run failed"<<std::endl;
    return;
  }
}

void SetDeviceEnv() {
  const char *path = "../tests/st/config_file/right_json/device";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitDeviceInformation(real_path);
}

void BuildModel(ModelBufferData &model_buffer_data) {
//  dlog_setlevel(GE_MODULE_NAME, DLOG_DEBUG, 1);
  vector<std::string> engine_list = {"AIcoreEngine"};
  auto add1 = OP_CFG(ADD).TensorDesc(FORMAT_NCHW, DT_FLOAT, {1, 1, 224, 224});
  auto add2 = OP_CFG(ADD).TensorDesc(FORMAT_NCHW, DT_FLOAT, {1, 1, 224, 224});
  auto data1 = OP_CFG(DATA);
  auto data2 = OP_CFG(DATA);
  auto print = OP_CFG("Print");
  DEF_GRAPH(g1) {
    CHAIN(NODE("data_1", data1)->EDGE(0, 0)->NODE("add_1", add1));
    CHAIN(NODE("data_2", data2)->EDGE(0, 1)->NODE("add_1", add1));
    CHAIN(NODE("add_1", add1)->EDGE(0, 0)->NODE("add_2", add2));
    CHAIN(NODE("data_2", data2)->EDGE(0, 1)->NODE("add_2", add2));
    CHAIN(NODE("add_2", add2)->EDGE(0, 0)->NODE("Print", print));
  };

  auto graph = ToGeGraph(g1);

  GeGenerator ge_generator;
  std::map<std::string, std::string> options;
  options.emplace(ge::SOC_VERSION, "Ascend910");
  ge_generator.Initialize(options);

  auto ret = ge_generator.GenerateOnlineModel(graph, {}, model_buffer_data);
  EXPECT_EQ(ret, SUCCESS);
}

ge::Status CreateQueue(uint32_t device_id, uint32_t &qid) {
  rtMemQueueAttr_t queue_attr = {0};
  queue_attr.depth = 100;
  queue_attr.workMode = RT_QUEUE_MODE_PUSH;
  queue_attr.flowCtrlFlag = 1;
  queue_attr.overWriteFlag = 0;
  queue_attr.flowCtrlDropTime = 1;

  uint32_t queue_id = 0;
  rtError_t rt_ret = rtMemQueueCreate(device_id, &queue_attr, &queue_id);
  if (rt_ret != RT_ERROR_NONE) {
    printf("create queue failed. \n");
    return ge::FAILED;
  }
  qid = queue_id;
  return ge::SUCCESS;
}

void SendPreDeployRequest(std::vector<RemoteDevicePtr> &clients) {
  deployer::DeployerResponse response;
  deployer::DeployerRequest pre_deploy_request;
  pre_deploy_request.set_type(deployer::kPreDeployModel);
  auto pre_deploy_req_body = pre_deploy_request.mutable_pre_deploy_model_request();
  auto exchange_plan = pre_deploy_req_body->mutable_exchange_plan();
  deployer::QueueDesc queue_desc;
  queue_desc.set_name("data-1");
  queue_desc.set_depth(2);
  queue_desc.set_type(2);  // tag
  *exchange_plan->add_queues() = queue_desc;
  queue_desc.set_type(1);  // queue
  *exchange_plan->add_queues() = queue_desc;

  queue_desc.set_name("output-1");
  queue_desc.set_type(1);  // queue
  *exchange_plan->add_queues() = queue_desc;
  queue_desc.set_type(2);  // tag
  *exchange_plan->add_queues() = queue_desc;
  auto input_binding = exchange_plan->add_bindings();
  input_binding->set_src_queue_index(0);
  input_binding->set_dst_queue_index(1);
  auto output_binding = exchange_plan->add_bindings();
  output_binding->set_src_queue_index(2);
  output_binding->set_dst_queue_index(3);

  auto submodel_desc = pre_deploy_req_body->add_submodels();
  submodel_desc->set_model_size(128);
  submodel_desc->set_model_name("any-model");
  submodel_desc->add_input_queue_indices(1);
  submodel_desc->add_output_queue_indices(2);

  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(pre_deploy_request, response), ge::SUCCESS);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }
}

TEST_F(STEST_helper_runtime, send_quest_success) {
  const char *path = "../tests/st/config_file/right_json/device";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitDeviceInformation(real_path);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });

  sleep(1);
  path = "../tests/st/config_file/right_json/host";
  realpath(path, real_path);
  Configurations::GetInstance().InitHostInformation(real_path);
  ge::DeviceManger device_manger;
  ASSERT_EQ(device_manger.Initialize(), ge::SUCCESS);
  const DeviceInfo *device_info = nullptr;
  ASSERT_EQ(device_manger.GetDeviceInfo(0, &device_info), ge::SUCCESS);
  HostInfo host_info = device_manger.GetHostInfo();

  std::vector<DeviceInfo> device_list = device_manger.GetDeviceInfoList();
  std::vector<RemoteDevicePtr> clients;
  for (const auto &device : device_list) {
    RemoteDevicePtr ptr(new RemoteDevice(device));
    ASSERT_EQ(ptr->Initialize(), ge::SUCCESS);
    clients.emplace_back(ptr);
  }

  SendPreDeployRequest(clients);

  ModelBufferData model_buffer_data{};
  BuildModel(model_buffer_data);
  deployer::DeployerResponse response;
  deployer::DeployerRequest pre_download_request;
  pre_download_request.set_type(deployer::kPreDownloadModel);
  auto request1 = pre_download_request.mutable_pre_download_request();
  request1->set_model_id(0);
  request1->set_total_size(model_buffer_data.length);
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(pre_download_request, response), ge::SUCCESS);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  const char *content = "helper";
  deployer::DeployerRequest download_request;
  download_request.set_type(deployer::kDownloadModel);
  auto request2 = download_request.mutable_download_model_request();
  request2->set_model_id(0);
  request2->set_offset(0);
  request2->set_om_content(model_buffer_data.data.get(), model_buffer_data.length);
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(download_request, response), ge::SUCCESS);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  deployer::DeployerRequest load_model_request;
  load_model_request.set_type(deployer::kLoadModel);
  auto request3 = load_model_request.mutable_load_model_request();
  request3->set_model_id(0);
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(load_model_request, response), ge::SUCCESS);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  deployer::DeployerRequest unload_model_request;
  unload_model_request.set_type(deployer::kUnloadModel);
  auto request4 = unload_model_request.mutable_unload_model_request();
  request4->set_model_id(0);
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(unload_model_request, response), ge::SUCCESS);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }
  sleep(10);
  for (auto &client : clients) {
    ASSERT_EQ(client->Finalize(), ge::SUCCESS);
  }

  grpc_server.Finalize();
  server_thread.join();
}

void StartClient() {
  ge::DeviceManger device_manger;
  ASSERT_EQ(device_manger.Initialize(), ge::SUCCESS);
  const DeviceInfo *device_info = nullptr;
  ASSERT_EQ(device_manger.GetDeviceInfo(0, &device_info), ge::SUCCESS);
  HostInfo host_info = device_manger.GetHostInfo();

  std::vector<DeviceInfo> device_list = device_manger.GetDeviceInfoList();
  std::vector<RemoteDevicePtr> clients;
  for (const auto &device : device_list) {
    RemoteDevicePtr ptr(new RemoteDevice(device));
    ptr->Initialize();
    // ASSERT_EQ(ptr->Initialize(), ge::SUCCESS);
    clients.emplace_back(ptr);
  }

  ModelBufferData model_buffer_data{};
  BuildModel(model_buffer_data);
  deployer::DeployerResponse response;
  deployer::DeployerRequest pre_download_request;
  pre_download_request.set_type(deployer::kPreDownloadModel);
  auto request1 = pre_download_request.mutable_pre_download_request();
  request1->set_model_id(0);
  request1->set_total_size(model_buffer_data.length);
  for (auto &client : clients) {
    // ASSERT_EQ(client->SendRequest(pre_download_request, response), ge::SUCCESS);
    client->SendRequest(pre_download_request, response);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  deployer::DeployerRequest download_request;
  download_request.set_type(deployer::kDownloadModel);
  auto request2 = download_request.mutable_download_model_request();
  request2->set_model_id(0);
  request2->set_offset(0);
  request2->set_om_content(model_buffer_data.data.get(), model_buffer_data.length);
  for (auto &client : clients) {
    // ASSERT_EQ(client->SendRequest(download_request, response), ge::SUCCESS);
    client->SendRequest(download_request, response);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  deployer::DeployerRequest load_model_request;
  load_model_request.set_type(deployer::kLoadModel);
  auto request3 = load_model_request.mutable_load_model_request();
  request3->set_model_id(0);
  for (auto &client : clients) {
    // ASSERT_EQ(client->SendRequest(load_model_request, response), ge::SUCCESS);
    client->SendRequest(load_model_request, response);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  deployer::DeployerRequest unload_model_request;
  unload_model_request.set_type(deployer::kUnloadModel);
  auto request4 = unload_model_request.mutable_unload_model_request();
  request4->set_model_id(0);
  for (auto &client : clients) {
    // ASSERT_EQ(client->SendRequest(unload_model_request, response), ge::SUCCESS);
    client->SendRequest(unload_model_request, response);
    // ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }
  for (auto &client : clients) {
    ASSERT_EQ(client->Finalize(), ge::SUCCESS);
  }
}


TEST_F(STEST_helper_runtime, multi_thread_send_quest_success) {
  Configurations::GetInstance().InitHostInformation("../tests/st/config_file/right_json/host");
  Configurations::GetInstance().InitDeviceInformation("../tests/st/config_file/right_json/device");
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });
  sleep(1);
  std::thread client_thread[10];
  const auto &all_options = GetThreadLocalContext().GetAllOptions();
  for (int i = 0; i < 10; i++) {
    client_thread[i] = std::thread([&]() {
      GetThreadLocalContext().SetGlobalOption(all_options);
      StartClient();
    });
  }
  for (int i = 0 ;i < 10; i++) {
    client_thread[i].join();
  }
  grpc_server.Finalize();
  server_thread.join();
}

TEST_F(STEST_helper_runtime, json_parser_fail) {
  ASSERT_NE(Configurations::GetInstance().InitHostInformation(""), ge::SUCCESS);
  const char *path = "../tests/st/config_file/wrong_json/value_fail/host";
  ASSERT_NE(Configurations::GetInstance().InitHostInformation(path), ge::SUCCESS);
  ASSERT_NE(Configurations::GetInstance().InitDeviceInformation(path), ge::SUCCESS);
}

TEST_F(STEST_helper_runtime, check_token_fail) {
  const char *path = "../tests/st/config_file/wrong_json/token_fail/device";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitDeviceInformation(real_path);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });

  sleep(1);
  path = "../tests/st/config_file/wrong_json/token_fail/host";
  realpath(path, real_path);
  setenv(config_path, real_path, 1);
  ge::DeviceManger device_manger;
  ASSERT_EQ(device_manger.Initialize(), ge::SUCCESS);
  std::vector<DeviceInfo> device_list = device_manger.GetDeviceInfoList();
  std::vector<RemoteDevicePtr> clients;
  for (const auto &device : device_list) {
    RemoteDevicePtr ptr(new RemoteDevice(device));
    ASSERT_NE(ptr->Initialize(), ge::SUCCESS);
    clients.emplace_back(ptr);
  }
  grpc_server.Finalize();
  server_thread.join();
}

TEST_F(STEST_helper_runtime, server_same_port_fail) {
  const char *path = "../tests/st/config_file/right_json/device";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitDeviceInformation(real_path);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });

  ge::GrpcServer grpc_server1;
  std::thread server_thread1 = std::thread([&]() {
    StartServer(grpc_server1);
  });

  sleep(1);
  grpc_server.Finalize();
  grpc_server1.Finalize();
  server_thread.join();
  server_thread1.join();
}

TEST_F(STEST_helper_runtime, client_not_init) {
  const char *path = "../tests/st/config_file/right_json/host";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitHostInformation(real_path);

  ge::DeviceManger device_manger;
  ASSERT_EQ(device_manger.Initialize(), ge::SUCCESS);
  const DeviceInfo *device_info = nullptr;
  ASSERT_EQ(device_manger.GetDeviceInfo(0, &device_info), ge::SUCCESS);

  std::vector<DeviceInfo> device_list = device_manger.GetDeviceInfoList();
  std::vector<RemoteDevicePtr> clients;
  for (const auto &device : device_list) {
    RemoteDevicePtr ptr(new RemoteDevice(device));
    clients.emplace_back(ptr);
  }

  deployer::DeployerResponse response;
  deployer::DeployerRequest pre_download_request;
  pre_download_request.set_type(deployer::kPreDownloadModel);
  auto request1 = pre_download_request.mutable_pre_download_request();
  request1->set_model_id(0);
  request1->set_total_size(6);
  for (auto &client : clients) {
    ASSERT_NE(client->SendRequest(pre_download_request, response), ge::SUCCESS);
  }

  for (auto &client : clients) {
    ASSERT_EQ(client->Finalize(), ge::SUCCESS);
  }
}

TEST_F(STEST_helper_runtime, parser_wrong_ip_fail) {
  const char *path = "../tests/st/config_file/wrong_json/ip_fail/device";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitDeviceInformation(real_path);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });

  sleep(1);
  grpc_server.Finalize();
  server_thread.join();
}

TEST_F(STEST_helper_runtime, send_model_fail) {
  const char *path = "../tests/st/config_file/right_json/device";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitDeviceInformation(real_path);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });

  sleep(1);

  path = "../tests/st/config_file/right_json/host";
  realpath(path, real_path);
  Configurations::GetInstance().InitHostInformation(real_path);
  ge::DeviceManger device_manger;
  ASSERT_EQ(device_manger.Initialize(), ge::SUCCESS);
  HostInfo host_info = device_manger.GetHostInfo();

  std::vector<DeviceInfo> device_list = device_manger.GetDeviceInfoList();
  std::vector<RemoteDevicePtr> clients;
  for (const auto &device : device_list) {
    RemoteDevicePtr ptr(new RemoteDevice(device));
    ASSERT_EQ(ptr->Initialize(), ge::SUCCESS);
    clients.emplace_back(ptr);
  }

  deployer::DeployerResponse response;
  deployer::DeployerRequest pre_download_request;
  pre_download_request.set_type(deployer::kPreDownloadModel);
  auto request1 = pre_download_request.mutable_pre_download_request();
  request1->set_model_id(0);
  request1->set_total_size(14);
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(pre_download_request, response), ge::SUCCESS);
    ASSERT_EQ(response.error_code(), ge::SUCCESS);
  }

  std::string content = "helper_runtime";
  deployer::DeployerRequest download_request;
  download_request.set_type(deployer::kDownloadModel);
  auto request2 = download_request.mutable_download_model_request();
  request2->set_model_id(0);
  request2->set_offset(0);
  request2->set_om_content(content.substr(0, 5));
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(download_request, response), ge::SUCCESS);
    ASSERT_NE(response.error_code(), ge::SUCCESS);
  }
  request2->set_offset(5);
  request2->set_om_content(content);
  for (auto &client : clients) {
    ASSERT_EQ(client->SendRequest(download_request, response), ge::SUCCESS);
    ASSERT_NE(response.error_code(), ge::SUCCESS);
  }

  for (auto &client : clients) {
    ASSERT_EQ(client->Finalize(), ge::SUCCESS);
  }
  grpc_server.Finalize();
  server_thread.join();
}

TEST_F(STEST_helper_runtime, InitHostDatagw) {
  std::string group_name = "DM_QS_GROUP";
  ge::MemoryGroupManager::GetInstance().MemGroupInit(group_name);
  uint32_t device_id = 0;
  uint32_t vf_id = 0;
  pid_t dgw_pid = 0;
  ge::DataGwManager::GetInstance().InitHostDataGwServer(device_id, vf_id, group_name, dgw_pid);
}

TEST_F(STEST_helper_runtime, Init_GetIp_And_Port_Success) {
  std::string group_name = "DM_QS_GROUP";
  ge::MemoryGroupManager::GetInstance().MemGroupInit(group_name);
  uint32_t device_id = 0;
  uint32_t vf_id = 0;
  pid_t dgw_pid = 0;
  ge::DataGwManager::GetInstance().InitQueue(device_id);
  MockDataGwManager mock;
  EXPECT_CALL(mock, InitHostDataGwServer).WillRepeatedly(Return(ge::SUCCESS));

  const char *path = "../tests/st/config_file/right_json/host";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitHostInformation(real_path);
  ge::Status res = ge::NetworkManager::GetInstance().Initialize();
  ASSERT_EQ(res, ge::SUCCESS);
  std::string ip = "";
  ge::NetworkManager::GetInstance().GetDataPanelIp(ip);
  printf("get ip = %s \n", ip.c_str());

  int32_t port = ge::NetworkManager::GetInstance().GetDataPanelPort();
  printf("get port %d \n", port);
}

TEST_F(STEST_helper_runtime, Init_Host_Bind_Queue_And_Tag_Success) {
  std::string group_name = "DM_QS_GROUP";
  ge::MemoryGroupManager::GetInstance().MemGroupInit(group_name);
  uint32_t device_id = 0;
  uint32_t vf_id = 0;
  pid_t dgw_pid = 0;
  ge::Status res = ge::SUCCESS;

  res = ge::DataGwManager::GetInstance().InitQueue(device_id);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("InitQueue failed \n");
  }
  MockDataGwManager mock;
  EXPECT_CALL(mock, InitHostDataGwServer).WillRepeatedly(Return(ge::SUCCESS));
  printf("dgw pid=%d \n", dgw_pid);
  dgw_pid = 9900;
  res = ge::DataGwManager::GetInstance().InitDataGwClient(dgw_pid, device_id);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("InitDataGwClient failed \n");
  }
  const char *path = "../tests/st/config_file/right_json/host";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitHostInformation(real_path);
  res = ge::NetworkManager::GetInstance().Initialize();
  if (res != ge::SUCCESS) {
    printf("Initialize failed \n");
  }
  ASSERT_EQ(res, ge::SUCCESS);
  std::string master_ip = "";
  ge::NetworkManager::GetInstance().GetDataPanelIp(master_ip);
  printf("get ip = %s \n", master_ip.c_str());

  int32_t port = ge::NetworkManager::GetInstance().GetDataPanelPort();
  printf("get port %d \n", port);
  uint32_t qid = 0;
  CreateQueue(device_id, qid);
  uint64_t hcom_handle = 0;
  int32_t hcom_tag = 0;
  bqs::CreateHcomInfo hcom_info;
  strcpy(hcom_info.masterIp, master_ip.c_str());
  hcom_info.masterPort = port;

  std::string local_ip = "127.0.0.1";
  strcpy(hcom_info.localIp, local_ip.c_str());
  hcom_info.localPort = 1;

  std::string remote_ip = "127.0.0.1";
  strcpy(hcom_info.remoteIp, remote_ip.c_str());
  hcom_info.remotePort = 2;
  res = ge::DataGwManager::GetInstance().CreateDataGwHandle(device_id, hcom_info);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("CreateDataGwHandle failed \n");
  }

  res = ge::DataGwManager::GetInstance().CreateDataGwTag(hcom_info.handle, "tag_name", device_id, hcom_tag);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("CreateDataGwTag failed \n");
  }

  bqs::Route queue_route;
  queue_route.src.type = bqs::EndpointType::QUEUE;
  queue_route.src.attr.queueAttr.queueId = qid;
  queue_route.dst.type = bqs::EndpointType::NAMED_COMM_CHANNEL;
  strcpy(queue_route.dst.attr.namedChannelAttr.localIp, local_ip.c_str());
  queue_route.dst.attr.namedChannelAttr.localPort = 1;
  strcpy(queue_route.dst.attr.namedChannelAttr.peerIp, remote_ip.c_str());
  queue_route.dst.attr.namedChannelAttr.peerPort = 2;
  strcpy(queue_route.dst.attr.namedChannelAttr.name, "tag_name");

  res = ge::DataGwManager::GetInstance().BindQueues(device_id, queue_route);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("BindQueues failed \n");
  }

  res = ge::DataGwManager::GetInstance().UnbindQueues(device_id, queue_route);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("UnbindQueues failed \n");
  }

  res = ge::DataGwManager::GetInstance().DestroyDataGwHandle(device_id, hcom_info.handle);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("DestroyDataGwHandle failed \n");
  }

  res = ge::DataGwManager::GetInstance().Shutdown();
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("Shutdown failed \n");
  }

  res = ge::DataGwManager::GetInstance().DestroyDataGwTag(device_id, hcom_info.handle, hcom_tag);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("DestroyDataGwTag failed \n");
  }

  res = ge::NetworkManager::GetInstance().Finalize();
}

TEST_F(STEST_helper_runtime, Init_Host_Bind_Tag_And_Queue_Success) {
  std::string group_name = "DM_QS_GROUP";
  ge::MemoryGroupManager::GetInstance().MemGroupInit(group_name);
  uint32_t device_id = 0;
  uint32_t vf_id = 0;
  pid_t dgw_pid = 0;
  ge::Status res = ge::SUCCESS;

  res = ge::DataGwManager::GetInstance().InitQueue(device_id);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("InitQueue failed \n");
  }
  MockDataGwManager mock;
  EXPECT_CALL(mock, InitHostDataGwServer).WillRepeatedly(Return(ge::SUCCESS));
  printf("dgw pid=%d \n", dgw_pid);
  dgw_pid = 9900;
  res = ge::DataGwManager::GetInstance().InitDataGwClient(dgw_pid, device_id);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("InitDataGwClient failed \n");
  }
  const char *path = "../tests/st/config_file/right_json/host";
  char real_path[200];
  realpath(path, real_path);
  Configurations::GetInstance().InitHostInformation(real_path);
  res = ge::NetworkManager::GetInstance().Initialize();
  if (res != ge::SUCCESS) {
    printf("Initialize failed \n");
  }
  ASSERT_EQ(res, ge::SUCCESS);

  std::string master_ip = "";
  ge::NetworkManager::GetInstance().GetDataPanelIp(master_ip);
  printf("get ip = %s \n", master_ip.c_str());

  int32_t port = ge::NetworkManager::GetInstance().GetDataPanelPort();
  printf("get port %d \n", port);
  uint32_t qid = 0;
  CreateQueue(device_id, qid);
  uint64_t hcom_handle = 0;
  int32_t hcom_tag = 0;
  bqs::CreateHcomInfo hcom_info;
  strcpy(hcom_info.masterIp, master_ip.c_str());
  hcom_info.masterPort = port;

  std::string local_ip = "127.0.0.1";
  strcpy(hcom_info.localIp, local_ip.c_str());
  hcom_info.localPort = 1;

  std::string remote_ip = "127.0.0.1";
  strcpy(hcom_info.remoteIp, remote_ip.c_str());
  hcom_info.remotePort = 2;
  res = ge::DataGwManager::GetInstance().CreateDataGwHandle(device_id, hcom_info);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("CreateDataGwHandle failed \n");
  }

  res = ge::DataGwManager::GetInstance().CreateDataGwTag(hcom_info.handle, "tag_name", device_id, hcom_tag);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("CreateDataGwTag failed \n");
  }

  bqs::Route queue_route;
  queue_route.src.type = bqs::EndpointType::NAMED_COMM_CHANNEL;
  strcpy(queue_route.src.attr.namedChannelAttr.localIp, local_ip.c_str());
  queue_route.src.attr.namedChannelAttr.localPort = 1;
  strcpy(queue_route.src.attr.namedChannelAttr.peerIp, remote_ip.c_str());
  queue_route.src.attr.namedChannelAttr.peerPort = 2;
  strcpy(queue_route.src.attr.namedChannelAttr.name, "tag_name");
  queue_route.dst.type = bqs::EndpointType::QUEUE;
  queue_route.dst.attr.queueAttr.queueId = qid;

  res = ge::DataGwManager::GetInstance().BindQueues(device_id, queue_route);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("BindQueues failed \n");
  }

  res = ge::DataGwManager::GetInstance().UnbindQueues(device_id, queue_route);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("UnbindQueues failed \n");
  }

  res = ge::DataGwManager::GetInstance().DestroyDataGwHandle(device_id, hcom_info.handle);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("DestroyDataGwHandle failed \n");
  }

  res = ge::DataGwManager::GetInstance().Shutdown();
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("Shutdown failed \n");
  }

  res = ge::DataGwManager::GetInstance().DestroyDataGwTag(device_id, hcom_info.handle, hcom_tag);
  ASSERT_EQ(res, ge::SUCCESS);
  if (res != ge::SUCCESS) {
    printf("DestroyDataGwTag failed \n");
  }

  res = ge::NetworkManager::GetInstance().Finalize();
}


TEST_F(STEST_helper_runtime, test_var_manager_serial_deserial) {
  const map<string, string> options{{"ge.graphMemoryMaxSize", "536870912"}};
  Status ret = VarManager::Instance(1)->Init(0x5a, 1, 0, 0x5a5a);
  ret = VarManager::Instance(1)->SetMemoryMallocSize(options, 1024UL * 1024UL * 1024UL);
  size_t graph_mem_max_size = VarManager::Instance(1)->graph_mem_max_size_;
  size_t var_mem_max_size = VarManager::Instance(1)->var_mem_max_size_;
  size_t var_mem_logic_base = VarManager::Instance(1)->var_mem_logic_base_;
  size_t use_max_mem_size = VarManager::Instance(1)->use_max_mem_size_;
  std::vector<int64_t> s = {1,2,3,4};
  GeShape shape(s);
  GeTensorDesc tensor_desc(shape);
  TensorUtils::SetSize(tensor_desc, shape.GetShapeSize());
  std::string str = "global_step";
  ret = VarManager::Instance(1)->AssignVarMem(str, tensor_desc, RT_MEMORY_HBM);
  EXPECT_EQ(ret, SUCCESS);
  TransNodeInfo trans_node_info;
  VarTransRoad fusion_road;
  fusion_road.emplace_back(trans_node_info);
  VarManager::Instance(1)->SetTransRoad(str, fusion_road);

  VarBroadCastInfo broadcast_info;
  broadcast_info.var_name = "test";
  VarManager::Instance(1)->SaveBroadCastInfo(0, broadcast_info);

  deployer::VarManagerInfo info;
  ret = VarManager::Instance(1)->VarManagerToSerial(1, info);
  EXPECT_EQ(ret, SUCCESS);
  auto session_id = info.session_id();
  EXPECT_EQ(session_id, 1);

  ret = VarManager::Instance(1)->VarManagerToDeserial(1, info);
  EXPECT_EQ(ret, SUCCESS);
  EXPECT_EQ(VarManager::Instance(1)->graph_mem_max_size_, floor(1024UL * 1024UL * 1024UL / 2));
  EXPECT_EQ(VarManager::Instance(1)->var_mem_max_size_, floor(1024UL * 1024UL * 1024UL * (5.0f / 32.0f)));
  EXPECT_EQ(VarManager::Instance(1)->version_, 0x5a);
  EXPECT_EQ(VarManager::Instance(1)->device_id_, 0);
  EXPECT_EQ(VarManager::Instance(1)->job_id_, 0x5a5a);
  EXPECT_EQ(VarManager::Instance(1)->graph_mem_max_size_, graph_mem_max_size);
  EXPECT_EQ(VarManager::Instance(1)->var_mem_max_size_, var_mem_max_size);
  EXPECT_EQ(VarManager::Instance(1)->var_mem_logic_base_, var_mem_logic_base);
  EXPECT_EQ(VarManager::Instance(1)->use_max_mem_size_, use_max_mem_size);
  EXPECT_EQ(VarManager::Instance(1)->var_resource_->session_id_, 1);

  EXPECT_EQ(VarManager::Instance(1)->var_resource_->var_offset_map_.size(), 1);
  EXPECT_EQ(VarManager::Instance(1)->var_resource_->var_addr_mgr_map_.size(), 1);
  EXPECT_EQ(VarManager::Instance(1)->var_resource_->cur_var_tensor_desc_map_.size(), 1);

  EXPECT_EQ(VarManager::Instance(1)->var_resource_->IsVarExist(str, tensor_desc), true);
  uintptr_t ptr = reinterpret_cast<uintptr_t>(VarManager::Instance(1)->GetVarMemoryBase(RT_MEMORY_HBM, 0));
  EXPECT_EQ(ptr, 0);
  EXPECT_EQ(VarManager::Instance(1)->mem_resource_map_.size(), 1);
  auto resource_src = VarManager::Instance(1)->mem_resource_map_[RT_MEMORY_HBM];
  auto resource = VarManager::Instance(1)->mem_resource_map_[RT_MEMORY_HBM];
  EXPECT_EQ(resource->var_mem_size_, 1536);
  EXPECT_EQ(resource->var_mem_size_, resource_src->var_mem_size_);

  ret = VarManager::Instance(1)->AssignVarMem("Hello_variable", tensor_desc, RT_MEMORY_HBM);
  EXPECT_EQ(ret, SUCCESS);

  EXPECT_EQ(resource->total_size_, resource_src->total_size_);
  EXPECT_EQ(resource->total_size_, var_mem_max_size);
}

TEST_F(STEST_helper_runtime, Init_Device_Dgw_Server_Success) {
  std::string group_name = "DM_QS_GROUP";
  ge::MemoryGroupManager::GetInstance().MemGroupInit(group_name);
  uint32_t device_id = 0;
  uint32_t vf_id = 0;
  pid_t dgw_pid = 0;
  ge::Status res = ge::DataGwManager::GetInstance().InitDeviceDataGwServer(device_id, group_name, dgw_pid);
  ASSERT_EQ(res, ge::SUCCESS);
}

TEST_F(STEST_helper_runtime, ProcessMultiVarManager) {
  const map<string, string> options{{"ge.graphMemoryMaxSize", "536870912"}};
  Status ret = VarManager::Instance(1)->Init(0x5a, 1, 0, 0x5a5a);
  ret = VarManager::Instance(1)->SetMemoryMallocSize(options, 1024UL * 1024UL * 1024UL);
  EXPECT_EQ(ret, SUCCESS);

  std::vector<int64_t> s = {1,2,3,4};
  GeShape shape(s);
  GeTensorDesc tensor_desc(shape);
  TensorUtils::SetSize(tensor_desc, shape.GetShapeSize());
  ret = VarManager::Instance(1)->AssignVarMem("var_node", tensor_desc, RT_MEMORY_HBM);
  EXPECT_EQ(ret, SUCCESS);

  ge::Client client;
  deployer::DeployerResponse response;
  deployer::MultiVarManagerRequest info;
  ret = client.ProcessMultiVarManager(info, response);
  ASSERT_EQ(ret, ge::SUCCESS);
  deployer::SharedContentDescRequest shared_request;
  auto shared_info = shared_request.mutable_shared_content_desc();
  shared_info->set_session_id(1);
  shared_info->set_mem_type(2);
  shared_info->set_total_length(5);
  shared_info->set_node_name("var_node");
  ret = client.ProcessSharedContent(shared_request, response);
  ASSERT_EQ(ret, ge::SUCCESS);
}

TEST_F(STEST_helper_runtime, TestDeployModel) {
  mock_handle = (void *)0xffffffff;
  mock_method = (void *)&MockInitializeHelperRuntime;
  ASSERT_EQ(ExecutionRuntime::InitHeterogeneousRuntime(), SUCCESS);

  std::vector<std::string> engine_list = {"AIcoreEngine"};
  auto add_1 = OP_CFG(ADD);
  auto add_2 = OP_CFG(ADD);
  auto data1 = OP_CFG(DATA);
  auto data2 = OP_CFG(DATA);
  auto data3 = OP_CFG(DATA);
  DEF_GRAPH(g1) {
    CHAIN(NODE("data_1", data1)->EDGE(0, 0)->NODE("add_1", add_1)->EDGE(0, 0)->NODE("add_2", add_2));
    CHAIN(NODE("data_2", data2)->EDGE(0, 1)->NODE("add_1", add_1));
    CHAIN(NODE("data_3", data3)->EDGE(0, 1)->NODE("add_2", add_2));
    CHAIN(NODE("add_2", add_2)->EDGE(0, 0)->NODE("Node_Output", NETOUTPUT));
  };

  auto graph = ToGeGraph(g1);
  map<AscendString, AscendString> options;
  Session session(options);
  session.AddGraph(1, graph, options);
  std::vector<InputTensorInfo> inputs;
  auto ret = session.BuildGraph(1, inputs);
  ASSERT_EQ(ret, SUCCESS);

  Shape shape({1, 1, 224, 224});
  TensorDesc tensor_desc(shape, FORMAT_NCHW, DT_FLOAT);
  std::vector<Tensor> input_tensors;
  for (int i = 0; i < 3; ++i) {
    std::vector<uint8_t> data(224 * 224 * sizeof(float));
    Tensor tensor(tensor_desc, data);
    input_tensors.emplace_back(tensor);
  }

  std::vector<Tensor> output_tensors;
  ret = session.RunGraph(1, input_tensors, output_tensors);
  ASSERT_EQ(ret, SUCCESS);
  ASSERT_EQ(output_tensors.size(), 1);

  std::mutex mu;
  std::condition_variable cv;
  bool done = false;
  auto callback = [&](Status status, std::vector<ge::Tensor> &outputs) {
    std::unique_lock<std::mutex> lk(mu);
    done = true;
    ret = status;
    cv.notify_all();
  };
  session.RunGraphAsync(1, input_tensors, callback);
  std::unique_lock<std::mutex> lk(mu);
  cv.wait_for(lk, std::chrono::seconds(5), [&]() { return done; });
  ASSERT_EQ(ret, SUCCESS);
}

TEST_F(STEST_helper_runtime, TestDeployModelNoTiling) {
  mock_handle = (void *)0xffffffff;
  mock_method = (void *)&MockInitializeHelperRuntime;
  ASSERT_EQ(ExecutionRuntime::InitHeterogeneousRuntime(), SUCCESS);

  std::vector<std::string> engine_list = {"AIcoreEngine"};
  auto add_1 = OP_CFG(ADD).Attr(ATTR_NAME_OP_NO_TILING, true);
  auto add_2 = OP_CFG(ADD).Attr(ATTR_NAME_OP_NO_TILING, true);
  auto data1 = OP_CFG(DATA).Attr(ATTR_NAME_OP_NO_TILING, true);
  auto data2 = OP_CFG(DATA);
  auto data3 = OP_CFG(DATA);
  DEF_GRAPH(g1) {
    CHAIN(NODE("data_1", data1)->EDGE(0, 0)->NODE("add_1", add_1)->EDGE(0, 0)->NODE("add_2", add_2));
    CHAIN(NODE("data_2", data2)->EDGE(0, 1)->NODE("add_1", add_1));
    CHAIN(NODE("data_3", data3)->EDGE(0, 1)->NODE("add_2", add_2));
    CHAIN(NODE("add_2", add_2)->EDGE(0, 0)->NODE("Node_Output", NETOUTPUT));
  };

  const auto SetUnknownOpKernelForNoTiling = [](const ComputeGraph::Vistor<NodePtr> &all_nodes) {
    GeTensorDesc tensor0(GeShape({1, -1, 224, 224}), FORMAT_NCHW, DT_INT64);
    std::vector<std::pair<int64_t, int64_t>> tensor0_range;
    tensor0_range.push_back(std::make_pair(1, 1));
    tensor0_range.push_back(std::make_pair(1, 1));
    tensor0_range.push_back(std::make_pair(224, 224));
    tensor0_range.push_back(std::make_pair(224, 224));
    tensor0.SetShapeRange(tensor0_range);
    TensorUtils::SetSize(tensor0, 501760);
    AttrUtils::SetBool(tensor0, ATTR_NAME_TENSOR_NO_TILING_MEM_TYPE, true);
    AttrUtils::SetInt(tensor0, ATTR_NAME_TENSOR_DESC_MEM_OFFSET, 0);
    std::vector<int64_t> max_shape_list = {1, 10, 224, 224};
    AttrUtils::SetListInt(tensor0, ATTR_NAME_TENSOR_MAX_SHAPE, max_shape_list);

    GeTensorDesc tensor1(GeShape({1, -1, 224, 224}), FORMAT_NCHW, DT_INT64);
    std::vector<std::pair<int64_t, int64_t>> tensor1_range;
    tensor1_range.push_back(std::make_pair(1, 1));
    tensor1_range.push_back(std::make_pair(1, 10));
    tensor1_range.push_back(std::make_pair(224, 224));
    tensor1_range.push_back(std::make_pair(224, 224));
    tensor1.SetShapeRange(tensor1_range);
    TensorUtils::SetSize(tensor1, 501760);
    AttrUtils::SetBool(tensor1, ATTR_NAME_TENSOR_NO_TILING_MEM_TYPE, true);
    AttrUtils::SetInt(tensor1, ATTR_NAME_TENSOR_DESC_MEM_OFFSET, 1024);
    AttrUtils::SetListInt(tensor1, ATTR_NAME_TENSOR_MAX_SHAPE, max_shape_list);

    for (const auto node : all_nodes) {
      const auto op_desc = node->GetOpDesc();
      if (op_desc->GetType() == DATA) {
        op_desc->SetOpKernelLibName("AiCoreLib");
        op_desc->SetOpEngineName("AIcoreEngine");
        op_desc->UpdateOutputDesc(0, tensor0);
        op_desc->SetOutputOffset({2048});
        op_desc->SetWorkspace({});
        op_desc->SetWorkspaceBytes({});
      } else if (op_desc->GetType() == ADD) {
        op_desc->SetOpKernelLibName("AiCoreLib");
        op_desc->SetOpEngineName("AIcoreEngine");
        op_desc->UpdateInputDesc(0, tensor0);
        op_desc->UpdateOutputDesc(0, tensor1);
        op_desc->SetInputOffset({2048});
        op_desc->SetOutputOffset({2112});
        op_desc->SetWorkspace({});
        op_desc->SetWorkspaceBytes({});
        vector<std::string> tiling_inline;
        vector<std::string> export_shape;
        AttrUtils::GetListStr(op_desc, ATTR_NAME_OP_TILING_INLINE_ENGINE, tiling_inline);
        tiling_inline.push_back("AIcoreEngine");
        AttrUtils::SetListStr(op_desc, ATTR_NAME_OP_TILING_INLINE_ENGINE, tiling_inline);
        AttrUtils::GetListStr(op_desc, ATTR_NAME_OP_EXPORT_SHAPE_ENGINE, export_shape);
        export_shape.push_back("AIcoreEngine");
        AttrUtils::SetListStr(op_desc, ATTR_NAME_OP_EXPORT_SHAPE_ENGINE, export_shape);
      } else {
        op_desc->SetOpKernelLibName("AiCoreLib");
        op_desc->SetOpEngineName("AIcoreEngine");
        op_desc->UpdateInputDesc(0, tensor1);
        op_desc->UpdateOutputDesc(0, tensor1);
        op_desc->SetInputOffset({2112});
        op_desc->SetSrcName({"add"});
        op_desc->SetSrcIndex({0});
      }
    }
  };
  auto compute_graph = ToComputeGraph(g1);
  EXPECT_NE(compute_graph, nullptr);
  SetUnknownOpKernelForNoTiling(compute_graph->GetDirectNode());

  auto graph = GraphUtils::CreateGraphFromComputeGraph(compute_graph);
  map<AscendString, AscendString> options;
  Session session(options);
  session.AddGraph(1, graph, options);
  std::vector<InputTensorInfo> inputs;
  auto ret = session.BuildGraph(1, inputs);
  ASSERT_EQ(ret, SUCCESS);

  Shape shape({1, 1, 224, 224});
  TensorDesc tensor_desc(shape, FORMAT_NCHW, DT_FLOAT);
  std::vector<Tensor> input_tensors;
  for (int i = 0; i < 3; ++i) {
    std::vector<uint8_t> data(224 * 224 * sizeof(float));
    Tensor tensor(tensor_desc, data);
    input_tensors.emplace_back(tensor);
  }

  std::vector<Tensor> output_tensors;
  ret = session.RunGraph(1, input_tensors, output_tensors);
  ASSERT_EQ(ret, SUCCESS);
  ASSERT_EQ(output_tensors.size(), 1);

  std::mutex mu;
  std::condition_variable cv;
  bool done = false;
  auto callback = [&](Status status, std::vector<ge::Tensor> &outputs) {
    std::unique_lock<std::mutex> lk(mu);
    done = true;
    ret = status;
    cv.notify_all();
  };
  session.RunGraphAsync(1, input_tensors, callback);
  std::unique_lock<std::mutex> lk(mu);
  cv.wait_for(lk, std::chrono::seconds(5), [&]() { return done; });
  ASSERT_EQ(ret, SUCCESS);
}

namespace {
class MockRuntime : public RuntimeStub {
 public:
  rtError_t rtGetIsHeterogenous(int32_t *heterogeneous) override {
    *heterogeneous = 1;
    return RT_ERROR_NONE;
  }
};

class MockRuntime2PG : public RuntimeStub {
 public:
  rtError_t rtGetIsHeterogenous(int32_t *heterogeneous) override {
    *heterogeneous = 1;
    return RT_ERROR_NONE;
  }

  rtError_t rtGetDeviceCount(int32_t *count) override {
    *count = 2;
    return RT_ERROR_NONE;
  }
};

class MockMmpaForHelperRuntime : public MmpaStubApi {
 public:
  void *DlOpen(const char *file_name, int32_t mode) {
    if (std::string(file_name) == "libgrpc_client.so") {
      return (void *) 0x12345678;
    }
    return dlopen(file_name, mode);
  }

  void *DlSym(void *handle, const char *func_name) override {
    if (std::string(func_name) == "InitializeHelperRuntime") {
      return (void *) &InitializeHelperRuntime;
    }
    return dlsym(handle, func_name);
  }

  int32_t DlClose(void *handle) override {
    if (handle == (void *) 0x12345678) {
      return 0;
    }
    return dlclose(handle);
  }
};

Graph BuildGraph() {
  DEF_GRAPH(graph_def) {
    auto arg_0 = OP_CFG(DATA)
        .InCnt(1)
        .OutCnt(1)
        .Attr(ATTR_NAME_INDEX, 0)
        .TensorDesc(FORMAT_ND, DT_INT32, {16});

    auto var = OP_CFG(VARIABLE)
        .InCnt(1)
        .OutCnt(1)
        .Attr(ATTR_NAME_INDEX, 0)
        .TensorDesc(FORMAT_ND, DT_INT32, {16});

    auto neg_1 = OP_CFG(NEG)
        .InCnt(1)
        .OutCnt(1)
        .TensorDesc(FORMAT_ND, DT_INT32, {16});

    auto net_output = OP_CFG(NETOUTPUT)
        .InCnt(2)
        .OutCnt(1)
        .TensorDesc(FORMAT_ND, DT_INT32, {16});

    CHAIN(NODE("arg_0", arg_0)
              ->NODE("neg_1", neg_1)
              ->NODE("Node_Output", net_output));

    CHAIN(NODE("var", var)
              ->NODE("Node_Output", net_output));
  };

  auto graph = ToGeGraph(graph_def);
  return graph;
}
}  // namespace

TEST_F(STEST_helper_runtime, TestDeployHeterogeneousModel) {
  MmpaStub::GetInstance().SetImpl(std::make_shared<MockMmpaForHelperRuntime>());
  RuntimeStub::SetInstance(std::make_shared<MockRuntime>());

  // 1. start server
  char real_path[200];
  realpath("st_run_data/json/helper_runtime/device", real_path);
  EXPECT_EQ(Configurations::GetInstance().InitDeviceInformation(real_path), SUCCESS);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });
  sleep(1);

  // 2. Init master
  realpath("st_run_data/json/helper_runtime/host", real_path);
  setenv("HELPER_RES_FILE_PATH", real_path, 1);
  GEFinalize();
  std::map<AscendString, AscendString> options;
  EXPECT_EQ(ge::GEInitialize(options), SUCCESS);
  GeRunningEnvFaker ge_env;
  ge_env.InstallDefault();

  // 3. BuildAndExecuteGraph
  auto graph = BuildGraph();
  Session session(options);
  uint32_t graph_id = 1;
  EXPECT_EQ(session.AddGraph(graph_id, graph), SUCCESS);

  Shape shape(std::vector<int64_t>({16}));
  TensorDesc tensor_desc(shape, FORMAT_ND, DT_INT32);
  Tensor tensor(tensor_desc);
  uint8_t buffer[16 * 4];
  tensor.SetData(buffer, sizeof(buffer));

  std::vector<Tensor> inputs{tensor};
  EXPECT_EQ(session.BuildGraph(graph_id, inputs), SUCCESS);
  GEFinalize();

  // 4. Cleanup
  grpc_server.Finalize();
  if (server_thread.joinable()) {
    server_thread.join();
  }
  MmpaStub::GetInstance().Reset();
  RuntimeStub::Reset();
}

TEST_F(STEST_helper_runtime, TestDeployHeterogeneousModel2PG) {
  MmpaStub::GetInstance().SetImpl(std::make_shared<MockMmpaForHelperRuntime>());
  RuntimeStub::SetInstance(std::make_shared<MockRuntime2PG>());

  // 1. start server
  char real_path[200];
  realpath("st_run_data/json/helper_runtime/device", real_path);
  EXPECT_EQ(Configurations::GetInstance().InitDeviceInformation(real_path), SUCCESS);
  ge::GrpcServer grpc_server;
  std::thread server_thread = std::thread([&]() {
    StartServer(grpc_server);
  });
  sleep(1);

  // 2. Init master
  realpath("st_run_data/json/helper_runtime/host", real_path);
  setenv("HELPER_RES_FILE_PATH", real_path, 1);
  GEFinalize();
  std::map<AscendString, AscendString> options;
  EXPECT_EQ(ge::GEInitialize(options), SUCCESS);
  GeRunningEnvFaker ge_env;
  ge_env.InstallDefault();

  // 3. BuildAndExecuteGraph
  auto graph = BuildGraph();
  Session session(options);
  uint32_t graph_id = 1;
  EXPECT_EQ(session.AddGraph(graph_id, graph), SUCCESS);

  Shape shape(std::vector<int64_t>({16}));
  TensorDesc tensor_desc(shape, FORMAT_ND, DT_INT32);
  Tensor tensor(tensor_desc);
  uint8_t buffer[16 * 4];
  tensor.SetData(buffer, sizeof(buffer));

  std::vector<Tensor> inputs{tensor};
  EXPECT_EQ(session.BuildGraph(graph_id, inputs), SUCCESS);
  GEFinalize();

  // 4. Cleanup
  grpc_server.Finalize();
  if (server_thread.joinable()) {
    server_thread.join();
  }
  MmpaStub::GetInstance().Reset();
  RuntimeStub::Reset();
}
}  // namespace ge
