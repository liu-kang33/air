/**
 * Copyright 2022 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include "external/ge/ge_api.h"
#include "graph/debug/ge_attr_define.h"
#include "graph/utils/node_adapter.h"
#include "framework/common/types.h"
#include "graph/utils/op_desc_utils.h"
#include "inc/external/graph/operator_reg.h"
#include "inc/external/graph/operator.h"
#include "inc/external/graph/operator_factory.h"
#include "graph/graph.h"

#include "ge_graph_dsl/graph_dsl.h"
#include "ge_graph_dsl/assert/check_utils.h"
#include "ge_graph_dsl/assert/graph_assert.h"
#include "graph/utils/graph_utils.h"
#include "ge_running_env/tensor_utils.h"

using namespace ge;
using namespace std;
namespace {
INFER_VALUE_RANGE_DEFAULT_REG(Add);
REG_OP(Shape)
    .OP_END_FACTORY_REG(Shape)
IMPL_INFER_VALUE_RANGE_FUNC(Shape, ShapeValueRangeFunc) {
  auto op_desc = OpDescUtils::GetOpDescFromOperator(op);
  auto output_tensor_desc = op_desc->MutableOutputDesc(0);
  std::vector<std::pair<int64_t, int64_t>> in_shape_range;
  op_desc->MutableInputDesc(0)->GetShapeRange(in_shape_range);
  if (!in_shape_range.empty()) {
    output_tensor_desc->SetValueRange(in_shape_range);
  }
  return GRAPH_SUCCESS;
};

const auto ShapeInfer = [](Operator &op) {
  auto op_desc = OpDescUtils::GetOpDescFromOperator(op);
  auto td = op_desc->MutableOutputDesc(0);
  auto input_dims = op_desc->MutableInputDesc(0)->MutableShape().GetDims();
  if (input_dims == UNKNOWN_RANK) {
    td->SetShape(ge::GeShape(UNKNOWN_SHAPE));
    td->SetOriginShape(ge::GeShape(UNKNOWN_SHAPE));
    td->SetShapeRange(std::vector<std::pair<int64_t, int64_t>>{{1, 8}});
  } else {
    int64_t size = static_cast<int64_t>(input_dims.size());
    std::vector<int64_t> size_v{size};
    td->SetShape(ge::GeShape(size_v));
    td->SetOriginShape(ge::GeShape(size_v));
  }
  uint32_t out_type = DT_INT32;
  (void)op.GetAttr("dtype", out_type);
  td->SetDataType((DataType)out_type);

  std::vector<std::pair<int64_t, int64_t>> inRange;
  op_desc->MutableInputDesc(0)->GetShapeRange(inRange);
  if (!inRange.empty()) {
    std::vector<int64_t> pre_op_range;
    pre_op_range.resize(2 * inRange.size());
    if (pre_op_range.size() >= INT_MAX) {
      return GRAPH_FAILED;
    }
    for (size_t i = 0; i < pre_op_range.size(); i = i + 2) {
      pre_op_range[i] = inRange[i / 2].first;
      pre_op_range[i + 1] = inRange[i / 2].second;
    }
    ge::AttrUtils::SetListInt(*td, "_pre_op_in_range", pre_op_range);
  }
  return GRAPH_SUCCESS;
};

INFER_FUNC_REG(Shape, ShapeInfer);

/*
*              (unknow)data2  
*                      |  
*                   shape   
*                    | |   data3
* data1(unknow)      add    |
*         \         /   switchn  const
*           reshape         |    /
*              |       expanddims
*              \           /
*                netoutput
*
*/
Graph BuildValueRangeInferGraph() {
  DEF_GRAPH(g1) {
    auto data1 = OP_CFG(DATA)
                     .Attr(ATTR_NAME_INDEX, 0)
                     .TensorDesc(FORMAT_NCHW, DT_FLOAT, {-1, -1, 4, -1})
                     .InCnt(1)
                     .OutCnt(1)
                     .Build("data1");
    auto data2 = OP_CFG(DATA)
                     .Attr(ATTR_NAME_INDEX, 1)
                     .TensorDesc(FORMAT_NCHW, DT_FLOAT, {-1, -1, 4, -1})
                     .InCnt(1)
                     .OutCnt(1)
                     .Build("data2");
    auto add = OP_CFG(ADD)
                     .TensorDesc(FORMAT_NCHW, DT_FLOAT, {4})
                     .InCnt(1)
                     .OutCnt(1)
                     .Build("add");
    auto data3 = OP_CFG(DATA)
                     .Attr(ATTR_NAME_INDEX, 2)
                     .TensorDesc(FORMAT_NCHW, DT_FLOAT, {8, 4, 16, 16})
                     .InCnt(1)
                     .OutCnt(1)
                     .Build("data3");
    auto switchn = OP_CFG("SwitchN")
                     .TensorDesc(FORMAT_NCHW, DT_FLOAT, {8, 4, 16, 16})
                     .InCnt(1)
                     .OutCnt(1)
                     .Build("switchn");
    auto shape = OP_CFG(SHAPE)
        .InCnt(1)
        .OutCnt(1)
        .Attr("_ge_attr_op_kernel_lib_name", "DNN_VM_GE_LOCAL_OP_STORE")
        .Attr("_force_infershape_when_running", true)
        .Build("shape");
    auto reshape = OP_CFG(RESHAPE)
        .InCnt(2)
        .OutCnt(1)
        .Attr("_ge_attr_op_kernel_lib_name", "DNN_VM_GE_LOCAL_OP_STORE")
        .Attr("_force_infershape_when_running", true)
        .Build("reshape");
    std::vector<int64_t> const_shape = {1};
    auto data_tensor = GenerateTensor(const_shape);
    auto const1 = OP_CFG(CONSTANT)
        .TensorDesc(FORMAT_ND, DT_INT32, {1})
        .Weight(data_tensor)
        .InCnt(1)
        .OutCnt(1)
        .Build("const1");
    auto expanddim = OP_CFG(EXPANDDIMS)
        .InCnt(2)
        .OutCnt(1)
        .Attr("_ge_attr_op_kernel_lib_name", "DNN_VM_GE_LOCAL_OP_STORE")
        .Build("expanddim");
    CHAIN(NODE(data1)->EDGE(0,0)
      ->NODE(reshape)->EDGE(0,0)
      ->NODE("netoutput", NETOUTPUT));
    CHAIN(NODE(data2)->EDGE(0,0)
      ->NODE(shape)->EDGE(0,0)
      ->NODE(add)->EDGE(0,1)
      ->NODE(reshape));
    CHAIN(NODE(shape)->EDGE(0,1)
      ->NODE(add));
    CHAIN(NODE(data3)->EDGE(0,0)
      ->NODE(switchn)->EDGE(0,0)
      ->NODE(expanddim)->EDGE(0,1)
      ->NODE("netoutput"));
    CHAIN(NODE(const1)->EDGE(0,1)
      ->NODE(expanddim));
  };
  auto graph = ToGeGraph(g1);
  auto compute_graph = GraphUtils::GetComputeGraph(graph);
  auto data1_node = compute_graph->FindNode("data1");
  auto out_desc = data1_node->GetOpDesc()->MutableOutputDesc(0);
  std::vector<std::pair<int64_t, int64_t>> shape_range = {make_pair(1, 100), make_pair(1, 240), make_pair(4, 4),
                                                          make_pair(192, 192)};
  out_desc->SetShapeRange(shape_range);
  auto data2_node = compute_graph->FindNode("data2");
  auto out_desc2 = data2_node->GetOpDesc()->MutableOutputDesc(0);
  out_desc2->SetShapeRange(shape_range);

  auto add_node = compute_graph->FindNode("add");
  auto out_desc_add = add_node->GetOpDesc()->MutableOutputDesc(0);
  out_desc_add->SetShapeRange(shape_range);

  auto shape_node = compute_graph->FindNode("shape");
  shape_node->GetOpDesc()->AddInferFunc(ShapeInfer);

  return graph;
}

///   sub_data1            Data1
///       |                  |
///    700Relu   ==>>  PartitionedCall
///       |                  |
///  sub_output1         NetOutput
Graph BuildPartitionedCallGraph() {
  DEF_GRAPH(sub) {
    auto netoutput = OP_CFG(NETOUTPUT).InCnt(1).OutCnt(1).Build("sub_output1");
    auto hcom1 = OP_CFG(HCOMALLREDUCE).InCnt(1).OutCnt(1).Build("hcom1");
    CHAIN(NODE("sub_relu", RELU)->NODE(hcom1)->NODE(netoutput));
  };

  DEF_GRAPH(root_graph) {
    CHAIN(NODE("data1", DATA)->NODE("partitionedcall", PARTITIONEDCALL, sub)->NODE("output1", NETOUTPUT));
  };
  sub.Layout();
  auto graph = ToGeGraph(root_graph);
  auto sub_graph = ToGeGraph(sub);
  auto compute_graph = GraphUtils::GetComputeGraph(graph);
  auto sub_compute_graph = GraphUtils::GetComputeGraph(sub_graph);
  auto partitionedcall = compute_graph->FindNode("partitionedcall");
  auto sub_relu = sub_compute_graph->FindNode("sub_relu");
  auto hcom1 = sub_compute_graph->FindNode("hcom1");
  auto sub_output1 = sub_compute_graph->FindNode("sub_output1");
  auto partition_output = partitionedcall->GetOpDesc()->MutableOutputDesc(0);
  partition_output->SetShape(GeShape({1,0,2}));
  auto sub_relu_output = sub_relu->GetOpDesc()->MutableOutputDesc(0);
  sub_relu_output->SetShape(GeShape({1,0,2}));
  auto hcom1_output = hcom1->GetOpDesc()->MutableOutputDesc(0);
  hcom1_output->SetShape(GeShape({1,0,2}));
  auto sub_output1_input = sub_output1->GetOpDesc()->MutableInputDesc(0);
  sub_output1_input->SetShape(GeShape({1,0,2}));
  auto sub_output1_output = sub_output1->GetOpDesc()->MutableOutputDesc(0);
  sub_output1_output->SetShape(GeShape({1,0,2}));
  AttrUtils::SetInt(sub_output1->GetOpDesc()->MutableInputDesc(0), ATTR_NAME_PARENT_NODE_INDEX, 0);
  return graph;
}

///   sub_data1               Data1
///      |                      |
///    hcom1(empty)  ==>>  PartitionedCall
///      |                      |
///    hcom3                hcom2(empty)
///      |                       |
///    sub_output1          NetOutput
Graph BuildPartitionedCall2EmptyOpGraph() {
  DEF_GRAPH(sub) {
    auto netoutput = OP_CFG(NETOUTPUT).InCnt(1).OutCnt(1).Build("sub_output1");
    auto hcom1 = OP_CFG(HCOMALLREDUCE).InCnt(1).OutCnt(1).Build("hcom1");
    auto hcom3 = OP_CFG(HCOMALLREDUCE).InCnt(1).OutCnt(1).Build("hcom3");
    CHAIN(NODE("sub_relu", RELU)->NODE(hcom1)->NODE(hcom3)->NODE(netoutput));
  };

  DEF_GRAPH(root_graph) {
    auto hcom2 = OP_CFG(HCOMALLREDUCE).InCnt(1).OutCnt(1).Build("hcom2");
    CHAIN(NODE("data1", DATA)->NODE("partitionedcall", PARTITIONEDCALL, sub)->NODE(hcom2)->NODE("output1", NETOUTPUT));
  };
  sub.Layout();
  auto graph = ToGeGraph(root_graph);
  auto sub_graph = ToGeGraph(sub);
  auto compute_graph = GraphUtils::GetComputeGraph(graph);
  auto sub_compute_graph = GraphUtils::GetComputeGraph(sub_graph);
  auto hcom2 = compute_graph->FindNode("hcom2");
  auto hcom1 = sub_compute_graph->FindNode("hcom1");

  auto hcom1_output = hcom1->GetOpDesc()->MutableOutputDesc(0);
  hcom1_output->SetShape(GeShape({1,0,2}));
  auto hcom2_output = hcom2->GetOpDesc()->MutableOutputDesc(0);
  hcom2_output->SetShape(GeShape({1,0,2}));
  return graph;
}
}  // namespace

class InferAndFoldingTest : public testing::Test {
 protected:
  void SetUp() {
  }
  void TearDown() {}
};

/*
* 该用例通过shape、add算子的value range推导，最终得出shape的输出value range为较精准的范围
*/
TEST_F(InferAndFoldingTest, test_infer_value_range) {
  INFER_VALUE_RANGE_CUSTOM_FUNC_REG(Shape, INPUT_IS_DYNAMIC, ShapeValueRangeFunc);
  INFER_VALUE_RANGE_DEFAULT_REG(ADD);
  
  // build graph
  Graph graph = BuildValueRangeInferGraph();
  DUMP_GRAPH_WHEN("OptimizeStage1_2");
  // new session & add graph
  map<AscendString, AscendString> options;
  Session session(options);
  uint32_t graph_id = 1;
  auto ret = session.AddGraph(graph_id, graph, options);
  EXPECT_EQ(ret, SUCCESS);
  // build input tensor
  std::vector<InputTensorInfo> inputs;
  // build_graph through session
  session.BuildGraph(graph_id, inputs); // we only care about compile stage

  CHECK_GRAPH(OptimizeStage1_2) {
    // check shape node output desc has value range
    auto shape_node = graph->FindNode("shape");
    auto shape_output_desc = shape_node->GetOpDesc()->GetOutputDescPtr(0);
    std::vector<std::pair<int64_t, int64_t>> expect_value_range = {make_pair(1, 100), make_pair(1, 240), make_pair(4, 4),
                                                          make_pair(192, 192)};
    std::vector<std::pair<int64_t, int64_t>> value_range;
    shape_output_desc->GetValueRange(value_range);

    for (size_t i =0; i < value_range.size(); ++i) {
      ASSERT_EQ(value_range[i].first, expect_value_range[i].first);
      ASSERT_EQ(value_range[i].second, expect_value_range[i].second);
    }

    // check expandims has been removed
    auto expanddim = graph->FindNode("expanddim");
    ASSERT_EQ(expanddim, nullptr);
  };
}
// 若在potential const生效过程中，节点的父节点已经被删除，则跳过优化
TEST_F(InferAndFoldingTest, test_replace_empty_tensor_with_partitioncall) {
  // build graph
  Graph graph = BuildPartitionedCallGraph();
  DUMP_GRAPH_WHEN("OptimizeStage1_2");
  // new session & add graph
  map<AscendString, AscendString> options;
  Session session(options);
  uint32_t graph_id = 1;
  auto ret = session.AddGraph(graph_id, graph, options);
  EXPECT_EQ(ret, SUCCESS);
  // build input tensor
  std::vector<InputTensorInfo> inputs;
  // build_graph through session
  session.BuildGraph(graph_id, inputs); // we only care about compile stage

  CHECK_GRAPH(OptimizeStage1_2) {
    // check partitionedcall has been removed
    auto partitionedcall = graph->FindNode("partitionedcall");
    ASSERT_EQ(partitionedcall, nullptr);
  };
}

// 若在potential const生效后，节点的父节点已经被删除，则跳过repass
// 如构图所示，hcom1在potential const生效时被加入repass
// 而partitioncall在hcom2常量折叠的时候被删除，并加入repass
// 那么在找下轮repass node的时候，通过hcom1找父节点时，应返回null并跳过repass
TEST_F(InferAndFoldingTest, test_replace_empty_tensor_both_in_subgraph_and_rootgraph) {
  // build graph
  Graph graph = BuildPartitionedCall2EmptyOpGraph();
  DUMP_GRAPH_WHEN("OptimizeStage1_2");
  // new session & add graph
  map<AscendString, AscendString> options;
  Session session(options);
  uint32_t graph_id = 1;
  auto ret = session.AddGraph(graph_id, graph, options);
  EXPECT_EQ(ret, SUCCESS);
  // build input tensor
  std::vector<InputTensorInfo> inputs;
  // build_graph through session
  session.BuildGraph(graph_id, inputs); // we only care about compile stage

  CHECK_GRAPH(OptimizeStage1_2) {
    // check partitionedcall has been removed
    auto partitionedcall = graph->FindNode("partitionedcall");
    ASSERT_EQ(partitionedcall, nullptr);
  };
}