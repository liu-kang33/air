/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "common/model/ge_root_model.h"
#include "graph/debug/ge_attr_define.h"

namespace ge {
void GeRootModel::SetSubgraphInstanceNameToModel(const std::string &instance_name, const GeModelPtr &ge_model) {
  (void)subgraph_instance_name_to_model_.insert(std::pair<std::string, GeModelPtr>(instance_name, ge_model));
}

Status GeRootModel::CheckIsUnknownShape(bool &is_dynamic_shape) {
  if (root_graph_ == nullptr) {
    return FAILED;
  }
  is_dynamic_shape = false;
  (void)AttrUtils::GetBool(root_graph_, ATTR_NAME_DYNAMIC_SHAPE_PARTITIONED, is_dynamic_shape);
  return SUCCESS;
}

const std::map<std::string, std::shared_ptr<ge::GeRootModel>> &GeRootModel::GetSubmodels() const {
  return submodels_;
}

void GeRootModel::SetModelRelation(std::shared_ptr<ModelRelation> model_relation) {
  model_relation_ = std::move(model_relation);
}

const ModelRelation *GeRootModel::GetModelRelation() const {
  return model_relation_.get();
}

const ge::GeRootModel *GeRootModel::GetSubmodel(const std::string &name) const {
  const auto &it = submodels_.find(name);
  if (it == submodels_.end()) {
    return nullptr;
  }
  return it->second.get();
}

Status GeRootModel::AddSubModel(const shared_ptr<ge::GeRootModel> &submodel) {
  if (!submodels_.emplace(submodel->GetModelName(), submodel).second) {
    GELOGE(INTERNAL_ERROR, "submodel already exist. name = %s", submodel->GetModelName().c_str());
    return INTERNAL_ERROR;
  }
  return SUCCESS;
}
}  // namespace ge