/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef GE_MODEL_GE_ROOT_MODEL_H_
#define GE_MODEL_GE_ROOT_MODEL_H_

#include <map>
#include "graph/compute_graph.h"
#include "common/model/ge_model.h"
#include "common/model/model_relation.h"

namespace ge {
class GeRootModel {
 public:
  GeRootModel() = default;
  explicit GeRootModel(const ComputeGraphPtr &root_graph) : root_graph_(root_graph), model_id_(INVALID_MODEL_ID) {};
  ~GeRootModel() = default;

  void SetSubgraphInstanceNameToModel(const std::string &instance_name, const GeModelPtr &ge_model);
  const std::map<std::string, GeModelPtr> &GetSubgraphInstanceNameToModel() const {
    return subgraph_instance_name_to_model_;
  };

  const ComputeGraphPtr &GetRootGraph() const { return root_graph_; }
  void SetModelId(uint32_t model_id) {
    const std::lock_guard<std::mutex> lock(model_ids_mutex_);
    model_id_ = model_id;
    // cached for removement
    model_ids_.emplace_back(model_id);
  }
  uint32_t GetModelId() const { return model_id_; }

  void SetIsSpecificStream(const bool is_specific_stream) { is_specific_stream_ = is_specific_stream; }

  bool IsSpecificStream() const { return is_specific_stream_; }

  void SetModelName(const std::string &model_name) { model_name_ = model_name; }

  const std::string &GetModelName() const { return model_name_; }

  std::vector<uint32_t> GetAllModelId() const { return model_ids_; }

  void ClearAllModelId() { model_ids_.clear(); }

  Status CheckIsUnknownShape(bool &is_dynamic_shape);

  void SetRootGraph(const ComputeGraphPtr graph) { root_graph_ = graph; }

  void SetTrainFlag(const bool flag) { train_flag_ = flag; }

  const std::map<std::string, std::shared_ptr<ge::GeRootModel>> &GetSubmodels() const;

  const GeRootModel *GetSubmodel(const std::string &name) const;

  Status AddSubModel(const std::shared_ptr<GeRootModel> &submodel);

  void SetModelRelation(std::shared_ptr<ModelRelation> model_relation);

  const ModelRelation *GetModelRelation() const;

 private:
  ComputeGraphPtr root_graph_ = nullptr;
  std::map<std::string, std::shared_ptr<ge::GeRootModel>> submodels_;
  std::map<std::string, GeModelPtr> subgraph_instance_name_to_model_;
  std::shared_ptr<ModelRelation> model_relation_;
  uint32_t model_id_ = 0U;
  // In multithread online secenario, same graph can owns different davinci_model for for concurrency
  std::vector<uint32_t> model_ids_;
  std::mutex model_ids_mutex_;
  bool train_flag_ = false;
  std::string model_name_;
  bool is_specific_stream_ = false;
};
using GeRootModelPtr = std::shared_ptr<ge::GeRootModel>;
}  // namespace ge
#endif  // GE_MODEL_GE_ROOT_MODEL_H_
