/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "graph/load/model_manager/task_info/ffts_plus_task_info.h"

#include <cmath>

#include "securec.h"
#include "graph/load/model_manager/task_info/ffts_plus_proto_transfer.h"
#include "graph/load/model_manager/davinci_model.h"
#include "graph/load/model_manager/model_utils.h"
#include "aicpu/common/aicpu_task_struct.h"
#include "hybrid/node_executor/aicpu/aicpu_ext_info.h"
#include "graph/load/model_manager/model_manager.h"

namespace {
const std::string kAicpuAllshape = "_AllShape";
const uint32_t kFwkAicpuKernelType = 1U;
const uint32_t kCustomAicpuKernelType = 4U;
const std::string ATTR_NAME_CACHE_PERSIST = "_cache_persist"; // temp code, wait for metadef code merge
}

namespace ge {
FftsPlusTaskInfo::~FftsPlusTaskInfo() {
}

Status FftsPlusTaskInfo::Init(const domi::TaskDef &task_def, DavinciModel *const davinci_model) {
  GELOGI("Init FftsPlusTaskInfo Start");
  fusion_op_info_.clear();
  GE_CHECK_NOTNULL(davinci_model);
  davinci_model_ = davinci_model;
  GE_CHK_STATUS_RET_NOLOG(SetStream(task_def.stream_id(), davinci_model_->GetStreamList()));

  const domi::FftsPlusTaskDef &ffts_plus_task_def = task_def.ffts_plus_task();
  const auto op_desc = davinci_model_->GetOpByIndex(ffts_plus_task_def.op_index());
  GE_CHECK_NOTNULL(op_desc);
  const int32_t ctx_num = ffts_plus_task_def.ffts_plus_ctx_size();
  for (int32_t i = 0; i < ctx_num; ++i) {
    const domi::FftsPlusCtxDef &ctx_def = ffts_plus_task_def.ffts_plus_ctx(i);
    const OpDescPtr op_desc = davinci_model_->GetOpByIndex(ctx_def.op_index());
    if (op_desc != nullptr) {
      GE_CHK_STATUS_RET(SetCachePersistentWay(op_desc), "[Call][SetCachePersistentWay] failed, node:%s.",
                        op_desc->GetName().c_str());
    }
  }

  const auto &rts_param = davinci_model_->GetRuntimeParam();
  const auto ffts_run_addr_handle = [&rts_param](const uintptr_t logic_addr, uint8_t *&mem_addr) -> Status {
    return ModelUtils::GetRtAddress(rts_param, logic_addr, mem_addr);
  };

  const auto ffts_addr_pref_handle = [&davinci_model](const std::string &kernel_name, void *&addr, uint32_t &pref_cnt) {
    return davinci_model->GetAddrAndPrefCnt(kernel_name, addr, pref_cnt);
  };

  const auto ffts_find_node_handle = [&davinci_model](const uint32_t index) {
    return davinci_model->GetOpByIndex(index);
  };

  const auto ffts_save_ctx_args_handle = [this](const OpDescPtr &descriptor, const size_t args_offset) {
    InitDumpArgs(descriptor, args_offset);
  };

  const auto ffts_init_ext_info_handle = [this](const OpDescPtr &op_descriptor,
                                                const domi::FftsPlusAicpuCtxDef &ctx_def, void *&addr) -> Status {
    return this->InitAicpuInfo(op_descriptor, ctx_def, addr);
  };

  args_size_ = sizeof(void *) * ffts_plus_task_def.addr_size();
  if (args_size_ > 0U) {
    GE_CHK_RT_RET(rtMalloc(&args_, args_size_, RT_MEMORY_HBM));
    GE_CHECK_NOTNULL(args_);
  }

  FftsPlusProtoTransfer ffts_proto_transfer(PtrToValue(args_), io_addrs_, ext_args_, mode_addr_idx_);
  ffts_proto_transfer.SetRunAddrHandle(ffts_run_addr_handle);
  ffts_proto_transfer.SetAddrPrefHandle(ffts_addr_pref_handle);
  ffts_proto_transfer.SetFindNodeHandle(ffts_find_node_handle);
  ffts_proto_transfer.SetSaveCtxArgsHandle(ffts_save_ctx_args_handle);
  ffts_proto_transfer.SetInitExtInfoHandle(ffts_init_ext_info_handle);

  GE_CHK_STATUS_RET_NOLOG(ffts_proto_transfer.Transfer(op_desc, ffts_plus_task_def, ffts_plus_task_info_));
  fusion_op_info_ = ffts_proto_transfer.GetAllFusionOpInfo();

  if (args_size_ < (sizeof(void *) * io_addrs_.size())) {
    REPORT_INNER_ERROR("E19999", "addr_size %d of FftsPlusTaskInfo is less than number of task_addr %zu",
                       ffts_plus_task_def.addr_size(), io_addrs_.size());
    GELOGE(FAILED, "[Check][Param] addr_size %d of FftsPlusTaskInfo is less than number of task_addr %zu",
           ffts_plus_task_def.addr_size(), io_addrs_.size());
    return FAILED;
  }

  GE_CHK_STATUS_RET_NOLOG(UpdateArgs());
  GELOGI("Init FftsPlusTaskInfo success, node: %s", op_desc->GetName().c_str());
  return SUCCESS;
}

Status FftsPlusTaskInfo::CalculateArgs(const domi::TaskDef &task_def, DavinciModel *const davinci_model) {
  (void)task_def;
  (void)davinci_model;
  return SUCCESS;
}

Status FftsPlusTaskInfo::UpdateArgs() {
  GE_CHECK_NOTNULL(davinci_model_);
  if (io_addrs_.empty()) {
    GELOGI("Init FftsPlusTaskInfo success, do not have io addrs");
    return SUCCESS;
  }

  if ((args_ == nullptr) || (args_size_ < (sizeof(void *) * io_addrs_.size()))) {
    REPORT_INNER_ERROR("E19999", "Invalid agrs: args size: %lu, adds size: %zu", args_size_, io_addrs_.size());
    GELOGE(FAILED, "[Check][Param] Invalid agrs: args size: %lu, adds size: %zu", args_size_, io_addrs_.size());
    return INTERNAL_ERROR;
  }

  std::vector<void *> io_addrs(io_addrs_.size(), nullptr);
  const auto &rts_param = davinci_model_->GetRuntimeParam();
  const auto addr_size = sizeof(void *) * io_addrs_.size();
  for (size_t i = 0U; i < io_addrs_.size(); ++i) {
    if (mode_addr_idx_.count(i) > 0U) {
      io_addrs[i] = reinterpret_cast<void *>(io_addrs_[i]);
      GELOGD("addr at idx:%zu is real addr", i);
      continue;
    }
    uint8_t *mem_addr = nullptr;
    if (ModelUtils::GetRtAddress(rts_param, io_addrs_[i], mem_addr) != SUCCESS) {
      GELOGE(INTERNAL_ERROR, "[Check][GetRtAddress] failed, logic addr is 0x%lx.", io_addrs_[i]);
      return INTERNAL_ERROR;
    }
    io_addrs[i] = mem_addr;
  }

  GELOGI("Memcpy to addr %p, addr_size=%lu, len=%lu", args_, args_size_, addr_size);
  GE_CHK_RT_RET(rtMemcpy(args_, args_size_, io_addrs.data(), addr_size, RT_MEMCPY_HOST_TO_DEVICE));
  return SUCCESS;
}

Status FftsPlusTaskInfo::Distribute() {
  GELOGI("FftsPlusTaskInfo Distribute Start.");
  GE_CHK_RT_RET(rtFftsPlusTaskLaunchWithFlag(&ffts_plus_task_info_, stream_, dump_flag_));
  GE_CHECK_NOTNULL(davinci_model_);
  GE_CHK_RT_RET(rtModelGetTaskId(davinci_model_->GetRtModelHandle(), &task_id_, &stream_id_));
  GELOGI("FftsPlusTaskInfo Distribute Success. task id is %u, stream id is %u.", task_id_, stream_id_);
  return SUCCESS;
}

Status FftsPlusTaskInfo::Release() {
  GE_FREE_RT_LOG(args_);
  for (auto &addr : ext_args_) {
    GE_FREE_RT_LOG(addr);
  }

  for (auto &ext_info_addr : ext_info_addrs_) {
    GE_FREE_RT_LOG(ext_info_addr);
  }

  CleanRtFftsPlusTask(ffts_plus_task_info_);
  return SUCCESS;
}

Status FftsPlusTaskInfo::SetCachePersistentWay(const OpDescPtr &op_desc) const {
  const vector<void *> input_addrs = ModelUtils::GetInputDataAddrs(davinci_model_->GetRuntimeParam(), op_desc);
  for (size_t i = 0U; i < op_desc->GetAllInputsSize(); ++i) {
    const GeTensorDescPtr tensor_desc = op_desc->MutableInputDesc(static_cast<uint32_t>(i));
    if (tensor_desc == nullptr) {
      continue;
    }
    uint32_t persistent_id = UINT_MAX;
    (void)AttrUtils::GetInt(tensor_desc, ATTR_NAME_CACHE_PERSIST, persistent_id);
    if (persistent_id != UINT_MAX) {
      int64_t tensor_size = 0;
      (void)TensorUtils::GetSize(*tensor_desc, tensor_size);
      // delete rtMemAdvise call, wait for RTS code merge
    }
  }
  return SUCCESS;
}

void FftsPlusTaskInfo::InitDumpArgs(const OpDescPtr &op_desc, const size_t args_offset) {
  if (davinci_model_->OpNeedDump(op_desc->GetName())) {
    GELOGD("Op:%s(%s) need dump in ffts plus task info", op_desc->GetName().c_str(), op_desc->GetType().c_str());
    dump_flag_ = RT_KERNEL_DUMPFLAG;
  }

  if (davinci_model_->GetOpDugReg() || davinci_model_->OpNeedDump(op_desc->GetName())) {
    if ((dump_args_offset_.count(op_desc->GetName()) == 0U)) {
      (void)dump_args_offset_.emplace(op_desc->GetName(), args_offset);
    }
  }
}

uintptr_t FftsPlusTaskInfo::FindDumpArgs(const std::string &op_name) const {
  const std::map<std::string, size_t>::const_iterator iter = dump_args_offset_.find(op_name);
  if (iter != dump_args_offset_.end()) {
    return (PtrToValue(args_) + (sizeof(void *) * iter->second));
  }
  GELOGD("op:%s not save args", op_name.c_str());
  return 0U;
}

void FftsPlusTaskInfo::PostProcess(const domi::TaskDef &task_def) {
  const domi::FftsPlusTaskDef &ffts_plus_task_def = task_def.ffts_plus_task();
  const int32_t ctx_num = ffts_plus_task_def.ffts_plus_ctx_size();
  // Dump times of the same operator, only one time is allowed
  std::set<std::string> dump_op_set;
  for (int32_t i = 0; i < ctx_num; ++i) {
    const domi::FftsPlusCtxDef &ctx_def = ffts_plus_task_def.ffts_plus_ctx(i);
    const OpDescPtr op_desc = davinci_model_->GetOpByIndex(ctx_def.op_index());
    if (op_desc == nullptr) {
      GELOGD("ctx op is nullptr, ctx id:%u, ctx type:%u, op index:%u",
             ctx_def.context_id(), ctx_def.context_type(), ctx_def.op_index());
      continue;
    }
    // Dump times of the same op, only one time is allowed
    if (dump_op_set.count(op_desc->GetName()) > 0U) {
      GELOGD("ctx op is save, ctx id:%u, ctx type:%u, op index:%u",
             ctx_def.context_id(), ctx_def.context_type(), ctx_def.op_index());
      continue;
    }
    if (FindDumpArgs(op_desc->GetName()) != 0U) {
      const bool call_dump = davinci_model_->OpNeedDump(op_desc->GetName()) && CallSaveDumpInfo();
      if (call_dump || davinci_model_->GetOpDugReg()) {
        davinci_model_->SaveDumpTask(task_id_, stream_id_, op_desc, FindDumpArgs(op_desc->GetName()));
        GELOGD("save ctx op, ctx id:%u, ctx type:%u, op index:%u, task id:%u, stream id:%u", ctx_def.context_id(),
               ctx_def.context_type(), ctx_def.op_index(), task_id_, stream_id_);
        (void)dump_op_set.emplace(op_desc->GetName());
      }
    }
    davinci_model_->SaveDumpOpInfo(op_desc, task_id_, stream_id_);
  }

  davinci_model_->SaveFftsPlusProfilingTask(task_def, *this);
}

Status FftsPlusTaskInfo::InitAicpuInfo(const OpDescPtr &op_desc, const domi::FftsPlusAicpuCtxDef &ctx_def,
                                       void *&addr) {
  // aicpu fwk op
  if (ctx_def.kernel_type() == kFwkAicpuKernelType) {
    return InitAicpuFwkExtInfo(op_desc, ctx_def, addr);
  }
  if (ctx_def.kernel_type() == kCustomAicpuKernelType) {
    bool loaded = false;
    const auto &kernel = ctx_def.kernel();
    auto &model_mgr = ModelManager::GetInstance();
    GE_CHK_STATUS_RET(model_mgr.LoadCustAicpuSo(davinci_model_->GetCustAICPUKernel(op_desc), kernel.so_name(), loaded),
                      "[Launch][CustAicpuSo] failed.");
  }
  return InitAicpuExtInfo(op_desc, ctx_def, addr);
}

Status FftsPlusTaskInfo::InitAicpuFwkAddrInfo(const OpDescPtr &op_desc, uint8_t *const ori_args_addr,
                                              const uint32_t args_size, const std::string &ext_info,
                                              const void *const aicpu_ext_info_addr) {
  const auto fwk_op_kernel = PtrToPtr<uint8_t, STR_FWK_OP_KERNEL>(ori_args_addr);
  const auto task_info_addr = PtrAdd(ori_args_addr, static_cast<size_t>(args_size), sizeof(STR_FWK_OP_KERNEL));
  const uint32_t task_info_addr_size = args_size - sizeof(STR_FWK_OP_KERNEL);
  const auto ret = CopyTaskInfoToWorkspace(op_desc, reinterpret_cast<const void *>(task_info_addr),
                                           task_info_addr_size);
  if (ret != SUCCESS) {
    GELOGE(ret, "[copy][TaskInfo] to workspace failed, op:%s.", op_desc->GetName().c_str());
    return ret;
  }
  const auto &rts_param = davinci_model_->GetRuntimeParam();
  const std::vector<void *> workspace_data_addrs = ModelUtils::GetWorkspaceDataAddrs(rts_param, op_desc);
  if (workspace_data_addrs.empty()) {
    REPORT_CALL_ERROR("E19999", "workspace_data_addrs is empty in op:%s(%s), check invalid",
                      op_desc->GetName().c_str(), op_desc->GetType().c_str());
    GELOGE(FAILED, "[Check][Param] workspace_data_addrs is empty in op:%s(%s).", op_desc->GetName().c_str(),
           op_desc->GetType().c_str());
    return FAILED;
  }

  const uint64_t workspace_base_addr = PtrToValue(workspace_data_addrs[0U]);
  const std::vector<void *> input_addrs = ModelUtils::GetInputAddrs(rts_param, op_desc);
  const std::vector<void *> output_addrs = ModelUtils::GetOutputAddrs(rts_param, op_desc);
  std::vector<void *> io_addrs;
  (void)io_addrs.insert(io_addrs.end(), input_addrs.begin(), input_addrs.end());
  (void)io_addrs.insert(io_addrs.end(), output_addrs.begin(), output_addrs.end());

  const auto addrs_size = sizeof(uint64_t) * (io_addrs.size());
  void *input_output_addr = nullptr;
  if (addrs_size > 0U) {
    GE_CHK_RT_RET(rtMalloc(&input_output_addr, addrs_size, RT_MEMORY_HBM));
    ext_info_addrs_.emplace_back(input_output_addr);
    GE_CHK_RT_RET(rtMemcpy(input_output_addr, addrs_size, io_addrs.data(), addrs_size, RT_MEMCPY_HOST_TO_DEVICE));
  }

  fwk_op_kernel->fwkKernelBase.fwk_kernel.workspaceBaseAddr = workspace_base_addr;
  fwk_op_kernel->fwkKernelBase.fwk_kernel.inputOutputAddr = PtrToValue(input_output_addr);
  fwk_op_kernel->fwkKernelBase.fwk_kernel.stepIDAddr = davinci_model_->GetGlobalStep();
  fwk_op_kernel->fwkKernelBase.fwk_kernel.extInfoLen = ext_info.size();
  fwk_op_kernel->fwkKernelBase.fwk_kernel.extInfoAddr = PtrToValue(aicpu_ext_info_addr);
  return SUCCESS;
}

Status FftsPlusTaskInfo::InitAicpuFwkExtInfo(const OpDescPtr &op_desc, const domi::FftsPlusAicpuCtxDef &ctx_def,
                                             void *&addr) {
  GELOGI("Begin to init aicpu fwk op ext info.");
  GE_CHECK_NOTNULL(davinci_model_);
  const auto &kernel = ctx_def.kernel();

  // create host args
  const size_t args_size = kernel.args().size();
  std::vector<uint8_t> ori_args_addr(kernel.args().data(), &kernel.args()[args_size]);

  void *aicpu_ext_info_addr = nullptr;
  const auto &ext_info = kernel.kernel_ext_info();
  GE_CHK_STATUS_RET(InitAicpuTaskExtInfo(op_desc, ext_info, aicpu_ext_info_addr),
                    "[Init][AicpuTaskExtInfo] failed, ext info size is %zu, op is %s",
                    ext_info.size(), op_desc->GetName().c_str());
  GELOGI("Node[%s] type[%s] kernel_ext_info size=%zu, aicpu_ext_info_addr=%p", op_desc->GetName().c_str(),
         op_desc->GetType().c_str(), ext_info.size(), aicpu_ext_info_addr);

  // 1 get loop cond variable for tensor array
  const auto session_id = davinci_model_->GetSessionId();
  const auto fwk_op_kernel = reinterpret_cast<STR_FWK_OP_KERNEL *>(ori_args_addr.data());
  fwk_op_kernel->fwkKernelBase.fwk_kernel.sessionID = session_id;

  // 2 Collect aicpu kernel
  const uint64_t kernel_id = hybrid::AicpuExtInfoHandler::GenerateKernelId();
  fwk_op_kernel->fwkKernelBase.fwk_kernel.kernelID = kernel_id;
  ModelManager::GetInstance().CreateAicpuKernel(session_id, davinci_model_->Id(),
                                                davinci_model_->SubModelId(), kernel_id);

  // 3 Create session
  const auto ret = ModelManager::GetInstance().CreateAicpuSession(session_id);
  if (ret != SUCCESS) {
    REPORT_CALL_ERROR("E19999", "CreateAicpuSession fail, session_id:%lu", session_id);
    GELOGE(ret, "[Create][AicpuSession] error. session id:%lu", session_id);
    return ret;
  }

  // 4 set workspace addr and input output data addr
  GE_CHK_STATUS_RET(InitAicpuFwkAddrInfo(op_desc, ori_args_addr.data(), args_size, ext_info, aicpu_ext_info_addr),
                    "[Init][InitAicpuFwkAddrInfo] failed, ext info size is %zu, op is %s", ext_info.size(),
                    op_desc->GetName().c_str());

  // 5. Return result
  GE_CHK_RT_RET(rtMalloc(&addr, sizeof(STR_FWK_OP_KERNEL), RT_MEMORY_HBM));
  ext_args_.emplace_back(addr);
  GE_CHK_RT_RET(rtMemcpy(addr, sizeof(STR_FWK_OP_KERNEL), static_cast<void *>(fwk_op_kernel),
                         sizeof(STR_FWK_OP_KERNEL), RT_MEMCPY_HOST_TO_DEVICE));
  GELOGI("Init aicpu fwk op context info Success. session id: %lu", session_id);
  return SUCCESS;
}

Status FftsPlusTaskInfo::InitAicpuExtInfo(const OpDescPtr &op_desc, const domi::FftsPlusAicpuCtxDef &ctx_def,
                                          void *&addr) {
  GELOGI("Begin to init aicpu op ext info.");
  const auto &kernel = ctx_def.kernel();
  // copy args to new host addr
  const size_t args_size = kernel.args().size();
  std::vector<uint8_t> ori_args_addr(kernel.args().data(), &kernel.args()[args_size]);

  // ctx_def.atm() == 0 is aicpu manual mode(only once looping), else is auto mode
  const auto thread_num = ctx_def.thread_dim();
  const auto task_param_offset = ctx_def.task_param_offset();
  GELOGI("[Init ffts plus aicpu ext info] thread num is %u, atm is %u, task_param_offset is %u", thread_num,
         ctx_def.atm(), task_param_offset);
  const auto &ext_info = kernel.kernel_ext_info();
  for (uint32_t i = 0U;  i < thread_num; ++i) {
    const auto aicpu_param_head = PtrToPtr<uint8_t, aicpu::AicpuParamHead>(&ori_args_addr[i * task_param_offset]);
    void *aicpu_ext_info_addr = nullptr;
    const auto init_ret = InitAicpuTaskExtInfo(op_desc, ext_info, aicpu_ext_info_addr);
    if (init_ret != SUCCESS) {
      GELOGE(init_ret, "[Init][AicpuTaskExtInfo] failed, ext_info size=%zu, op=%s", ext_info.size(),
             op_desc->GetName().c_str());
      return init_ret;
    }

    GELOGI("Node[%s] type[%s] kernel_ext_info size=%zu, aicpu_ext_info_addr_=%p", op_desc->GetName().c_str(),
           op_desc->GetType().c_str(), ext_info.size(), aicpu_ext_info_addr);
    aicpu_param_head->extInfoAddr = PtrToValue(aicpu_ext_info_addr);
    aicpu_param_head->extInfoLength = static_cast<uint32_t>(ext_info.size());
    const uintptr_t io_addr = PtrToValue(&ori_args_addr[(i * task_param_offset) + sizeof(aicpu::AicpuParamHead)]);
    GE_CHK_STATUS_RET(InitAicpuIoAddrs(op_desc, io_addr), "Init aicpu[%s] io addrs failed", op_desc->GetName().c_str());
  }

  // malloc device memory for args
  GE_CHK_RT_RET(rtMalloc(&addr, args_size, RT_MEMORY_HBM));
  ext_args_.emplace_back(addr);
  // copy args to device
  GE_CHK_RT_RET(rtMemcpy(addr, args_size, static_cast<void *>(ori_args_addr.data()), args_size,
                         RT_MEMCPY_HOST_TO_DEVICE));
  GELOGI("Init aicpu op context info success");
  return SUCCESS;
}

Status FftsPlusTaskInfo::InitAicpuTaskExtInfo(const OpDescPtr &op_desc, const std::string &ext_info,
                                              void *&aicpu_ext_info_addr) {
  GE_CHECK_NOTNULL(davinci_model_);
  if (ext_info.empty()) {
    return SUCCESS;
  }

  int32_t unknown_shape_type_val = 0;
  (void)AttrUtils::GetInt(op_desc, ::ge::ATTR_NAME_UNKNOWN_SHAPE_TYPE, unknown_shape_type_val);
  const UnknowShapeOpType unknown_type = static_cast<UnknowShapeOpType>(unknown_shape_type_val);
  const uint32_t num_inputs = static_cast<uint32_t>(op_desc->GetInputsSize());
  const uint32_t num_outputs = static_cast<uint32_t>(op_desc->GetOutputsSize());
  const auto ext_handle = MakeShared<hybrid::AicpuExtInfoHandler>(op_desc->GetName(), num_inputs,
                                                                  num_outputs, unknown_type);
  GE_CHK_BOOL_RET_STATUS(ext_handle != nullptr, FAILED, "[Malloc][Memory] for aicpu_ext_handle failed!");
  GE_CHK_STATUS_RET(ext_handle->Parse(ext_info),
                    "[Parse][KernelExtInfo] failed, kernel_ext_info_size=%zu, op:%s.",
                    ext_info.size(), op_desc->GetName().c_str());
  GE_CHK_STATUS_RET(ext_handle->UpdateSessionInfoId(davinci_model_->GetSessionId()),
                    "[Update][SessionInfoSessionId] failed, op:%s", op_desc->GetName().c_str());
  GELOGD("Update aicpu_task ext_info session_info session_id is %lu", davinci_model_->GetSessionId());
  GE_CHK_STATUS_RET(ext_handle->UpdateExecuteMode(true),
                    "[Update][ExecuteMode] failed, op:%s", op_desc->GetName().c_str());
  GELOGD("Update aicpu_task ext_info bit_map execute mode to 1.");
  bool all_shape = false;
  (void)AttrUtils::GetBool(op_desc, kAicpuAllshape, all_shape);
  if (all_shape) {
    GELOGD("Aicpu all_shape kernel need to update io shape.");
    for (uint32_t i = 0U; i < num_inputs; i++) {
      const auto input_desc = op_desc->MutableInputDesc(i);
      GE_CHECK_NOTNULL(input_desc);
      GE_CHK_STATUS_RET(ext_handle->UpdateInputShapeAndType(i, *input_desc),
                        "[Call][UpdateInputShapeAndType] Input[%u] update input shape failed, op:%s.",
                        i, op_desc->GetName().c_str());
    }
    for (uint32_t j = 0U; j < num_outputs; j++) {
      const auto output_desc = op_desc->MutableOutputDesc(j);
      GE_CHECK_NOTNULL(output_desc);
      GE_CHK_STATUS_RET(ext_handle->UpdateOutputShapeAndType(j, *output_desc),
                        "[Call][UpdateOutputShapeAndType] Output[%u] update output shape failed, op:%s.",
                        j, op_desc->GetName().c_str());
    }
  }

  GE_CHK_RT_RET(rtMalloc(&aicpu_ext_info_addr, ext_handle->GetExtInfoLen(), RT_MEMORY_HBM));
  ext_info_addrs_.emplace_back(aicpu_ext_info_addr);
  GE_CHK_RT_RET(rtMemcpy(aicpu_ext_info_addr, ext_handle->GetExtInfoLen(), ext_handle->GetExtInfo(),
                         ext_handle->GetExtInfoLen(), RT_MEMCPY_HOST_TO_DEVICE));
  return SUCCESS;
}

Status FftsPlusTaskInfo::CopyTaskInfoToWorkspace(const OpDescPtr &op_desc, const void *const task_info_addr,
                                                 const size_t task_info_addr_size) const {
  GE_CHECK_NOTNULL(davinci_model_);
  const auto &rts_param = davinci_model_->GetRuntimeParam();
  // Userspace copy need virtual address.
  const std::vector<int64_t> workspace_data_sizes = ModelUtils::GetWorkspaceSize(op_desc);
  const std::vector<void *> workspace_data_addrs = ModelUtils::GetWorkspaceDataAddrs(rts_param, op_desc);
  if (workspace_data_addrs.empty() || workspace_data_sizes.empty()) {
    REPORT_INNER_ERROR("E19999", "Node:%s(%s) workspace addr:%zu or size:%zu empty, check invalid",
                       op_desc->GetName().c_str(), op_desc->GetType().c_str(),
                       workspace_data_addrs.size(), workspace_data_sizes.size());
    GELOGE(FAILED, "[Check][Param] Node:%s invalid workspace, addrs is %zu, size is %zu.", op_desc->GetName().c_str(),
           workspace_data_addrs.size(), workspace_data_sizes.size());
    return FAILED;
  }

  if (workspace_data_addrs[0U] == nullptr) {
    REPORT_INNER_ERROR("E19999", "Node:%s(%s) workspace addr is nullptr, check invalid",
                       op_desc->GetName().c_str(), op_desc->GetType().c_str());
    GELOGE(FAILED, "[Check][Param] Node:%s workspace addrs is null.", op_desc->GetName().c_str());
    return FAILED;
  }

  if ((workspace_data_sizes[0U] < static_cast<int64_t>(task_info_addr_size)) ||
      (workspace_data_sizes[0U] > static_cast<int64_t>(rts_param.mem_size))) {
    REPORT_INNER_ERROR("E19999",
                       "Node:%s(%s) workspace size:%ld < task info size:%zu or workspace size > total mem "
                       "size %lu, check invalid",
                       op_desc->GetName().c_str(), op_desc->GetType().c_str(), workspace_data_sizes[0U],
                       task_info_addr_size, rts_param.mem_size);
    GELOGE(FAILED,
           "[Check][Param] Node:%s workspace size is %ld, task info size is %zu or workspace size > total mem "
           "size %lu, check invalid",
           op_desc->GetName().c_str(), workspace_data_sizes[0U], task_info_addr_size, rts_param.mem_size);
    return FAILED;
  }

  GE_CHK_RT_RET(rtMemcpy(workspace_data_addrs[0U], static_cast<uint64_t>(workspace_data_sizes[0U]),
                         task_info_addr, static_cast<uint64_t>(task_info_addr_size), RT_MEMCPY_HOST_TO_DEVICE));

  return SUCCESS;
}

Status FftsPlusTaskInfo::InitAicpuIoAddrs(const OpDescPtr &op_desc, const uintptr_t &io_addr) const {
  const RuntimeParam &rts_param = davinci_model_->GetRuntimeParam();
  const std::vector<void *> input_addrs = ModelUtils::GetInputAddrs(rts_param, op_desc);
  const std::vector<void *> output_addrs = ModelUtils::GetOutputAddrs(rts_param, op_desc);
  const vector<int64_t> input_size = ModelUtils::GetInputSize(op_desc);
  const vector<int64_t> output_size = ModelUtils::GetOutputSize(op_desc);
  if (input_addrs.size() != input_size.size()) {
    GELOGE(FAILED, "input addrs size[%zu] is not equal to input size[%zu]", input_addrs.size(), input_size.size());
    return FAILED;
  }
  if (output_addrs.size() != output_size.size()) {
    GELOGE(FAILED, "output addrs size[%zu] is not equal to output size[%zu]", output_addrs.size(), output_size.size());
    return FAILED;
  }
  std::vector<void *> io_addrs;
  const uint64_t *const input_offset = PtrToPtr<void, uint64_t>(ValueToPtr(io_addr));
  for (size_t i = 0U; i < input_addrs.size(); ++i) {
    const auto step = input_offset[i];
    io_addrs.emplace_back(ValueToPtr(PtrToValue(input_addrs[i]) + step));
    GELOGI("Node[%s] type[%s] index[%zu] size=%zu, input size[%lu], input addr=%p", op_desc->GetName().c_str(),
           op_desc->GetType().c_str(), i, step, input_offset[i], ValueToPtr(PtrToValue(input_addrs[i]) + step));
  }

  const uint64_t *const output_offset =
    PtrToPtr<void, uint64_t>(ValueToPtr(io_addr + (input_addrs.size() * sizeof(uint64_t))));
  for (size_t i = 0U; i < output_addrs.size(); ++i) {
    const auto step = output_offset[i];
    io_addrs.emplace_back(ValueToPtr(PtrToValue(output_addrs[i]) + step));
    GELOGI("Node[%s] type[%s] index[%zu] size=%zu, output size[%lu], output addr=%p", op_desc->GetName().c_str(),
           op_desc->GetType().c_str(), i, step, output_offset[i], ValueToPtr(PtrToValue(output_addrs[i]) + step));
  }

  GELOGI("Node[%s] type[%s] io_addrs size is [%zu]", op_desc->GetName().c_str(), op_desc->GetType().c_str(),
         io_addrs.size());
  if (!io_addrs.empty()) {
    const auto addrs_size = sizeof(uint64_t) * io_addrs.size();
    const errno_t sec_ret = memcpy_s(reinterpret_cast<void *>(io_addr), addrs_size, io_addrs.data(), addrs_size);
    if (sec_ret != EOK) {
      REPORT_CALL_ERROR("E19999", "Call memcpy_s fail, size:%lu, ret:0x%X", addrs_size, sec_ret);
      GELOGE(FAILED, "[Call][Memcpy] failed, size:%lu, ret:0x%X", addrs_size, sec_ret);
      return FAILED;
    }
  }
  return SUCCESS;
}

REGISTER_TASK_INFO(RT_MODEL_TASK_FFTS_PLUS_TASK, FftsPlusTaskInfo);
}  // namespace ge
