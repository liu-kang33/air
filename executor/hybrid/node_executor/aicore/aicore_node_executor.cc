/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hybrid/node_executor/aicore/aicore_node_executor.h"
#include "framework/common/taskdown_common.h"
#include "hybrid/executor/hybrid_execution_context.h"
#include "external/runtime/rt_error_codes.h"
#include "single_op/task/build_task_utils.h"

namespace ge {
namespace hybrid {
REGISTER_NODE_EXECUTOR_BUILDER(NodeExecutorManager::ExecutorType::AICORE, AiCoreNodeExecutor);
AiCoreNodeTask::AiCoreNodeTask(std::vector<std::unique_ptr<AiCoreOpTask>> &&tasks)
    : NodeTask(), tasks_(std::move(tasks)) {
}

Status AiCoreNodeExecutor::LoadTask(const HybridModel &model, const NodePtr &node, shared_ptr<NodeTask> &task) const {
  GE_CHECK_NOTNULL(node);
  GELOGI("AiCoreNodeExecutor(%s) LoadTask Start.", node->GetName().c_str());
  const auto *const task_defs = model.GetTaskDefs(node);
  if ((task_defs == nullptr) || task_defs->empty()) {
    const auto node_item = model.GetNodeItem(node);
    GE_CHECK_NOTNULL(node_item);
    if (node_item->IsNoOp()) {
      task = MakeShared<NoOpTask>();
      return SUCCESS;
    } else {
      GELOGE(FAILED, "[Get][Task_defs]Task_defs is empty for node (%s(%s)), check invalid",
             node->GetName().c_str(), node->GetType().c_str());
      REPORT_CALL_ERROR("E19999", "Task_defs is empty for node (%s(%s)), check invalid",
                        node->GetName().c_str(), node->GetType().c_str());
      return FAILED;
    }
  }

  AiCoreTaskBuilder builder(node->GetOpDesc(), *task_defs);
  std::unique_ptr<AiCoreNodeTask> node_task;
  GE_CHK_STATUS_RET(builder.BuildTask(node_task, model),
                    "[Invoke][BuildTask][%s(%s)] Failed to build op tasks.",
                    node->GetName().c_str(), node->GetType().c_str());
  std::unique_ptr<NodeTask> aicpu_task;
  GE_CHK_STATUS_RET(builder.LoadAicpuTask(aicpu_task, model, node));
  node_task->SetAicpuTask(std::move(aicpu_task));
  task = std::move(node_task);
  GELOGI("AiCoreNodeExecutor(%s) LoadTask End.", node->GetName().c_str());
  return SUCCESS;
}

Status AiCoreNodeTask::ExecuteAsync(TaskContext &context, const std::function<void()> &done_callback) {
  RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeTaskExecuteAsync] Start");
  if (context.GetNodeItem().IsNoOp()) {
    GELOGD("[%s] Skipping execution for op with empty outputs", context.GetNodeName());
    const auto ret = context.TryExecuteCallback(done_callback);
    RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeTaskExecuteAsync] End");
    return ret;
  }

  if (aicpu_exec_) {
    GELOGI("Node[%s] Partially supported task executing tiling failed, switch to aicpu execution.",
           context.GetNodeName());
    return aicpu_task_->ExecuteAsync(context, done_callback);
  }
  GELOGI("[%s] ExecuteAsync Start.", context.GetNodeName());
  for (auto it = tasks_.begin(); it != tasks_.end(); ++it) {
    // AtomicAddrClean has 2 tasks
    if ((tasks_.size() == 2U) && (it == tasks_.begin()) && (!(*(tasks_.rbegin()))->GetClearAtomic())) {
      // add for misra 6-6-3
    } else {
      RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeLaunchKernel] Start");
      GE_CHK_STATUS_RET_NOLOG((*it)->LaunchKernel(context.GetStream()));
      GE_CHK_STATUS_RET_NOLOG(CheckOverflow(context));
      GE_CHECK_NOTNULL(context.GetExecutionContext()->model);
      GELOGD("[DEBUG_TASK_INFO : Executor Task] %s/%s %s",
             context.GetExecutionContext()->model->GetModelName().c_str(),
             (*it)->GetName().empty() ? (*it)->GetLogName().c_str() : (*it)->GetName().c_str(),
             BuildTaskUtils::GetTaskInfo(context).c_str());
      // save profiling data
      GE_CHK_STATUS_RET(context.SaveProfilingTaskDescInfo(kTaskTypeAicore, (*it)->GetBlockDim(), (*it)->GetOpType()),
                        "[Save][Profiling] failed for node[%s]!", context.GetNodeName());
      (*it)->FillExtraOpInfo(context.MutableExtraOpInfo());
      RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeLaunchKernel] End");
    }
  }
  auto callback = done_callback;
  // only last task need update outputs shape
  const auto task = tasks_.back().get();
  if (task->GetUnknownShapeOpType() == DEPEND_SHAPE_RANGE) {
    callback = [=, &context]() {
      Status callback_ret = SUCCESS;
      GELOGD("Node[%s] need update outputs shape.", context.GetNodeName());
      callback_ret = task->UpdateOutputsShape(context);
      if (done_callback != nullptr) {
        context.SetStatus(callback_ret);
        done_callback();
      }
    };
  }
  if (callback != nullptr) {
    RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeRegisterCallback] Start");
    GE_CHK_STATUS_RET_NOLOG(context.RegisterCallback(callback));
    RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeRegisterCallback] End");
  }

  GELOGD("[%s] ExecuteAsync End.", context.GetNodeName());
  RECORD_EXECUTION_EVENT(context.GetExecutionContext(), context.GetNodeName(), "[AiCoreNodeTaskExecuteAsync] End");
  return SUCCESS;
}

Status AiCoreNodeTask::UpdateArgs(TaskContext &context) {
  GELOGI("[%s] AiCoreNodeTask UpdateArgs Start.", context.GetNodeName());
  if (aicpu_exec_) {
    GE_CHK_STATUS_RET_NOLOG(aicpu_task_->UpdateArgs(context));
  } else {
    for (auto it = tasks_.rbegin(); it != tasks_.rend(); ++it) {
      GE_CHK_STATUS_RET_NOLOG((*it)->UpdateArgs(context));
      // AtomicAddrClean has 2 tasks
      if ((tasks_.size() == 2U) && (it == tasks_.rbegin()) && (!(*it)->GetClearAtomic())) {
        break;
      }
    }
  }

  GELOGI("[%s] AiCoreNodeTask UpdateArgs End.", context.GetNodeName());
  return SUCCESS;
}

Status AiCoreNodeTask::UpdateTilingData(TaskContext &task_context) {
  GELOGD("[%s] PrepareWithShape started", task_context.GetNodeName());
  for (auto it = tasks_.rbegin(); it != tasks_.rend(); ++it) {
    const auto ret = (*it)->PrepareWithShape(task_context);
    if (ret != SUCCESS) {
      if (aicpu_task_ != nullptr) {
        aicpu_exec_ = true;
        return SUCCESS;
      } else {
        REPORT_CALL_ERROR("EZ9999", "[Update][Tilingdata] Node[%s](%s) tiling failed!", task_context.GetNodeName(),
                          task_context.GetNodeItem().NodeType().c_str());
        GELOGE(ret, "[Update][Tilingdata] Node[%s](%s) tiling failed!", task_context.GetNodeName(),
               task_context.GetNodeItem().NodeType().c_str());
        return ret;
      }
    }

    // AtomicAddrClean has 2 tasks
    if ((tasks_.size() == 2U) && (it == tasks_.rbegin()) && (!(*it)->GetClearAtomic())) {
      break;
    }
  }
  GELOGD("[%s] Done PrepareWithShape successfully.", task_context.GetNodeName());
  return SUCCESS;
}

bool AiCoreNodeTask::IsSupportDynamicShape() {
  for (size_t i = 0U; i < tasks_.size(); ++i) {
    if (!tasks_[i]->IsDynamicShapeSupported()) {
      GELOGD("[%s] Task does not support dynamic shape.", tasks_[i]->GetName().c_str());
      return false;
    }
  }

  return true;
}

bool AiCoreNodeTask::IsSupportHostMemInputOpt() const {
  return true;
}

void AiCoreNodeTask::SetHostMemInput(const uintptr_t host_addr, const size_t length) {
  for (size_t i = 0U; i < tasks_.size(); ++i) {
    tasks_[i]->SetHostMemInput(host_addr, length);
  }
  return;
}

Status AiCoreNodeTask::CheckOverflow(TaskContext &context) const {
  const DumpProperties &dump_properties = context.GetDumpProperties();
  if (dump_properties.IsOpDebugOpen()) {
    GELOGD("Op %s is doing overflow check in hybrid engine", context.GetNodeName());
    const auto rt_ret = rtStreamSynchronize(context.GetStream());
    if (rt_ret == ACL_ERROR_RT_AICORE_OVER_FLOW) {
      context.SetOverFlow(true);
      GELOGW("Dynamic shape op %s is over flow", context.GetNodeName());
      return SUCCESS;
    } else if (rt_ret != RT_ERROR_NONE) {
      GELOGE(RT_FAILED, "[Invoke][RtStreamSynchronize] failed, ret:%d.", rt_ret);
      REPORT_CALL_ERROR("E19999", "rtStreamSynchronize failed, ret:%d.", rt_ret);
      return RT_ERROR_TO_GE_STATUS(rt_ret);
    } else {
      // add for misra rule 6-4-2
    }
    return SUCCESS;
  }
  GELOGD("Opdebug is not open in hybrid engine");
  return SUCCESS;
}

void AiCoreNodeTask::SetAicpuTask(std::unique_ptr<NodeTask> aicpu_task) {
  aicpu_task_ = std::move(aicpu_task);
}
}  // namespace hybrid
}  // namespace ge
