/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_HYBRID_KERNEL_AICORE_NODE_EXECUTOR_H_
#define GE_HYBRID_KERNEL_AICORE_NODE_EXECUTOR_H_

#include <map>
#include <mutex>
#include "hybrid/node_executor/aicore/aicore_task_builder.h"
#include "hybrid/node_executor/node_executor.h"

namespace ge {
namespace hybrid {
class AiCoreNodeTask : public NodeTask {
 public:
  explicit AiCoreNodeTask(std::vector<std::unique_ptr<AiCoreOpTask>> &&tasks);
  ~AiCoreNodeTask() override = default;
  bool IsSupportDynamicShape() override;
  bool IsSupportHostMemInputOpt() const override;
  void SetHostMemInput(const uintptr_t host_addr, const size_t length) override;
  Status UpdateTilingData(TaskContext &task_context) override;

  Status UpdateArgs(TaskContext &context) override;
  Status ExecuteAsync(TaskContext &context, const std::function<void()> &done_callback) override;

  void SetAicpuTask(std::unique_ptr<NodeTask> aicpu_task);

 private:
  Status CheckOverflow(TaskContext &context) const;
  std::vector<std::unique_ptr<AiCoreOpTask>> tasks_;

  std::unique_ptr<NodeTask> aicpu_task_;
  bool aicpu_exec_ = false;
};

class AiCoreNodeExecutor : public NodeExecutor {
 public:
  Status LoadTask(const HybridModel &model, const NodePtr &node, shared_ptr<NodeTask> &task) const override;
};

}  // namespace hybrid
}  // namespace ge

#endif //GE_HYBRID_KERNEL_AICORE_NODE_EXECUTOR_H_