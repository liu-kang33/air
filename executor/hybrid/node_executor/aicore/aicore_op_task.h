/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_HYBRID_KERNEL_AICORE_OP_TASK_H_
#define GE_HYBRID_KERNEL_AICORE_OP_TASK_H_

#include <memory>
#include <vector>
#include "common/dump/exception_dumper.h"
#include "framework/common/ge_inner_error_codes.h"
#include "runtime/stream.h"
#include "hybrid/common/tensor_value.h"
#include "hybrid/node_executor/task_context.h"
#include "proto/task.pb.h"
#include "register/op_tiling.h"

namespace ge {
namespace hybrid {
class TbeHandleHolder {
 public:
  explicit TbeHandleHolder(void *bin_handle);
  ~TbeHandleHolder();

 private:
  friend class TbeHandleRegistry;
  void *bin_handle_ = nullptr;
};

class TbeHandleRegistry {
 public:
  static TbeHandleRegistry &GetInstance() {
    static TbeHandleRegistry instance;
    return instance;
  }

  bool AddHandle(std::unique_ptr<TbeHandleHolder> &&holder);

 private:
  std::set<std::unique_ptr<TbeHandleHolder>> registered_handles_;
};

class AiCoreOpTask {
 public:
  AiCoreOpTask() = default;
  virtual ~AiCoreOpTask() = default;

  virtual Status Init(const OpDesc &op_desc, const domi::TaskDef &task_def);

  bool IsDynamicShapeSupported() const;

  // do preparation with shape(without actual io memory)
  Status PrepareWithShape(const TaskContext &context);

  virtual Status UpdateArgs(TaskContext &task_context);

  Status LaunchKernel(rtStream_t stream) const;

  const std::string& GetName() const;

  const std::string& GetLogName() const {return log_name_;}

  bool GetClearAtomic() const {return clear_atomic_;}

  uint32_t GetBlockDim() const {return block_dim_;}

  void SetSingleOp(const bool is_single_op) {is_single_op_ = is_single_op;}

  void SetOverflowAddr(void *const overflow_addr) {overflow_addr_  = overflow_addr;};

  virtual const std::string& GetOpType() const;

  UnknowShapeOpType GetUnknownShapeOpType() const {
    return unknown_shape_op_type_;
  }

  Status UpdateOutputsShape(const TaskContext &context) const;
  void SetHostMemInput(const uintptr_t host_addr, const size_t length);

  void FillExtraOpInfo(ExtraOpInfo &extra_op_info) const {
    extra_op_info.node_info = node_info_;
    extra_op_info.tiling_data = tiling_data_;
    extra_op_info.tiling_key = tiling_key_;
    extra_op_info.args = PtrToValue(args_.get()) + offset_;
  }

 protected:
  Status UpdateTilingInfo(const TaskContext &context);
  virtual std::string GetKeyForOpParamSize() const;
  virtual std::string GetKeyForTbeKernel() const;
  virtual std::string GetKeyForTvmMagic() const;
  virtual std::string GetKeyForTvmMetaData() const;
  virtual std::string GetKeyForKernelName(const OpDesc &op_desc) const;
  virtual Status CalcTilingInfo(const NodePtr &node, const Operator &op, optiling::utils::OpRunInfo &tiling_info) const;
  Status DoInit(const OpDesc &op_desc, const domi::TaskDef &task_def);

 private:
  static Status ValidateTaskDef(const domi::TaskDef &task_def);
  Status InitWithTaskDef(const OpDesc &op_desc, const domi::TaskDef &task_def);
  Status InitTilingInfo(const OpDesc &op_desc);
  Status RegisterTbeHandle(const OpDesc &op_desc);
  Status RegisterKernelHandle(const OpDesc &op_desc);
  Status InitWithKernelDef(const OpDesc &op_desc, const domi::TaskDef &task_def);
  Status InitWithKernelDefWithHandle(const OpDesc &op_desc, const domi::TaskDef &task_def);
  Status UpdateShapeToOutputDesc(const TaskContext &context, const GeShape &shape, const int32_t output_index) const;
  Status LaunchKernelWithTiling(rtStream_t stream) const;
  Status UpdateArgBase(const TaskContext &task_context, uint32_t &index);
  void SetTaskTag() const;

  std::string stub_name_;
  void *stub_func_ = nullptr;
  std::unique_ptr<uint8_t[]> args_ = nullptr;
  uint32_t block_dim_ = 1U;
  bool clear_atomic_ = true;
  bool is_single_op_ = false;
  std::vector<int32_t> output_indices_to_skip_;
#ifdef ONLY_COMPILE_OPEN_SRC
  std::string original_kernel_key_;
#endif
  std::string node_info_;
  uint64_t tiling_key_ = 0U;
  void *handle_ = nullptr;
  bool is_dynamic_ = false;
  uint64_t log_id_ = 0U;
  std::string log_name_;
  std::string op_type_;
  UnknowShapeOpType unknown_shape_op_type_ = DEPEND_IN_SHAPE;
  std::unique_ptr<TensorBuffer> shape_buffer_ = nullptr;
  std::unique_ptr<uint8_t[]> host_shape_buffer_ = nullptr;
  size_t host_mem_input_len_ = 0U;
  uintptr_t host_mem_input_addr_ = 0U;
  std::string op_name_;
  std::string tiling_data_;
  uint32_t max_tiling_size_ = 0U;
  bool need_tiling_ = false;

  uintptr_t *arg_base_ = nullptr;
  void *overflow_addr_ = nullptr;
  uint32_t args_size_ = 0U;
  uint32_t args_size_without_tiling_ = 0U;
  uint32_t max_arg_count_ = 0U;
  std::unique_ptr<rtArgsWithTiling_t> args_with_tiling_;
  uint32_t offset_ = 0U;
  friend class AtomicAddrCleanOpTask;
};

class AtomicAddrCleanOpTask : public AiCoreOpTask {
 public:
  Status Init(const OpDesc &op_desc, const domi::TaskDef &task_def) override;
  Status UpdateArgs(TaskContext &task_context) override;
  const std::string& GetOpType() const override;

 protected:
  std::string GetKeyForOpParamSize() const override;
  std::string GetKeyForTbeKernel() const override;
  std::string GetKeyForTvmMagic() const override;
  std::string GetKeyForTvmMetaData() const override;
  std::string GetKeyForKernelName(const OpDesc &op_desc) const override;
  Status CalcTilingInfo(const NodePtr &node, const Operator &op,
                        optiling::utils::OpRunInfo &tiling_info) const override;

 private:
  Status InitAtomicAddrCleanIndices(const OpDesc &op_desc);
  std::vector<int32_t> atomic_output_indices_;
  std::vector<int32_t> atomic_workspace_indices_;
};
}  // namespace hybrid
}  // namespace ge
#endif //GE_HYBRID_KERNEL_AICORE_OP_TASK_H_
