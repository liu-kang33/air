/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_HYBRID_CONTROLOP_CONTROL_OP_EXECUTOR_H_
#define GE_HYBRID_CONTROLOP_CONTROL_OP_EXECUTOR_H_

#include <vector>
#include "hybrid/node_executor/node_executor.h"
#include "hybrid/model/graph_item.h"

namespace ge {
namespace hybrid {
class ControlOpNodeTask : public NodeTask {
 public:
  ControlOpNodeTask() = default;
  ~ControlOpNodeTask() override = default;
  GE_DELETE_ASSIGN_AND_COPY(ControlOpNodeTask);
  using NodeTask::Init;
  virtual Status Init(const NodePtr &node, const HybridModel &model) = 0;
  Status UpdateArgs(TaskContext &context) override;

  Status ExecuteAsync(TaskContext &context, const std::function<void()> &done_callback) override;

 protected:
  virtual Status DoExecuteAsync(TaskContext &task_context, const std::function<void()> &done_callback) const = 0;
  static Status ToBool(const TensorValue &tensor, const DataType data_type, bool &value);
  Status ExecuteSubgraph(const GraphItem *subgraph, const TaskContext &task_context,
                         const std::function<void()> &done_callback) const;
};

class IfOpNodeTask : public ControlOpNodeTask {
 public:
  Status Init(const NodePtr &node, const HybridModel &model) override;

 protected:
  Status DoExecuteAsync(TaskContext &task_context, const std::function<void()> &done_callback) const override;

 private:
  static constexpr int32_t kIfCondIndex = 0;
  static constexpr uint32_t kThenBranchIndex = 0U;
  static constexpr uint32_t kElseBranchIndex = 1U;

  const GraphItem *then_ = nullptr;
  const GraphItem *else_ = nullptr;
};

class CaseOpNodeTask : public ControlOpNodeTask {
 public:
  Status Init(const NodePtr &node, const HybridModel &model) override;

 protected:
  const GraphItem* SelectBranch(size_t branch_index) const;
  Status DoExecuteAsync(TaskContext &task_context, const std::function<void()> &done_callback) const override;

 private:
  static constexpr int32_t kCaseBranchIndex = 0;
  static constexpr size_t kMaxBranchNum = SIZE_MAX;
  static constexpr size_t kMinBranchNum = 1U;

  std::vector<const GraphItem *> subgraphs_;
};

class WhileOpNodeTask : public ControlOpNodeTask {
 public:
  Status Init(const NodePtr &node, const HybridModel &model) override;

 protected:
  Status DoExecuteAsync(TaskContext &task_context, const std::function<void()> &done_callback) const override;
  Status ExecuteCond(const TaskContext &task_context, bool &is_continue) const;

  static Status MoveOutputs2Inputs(const TaskContext &task_context);
  Status ExecuteOneLoop(const TaskContext &task_context, bool &is_continue) const;

 private:
  static constexpr uint32_t kCondBranchIndex = 0U;
  static constexpr uint32_t kBodyBranchIndex = 1U;
  static constexpr size_t kCondOutputSize = 1U;

  const GraphItem *cond_ = nullptr;
  const GraphItem *body_ = nullptr;
};

class ControlOpNodeExecutor : public NodeExecutor {
 public:
  Status LoadTask(const HybridModel &model, const NodePtr &node, shared_ptr<NodeTask> &task) const override;
  Status PrepareTask(NodeTask &task, TaskContext &context) const override;
};
}  // namespace hybrid
}  // namespace ge
#endif // GE_HYBRID_CONTROLOP_CONTROL_OP_EXECUTOR_H_
