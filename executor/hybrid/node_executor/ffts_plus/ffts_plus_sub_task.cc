/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hybrid/node_executor/ffts_plus/ffts_plus_sub_task.h"
#include "hybrid/node_executor/ffts_plus/ffts_plus_sub_task_factory.h"
#include "graph/load/model_manager/task_info/ffts_plus_proto_transfer.h"

#include "common/ge_call_wrapper.h"
#include "graph/utils/op_desc_utils.h"
#include "framework/common/op/ge_op_utils.h"

namespace ge {
namespace {
constexpr uint8_t kTaskAutoAicCtxIndex = 0U;
constexpr uint8_t kTaskAutoTailAicCtxIndex = 1U;
constexpr uint8_t kTaskAutoAivCtxIndex = 2U;
constexpr uint8_t kTaskAutoTailAivCtxIndex = 3U;
constexpr uint8_t kTilingPos = 0U;
constexpr uint8_t kTilingTailPos = 1U;

const std::string kTaskCoreTypeAIC = "AIC";
const std::string kTaskCoreTypeAIV = "AIV";
const std::string kTaskCoreTypeMixAIC = "MIX_AIC";
const std::string kTaskCoreTypeMixAIV = "MIX_AIV";
const std::string kTaskCoreTypeAiCpu = "AICPU";
const std::string kTaskCoreTypeMixL2 = "MIX_L2";

inline size_t MemSizeAlign(const size_t size) {
  constexpr uint32_t kAlignSize = 32U;
  return (((size + kAlignSize) - 1U) / kAlignSize) * kAlignSize;
}

// stub for tune
Status FFTSNodeThread(const ge::ComputeGraph &compute_graph, const std::string &node_name) {
  (void)node_name;
  return compute_graph.GetAllNodes().empty() ? ge::FAILED : ge::SUCCESS;
}
}

namespace hybrid {
REGISTER_FFTS_PLUS_SUB_TASK_CREATOR(kTaskCoreTypeAIC, FftsPlusAicAivTask);
REGISTER_FFTS_PLUS_SUB_TASK_CREATOR(kTaskCoreTypeAIV, FftsPlusAicAivTask);
REGISTER_FFTS_PLUS_SUB_TASK_CREATOR(kTaskCoreTypeMixAIC, FftsPlusMixAicAivTask);
REGISTER_FFTS_PLUS_SUB_TASK_CREATOR(kTaskCoreTypeMixAIV, FftsPlusMixAicAivTask);
REGISTER_FFTS_PLUS_SUB_TASK_CREATOR(kTaskCoreTypeAiCpu, FftsPlusAiCpuTask);
REGISTER_FFTS_PLUS_SUB_TASK_CREATOR(kTaskCoreTypeMixL2, FftsPlusMixL2Task);

Status FftsPlusSubTask::Load(const HybridModel &model, const NodePtr &node) {
  GE_CHECK_NOTNULL(node);
  // Get FftsPlusUpdate instance by CORE_TYPE.
  std::string op_core_type;
  (void)AttrUtils::GetStr(node->GetOpDesc(), ATTR_NAME_CUBE_VECTOR_CORE_TYPE, op_core_type);
  ffts_plus_ctx_update_ = FftsPlusUpdateManager::Instance().GetUpdater(op_core_type);
  GE_CHECK_NOTNULL(ffts_plus_ctx_update_);

  // Get FftsPlusNodeTask of PartitionedCall.
  const auto &owner_graph = node->GetOwnerComputeGraph();
  GE_CHECK_NOTNULL(owner_graph);
  const auto &parent_node = owner_graph->GetParentNode();
  GE_CHECK_NOTNULL(parent_node);
  ffts_node_item_ = model.GetNodeItem(parent_node);
  GE_CHECK_NOTNULL(ffts_node_item_);

  GELOGD("[%s] Set FftsPlus Node success: %s.", node->GetName().c_str(), ffts_node_item_->NodeName().c_str());
  return SUCCESS;
}

/**
 * @brief Init FFTS Plus Node Task for Update.
 * @param task_context: instance of TaskContext
 * @return SUCCESS on success, error code otherwise
 */
Status FftsPlusSubTask::Init(TaskContext &context) {
  if (ffts_node_task_ != nullptr) {
    return SUCCESS;
  }

  GE_CHECK_NOTNULL(ffts_node_item_);
  GE_CHECK_NOTNULL(ffts_node_item_->kernel_task);
  ffts_node_task_ = dynamic_cast<FftsPlusNodeTask *>(ffts_node_item_->kernel_task.get());
  GE_CHECK_NOTNULL(ffts_node_task_);

  GELOGD("[%s] Get FftsPlusNodeTask success.", context.GetNodeName());
  return SUCCESS;
}

/**
 * @brief Update tiling data and ffts context.
 * @param task_context: instance of TaskContext
 * @return SUCCESS on success, error code otherwise
 */
Status FftsPlusSubTask::UpdateTilingData(TaskContext &task_context) {
  // Step1: Get FftsPlusNodeTask for first time (Task create not in topological order)
  GELOGD("[%s] Start to Update FFTS Plus context.", task_context.GetNodeName());
  GE_CHK_STATUS_RET_NOLOG(Init(task_context));
  GE_CHECK_NOTNULL(ffts_node_task_);

  // Step2: Thread slice for Node.
  GE_TIMESTAMP_START(FFTSNodeThread);
  const auto &node_item = task_context.GetNodeItem();
  const auto &subgraph = node_item.node->GetOwnerComputeGraph();
  GE_CHECK_NOTNULL(subgraph);
  Status status = FFTSNodeThread(*subgraph, node_item.NodeName());
  if (status != SUCCESS) {
    REPORT_INNER_ERROR("E19999", "[%s] FFTS Node-Thread failed: %u.", task_context.GetNodeName(), status);
    GELOGE(INTERNAL_ERROR, "[%s] FFTS Node-Thread failed: %u.", task_context.GetNodeName(), status);
    return INTERNAL_ERROR;
  }
  GE_TIMESTAMP_EVENT_END(FFTSNodeThread, task_context.GetNodeName());

  // Step3: Op Tiling for Node.
  GE_TIMESTAMP_START(InitThreadRunInfo);
  AutoThreadSubTaskFlush sub_task_flush{};
  sub_task_flush.device_id = ffts_node_task_->device_id_;
  GE_CHK_STATUS_RET_NOLOG(InitThreadRunInfo(task_context, sub_task_flush));
  GE_TIMESTAMP_EVENT_END(InitThreadRunInfo, task_context.GetNodeName());

  // Step5: Get Thread slice parameters.
  GE_TIMESTAMP_START(InitThreadRunParam);
  AutoThreadParam params{};
  status = ffts_plus_ctx_update_->GetAutoThreadParam(node_item.node, sub_task_flush.op_run_info, params);
  GE_CHK_STATUS_RET(status, "[%s] FFTS GetAutoThreadParam failed, status: %u", task_context.GetNodeName(), status);
  if (params.thread_dim == 0U) {
    REPORT_INNER_ERROR("E19999", "[%s] FFTS GetAutoThreadParam failed: thread dim is zero", task_context.GetNodeName());
    GELOGE(FAILED, "[%s] FFTS GetAutoThreadParam failed: thread dim is zero", task_context.GetNodeName());
    return FAILED;
  }
  GE_CHK_STATUS_RET_NOLOG(InitThreadRunParam(task_context, params, sub_task_flush));
  GE_TIMESTAMP_EVENT_END(InitThreadRunParam, task_context.GetNodeName());

  // Step6: Update task slice context.
  GE_TIMESTAMP_START(UpdateSubTaskAndCache);
  status = ffts_plus_ctx_update_->UpdateSubTaskAndCache(node_item.node, sub_task_flush,
                                                        ffts_node_task_->ffts_plus_task_info_);
  if (status != SUCCESS) {
    REPORT_INNER_ERROR("E19999", "[%s] FFTS UpdateSubTaskAndCache failed: %u.", task_context.GetNodeName(), status);
    GELOGE(INTERNAL_ERROR, "[%s] FFTS UpdateSubTaskAndCache failed: %u.", task_context.GetNodeName(), status);
    return INTERNAL_ERROR;
  }
  GE_TIMESTAMP_EVENT_END(UpdateSubTaskAndCache, task_context.GetNodeName());

  GELOGD("[%s] Done Update FFTS Plus context successfully.", task_context.GetNodeName());
  return SUCCESS;
}

void FftsPlusSubTask::InitCtxIoAddrs(const AutoThreadParam &params, const size_t ctx_idx, const uintptr_t data_base,
                                     std::vector<uintptr_t> &io_addrs) const {
  for (uint32_t i = 0U; i < params.thread_dim; ++i) {
    const size_t ctx_io_idx = ((io_addrs.size() / params.thread_dim) * i) + ctx_idx;
    GELOGD("addr base: 0x%lx, addr index: %zu, thread index: %u, thread addr offset: 0x%lx, args index: %zu",
           data_base, ctx_idx, i, params.task_addr_offset[ctx_idx], ctx_io_idx);
    io_addrs[ctx_io_idx] = (data_base + (params.task_addr_offset[ctx_idx] * i));
  }
}

Status FftsPlusAiCoreTask::InitThreadRunInfo(TaskContext &task_context, AutoThreadSubTaskFlush &sub_task_flush) {
  GE_TIMESTAMP_START(OpFftsCalculateV2);
  const auto &node_item = task_context.GetNodeItem();
  const Status status = optiling::OpFftsCalculateV2(*node_item.node, sub_task_flush.op_run_info);
  if (status != SUCCESS) {
    REPORT_INNER_ERROR("E19999", "[%s] FFTS Calculate tiling failed: %u.", task_context.GetNodeName(), status);
    GELOGE(INTERNAL_ERROR, "[%s] FFTS Calculate tiling failed: %u.", task_context.GetNodeName(), status);
    return INTERNAL_ERROR;
  }
  GE_TIMESTAMP_EVENT_END(OpFftsCalculateV2, task_context.GetNodeName());

  // Step4: Get Start PC and prefetch count.
  GE_TIMESTAMP_START(UpdateAddrAndPrefCnt);
  GE_CHK_STATUS_RET_NOLOG(UpdateAddrAndPrefCnt(*node_item.op_desc, sub_task_flush));
  tiling_num_ = sub_task_flush.op_run_info.size() / 2U; // Non-Tail + Tail

  // Step5: Prepare workspace and Tiling data.
  host_data_.clear();
  std::vector<int64_t> workspaces;
  GE_CHK_STATUS_RET_NOLOG(InitOpRunInfo(sub_task_flush.op_run_info, workspaces));
  node_item.op_desc->SetWorkspaceBytes(workspaces);
  GE_TIMESTAMP_EVENT_END(UpdateAddrAndPrefCnt, task_context.GetNodeName());

  return status;
}

Status FftsPlusAiCoreTask::InitThreadRunParam(TaskContext &task_context, const AutoThreadParam &params,
                                              AutoThreadSubTaskFlush &sub_task_flush) {
  const NodeItem &node_item = task_context.GetNodeItem();
  GE_TIMESTAMP_START(AllocateWorkspaces);
  const std::vector<int64_t> &workspaces = node_item.op_desc->GetWorkspaceBytes();
  const int32_t io_addr_size = node_item.num_inputs + node_item.num_outputs;
  const size_t addr_size = static_cast<size_t>(io_addr_size) + workspaces.size();
  if (addr_size != params.task_addr_offset.size()) {
    REPORT_INNER_ERROR("E19999", "[%s] Invalid task addr size: %zu, inputs: %d, outputs: %d, workspaces: %zu",
                       task_context.GetNodeName(), params.task_addr_offset.size(),
                       node_item.num_inputs, node_item.num_outputs, workspaces.size());
    GELOGE(INTERNAL_ERROR, "[%s] Invalid task addr size: %zu, inputs: %d, outputs: %d, workspaces: %zu",
           task_context.GetNodeName(), params.task_addr_offset.size(), node_item.num_inputs, node_item.num_outputs,
           workspaces.size());
    return FAILED;
  }

  // Step7: Allocate workspace and memory for tiling and args.
  RECORD_EXECUTION_EVENT(task_context.GetExecutionContext(), task_context.GetNodeName(), "[ffts_plus_malloc] Start");
  GE_CHK_STATUS_RET_NOLOG(task_context.AllocateWorkspaces());

  // Addrs Num: Input / Output / Workspace / Tiling.
  std::vector<uintptr_t> io_addrs((addr_size + tiling_num_) * params.thread_dim);
  task_args_size_ = tiling_data_len_ + (sizeof(uintptr_t) * io_addrs.size());
  GE_CHK_STATUS_RET_NOLOG(task_context.AllocateWorkspace(task_args_size_, task_args_base_));
  sub_task_flush.args_base = ValueToPtr(PtrToValue(task_args_base_) + tiling_data_len_);
  RECORD_EXECUTION_EVENT(task_context.GetExecutionContext(), task_context.GetNodeName(), "[ffts_plus_malloc] End");
  GELOGD("[%s] Addr offset size: %zu, inputs: %d, outputs: %d, workspaces: %zu, tiling data len: %zu, args base: %p",
         task_context.GetNodeName(), params.task_addr_offset.size(), node_item.num_inputs, node_item.num_outputs,
         workspaces.size(), tiling_data_len_, sub_task_flush.args_base);
  GE_TIMESTAMP_EVENT_END(AllocateWorkspaces, task_context.GetNodeName());

  // Step8: Arrange context args and Flush Task.
  GE_TIMESTAMP_START(InitTaskAddrs);
  GE_CHK_STATUS_RET_NOLOG(InitTaskAddrs(task_context, params, sub_task_flush, io_addrs));
  GE_TIMESTAMP_EVENT_END(InitTaskAddrs, task_context.GetNodeName());
  return SUCCESS;
}

Status FftsPlusAiCoreTask::ExecuteAsync(TaskContext &context, const std::function<void()> &done_callback) {
  (void)done_callback;
  GELOGD("[%s] Start to execute.", context.GetNodeName());
  if ((task_args_base_ != nullptr) && (!host_data_.empty())) {
    GELOGD("[%s]Copy, mem size: %zu, data size: %zu", context.GetNodeName(), task_args_size_, host_data_.size());
    GE_CHK_RT_RET(rtMemcpyAsync(task_args_base_, task_args_size_, host_data_.data(), host_data_.size(),
                                RT_MEMCPY_HOST_TO_DEVICE_EX, context.GetStream()));
  } else {
    GELOGW("[%s]Empty, mem size: %zu, data size: %zu", context.GetNodeName(), task_args_size_, host_data_.size());
  }

  GELOGD("[%s] Done executing successfully.", context.GetNodeName());
  return SUCCESS;
}

/**
 * @brief Set workspace and copy Tiling data.
 * @param op_run_info: OpRunInfo from OpFftsCalculateV2
 * @param workspaces: workspace bytes for current Node
 * @param tiling_offset: Offset set for tiling data
 * @param tiling_data_len: Total memory size for tiling data.
 * @return SUCCESS on success, error code otherwise
 */
Status FftsPlusAiCoreTask::InitOpRunInfo(const std::vector<optiling::utils::OpRunInfo> &op_run_info,
                                         std::vector<int64_t> &workspaces) {
  for (const auto &run_info : op_run_info) {
    std::vector<int64_t> workspace;
    run_info.GetAllWorkspaces(workspace);
    (void)workspaces.insert(workspaces.end(), workspace.begin(), workspace.end());

    tiling_offset_.emplace_back(tiling_data_len_);
    const auto &tiling_data = run_info.GetAllTilingData().str();
    if (tiling_data.empty()) {
      continue;
    }

    const auto data_pos = host_data_.size();
    tiling_data_len_ += MemSizeAlign(tiling_data.size());
    host_data_.resize(tiling_data_len_);
    GELOGD("Tiling data size: %zu, Total size: %zu, host data size: %zu, copy offset: %zu",
           tiling_data.size(), tiling_data_len_, host_data_.size(), data_pos);
    const errno_t sec_ret = memcpy_s(&host_data_[data_pos], host_data_.size() - data_pos,
                                     tiling_data.data(), tiling_data.size());
    if (sec_ret != EOK) {
      GELOGE(INTERNAL_ERROR, "Copy tiling data failed, data size=%zu, data pos: %zu, tiling size:%zu, err: %d.",
             host_data_.size(), data_pos, tiling_data.size(), sec_ret);
      return INTERNAL_ERROR;
    }
  }

  return SUCCESS;
}

Status FftsPlusAiCoreTask::InitTaskAddrs(const TaskContext &task_context, const AutoThreadParam &params,
                                         AutoThreadSubTaskFlush &sub_task_flush, std::vector<uintptr_t> &io_addrs) {
  size_t io_index = 0U;
  const auto &node_item = task_context.GetNodeItem();
  for (int32_t i = 0; i < node_item.num_inputs; ++i) {
    const TensorValue *const tensor = task_context.GetInput(i);
    GE_CHECK_NOTNULL(tensor);
    const uintptr_t data_base = PtrToValue(tensor->GetData());
    InitCtxIoAddrs(params, io_index + static_cast<uint32_t>(i), data_base, io_addrs);
    sub_task_flush.input_addr_base.emplace_back(data_base);
  }
  io_index += static_cast<uint32_t>(node_item.num_inputs);

  for (int32_t i = 0; i < node_item.num_outputs; ++i) {
    const TensorValue *const tensor = task_context.GetOutput(i);
    GE_CHECK_NOTNULL(tensor);
    const uintptr_t data_base = PtrToValue(tensor->GetData());
    InitCtxIoAddrs(params, io_index + static_cast<uint32_t>(i), data_base, io_addrs);
    sub_task_flush.output_addr_base.emplace_back(data_base);
  }
  io_index += static_cast<uint32_t>(node_item.num_outputs);

  const auto &workspaces = node_item.op_desc->GetWorkspaceBytes();
  for (size_t i = 0U; i < workspaces.size(); ++i) {
    const void *const workspace = task_context.MutableWorkspace(static_cast<int32_t>(i));
    GE_CHECK_NOTNULL(workspace);
    InitCtxIoAddrs(params, io_index + i, PtrToValue(workspace), io_addrs);
  }
  io_index += workspaces.size();

  InitOpTiling(params, io_index, PtrToValue(task_args_base_), io_addrs);

  // Step9: Append args data for rtMemcpyAsync.
  const auto data_pos = host_data_.size();
  const size_t task_args_bytes = sizeof(uintptr_t) * io_addrs.size();
  host_data_.resize(data_pos + task_args_bytes);
  const errno_t sec_ret = memcpy_s(&host_data_[data_pos], host_data_.size() - data_pos,
                                   io_addrs.data(), task_args_bytes);
  if (sec_ret != EOK) {
    GELOGE(INTERNAL_ERROR, "Copy args data failed, data size=%zu, data pos: %zu, args size:%zu, err: %d.",
           host_data_.size(), data_pos, io_addrs.size(), sec_ret);
    return INTERNAL_ERROR;
  }

  return SUCCESS;
}

void FftsPlusAicAivTask::InitOpTiling(const AutoThreadParam &params, const size_t ctx_idx, const uintptr_t data_base,
                                      std::vector<uintptr_t> &io_addrs) const {
  const size_t io_size = io_addrs.size() / params.thread_dim;
  for (uint32_t i = 0U; i < params.thread_dim; ++i) {
    const auto data_pos = (i < (params.thread_dim - 1U)) ? tiling_offset_[kTilingPos] : tiling_offset_[kTilingTailPos];
    GELOGD("addr base: 0x%lx, addr index: %zu, thread index: %u, thread addr offset: 0x%lx, args index: %zu",
           data_base, ctx_idx, i, data_pos, (io_size * i) + ctx_idx);
    io_addrs[(io_size * i) + ctx_idx] = (data_base + data_pos);
  }
}

Status FftsPlusAicAivTask::UpdateAddrAndPrefCnt(const OpDesc &op_desc, AutoThreadSubTaskFlush &sub_task_flush) {
  const auto &run_info = sub_task_flush.op_run_info;
  if (run_info.size() <= kTaskAutoTailAicCtxIndex) {
    REPORT_INNER_ERROR("E19999", "[%s] Run info invalid, size is: %zu", op_desc.GetName().c_str(), run_info.size());
    GELOGE(INTERNAL_ERROR, "[%s] Run info invalid, size is: %zu", op_desc.GetName().c_str(), run_info.size());
    return INTERNAL_ERROR;
  }

  uint32_t prefetch_cnt = 0U;
  Status status = ffts_node_task_->GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoAicCtxIndex].GetTilingKey(),
                                                     sub_task_flush.aic_non_tail_task_start_pc, prefetch_cnt);
  GE_CHK_STATUS_RET(status, "[%s] Get AIC task start PC failed.", op_desc.GetName().c_str());

  uint32_t tail_prefetch_cnt = 0U;
  status = ffts_node_task_->GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoTailAicCtxIndex].GetTilingKey(),
                                              sub_task_flush.aic_tail_task_start_pc, tail_prefetch_cnt);
  GE_CHK_STATUS_RET(status, "[%s] Get tail AIC task start PC failed.", op_desc.GetName().c_str());

  sub_task_flush.aic_icache_prefetch_cnt = std::min(prefetch_cnt, tail_prefetch_cnt);
  return SUCCESS;
}

Status FftsPlusMixAicAivTask::UpdateAddrAndPrefCnt(const OpDesc &op_desc, AutoThreadSubTaskFlush &sub_task_flush) {
  const auto &run_info = sub_task_flush.op_run_info;
  if (run_info.size() <= kTaskAutoTailAivCtxIndex) {
    REPORT_INNER_ERROR("E19999", "[%s] Tiling info invalid, size is: %zu", op_desc.GetName().c_str(), run_info.size());
    GELOGE(INTERNAL_ERROR, "[%s] Tiling info invalid, size is: %zu", op_desc.GetName().c_str(), run_info.size());
    return SUCCESS; // Test Feature: OpFftsCalculateV2 not Ready.
  }

  uint32_t aic_prefetch_cnt = 0U;
  Status status = ffts_node_task_->GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoAicCtxIndex].GetTilingKey(),
                                                     sub_task_flush.aic_non_tail_task_start_pc, aic_prefetch_cnt);
  GE_CHK_STATUS_RET(status, "[%s] Get MIX AIC task start PC failed.", op_desc.GetName().c_str());

  uint32_t tail_aic_prefetch_cnt = 0U;
  status = ffts_node_task_->GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoTailAicCtxIndex].GetTilingKey(),
                                              sub_task_flush.aic_tail_task_start_pc, tail_aic_prefetch_cnt);
  GE_CHK_STATUS_RET(status, "[%s] Get Tail MIX AIC task start PC failed.", op_desc.GetName().c_str());
  sub_task_flush.aic_icache_prefetch_cnt = std::min(aic_prefetch_cnt, tail_aic_prefetch_cnt);

  uint32_t aiv_prefetch_cnt = 0U;
  status = ffts_node_task_->GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoAivCtxIndex].GetTilingKey(),
                                              sub_task_flush.aiv_non_tail_task_start_pc, aiv_prefetch_cnt);
  GE_CHK_STATUS_RET(status, "[%s] Get MIX AIV task start PC failed.", op_desc.GetName().c_str());

  uint32_t tail_aiv_prefetch_cnt = 0U;
  status = ffts_node_task_->GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoTailAivCtxIndex].GetTilingKey(),
                                              sub_task_flush.aiv_tail_task_start_pc, tail_aiv_prefetch_cnt);
  GE_CHK_STATUS_RET(status, "[%s] Get Tail MIX AIV task start PC failed.", op_desc.GetName().c_str());
  sub_task_flush.aiv_icache_prefetch_cnt = std::min(aiv_prefetch_cnt, tail_aiv_prefetch_cnt);

  return SUCCESS;
}

void FftsPlusMixAicAivTask::InitOpTiling(const AutoThreadParam &params, const size_t ctx_idx, const uintptr_t data_base,
                                         std::vector<uintptr_t> &io_addrs) const {
  const size_t io_size = params.task_addr_offset.size() + 1U;
  for (uint32_t i = 0U; i < params.thread_dim; ++i) {
    const auto data_pos = (i < (params.thread_dim - 1U)) ? tiling_offset_[kTilingPos] : tiling_offset_[kTilingTailPos];
    GELOGD("addr base is 0x%lx, addr index is %zu, thread index is %u, tiling addr offset is 0x%lx",
           data_base, ctx_idx, i, data_pos);
    io_addrs[(io_size * i) + ctx_idx] = (data_base + data_pos);
  }
}

Status FftsPlusAiCpuTask::ExecuteAsync(TaskContext &context, const std::function<void()> &done_callback) {
  (void)context;
  (void)done_callback;
  return SUCCESS;
}

Status FftsPlusAiCpuTask::InitThreadRunParam(TaskContext &task_context, const AutoThreadParam &params,
                                             AutoThreadSubTaskFlush &sub_task_flush) {
  if (params.args_size > 0U) {
    const size_t alloc_size = params.args_size;
    GE_CHK_STATUS_RET_NOLOG(task_context.AllocateWorkspace(alloc_size, sub_task_flush.args_base));
  }

  if (params.extinfo_size > 0U) {
    const size_t alloc_size = params.extinfo_size;
    GE_CHK_STATUS_RET_NOLOG(task_context.AllocateWorkspace(alloc_size, sub_task_flush.extinfo_base));
  }

  const auto &node_item = task_context.GetNodeItem();
  for (int32_t i = 0; i < node_item.num_inputs; ++i) {
    const TensorValue *const tensor = task_context.GetInput(i);
    GE_CHECK_NOTNULL(tensor);
    sub_task_flush.input_addr_base.emplace_back(PtrToValue(tensor->GetData()));
  }

  for (int32_t i = 0; i < node_item.num_outputs; ++i) {
    const TensorValue *const tensor = task_context.GetOutput(i);
    GE_CHECK_NOTNULL(tensor);
    sub_task_flush.output_addr_base.emplace_back(PtrToValue(tensor->GetData()));
  }

  return SUCCESS;
}

Status FftsPlusMixL2Task::Load(const HybridModel &model, const NodePtr &node) {
  GE_CHECK_NOTNULL(node);
  const auto &op_desc = node->GetOpDesc();
  GE_CHECK_NOTNULL(op_desc);

  // Get FftsPlusUpdate instance by CORE_TYPE.
  std::string op_core_type;
  (void)AttrUtils::GetStr(op_desc, ATTR_NAME_CUBE_VECTOR_CORE_TYPE, op_core_type);
  ffts_plus_ctx_update_ = FftsPlusUpdateManager::Instance().GetUpdater(op_core_type);
  GE_CHECK_NOTNULL(ffts_plus_ctx_update_);

  const auto *const task_defs = model.GetTaskDefs(node);
  if ((task_defs == nullptr) || (task_defs->size() != 1U)) {
    REPORT_INNER_ERROR("E19999", "[%s] has no taskdef", op_desc->GetName().c_str());
    GELOGE(INTERNAL_ERROR, "[%s] has no taskdef", op_desc->GetName().c_str());
    return INTERNAL_ERROR;
  }

  GELOGD("[%s] start to transfer taskdef", node->GetName().c_str());
  GE_CHK_STATUS_RET_NOLOG(bin_kernel_handle_.RegisterKernelHandle(op_desc));

  const domi::TaskDef &task_def = task_defs->at(0U);
  const domi::FftsPlusTaskDef &ffts_plus_task_def = task_def.ffts_plus_task();

  std::vector<uintptr_t> io_addrs;
  std::set<size_t> mode_addr_idx;
  FftsPlusProtoTransfer ffts_proto_transfer(0U, io_addrs, ext_args_, mode_addr_idx);
  const auto find_node_handle = [op_desc](const uint32_t index_object) -> OpDescPtr {
    (void)index_object;
    return op_desc;
  };
  ffts_proto_transfer.SetFindNodeHandle(find_node_handle);
  GE_CHK_STATUS_RET_NOLOG(ffts_proto_transfer.Transfer(op_desc, ffts_plus_task_def, ffts_plus_task_info_));
  GELOGD("[%s] Done initialization successfully.", node->GetName().c_str());
  return SUCCESS;
}

Status FftsPlusMixL2Task::UpdateTilingData(TaskContext &task_context) {
  const auto &node_name = task_context.GetNodeName();
  GELOGD("[%s] start to update context", node_name);

  // Op Tiling for Node.
  AutoThreadSubTaskFlush sub_task_flush;
  const auto op = task_context.GetNodeState()->GetOperator(task_context.GetExecutionContext()->stage_id);
  GE_CHECK_NOTNULL(op);
  sub_task_flush.op_run_info.resize(1U);
  GE_TIMESTAMP_START(GetTilingRunInfo);
  GE_CHK_STATUS_RET(GetTilingRunInfo(*op, sub_task_flush.op_run_info.front()), "[%s] calculate tiling failed",
                    node_name);
  GE_TIMESTAMP_END(GetTilingRunInfo, "GetTilingRunInfo");
  // Get Start PC and prefetch count.
  GE_TIMESTAMP_START(UpdateAddrAndPrefCnt);
  const auto &node_item = task_context.GetNodeItem();
  GE_CHK_STATUS_RET(UpdateAddrAndPrefCnt(*node_item.op_desc, sub_task_flush),
                    "[%s] update pc and prefetch count failed", node_name);
  GE_TIMESTAMP_END(UpdateAddrAndPrefCnt, "UpdateAddrAndPrefCnt");
  // Prepare workspace and Tiling data.
  host_data_.clear();
  std::vector<int64_t> workspaces;
  GE_TIMESTAMP_START(InitOpRunInfo);
  GE_CHK_STATUS_RET(InitOpRunInfo(sub_task_flush.op_run_info, workspaces), "[%s] handle tiling result failed",
                    node_name);
  GE_TIMESTAMP_END(InitOpRunInfo, "InitOpRunInfo");
  node_item.op_desc->SetWorkspaceBytes(workspaces);
  // Allocate workspace and memory for tiling and args.
  GE_TIMESTAMP_START(AllocateWorkspaces);
  RECORD_EXECUTION_EVENT(task_context.GetExecutionContext(), node_name, "[mixl2_malloc] Start");
  GE_CHK_STATUS_RET(task_context.AllocateWorkspaces(), "[%s] malloc failed", node_name);
  // Addrs Num: Input / Output / Workspace / Tiling.
  const size_t tiling_num = 1U;  // only cube need tiling
  const int32_t io_addr_size = node_item.num_inputs + node_item.num_outputs;
  const size_t addr_size = static_cast<size_t>(io_addr_size) + workspaces.size();
  std::vector<uintptr_t> io_addrs(addr_size + tiling_num);
  task_args_size_ = tiling_data_len_ + (sizeof(uintptr_t) * io_addrs.size());
  GE_CHK_STATUS_RET(task_context.AllocateWorkspace(task_args_size_, task_args_base_), "[%s] malloc failed", node_name);
  sub_task_flush.args_base = ValueToPtr(PtrToValue(task_args_base_) + tiling_data_len_);
  RECORD_EXECUTION_EVENT(task_context.GetExecutionContext(), node_name, "[mixl2_malloc] End");
  GELOGD("[%s] inputs: %d, outputs: %d, workspaces: %zu, tiling data len: %zu, args base: %p", node_name,
         node_item.num_inputs, node_item.num_outputs, workspaces.size(), tiling_data_len_, sub_task_flush.args_base);
  GE_TIMESTAMP_END(AllocateWorkspaces, "AllocateWorkspaces");
  // Arrange context args and Flush Task.
  GE_TIMESTAMP_START(InitTaskAddrs);
  GE_CHK_STATUS_RET(InitTaskAddrs(task_context, sub_task_flush, io_addrs), "[%s] init task addr failed", node_name);
  GE_TIMESTAMP_END(InitTaskAddrs, "InitTaskAddrs");
  // Update rts ffts_plus_task_info_ info
  GE_TIMESTAMP_START(UpdateSubTaskAndCache);
  GE_CHK_STATUS_RET(ffts_plus_ctx_update_->UpdateSubTaskAndCache(node_item.node, sub_task_flush, ffts_plus_task_info_),
                    "[%s] handle tiling result failed", node_name);
  GE_TIMESTAMP_END(UpdateSubTaskAndCache, "UpdateSubTaskAndCache");
  GELOGD("[%s] Done Update FFTS Plus MixL2 context successfully.", task_context.GetNodeName());
  return SUCCESS;
}

Status FftsPlusMixL2Task::UpdateAddrAndPrefCnt(const OpDesc &op_desc, AutoThreadSubTaskFlush &sub_task_flush) {
  const auto &run_info = sub_task_flush.op_run_info;
  if (run_info.empty()) {
    REPORT_INNER_ERROR("E19999", "[%s] Run info invalid, size is: %zu", op_desc.GetName().c_str(), run_info.size());
    GELOGE(INTERNAL_ERROR, "[%s] Run info invalid, size is: %zu", op_desc.GetName().c_str(), run_info.size());
    return INTERNAL_ERROR;
  }
  // Only Cube need tiling
  return bin_kernel_handle_.GetAddrAndPrefCnt(op_desc, run_info[kTaskAutoAicCtxIndex].GetTilingKey(),
                                              sub_task_flush.aic_non_tail_task_start_pc,
                                              sub_task_flush.aic_icache_prefetch_cnt);
}

Status FftsPlusMixL2Task::GetTilingRunInfo(const ge::Operator &op, optiling::OpRunInfoV2 &op_run_info) {
  return optiling::OpParaCalculateV2(op, op_run_info);
}

Status FftsPlusMixL2Task::ExecuteAsync(TaskContext &context, const function<void()> &done_callback) {
  const auto &node_item = context.GetNodeItem();
  const auto &op_desc = node_item.node->GetOpDesc();
  GE_CHECK_NOTNULL(op_desc);
  GELOGI("[%s] FFTS Plus MixL2 launch start.", op_desc->GetName().c_str());
  if ((task_args_base_ != nullptr) && (!host_data_.empty())) {
    GELOGD("[%s] start copy: mem size: %zu, data size: %zu", op_desc->GetName().c_str(), task_args_size_,
           host_data_.size());
    GE_CHK_RT_RET(rtMemcpyAsync(task_args_base_, task_args_size_, host_data_.data(), host_data_.size(),
                                RT_MEMCPY_HOST_TO_DEVICE_EX, context.GetStream()));
  } else {
    GELOGW("[%s] no need copy mem size: %zu, data size: %zu", op_desc->GetName().c_str(), task_args_size_,
           host_data_.size());
  }
  const rtError_t rt_ret = rtFftsPlusTaskLaunch(&ffts_plus_task_info_, context.GetStream());
  if (rt_ret != RT_ERROR_NONE) {
    REPORT_INNER_ERROR("E19999", "[%s] Call rtFftsPlusTaskLaunch failed: 0x%X", context.GetNodeName(), rt_ret);
    GELOGE(RT_FAILED, "[Check][RT][%s] Call rtFftsPlusTaskLaunch failed: 0x%X", context.GetNodeName(), rt_ret);
    return RT_ERROR_TO_GE_STATUS(rt_ret);
  }

  const auto callback_func = [=]() {
    GELOGD("[%s] On FFTS Plus MixL2 callback", op_desc->GetName().c_str());
    if (done_callback != nullptr) {
      done_callback();
    }
    GELOGD("[%s] DoneFFTS Plus MixL2 callback.", op_desc->GetName().c_str());
    return SUCCESS;
  };

  GE_CHK_STATUS_RET(context.RegisterCallback(callback_func), "[Register][Callback] failed for [%s]",
                    context.GetNodeName());
  GELOGD("[%s] Done executing FFTS Plus MixL2 successfully.", context.GetNodeName());
  return SUCCESS;
}

Status FftsPlusMixL2Task::InitTaskAddrs(const TaskContext &task_context, AutoThreadSubTaskFlush &sub_task_flush,
                                        std::vector<uintptr_t> &io_addrs) {
  size_t io_index = 0U;
  const auto &node_item = task_context.GetNodeItem();
  for (int32_t i = 0; i < node_item.num_inputs; ++i) {
    const TensorValue *const tensor = task_context.GetInput(i);
    GE_CHECK_NOTNULL(tensor);
    const uintptr_t data_base = PtrToValue(tensor->GetData());
    (void)sub_task_flush.input_addr_base.emplace_back(data_base);
    InitMixL2Addrs(io_index + static_cast<uint32_t>(i), data_base, io_addrs);
  }
  io_index += static_cast<uint32_t>(node_item.num_inputs);

  for (int32_t i = 0; i < node_item.num_outputs; ++i) {
    const TensorValue *const tensor = task_context.GetOutput(i);
    GE_CHECK_NOTNULL(tensor);
    const uintptr_t data_base = PtrToValue(tensor->GetData());
    (void)sub_task_flush.output_addr_base.emplace_back(data_base);
    InitMixL2Addrs(io_index + static_cast<uint32_t>(i), data_base, io_addrs);
  }
  io_index += static_cast<uint32_t>(node_item.num_outputs);

  const auto &workspaces = node_item.op_desc->GetWorkspaceBytes();
  for (size_t i = 0U; i < workspaces.size(); ++i) {
    const void *const workspace = task_context.MutableWorkspace(static_cast<int32_t>(i));
    GE_CHECK_NOTNULL(workspace);
    InitMixL2Addrs(io_index + i, PtrToValue(workspace), io_addrs);
  }
  io_index += workspaces.size();
  // tiling addr
  InitMixL2Addrs(io_index, PtrToValue(task_args_base_), io_addrs);

  const auto data_pos = host_data_.size();
  const size_t task_args_bytes = sizeof(uintptr_t) * io_addrs.size();
  host_data_.resize(data_pos + task_args_bytes);
  const errno_t sec_ret =
      memcpy_s(&host_data_[data_pos], host_data_.size() - data_pos, io_addrs.data(), task_args_bytes);
  if (sec_ret != EOK) {
    GELOGE(INTERNAL_ERROR, "Copy args data failed, data size=%zu, data pos: %zu, args size:%zu, err: %d.",
           host_data_.size(), data_pos, io_addrs.size(), sec_ret);
    return INTERNAL_ERROR;
  }

  return SUCCESS;
}

void FftsPlusMixL2Task::InitMixL2Addrs(const size_t io_index, const uintptr_t data_base, vector<uintptr_t> &io_addrs) {
  GELOGD("addr base: 0x%lx, args index: %zu", data_base, io_index);
  io_addrs[io_index] = data_base;
}

FftsPlusMixL2Task::~FftsPlusMixL2Task() {
  for (auto &addr : ext_args_) {
    GE_FREE_RT_LOG(addr);
  }
  bin_kernel_handle_.CleanTbeHandle();
  CleanRtFftsPlusTask(ffts_plus_task_info_);
}
} // namespace hybrid
} // namespace ge