/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef GE_HYBRID_MODEL_NODE_ITEM_H_
#define GE_HYBRID_MODEL_NODE_ITEM_H_

#include <mutex>
#include <vector>
#include "graph/node.h"
#include "graph/op_desc.h"
#include "graph/utils/node_utils.h"
#include "graph/runtime_inference_context.h"

#include "hybrid/common/tensor_value.h"
#include "hybrid/model/infer/node_shape_infer.h"

namespace ge {
namespace hybrid {
class NodeTask;
class NodeExecutor;
struct GraphExecutionContext;
class SubgraphContext;

struct FusedSubgraph {
  std::map<int32_t, std::vector<GeTensorDescPtr>> input_mapping;
  std::map<int32_t, OpDescPtr> output_mapping;
  // [ {{src_node_id, output_idx}, {dst_op_desc, input_idx} ]
  std::vector<std::pair<std::pair<int64_t, int32_t>, std::pair<OpDesc *, int32_t>>> data_dependencies;
  std::vector<NodePtr> nodes;
  ComputeGraphPtr graph;
};

bool IsControlFlowV2Op(const std::string &op_type);

bool IsFftsGraphNode(const OpDesc &op_desc);

bool IsFftsKernelNode(const OpDesc &op_desc);

class OptionalMutexGuard {
 public:
  OptionalMutexGuard(std::mutex *const mutex, const std::string &name);
  ~OptionalMutexGuard();

 private:
  std::mutex *mu_{nullptr};
  std::string name_;
};

// for caching static information across execution
struct NodeItem : NodeShapeInfer {
  ~NodeItem() = default;
  static Status Create(const NodePtr &node_ptr, std::unique_ptr<NodeItem> &node_item);

  OpDescPtr GetOpDesc() const {
    return node->GetOpDesc();
  }

  GeTensorDescPtr MutableOutputDesc(const int32_t index) const;

  Status UpdateInputDesc(const int32_t index, const GeTensorDesc &tensor_desc) const;

  GeTensorDescPtr MutableInputDesc(const int32_t index) const;

  Status GetInputDesc(const int32_t index, GeTensorDesc &tensor_desc) const;

  Status GetCanonicalInputIndex(const uint32_t index, int32_t &canonical_index) const;

  bool IsNoOp() const;

  bool IsControlFlowV2Op() const {
    return is_ctrl_flow_v2_op_;
  }

  bool IsControlFlowOp() const {
    return is_ctrl_flow_op_;
  }

  bool IsMergeOp() const {
    return is_merge_op_;
  }

  bool IsEnterOp() const {
    return kEnterOpTypes.count(node_type) > 0U;
  }

  bool IsExitOp() const {
    return kExitOpTypes.count(node_type) > 0U;
  }

  bool IsHcclOp() const;

  bool IsFftsSubNode() const {
    return is_ffts_sub_node_;
  }
  void SetToDynamic();

  void SetDataSend(NodeItem *const node_item, int32_t anchor_index);
  void SetCtrlSend(NodeItem *const node_item, const uint32_t switch_index);
  void SetMergeCtrl(NodeItem *const node_item, const uint32_t merge_index);
  size_t GetMergeCtrl(const uint32_t merge_index) const;

  OptionalMutexGuard MutexGuard(const std::string &name) const {
    return OptionalMutexGuard(copy_mu_.get(), name + "_" + node_name);
  }

  std::string DebugString() const;

  NodePtr node;
  int64_t node_id = -1;
  int32_t num_inputs = 0;
  int32_t num_outputs = 0;
  int32_t input_start = -1;
  int32_t output_start = -1;
  bool has_observer = false;
  bool has_optional_inputs = false;
  std::vector<ge::NodePtr> dependents_for_shape_inference;
  std::vector<ge::NodePtr> dependents_for_execution;
  std::set<int32_t> to_const_output_id_list;
  // src_output_id, dst_anchor_id, dst_node
  std::vector<std::vector<std::pair<int32_t, NodeItem *>>> outputs;

  // for linked drive
  bool is_root_node_ = false;
  bool is_ctrl_flow_v2_op_ = false;
  bool is_ctrl_flow_op_ = false;
  bool is_merge_op_ = false;
  bool is_enter_active_ = false;
  bool is_ffts_sub_node_ = false;
  int64_t frame_index_ = -1;
  int64_t parent_frame_ = -1;
  std::set<const NodeItem *> root_ctrl_;  // Recv ctrl from root node
  std::map<const NodeItem *, std::set<int32_t>> root_data_;  // Recv data from root node
  std::set<const NodeItem *> enter_ctrl_; // Recv ctrl from Enter node
  std::map<const NodeItem *, std::set<int32_t>> enter_data_; // Recv data from Enter node
  std::set<const NodeItem *> data_send_;  // Send data notify to
  std::map<const NodeItem *, int32_t> data_recv_;  // Recv data notify from
  std::set<const NodeItem *> ctrl_send_;  // Send ctrl notify to
  std::set<const NodeItem *> ctrl_recv_;  // Recv ctrl notify from
  std::vector<std::set<const NodeItem *>> switch_groups_;  // Send ctrl notify to

  std::shared_ptr<NodeTask> kernel_task;
  std::unique_ptr<FusedSubgraph> fused_subgraph;
  const NodeExecutor *node_executor = nullptr;
  std::map<int32_t, ge::NodePtr> ref_outputs;
  std::map<int32_t, int32_t> reuse_inputs;
  std::map<int32_t, int32_t> reuse_outputs;
  int32_t num_static_input_shapes = 0;
  bool is_profiling_report = false;
  // Indexes which will skip checking the amount of input data
  std::unordered_set<int32_t> skip_sufficiency_of_input_check_;

 private:
  explicit NodeItem(NodePtr node_ptr);
  Status Init();
  Status InitInputsAndOutputs();
  void ResolveOptionalInputs();
  Status ResolveDynamicState();
  Status ResolveStaticInputsAndOutputs();
  void ResolveUnknownShapeType();
  void ResolveFftsPlusStatus();
  GeTensorDescPtr DoGetInputDesc(const int32_t index) const;

  std::vector<uint32_t> input_desc_indices_;
  std::shared_ptr<std::mutex> copy_mu_;
  mutable std::mutex mu_;
};
}  // namespace hybrid
}  // namespace ge

#endif // GE_HYBRID_MODEL_NODE_ITEM_H_
