/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef RUNTIME_COMMUNICATION_COMMUNICATION_DOMAIN_H_
#define RUNTIME_COMMUNICATION_COMMUNICATION_DOMAIN_H_
#include <string>
#include <map>
#include <vector>

#include "ge/ge_api_error_codes.h"

#include "cluster/cluster_data.h"
#include "cluster/cluster_manager.h"
namespace ge {
class CommDomainManager {
 public:
  Status Init(const std::string &local_addr, uint32_t local_port, const std::vector<DeviceInfo> &devices);
  const std::string &GetRankTable() {
    return rank_table_;
  }
  Status InitHcclByString(std::string rankTable, int32_t rankId) {
    return SUCCESS;
  }
  Status DestroyHccl() {
    return SUCCESS;
  }

 private:
  Status GenerateRankTable(const std::string &master_ip, uint32_t master_port,
                           const std::vector<ClusterNodeInfo> &nodes);
  std::string rank_table_;
};
}  // namespace ge
#endif  // RUNTIME_COMMUNICATION_COMMUNICATION_DOMAIN_H_
