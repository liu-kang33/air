/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nlohmann/json.hpp"
#include "framework/common/debug/ge_log.h"

#include "communication_domain.h"

namespace ge {
struct CommDomainRank {
  std::string rank_id;
};

struct CommDomainNode {
  std::string node_addr;
  std::vector<CommDomainRank> ranks;
};

struct CommDomainRankTable {
  std::string collective_id;
  std::string master_ip;
  std::string master_port;
  std::vector<CommDomainNode> node_list;
  std::string status;
  std::string version;
};

void to_json(nlohmann::json &j, const CommDomainRank &p) {
  j = nlohmann::json{{"rank_id", p.rank_id}};
}

void to_json(nlohmann::json &j, const CommDomainNode &p) {
  j = nlohmann::json{{"node_addr", p.node_addr}, {"ranks", p.ranks}};
}

void to_json(nlohmann::json &j, const CommDomainRankTable &p) {
  j = nlohmann::json{{"collective_id", p.collective_id}, {"master_ip", p.master_ip}, {"master_port", p.master_port},
                     {"node_list", p.node_list},         {"status", p.status},       {"version", p.version}};
}

Status CommDomainManager::GenerateRankTable(const std::string &master_ip, uint32_t master_port,
                                            const std::vector<ClusterNodeInfo> &nodes) {
  CommDomainRankTable rank_table;
  rank_table.collective_id = master_ip + "-" + std::to_string(getpid());
  rank_table.master_ip = master_ip;
  rank_table.master_port = std::to_string(master_port);

  for (const auto &node : nodes) {
    if (node.ranks.size() == 0) {
      continue;
    }
    CommDomainNode n;
    n.node_addr = node.ipaddr;
    for (const auto &rank : node.ranks) {
      CommDomainRank domain_rank;
      domain_rank.rank_id = std::to_string(rank);
      n.ranks.emplace_back(domain_rank);
    }
    rank_table.node_list.emplace_back(n);
  }

  rank_table.status = "completed";
  rank_table.version = "1.1";

  try {
    nlohmann::json j = rank_table;
    rank_table_ = j.dump();
  } catch (const nlohmann::json::exception &e) {
    GELOGE(FAILED, "[Comm][Json]Generate Rank Table Failed,exception:%s.", e.what());
    return FAILED;
  }
  GELOGI("[Comm][Manager] Rank table is %s.", rank_table_.c_str());
  return SUCCESS;
}

Status CommDomainManager::Init(const std::string &local_addr, uint32_t local_port,
                               const std::vector<DeviceInfo> &devices) {
  ClusterManagerFactory factory;
  GELOGI("[Comm][Manager] Commnuication domain init start.");
  std::string addr = local_addr + ":" + std::to_string(local_port);
  auto worker = factory.Create(addr, devices);
  if (worker == nullptr) {
    return FAILED;
  }
  if (worker->Init() != SUCCESS) {
    return FAILED;
  }
  if (GenerateRankTable(local_addr, local_port, worker->GetNodeList()) != SUCCESS) {
    return FAILED;
  }
  return SUCCESS;
}

}  // namespace ge
