/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INC_CLIENT_MANAGER_H_
#define INC_CLIENT_MANAGER_H_

#include <cstdint>
#include <map>
#include <mutex>
#include <functional>
#include <thread>
#include <atomic>
#include "common/data_flow/route/helper_exchange_deployer.h"
#include "deploy/daemon/executor_manager.h"
#include "ge/ge_api_error_codes.h"
#include "proto/deployer.pb.h"
#include "proto/var_manager.pb.h"

namespace ge {
class Client {
 public:
  /*
   *  @ingroup ge
   *  @brief   record client
   *  @param   [in]  None
   *  @return  None
   */
  void Initialize();

  /*
   *  @ingroup ge
   *  @brief   check client is expired
   *  @param   [in]  None
   *  @return  Expired or not
   */
  bool IsExpired();

  /*
   *  @ingroup ge
   *  @brief   set client is executing
   *  @param   [in]  bool
   *  @return  None
   */
  void SetIsExecuting(bool is_executing);

  /*
   *  @ingroup ge
   *  @brief   check client is executing
   *  @param   [in]  None
   *  @return  None
   */
  bool IsExecuting();

  /*
   *  @ingroup ge
   *  @brief   record client last heartbeat time
   *  @param   [in]  None
   *  @return  None
   */
  void OnHeartbeat();

  /*
   *  @ingroup ge
   *  @brief   finalize client
   *  @param   [in]  None
   *  @return  None
   */
  void Finalize();

  /*
   *  @ingroup ge
   *  @brief   pre-download model
   *  @param   [in]  const DeployerRequest *
   *  @param   [in]  DeployerResponse *
   *  @return  SUCCESS or FAILED
   */
  Status PreDownloadModel(const deployer::DeployerRequest &request,
                          deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   download model
   *  @param   [in]  const DeployerRequest *
   *  @param   [in]  DeployerResponse *
   *  @return  SUCCESS or FAILED
   */
  Status DownloadModel(const deployer::DeployerRequest &request,
                       deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   load model
   *  @param   [in]  const DeployerRequest *
   *  @param   [in]  DeployerResponse *
   *  @return  SUCCESS or FAILED
   */
  Status LoadModel(const deployer::DeployerRequest &request,
                   deployer::DeployerResponse &response);

  Status UnloadModel(const deployer::DeployerRequest &request,
                     deployer::DeployerResponse &response);

  void PreDeployModel(const deployer::PreDeployModelRequest &request,
                      deployer::DeployerResponse &response);

  Status ProcessMultiVarManager(const deployer::MultiVarManagerRequest &request,
                                deployer::DeployerResponse &response);

  Status ProcessSharedContent(const deployer::SharedContentDescRequest &request,
                              deployer::DeployerResponse &response);

 private:
  struct ClientContext {
    int32_t device_id;
    uint32_t root_model_id;
    uint32_t sub_model_id;
    bool operator < (const ClientContext &other) const {
      if (device_id != other.device_id) {
        return device_id < other.device_id;
      } else if (root_model_id != other.root_model_id) {
        return root_model_id < other.root_model_id;
      } else {
        return sub_model_id < other.sub_model_id;
      }
    }
  };

  static Status SetResponse(const deployer::ExecutorResponse &executor_response,
                            deployer::DeployerResponse &deployer_response);
  Status GetQueues(const ClientContext &client_context, std::vector<uint32_t> &input_queues,
                   std::vector<uint32_t> &output_queues) const;

  std::mutex mu_;
  int64_t client_id_ = 0;
  std::chrono::steady_clock::time_point last_heartbeat_ts_;
  bool is_executing_ = false;
  std::map<ClientContext, ExchangeRoute> exchange_routes_;
  std::map<ClientContext, deployer::SubmodelDesc> submodel_descs_;
  ExecutorManager executor_manager_;
};

class ClientManager {
 public:
  ClientManager() = default;
  ~ClientManager();

  /*
   *  @ingroup ge
   *  @brief   init heartbeat thread
   *  @param   [in]  None
   *  @return  None
   */
  Status Initialize();

  /*
   *  @ingroup ge
   *  @brief   finalize client manager
   *  @param   [in]  None
   *  @return  None
   */
  void Finalize();

  /*
   *  @ingroup ge
   *  @brief   create client
   *  @param   [in]  int64_t &
   *  @return  SUCCESS or FAILED
   */
  Status CreateClient(int64_t &client_id);

  /*
   *  @ingroup ge
   *  @brief   close client
   *  @param   [in]  int64_t
   *  @return  SUCCESS or FAILED
   */
  Status CloseClient(const int64_t client_id);

  /*
   *  @ingroup ge
   *  @brief   get client by client_id
   *  @param   [in]  uint32_t
   *  @return  model
   */
  Client *GetClient(const int64_t client_id);

  static ClientManager &GetInstance() {
    static ClientManager instance;
    return instance;
  }

 private:
  /*
   *  @ingroup ge
   *  @brief   heartbeat thread
   *  @param   [in]  None
   *  @return  None
   */
  void HeartbeatsThread();
  std::map<int64_t, std::unique_ptr<Client>> clients_;
  std::atomic_bool running_{};
  std::mutex mu_;
  std::thread heart_beats_thread_;
  int64_t client_id_gen_ = 0;
};
}  // namespace ge
#endif  // INC_CLIENT_MANAGER_H_