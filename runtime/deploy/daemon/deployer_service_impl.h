/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef AIR_RUNTIME_DEPLOY_DAEMON_DEPLOYER_SERVICE_IMPL_H_
#define AIR_RUNTIME_DEPLOY_DAEMON_DEPLOYER_SERVICE_IMPL_H_

#include <map>
#include <string>
#include <vector>
#include "common/config/json_parser.h"
#include "deploy/daemon/client_manager.h"
#include "ge/ge_api_error_codes.h"
#include "client_manager.h"
#include "proto/deployer.pb.h"

namespace ge {
struct ClientAddr {
  std::string ip;
  std::string port;
};

struct RequestContext {
  std::string peer_url;
};

struct ClientAddrManager {
  std::string connect_status;
  std::map<std::string, ClientAddr> client_addrs;
};

class DeployerServiceImpl {
 public:
  static DeployerServiceImpl &GetInstance() {
    static DeployerServiceImpl instance;
    return instance;
  }
  /*
   *  @ingroup ge
   *  @brief   deployer service interface
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return  SUCCESS or FAILED
   */
  Status Process(const RequestContext &context,
                 const deployer::DeployerRequest &request,
                 deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   deployer service interface
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return  SUCCESS or FAILED
   */
  using ProcessFunc = std::function<void(const RequestContext &context,
                                         const deployer::DeployerRequest &request,
                                         deployer::DeployerResponse &response)>;

  /*
  *  @ingroup ge
  *  @brief   register deployer server api
  *  @param   [in]  DeployerRequestType
  *  @param   [in]  const ProcessFunc &
  *  @return: None
   *
  */
  void RegisterReqProcessor(const deployer::DeployerRequestType type, const ProcessFunc &fn);

  /*
   *  @ingroup ge
   *  @brief   generate client id
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void GenerateClientId(const RequestContext &context,
                               const deployer::DeployerRequest &request,
                               deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   client heartbeat process
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void ClientHeartbeatProcess(const RequestContext &context,
                                     const deployer::DeployerRequest &request,
                                     deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   server disconnect client
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void DisconnectClient(const RequestContext &context, const deployer::DeployerRequest &request,
                               deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   pre-download model
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void PreDeployModelProcess(const RequestContext &context,
                                    const deployer::DeployerRequest &request,
                                    deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   pre download model
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void PreDownloadModelProcess(const RequestContext &context,
                                      const deployer::DeployerRequest &request,
                                      deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   download model
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void DownloadModelProcess(const RequestContext &context,
                                   const deployer::DeployerRequest &request,
                                   deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   load model
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void LoadModelProcess(const RequestContext &context,
                               const deployer::DeployerRequest &request,
                               deployer::DeployerResponse &response);

  /*
   *  @ingroup ge
   *  @brief   unload model
   *  @param   [in]  context        grpc context
   *  @param   [in]  request        service request
   *  @param   [out] response       service response
   *  @return: None
   */
  static void UnloadModelProcess(const RequestContext &context,
                                 const deployer::DeployerRequest &request,
                                 deployer::DeployerResponse &response);

  static void MultiVarManagerInfoProcess(const RequestContext &context,
                                         const deployer::DeployerRequest &request,
                                         deployer::DeployerResponse &response);

  static void SharedContentProcess(const RequestContext &context,
                                   const deployer::DeployerRequest &request,
                                   deployer::DeployerResponse &response);

 private:
  static bool GetClient(int64_t client_id, Client **client, deployer::DeployerResponse &response);
  static void ConfigClientAddrToJson(nlohmann::json &j, const ClientAddrManager &client_addr_manager);
  static void RecordClientInfo(const RequestContext &context, DeployerServiceImpl *deployer_service_impl);
  static void DeleteClientInfo(const RequestContext &context, DeployerServiceImpl *deployer_service_impl);
  static void UpdateJsonFile(DeployerServiceImpl *deployer_service_impl);
  static void GetClientIpAndPort(const RequestContext &context, std::string &key, ClientAddr &client);

  std::map<deployer::DeployerRequestType, ProcessFunc> process_fns_;
  ClientAddrManager client_addr_manager_;
  std::map<std::string, ClientAddr> client_addrs_;
  std::mutex mu_;
};
}  // namespace ge

#endif  // AIR_RUNTIME_DEPLOY_DAEMON_DEPLOYER_SERVICE_IMPL_H_
