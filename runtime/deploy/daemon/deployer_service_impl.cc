/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/daemon/deployer_service_impl.h"

#include <fstream>
#include <string>
#include <vector>
#include "common/config/json_parser.h"
#include "deploy/daemon/client_manager.h"
#include "ge/ge_api_error_codes.h"
#include "proto/deployer.pb.h"
#include "common/string_util.h"
#include "common/utils/rts_api_utils.h"
#include "framework/common/debug/ge_log.h"
#include "securec.h"

namespace ge {
class ServiceRegister {
 public:
  ServiceRegister(const deployer::DeployerRequestType type, const DeployerServiceImpl::ProcessFunc &fn) {
    DeployerServiceImpl::GetInstance().RegisterReqProcessor(type, fn);
  }

  ~ServiceRegister() = default;
};
const char *kConfigFilePathEnv = "HELPER_RES_FILE_PATH";

#define REGISTER_REQUEST_PROCESSOR(type, fn) \
REGISTER_REQUEST_PROCESSOR_UNIQ_DEPLOYER(__COUNTER__, type, fn)

#define REGISTER_REQUEST_PROCESSOR_UNIQ_DEPLOYER(ctr, type, fn) \
REGISTER_REQUEST_PROCESSOR_UNIQ(ctr, type, fn)

#define REGISTER_REQUEST_PROCESSOR_UNIQ(ctr, type, fn) \
static ::ge::ServiceRegister register_request_processor##ctr \
__attribute__((unused)) = \
::ge::ServiceRegister(type, fn)

REGISTER_REQUEST_PROCESSOR(deployer::kInitRequest, DeployerServiceImpl::GenerateClientId);
REGISTER_REQUEST_PROCESSOR(deployer::kDisconnect, DeployerServiceImpl::DisconnectClient);
REGISTER_REQUEST_PROCESSOR(deployer::kPreDeployModel, DeployerServiceImpl::PreDeployModelProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kPreDownloadModel, DeployerServiceImpl::PreDownloadModelProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kDownloadModel, DeployerServiceImpl::DownloadModelProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kHeartbeat, DeployerServiceImpl::ClientHeartbeatProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kLoadModel, DeployerServiceImpl::LoadModelProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kUnloadModel, DeployerServiceImpl::UnloadModelProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kDownloadVarManager, DeployerServiceImpl::MultiVarManagerInfoProcess);
REGISTER_REQUEST_PROCESSOR(deployer::kDownloadSharedContent, DeployerServiceImpl::SharedContentProcess);

void DeployerServiceImpl::RegisterReqProcessor(const deployer::DeployerRequestType type,
                                               const DeployerServiceImpl::ProcessFunc &fn) {
  process_fns_.emplace(type, fn);
}

void DeployerServiceImpl::ConfigClientAddrToJson(nlohmann::json &j, const ClientAddrManager &client_addr_manager) {
  for (auto &client : client_addr_manager.client_addrs) {
    nlohmann::json client_addr;
    client_addr = nlohmann::json{{"ip", client.second.ip}, {"port", client.second.port}};
    j["connections"].push_back(client_addr);
  }
}

void DeployerServiceImpl::GetClientIpAndPort(const RequestContext &context,
                                             std::string &key,
                                             ClientAddr &client) {
  std::vector<std::string> address = ge::StringUtils::Split(context.peer_url, ':');
  if ((address[1].empty()) || (address[2].empty())) {
    return;
  }
  client.ip = address[1];
  client.port = address[2];
  key = address[1] + address[2];
}

void DeployerServiceImpl::UpdateJsonFile(DeployerServiceImpl *deployer_service_impl) {
  deployer_service_impl->client_addr_manager_.client_addrs = deployer_service_impl->client_addrs_;
  nlohmann::json json;
  ConfigClientAddrToJson(json, deployer_service_impl->client_addr_manager_);
  std::ofstream file("client.json");
  file << json;
}

void DeployerServiceImpl::RecordClientInfo(const RequestContext &context,
                                           DeployerServiceImpl *deployer_service_impl) {
  std::lock_guard<std::mutex> lk(deployer_service_impl->mu_);
  ClientAddr client;
  std::string key;
  GetClientIpAndPort(context, key, client);

  auto it = deployer_service_impl->client_addrs_.find(key);
  if (it != deployer_service_impl->client_addrs_.end()) {
    return;
  }
  deployer_service_impl->client_addrs_.emplace(key, client);
  UpdateJsonFile(deployer_service_impl);
}

void DeployerServiceImpl::DeleteClientInfo(const RequestContext &context,
                                           DeployerServiceImpl *deployer_service_impl) {
  std::lock_guard<std::mutex> lk(deployer_service_impl->mu_);
  ClientAddr client;
  std::string key;
  GetClientIpAndPort(context, key, client);

  auto it = deployer_service_impl->client_addrs_.find(key);
  if (it == deployer_service_impl->client_addrs_.end()) {
    return;
  }
  deployer_service_impl->client_addrs_.erase(key);
  UpdateJsonFile(deployer_service_impl);
}

void DeployerServiceImpl::GenerateClientId(const RequestContext &context,
                                           const deployer::DeployerRequest &request,
                                           deployer::DeployerResponse &response) {
  auto request_token = request.init_request().token();
  const ge::DeviceInfo &device_info = Configurations::GetInstance().GetDeviceInfo();
  if (request_token != device_info.token) {
    REPORT_INNER_ERROR("E19999", "Check token failed, request token = %s, device token = %s",
                       request_token.c_str(), device_info.token.c_str());
    GELOGE(FAILED, "[Check][Token] Check token failed.request token = %s, device token = %s",
           request_token.c_str(), device_info.token.c_str());
    response.set_error_code(FAILED);
    response.set_error_message("Check token failed.");
    return;
  }

  int64_t client_id = 0;
  auto res = ClientManager::GetInstance().CreateClient(client_id);
  if (res != SUCCESS) {
        REPORT_CALL_ERROR("E19999", "Create client failed.");
    GELOGE(FAILED, "[Create][Client] Create client failed.");
    response.set_error_code(FAILED);
    response.set_error_message("Create client id failed.");
    return;
  }

  int32_t dev_count = 1;
  res = RtsApiUtils::GetDeviceCount(dev_count);
  if (res != SUCCESS) {
    REPORT_CALL_ERROR("E19999", "Get device count failed.");
    GELOGE(FAILED, "[Get][DeviceCount] failed.");
    response.set_error_code(FAILED);
    response.set_error_message("Get device count failed.");
    return;
  }
  GELOGI("Get device count[%d].", dev_count);

  RecordClientInfo(context, &DeployerServiceImpl::GetInstance());
  response.mutable_init_response()->set_client_id(client_id);
  response.mutable_init_response()->set_dev_count(dev_count);
}

bool DeployerServiceImpl::GetClient(int64_t client_id, Client **client, deployer::DeployerResponse &response) {
  *client = ClientManager::GetInstance().GetClient(client_id);
  if (*client != nullptr) {
    return true;
  }

  REPORT_CALL_ERROR("E19999", "Get client[%ld] failed process heartbeat.", client_id);
  GELOGE(FAILED, "[Get][Client] Get client[%ld] failed process heartbeat.", client_id);
  response.set_error_code(FAILED);
  response.set_error_message("Not exist client id");
  return false;
}

void DeployerServiceImpl::ClientHeartbeatProcess(const RequestContext &context,
                                                 const deployer::DeployerRequest &request,
                                                 deployer::DeployerResponse &response) {
  int64_t client_id = request.client_id();
  Client *client = nullptr;
  if (!GetClient(client_id, &client, response)) {
    return;
  }

  client->SetIsExecuting(true);
  client->OnHeartbeat();
  client->SetIsExecuting(false);

  if (client->IsExpired()) {
    GELOGI("Client[%ld] was expired during execution.", client_id);
    ClientManager::GetInstance().CloseClient(client_id);
    response.set_error_code(FAILED);
    response.set_error_message("Time expired");
    return;
  }
  GELOGI("Client[%ld] heartbeat dose not expired.", client_id);
}

void DeployerServiceImpl::DisconnectClient(const RequestContext &context,
                                           const deployer::DeployerRequest &request,
                                           deployer::DeployerResponse &response) {
  int64_t client_id = request.client_id();
  Client *client = nullptr;
  if (GetClient(client_id, &client, response)) {
    ClientManager::GetInstance().CloseClient(client_id);
    DeleteClientInfo(context, &DeployerServiceImpl::GetInstance());
  }
}

void DeployerServiceImpl::PreDeployModelProcess(const RequestContext &context,
                                                const deployer::DeployerRequest &request,
                                                deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    client->PreDeployModel(request.pre_deploy_model_request(), response);
  }
}

void DeployerServiceImpl::PreDownloadModelProcess(const RequestContext &context,
                                                  const deployer::DeployerRequest &request,
                                                  deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    (void) client->PreDownloadModel(request, response);
  }
}

void DeployerServiceImpl::DownloadModelProcess(const RequestContext &context,
                                               const deployer::DeployerRequest &request,
                                               deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    (void) client->DownloadModel(request, response);
  }
}

void DeployerServiceImpl::LoadModelProcess(const RequestContext &context,
                                           const deployer::DeployerRequest &request,
                                           deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    (void) client->LoadModel(request, response);
  }
}

void DeployerServiceImpl::UnloadModelProcess(const RequestContext &context,
                                             const deployer::DeployerRequest &request,
                                             deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    (void) client->UnloadModel(request, response);
  }
}

void DeployerServiceImpl::MultiVarManagerInfoProcess(const RequestContext &context,
                                                     const deployer::DeployerRequest &request,
                                                     deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    client->ProcessMultiVarManager(request.multi_var_manager_request(), response);
  }
}

void DeployerServiceImpl::SharedContentProcess(const RequestContext &context,
                                               const deployer::DeployerRequest &request,
                                               deployer::DeployerResponse &response) {
  Client *client = nullptr;
  if (GetClient(request.client_id(), &client, response)) {
    client->ProcessSharedContent(request.shared_content_desc_request(), response);
  }
}

Status DeployerServiceImpl::Process(const RequestContext &context,
                                    const deployer::DeployerRequest &request,
                                    deployer::DeployerResponse &response) {
  auto type = request.type();
  GELOGD("[Process][Request] start, type = %d", static_cast<int32_t>(type));
  auto it = process_fns_.find(type);
  if (it == process_fns_.end()) {
    REPORT_INNER_ERROR("E19999", "Find api type[%d]  failed.", type);
    GELOGE(FAILED, "[Find][Api] Find api type[%d] failed.", type);
    response.set_error_code(FAILED);
    response.set_error_message("Api dose not exist");
    return SUCCESS;
  }

  auto process_fn = it->second;
  if (process_fn == nullptr) {
    REPORT_INNER_ERROR("E19999", "Function pointer dose not exist.");
    GELOGE(FAILED, "[Find][Function] Find pointer dose not exist.");
    response.set_error_code(FAILED);
    response.set_error_message("Find pointer dose not exist.");
    return SUCCESS;
  }

  process_fn(context, request, response);
  GELOGD("[Process][Request] succeeded, type = %d", static_cast<int32_t>(type));
  return SUCCESS;
}
}  // namespace ge
