/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/daemon/client_manager.h"
#include <thread>
#include <string>
#include <memory>
#include "debug/ge_util.h"
#include "framework/common/debug/ge_log.h"
#include "securec.h"
#include "common/data_flow/queue/helper_exchange_service.h"
#include "common/data_flow/route/helper_exchange_deployer.h"
#include "deploy/daemon/executor_manager.h"

namespace ge {
namespace {
constexpr long kHeartbeatIntervalSec = 60;
}

Status ClientManager::Initialize() {
  if (running_) {
    return SUCCESS;
  }
  running_ = true;
  heart_beats_thread_ = std::thread([&]() {
    HeartbeatsThread();
  });
  return SUCCESS;
}

ClientManager::~ClientManager() {
  Finalize();
}

void ClientManager::Finalize() {
  running_ = false;
  if (heart_beats_thread_.joinable()) {
    heart_beats_thread_.join();
  }
}

Status ClientManager::CreateClient(int64_t &client_id) {
  std::lock_guard<std::mutex> lk(mu_);
  int64_t new_client_id = client_id_gen_;
  std::unique_ptr<Client> new_client(new(std::nothrow) Client());
  GE_CHECK_NOTNULL(new_client);
  new_client->Initialize();
  clients_.emplace(new_client_id, std::move(new_client));
  ++client_id_gen_;
  client_id = new_client_id;
  return SUCCESS;
}

Status ClientManager::CloseClient(const int64_t client_id) {
  std::lock_guard<std::mutex> lk(mu_);
  auto it = clients_.find(client_id);
  if (it == clients_.end()) {
    REPORT_INNER_ERROR("E19999", "Client[%ld] does not exist in client manager.", client_id);
    GELOGE(FAILED, "[Close][Client]Client[%ld] does not exist in client manager.", client_id);
    return FAILED;
  }

  it->second->Finalize();
  clients_.erase(it);
  return SUCCESS;
}

void ClientManager::HeartbeatsThread() {
  while (running_) {
    std::vector<int64_t> expired_clients_;
    std::this_thread::sleep_for(std::chrono::seconds(kHeartbeatIntervalSec));
    {
      std::lock_guard<std::mutex> lk(mu_);
      for (auto &it : clients_) {
        if (it.second->IsExpired()) {
          if (it.second->IsExecuting()) {
            GELOGD("Client[%ld] is still executing, check next time.", it.first);
          } else {
            GELOGD("Client[%ld] is not executing.", it.first);
            expired_clients_.push_back(it.first);
          }
        }
      }
    }

    for (int64_t client_id : expired_clients_) {
      GELOGD("Client[%ld] expired, close it.", client_id);
      (void) CloseClient(client_id);
    }
  }
}

Client *ClientManager::GetClient(const int64_t client_id) {
  std::lock_guard<std::mutex> lk(mu_);
  auto it = clients_.find(client_id);
  if (it == clients_.end()) {
    REPORT_INNER_ERROR("E19999", "Get client[%ld] failed.", client_id);
    GELOGE(FAILED, "[Get][Client]Get client[%ld] failed.", client_id);
    return nullptr;
  }
  return it->second.get();
}

void Client::Initialize() {
  OnHeartbeat();
}

bool Client::IsExpired() {
  std::lock_guard<std::mutex> lk(mu_);
  long diff =
      std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - last_heartbeat_ts_).count();
  return diff > kHeartbeatIntervalSec;
}

bool Client::IsExecuting() {
  std::lock_guard<std::mutex> lk(mu_);
  return is_executing_;
}

void Client::SetIsExecuting(bool is_executing) {
  std::lock_guard<std::mutex> lk(mu_);
  is_executing_ = is_executing;
}

void Client::OnHeartbeat() {
  std::lock_guard<std::mutex> lk(mu_);
  last_heartbeat_ts_ = std::chrono::steady_clock::now();
}

void Client::Finalize() {
}

void Client::PreDeployModel(const deployer::PreDeployModelRequest &request,
                            deployer::DeployerResponse &response) {
  const auto &exchange_plan = request.exchange_plan();
  uint32_t root_model_id = request.root_model_id();
  const int32_t device_id = request.device_id();
  // TODO
  ClientContext plan_context{device_id, root_model_id, 0};
  HelperExchangeDeployer exchange_deployer(HelperExchangeService::GetInstance(), exchange_plan, device_id);
  auto ret = exchange_deployer.Deploy(exchange_routes_[plan_context]);
  if (ret != SUCCESS) {
    GELOGE(ret, "Failed to deploy local exchange");
    response.set_error_code(FAILED);
    response.set_error_message("Failed to deploy local exchange");
    return;
  }
  GELOGI("PreDeploy model successfully");
  std::map<uint32_t, deployer::SubmodelDesc> submodel_desc_map;
  for (int32_t i = 0; i < request.submodels_size(); ++i) {
    ClientContext submodel_context{device_id, root_model_id, static_cast<uint32_t>(i)};
    submodel_descs_[submodel_context] = request.submodels(i);
  }
  response.set_error_code(SUCCESS);
}

Status Client::PreDownloadModel(const deployer::DeployerRequest &request,
                                deployer::DeployerResponse &response) {
  const auto &req_body = request.pre_download_request();
  uint32_t model_id = req_body.model_id();
  uint32_t root_model_id = req_body.root_model_id();
  const int32_t device_id = req_body.device_id();
  GELOGD("[PreDownload][Model] start, request = %s", req_body.DebugString().c_str());
  ExecutorProcess *executor = nullptr;
  ExecutorManager::ExecutorId context = {0, device_id};
  GE_CHK_STATUS_RET(executor_manager_.GetOrForkExecutorProcess(context, &executor),
                    "[PreDownload][Model] Failed to get executor");
  deployer::ExecutorRequest executor_request;
  auto exec_req_body = executor_request.mutable_pre_download_message();
  GE_CHECK_NOTNULL(exec_req_body);
  exec_req_body->set_model_id(model_id);
  exec_req_body->set_root_model_id(root_model_id);
  exec_req_body->set_model_size(req_body.total_size());
  deployer::ExecutorResponse executor_response;
  GE_CHK_STATUS_RET(executor->SendRequest(executor_request, executor_response),
                    "[PreDownload][Model] Failed to send request");
  GE_CHK_STATUS_RET(SetResponse(executor_response, response),
                    "[PreDownload][Model] failed, model_id = %u, model_size = %lu", model_id, req_body.total_size());
  GELOGD("[PreDownload][Model] succeeded, model_id = %u, offset = %lu", model_id, req_body.total_size());
  return SUCCESS;
}

Status Client::DownloadModel(const deployer::DeployerRequest &request,
                             deployer::DeployerResponse &response) {
  const auto &req_body = request.download_model_request();
  const int32_t device_id = req_body.device_id();
  GELOGD("[Download][Model] start, model_id = %u", req_body.model_id());
  ExecutorProcess *executor = nullptr;
  ExecutorManager::ExecutorId context{0, device_id};
  GE_CHK_STATUS_RET(executor_manager_.GetExecutorProcess(context, &executor),
                    "[Download][Model] Failed to get executor");
  deployer::ExecutorRequest executor_request;
  auto exec_req_body = executor_request.mutable_download_model_message();
  GE_CHECK_NOTNULL(exec_req_body);
  uint32_t model_id = req_body.model_id();
  uint32_t root_model_id = req_body.root_model_id();
  exec_req_body->set_model_id(model_id);
  exec_req_body->set_root_model_id(root_model_id);
  exec_req_body->set_offset(req_body.offset());
  exec_req_body->set_model_data(req_body.om_content());
  deployer::ExecutorResponse executor_response;
  GE_CHK_STATUS_RET(executor->SendRequest(executor_request, executor_response),
                    "[Download][Model] Failed to send request");
  GE_CHK_STATUS_RET(SetResponse(executor_response, response),
                    "[Download][Model] failed, model_id = %u, offset = %lu", model_id, exec_req_body->offset());
  GELOGD("[Download][Model] succeeded, model_id = %u, offset = %lu", model_id, exec_req_body->offset());
  return SUCCESS;
}

Status Client::LoadModel(const deployer::DeployerRequest &request,
                         deployer::DeployerResponse &response) {
  const auto &req_body = request.load_model_request();
  uint32_t sub_model_id = req_body.model_id();
  uint32_t root_model_id = req_body.root_model_id();
  const int32_t device_id = req_body.device_id();
  GELOGD("[Load][Model] start, sub_model_id = %u", sub_model_id);
  std::vector<uint32_t> input_queues;
  std::vector<uint32_t> output_queues;
  ClientContext plan_context{device_id, root_model_id, sub_model_id};
  GE_CHK_STATUS_RET(GetQueues(plan_context, input_queues, output_queues),
                    "[Load][Model] failed to resolve queue ids, sub_model_id = %u", sub_model_id);

  ExecutorProcess *executor = nullptr;
  ExecutorManager::ExecutorId context{0, device_id};
  GE_CHK_STATUS_RET(executor_manager_.GetExecutorProcess(context, &executor),
                    "[Download][Model] Failed to get executor");
  deployer::ExecutorRequest executor_request;
  auto exec_req_body = executor_request.mutable_load_model_message();
  GE_CHECK_NOTNULL(exec_req_body);
  exec_req_body->set_model_id(sub_model_id);
  exec_req_body->set_root_model_id(root_model_id);
  exec_req_body->mutable_input_queues()->Add(input_queues.begin(), input_queues.end());
  exec_req_body->mutable_output_queues()->Add(output_queues.begin(), output_queues.end());
  deployer::ExecutorResponse executor_response;
  GE_CHK_STATUS_RET(executor->SendRequest(executor_request, executor_response),
                    "[Load][Model] Failed to send request");
  GE_CHK_STATUS_RET(SetResponse(executor_response, response),
                    "[Load][Model] failed, request = %s",
                    exec_req_body->DebugString().c_str());
  GELOGD("[Load][Model] succeeded, sub_model_id = %u, input_queues = %s, output_queues = %s",
         sub_model_id, ToString(input_queues).c_str(), ToString(output_queues).c_str());
  return SUCCESS;
}

Status Client::UnloadModel(const deployer::DeployerRequest &request,
                           deployer::DeployerResponse &response) {
  const auto &req_body = request.unload_model_request();
  uint32_t model_id = req_body.model_id();
  const int32_t device_id = req_body.device_id();
  GELOGD("[Unload][Model] start, device_id = %d, model_id = %u", device_id, model_id);
  ExecutorProcess *executor = nullptr;
  ExecutorManager::ExecutorId context{0, device_id};
  GE_CHK_STATUS_RET(executor_manager_.GetExecutorProcess(context, &executor),
                    "[Download][Model] Failed to get executor, model_id = %u", model_id);
  deployer::ExecutorRequest executor_request;
  auto exec_req_body = executor_request.mutable_unload_model_message();
  GE_CHECK_NOTNULL(exec_req_body);
  exec_req_body->set_model_id(model_id);
  deployer::ExecutorResponse executor_response;
  auto ret = executor->SendRequest(executor_request, executor_response);
  ClientContext plan_context{device_id, model_id, 0};
  (void) HelperExchangeDeployer::Undeploy(HelperExchangeService::GetInstance(), exchange_routes_[plan_context]);
  if (ret != SUCCESS) {
    GELOGE(FAILED, "[Unload][Model] Failed to send request to executor");
    response.set_error_code(FAILED);
    response.set_error_message("Failed to send request to executor");
    return SUCCESS;
  }
  GE_CHK_STATUS_RET(SetResponse(executor_response, response),
                    "[Unload][Model] failed, request = %s",
                    exec_req_body->DebugString().c_str());
  GELOGD("[Unload][Model] succeeded, model_id = %u", model_id);
  return SUCCESS;
}

Status Client::ProcessMultiVarManager(const deployer::MultiVarManagerRequest &request,
                                      deployer::DeployerResponse &response) {
  GELOGI("[process][var_manager] Begin.");
  ExecutorProcess *executor = nullptr;
  const int32_t device_id = request.device_id();
  ExecutorManager::ExecutorId context{0, device_id};
  GE_CHK_STATUS_RET(executor_manager_.GetOrForkExecutorProcess(context, &executor),
                    "[process][multi-var_manager] Failed to get executor.");
  deployer::ExecutorRequest executor_request;
  auto multi_var_manager = executor_request.mutable_multi_var_manager_info();
  *multi_var_manager = request.multi_var_manager_info();
  deployer::ExecutorResponse executor_response;
  GE_CHK_STATUS_RET(executor->SendRequest(executor_request, executor_response),
                    "[Process][VarManager] Failed to send request.");
  GE_CHK_STATUS_RET(SetResponse(executor_response, response), "[Process][VarManager] failed.");
  GELOGI("[process][var_manager] SUCCESS.");
  return SUCCESS;
}

Status Client::ProcessSharedContent(const deployer::SharedContentDescRequest &request,
                                    deployer::DeployerResponse &response) {
  ExecutorProcess *executor = nullptr;
  const int32_t device_id = request.device_id();
  ExecutorManager::ExecutorId context{0, device_id};
  GE_CHK_STATUS_RET(executor_manager_.GetExecutorProcess(context, &executor), "Failed to get executor process.");
  deployer::ExecutorRequest executor_request;
  auto shared_content_desc = executor_request.mutable_shared_content_desc();
  *shared_content_desc = request.shared_content_desc();
  deployer::ExecutorResponse executor_response;
  GE_CHK_STATUS_RET(executor->SendRequest(executor_request, executor_response),
                    "[Process][shared_content] Failed to send request.");
  GE_CHK_STATUS_RET(SetResponse(executor_response, response),
                    "[Process][shared_content] Failed.");
  GELOGI("[process][shared_content] SUCCESS.");
  return SUCCESS;
}

Status Client::SetResponse(const deployer::ExecutorResponse &executor_response,
                           deployer::DeployerResponse &deployer_response) {
  auto ret = executor_response.status_code();
  deployer_response.set_error_code(ret);
  deployer_response.set_error_message(executor_response.error_message());
  return ret;
}

Status Client::GetQueues(const ClientContext &client_context,
                         std::vector<uint32_t> &input_queues,
                         std::vector<uint32_t> &output_queues) const {
  const auto &it = submodel_descs_.find(client_context);
  ClientContext plan_context{client_context};
  plan_context.sub_model_id = 0;
  const auto &route_it = exchange_routes_.find(plan_context);
  if (it == submodel_descs_.end() || route_it == exchange_routes_.end()) {
    GELOGE(FAILED, "model not pre-deployed, device_id = %d, root_model_id = %u, sub_model_id = %u.",
           client_context.device_id, client_context.root_model_id, client_context.sub_model_id);
    return FAILED;
  }

  const auto &submodel_desc = it->second;
  const auto &exchange_route = route_it->second;
  std::vector<int32_t>
      input_queue_indices(submodel_desc.input_queue_indices().begin(), submodel_desc.input_queue_indices().end());
  GE_CHK_STATUS_RET(exchange_route.GetQueueIds(input_queue_indices, input_queues),
                    "Failed to get input queue ids");
  std::vector<int32_t>
      output_queue_indices(submodel_desc.output_queue_indices().begin(), submodel_desc.output_queue_indices().end());
  GE_CHK_STATUS_RET(exchange_route.GetQueueIds(output_queue_indices, output_queues),
                    "Failed to get output queue ids");
  return SUCCESS;
}
} // namespace ge