/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AIR_RUNTIME_DEPLOY_DAEMON_EXECUTOR_MANAGER_H_
#define AIR_RUNTIME_DEPLOY_DAEMON_EXECUTOR_MANAGER_H_

#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <vector>
#include "ge/ge_api_error_codes.h"
#include "deploy/executor/executor_event_defs.h"
#include "proto/deployer.pb.h"

namespace ge {
class ExecutorProcess {
 public:
  ExecutorProcess(int32_t pid, int32_t device_id, std::string queue_name, std::string group_name);
  virtual ~ExecutorProcess();
  Status Initialize();
  Status Finalize();
  Status SendRequest(deployer::ExecutorRequest &request, deployer::ExecutorResponse &resp) const;

 private:
  Status InitEventGroup();
  Status InitMessageQueue();
  Status SendEvent(ExecutorEventType event_type) const;
  Status WaitForExecutorInitialized();
  Status WaitEvent(ExecutorEventType expected_event_type) const;
  int32_t pid_;
  int32_t device_id_;
  uint32_t event_group_id_ = 1U;
  uint32_t msg_queue_id_ = 0U;
  std::string msg_queue_name_;
  std::string group_name_;
};

class ExecutorManager {
 public:
  struct ExecutorId {
    int64_t context_id;
    int32_t device_id;
    bool operator < (const ExecutorId &other) const {
      if (context_id != other.context_id) {
        return context_id < other.context_id;
      } else {
        return device_id <  other.device_id;
      }
    }
  };

  Status GetOrForkExecutorProcess(const ExecutorId &context, ExecutorProcess **executor_process);
  Status GetExecutorProcess(const ExecutorId &context, ExecutorProcess **executor_process) const;

 private:
  static Status LoadAndExecuteChildProcess(const int32_t device_id,
                                           const std::string &msg_queue_name,
                                           const std::string &group_name);
  Status ForAndInit(const int32_t device_id, std::unique_ptr<ExecutorProcess> &executor_process);

  mutable std::mutex mu_;
  std::map<ExecutorId, std::unique_ptr<ExecutorProcess>> processes_;
};
}  // namespace ge

#endif  // AIR_RUNTIME_DEPLOY_DAEMON_EXECUTOR_MANAGER_H_
