/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/daemon/executor_manager.h"
#include <unistd.h>
#include <vector>
#include "framework/common/types.h"
#include "framework/common/debug/ge_log.h"
#include "mmpa/mmpa_api.h"
#include "runtime/rt_mem_queue.h"
#include "common/utils/bind_cpu_utils.h"
#include "common/utils/rts_api_utils.h"
#include "common/mem_grp/memory_group_manager.h"
#include "common/data_flow/queue/helper_exchange_service.h"
namespace ge {
namespace {
constexpr uint32_t kEventGroupId = 1;
constexpr int32_t kDefaultTimeout = 60 * 1000;
}
ExecutorProcess::ExecutorProcess(int32_t pid, int32_t device_id, std::string queue_name, std::string group_name)
    : pid_(pid), device_id_(device_id), msg_queue_name_(std::move(queue_name)), group_name_(std::move(group_name)) {
}

Status ExecutorProcess::Initialize() {
  GE_CHK_STATUS_RET_NOLOG(InitEventGroup());
  GE_CHK_STATUS_RET_NOLOG(InitMessageQueue());
  GE_CHK_STATUS_RET_NOLOG(WaitForExecutorInitialized());
  GELOGD("[Initialize] succeeded");
  return SUCCESS;
}

Status ExecutorProcess::InitEventGroup() {
  static std::mutex mu;
  static std::set<int32_t> initialized_devices;
  std::lock_guard<std::mutex> lk(mu);
  if (initialized_devices.count(device_id_) == 0) {
    GE_CHK_STATUS_RET(RtsApiUtils::EschedCreateGroup(device_id_, event_group_id_, RT_GRP_TYPE_BIND_CP_CPU),
                      "[Create][EschedGroup] failed, device_id = %d", device_id_);
    GELOGD("[Create][EschedGroup] succeeded, device_id = %d", device_id_, event_group_id_);
    uint64_t event_bitmap = 1ULL << static_cast<uint32_t>(RT_EVENT_QUEUE_ENQUEUE);
    GE_CHK_STATUS_RET(RtsApiUtils::EschedSubscribeEvent(device_id_, event_group_id_, 0, event_bitmap));
    GELOGD("[Subscribe][Event] succeeded");
    initialized_devices.emplace(device_id_);
  }
  return SUCCESS;
}

Status ExecutorProcess::Finalize() {
  if (pid_ == 0) {
    return SUCCESS;
  }

  if (SendEvent(ExecutorEventType::kFinalize) != SUCCESS) {
    GELOGW("Failed to send finalize command to executor, pid = %d", pid_);
    GELOGI("Send SIGTERM signal to executor, pid = %d", pid_);
    if (kill(pid_, SIGTERM) != 0) {
      GELOGE(FAILED, "Fail to kill queue schedule, pid=%d, error=%s.", pid_, strerror(errno));
      return FAILED;
    }
  }
  pid_ = 0;
  return SUCCESS;
}

Status ExecutorManager::LoadAndExecuteChildProcess(const int32_t device_id,
                                                   const std::string &msg_queue_name,
                                                   const std::string &group_name) {
  auto device_id_str = std::to_string(device_id);
  auto event_group_id = std::to_string(kEventGroupId);
  const std::string bin_path = "/usr/local/Ascend/modeldeployer/bin/npu_executor_main";
  const std::string enable = "1";
  const std::string process_name = "npu_executor";
  const int32_t override = 1;

  (void) mmSetEnv("AICPU_ADD_BUFFERGROUP", enable.c_str(), override);
  const char_t *argv[] = {
      process_name.c_str(),
      group_name.c_str(),
      msg_queue_name.c_str(),
      device_id_str.c_str(),
      event_group_id.c_str(),
      nullptr
  };
  auto res = ::execv(bin_path.c_str(), const_cast<char_t **>(argv));
  if (res < 0) {
    GELOGE(FAILED, "[Execute] npu executor failed, path = %s, err_msg = %s", bin_path.c_str(), strerror(errno));
    return FAILED;
  }
  return SUCCESS;
}

Status ExecutorProcess::InitMessageQueue() {
  GE_CHK_STATUS_RET(MemoryGroupManager::GetInstance().MemGrpAddProc(group_name_, pid_, false, true),
                    "Failed to add group, pid = %d", pid_);
  uint32_t queue_depth = 3;
  GE_CHK_STATUS_RET(HelperExchangeService::GetInstance().CreateQueue(device_id_,
                                                                     msg_queue_name_,
                                                                     queue_depth,
                                                                     RT_MQ_MODE_PUSH,
                                                                     msg_queue_id_),
                    "[Create][MsgQueue] failed");
  rtMemQueueShareAttr_t c_queue_attr{};
  c_queue_attr.read = 1;
  GE_CHK_RT_RET(rtMemQueueGrant(device_id_, msg_queue_id_, pid_, &c_queue_attr));
  return SUCCESS;
}

Status ExecutorProcess::WaitEvent(ExecutorEventType expected_event_type) const {
  GE_CHK_STATUS_RET_NOLOG(BindCpuUtils::BindCore(0));
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::EschedAttachDevice(0));
  uint32_t retry_cnt = 3;
  while (retry_cnt > 0) {
    // grpc server always wait event on device 0
    GELOGD("[Wait][Event] start, device_id = 0, group_id = %u", event_group_id_);
    rtEschedEventSummary_t event_info;
    auto ret = rtEschedWaitEvent(0, event_group_id_, 0, kDefaultTimeout, &event_info);
    if (ret == RT_ERROR_NONE) {
      GELOGD("[Wait][Event] success, event_id = %d, sub_event_id = %u", event_info.eventId, event_info.subeventId);
      auto event_type = static_cast<ExecutorEventType>(event_info.subeventId);
      if (event_type != expected_event_type) {
        GELOGE(FAILED,
               "expect event type = %d, but got = %u, err_msg = %s",
               static_cast<int32_t>(expected_event_type),
               event_info.subeventId,
               event_info.msgLen > 0U ? event_info.msg : "");
        return FAILED;
      }
      return SUCCESS;
    }

    --retry_cnt;
    GELOGD("[Wait][Event] timeout, retry_count = %d", retry_cnt);
  }
  GELOGE(FAILED, "Wait timeout, device_id = %d, group_id = %u", device_id_, event_group_id_);
  return FAILED;
}

Status ExecutorProcess::WaitForExecutorInitialized() {
  GE_CHK_STATUS_RET(WaitEvent(ExecutorEventType::kSuccess),
                    "Failed to wait for executor process initialize.");
  return SUCCESS;
}

Status ExecutorProcess::SendRequest(deployer::ExecutorRequest &request, deployer::ExecutorResponse &resp) const {
  auto req_size = request.ByteSizeLong();
  ExchangeService::FillFunc fill_func = [&request](void *buffer, size_t size) {
    if (request.SerializeToArray(buffer, static_cast<int32_t>(size))) {
      return SUCCESS;
    }
    GELOGE(FAILED, "SerializeToArray failed");
    return FAILED;
  };

  GE_CHK_STATUS_RET(HelperExchangeService::GetInstance().Enqueue(device_id_, msg_queue_id_, req_size, fill_func),
                    "Failed to enqueue request");
  GELOGD("[Enqueue][Request] succeeded");
  GE_CHK_STATUS_RET_NOLOG(SendEvent(ExecutorEventType::kRequest));
  GELOGD("[Send][Event] succeeded");
  GE_CHK_STATUS_RET_NOLOG(WaitEvent(ExecutorEventType::kSuccess));
  GELOGD("[Wait][Event] succeeded");
  return SUCCESS;
}

Status ExecutorProcess::SendEvent(ExecutorEventType event_type) const {
  rtEschedEventSummary_t event_info;
  event_info.eventId = RT_EVENT_QUEUE_ENQUEUE;
  event_info.subeventId = static_cast<uint32_t>(event_type);
  event_info.pid = pid_;
  event_info.grpId = event_group_id_;
  event_info.msg = nullptr;
  event_info.msgLen = 0;
  GE_CHK_STATUS_RET(RtsApiUtils::EschedSubmitEvent(device_id_, event_info),
                    "[Submit][Event] failed, device_id = %d, group = %u, pid = %d",
                    device_id_, event_group_id_, pid_);
  GELOGD("[Submit][Event] succeeded, device_id = %d, group = %u, pid = %d",
         device_id_, event_group_id_, pid_);
  return SUCCESS;
}

ExecutorProcess::~ExecutorProcess() {
  (void) Finalize();
}

Status ExecutorManager::ForAndInit(const int32_t device_id, std::unique_ptr<ExecutorProcess> &executor_process) {
  const auto group_name = MemoryGroupManager::GetInstance().GetQsMemGroupName();
  auto pid = fork();
  if (pid < 0) {
    GELOGE(FAILED, "[Fork][Process] failed");
    return FAILED;
  }

  if (pid == 0) {
    // In child process
    std::string msg_queue_name = "queue.npu_executor_" + std::to_string(mmGetPid());
    return LoadAndExecuteChildProcess(device_id, msg_queue_name, group_name);
  }

  // In parent process
  GELOGD("[Fork][Process] succeeded, pid = %d", pid);
  std::string msg_queue_name = "queue.npu_executor_" + std::to_string(pid);
  executor_process.reset(new (std::nothrow)ExecutorProcess(pid, device_id, msg_queue_name, group_name));
  GE_CHECK_NOTNULL(executor_process);
  GE_CHK_STATUS_RET(executor_process->Initialize(), "Failed to initialize executor process");
  return SUCCESS;
}

Status ExecutorManager::GetOrForkExecutorProcess(const ExecutorId &context,
                                                 ExecutorProcess **executor_process) {
  std::lock_guard<std::mutex> lk(mu_);
  auto it = processes_.find(context);
  if (it != processes_.end()) {
    *executor_process = it->second.get();
    return SUCCESS;
  }

  GELOGD("Executor process not exist, start to create, context_id = %ld, device id = %d",
         context.context_id, context.device_id);
  std::unique_ptr<ExecutorProcess> process;
  GE_CHK_STATUS_RET(ForAndInit(context.device_id, process),
                    "Failed to fork and execute executor process, context_id = %ld, device id = %d",
                    context.context_id, context.device_id);
  GE_CHECK_NOTNULL(process);
  GELOGD("Executor process started successfully, context_id = %ld, device id = %d",
         context.context_id, context.device_id);
  *executor_process = process.get();
  (void)processes_.emplace(context, std::move(process));
  return SUCCESS;
}

Status ExecutorManager::GetExecutorProcess(const ExecutorId &context,
                                           ExecutorProcess **executor_process) const {
  std::lock_guard<std::mutex> lk(mu_);
  auto it = processes_.find(context);
  if (it != processes_.end()) {
    *executor_process = it->second.get();
    return SUCCESS;
  }

  GELOGE(FAILED, "Executor process not exist, context_id = %ld, device id = %d",
         context.context_id, context.device_id);
  return FAILED;
}
}  // namespace ge
