/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2021. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/executor/engine_daemon.h"
#include "runtime/rt_mem_queue.h"
#include "mmpa/mmpa_api.h"
#include "proto/deployer.pb.h"
#include "event_handler.h"
#include "executor_event_defs.h"
#include "common/utils/bind_cpu_utils.h"

namespace ge {
namespace {
constexpr int32_t kDefaultTimeout = 10 * 1000;  // 10s
constexpr uint32_t kCpuNumsInDevice = 8U;
}

Status EngineDaemon::InitializeWithArgs(int32_t argc, char **argv) {
  GE_CHK_STATUS_RET_NOLOG(ParseCmdLineArgs(argc, argv));
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::MemGrpAttach(mem_group_name_, kDefaultTimeout));
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::MbufInit());
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::SetDevice(device_id_));
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::MemQueueInit(device_id_));
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::EschedAttachDevice(device_id_));
  // each device has 8 cpu core
  GE_CHK_STATUS_RET_NOLOG(BindCpuUtils::BindCore(device_id_ * kCpuNumsInDevice));
  GE_CHK_STATUS_RET_NOLOG(InitMessageQueue());
  GE_CHK_STATUS_RET_NOLOG(event_handler_.Initialize());
  GE_CHK_STATUS_RET_NOLOG(NotifyInitialized());
  return SUCCESS;
}

Status EngineDaemon::LoopEvents() {
  GELOGD("Event loop started");
  while (true) {
    ExecutorEventType event_type;
    bool is_timeout = false;
    (void) WaitEvent(event_type, is_timeout);
    if (is_timeout) {
      GELOGD("No event was received, continue");
      continue;
    }

    if (event_type == ExecutorEventType::kHeartbeat) {
      GELOGD("Heartbeat received");
    } else if (event_type == ExecutorEventType::kFinalize) {
      GELOGD("Finalize received");
      break;
    } else if (event_type == ExecutorEventType::kRequest) {
      GELOGD("Request received, start dequeue");
      GE_CHK_STATUS_RET_NOLOG(HandleEvent());
    } else {
      GELOGE(UNSUPPORTED, "Unsupported event type: %d", static_cast<int32_t>(event_type));
      GE_CHK_STATUS_RET_NOLOG(SendEvent(ExecutorEventType::kFailure, "unsupported event type"));
    }

  }
  return SUCCESS;
}

Status EngineDaemon::HandleEvent() {
  void *mbuf = nullptr;
  GE_CHK_RT_RET(rtMemQueueDeQueue(device_id_, msg_queue_id_, &mbuf));
  GELOGD("[Dequeue][Message] success");
  GE_MAKE_GUARD(mbuf, [mbuf]() {
    (void) rtMbufFree(mbuf);
  });
  void *buffer_addr = nullptr;
  uint64_t buffer_size = 0;
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::MbufGetBufferAddr(mbuf, &buffer_addr));
  GE_CHK_STATUS_RET_NOLOG(RtsApiUtils::MbufGetBufferSize(mbuf, buffer_size));
  GELOGD("[Parse][Message] addr = %p, size = %lu", buffer_addr, buffer_size);
  deployer::ExecutorRequest request;
  if (!request.ParseFromArray(buffer_addr, static_cast<int32_t>(buffer_size))) {
    GELOGE(PARAM_INVALID, "[Parse][Message] failed");
    return FAILED;
  }

  GELOGD("On event: %s", request.DebugString().c_str());
  deployer::ExecutorResponse response;
  event_handler_.HandleEvent(request, response);
  if (response.status_code() == SUCCESS) {
    GELOGD("[Handle][Event] succeeded");
    GE_CHK_STATUS_RET_NOLOG(SendEvent(ExecutorEventType::kSuccess));
  } else {
    GELOGD("[Handle][Event] failed, error_code = %u, error_msg = %s",
           response.status_code(),
           response.error_message().c_str());
    GE_CHK_STATUS_RET_NOLOG(SendEvent(ExecutorEventType::kFailure, response.error_message()));
  }
  return SUCCESS;
}

Status EngineDaemon::ParseCmdLineArgs(int32_t argc, char **argv) {
  const int32_t kExpectedArgCount = 5;
  if (argc != kExpectedArgCount) {
    GELOGE(PARAM_INVALID, "[Parse][Args] failed, arg count (%d) is invalid", argc);
    return PARAM_INVALID;
  }
  const char *memory_group_name = argv[1];
  GE_CHECK_NOTNULL(memory_group_name);
  mem_group_name_ = std::string(memory_group_name);
  const char *msg_queue_name = argv[2];
  GE_CHECK_NOTNULL(msg_queue_name);
  msg_queue_name_ = std::string(msg_queue_name);
  const char *device_id = argv[3];
  GE_CHECK_NOTNULL(device_id);
  GE_CHK_STATUS_RET_NOLOG(ToNumber(device_id, device_id_));
  const char *event_group_id = argv[4];
  GE_CHECK_NOTNULL(event_group_id);
  GE_CHK_STATUS_RET_NOLOG(ToNumber(event_group_id, event_group_id_));
  GELOGD("[Parse][Args] succeeded, mem_grp_name = %s, msg_q_name = %s, device_id = %d, evt_group_id = %u",
         mem_group_name_.c_str(), msg_queue_name_.c_str(), device_id_, event_group_id_);
  return SUCCESS;
}

Status EngineDaemon::WaitEvent(ExecutorEventType &event_type, bool &is_timeout) {
  rtEschedEventSummary_t in_event;
  auto ret = rtEschedWaitEvent(device_id_, event_group_id_, 0, kDefaultTimeout, &in_event);
  if (ret != RT_ERROR_NONE) {
    // TODO check fail
    GELOGE(RT_FAILED, "Failed to invoke rtEschedWaitEvent, device_id = %d, group_id = %u, ret = 0x%X",
           device_id_, event_group_id_, ret);
    is_timeout = true;
    return FAILED;
  }
  event_type = static_cast<ExecutorEventType>(in_event.subeventId);
  return SUCCESS;
}

Status EngineDaemon::InitMessageQueue() {
  GE_CHK_STATUS_RET(RtsApiUtils::MemQueryGetQidByName(device_id_, msg_queue_name_, msg_queue_id_),
                    "Failed to query msg queue id, device_id = %d, queue name = [%s]",
                    device_id_, msg_queue_name_.c_str());
  GELOGD("[Query][MsgQueueId] succeeded, queue_name = %s, queue_id = %u", msg_queue_name_.c_str(), msg_queue_id_);
  GE_CHK_STATUS_RET(RtsApiUtils::MemQueueAttach(device_id_, msg_queue_id_, kDefaultTimeout),
                    "[Attach][MsgQueue] failed, device_id = %d, queue_id = %u", device_id_, msg_queue_id_);
  GELOGD("[Attach][MsgQueue] succeeded, device_id = %d, queue_id = %u", device_id_, msg_queue_id_);
  GE_CHK_STATUS_RET(RtsApiUtils::EschedCreateGroup(device_id_, event_group_id_, RT_GRP_TYPE_BIND_CP_CPU));
  uint64_t mask = 1ULL << static_cast<uint32_t>(RT_EVENT_QUEUE_ENQUEUE);
  GE_CHK_STATUS_RET(RtsApiUtils::EschedSubscribeEvent(device_id_, event_group_id_, 0, mask),
                    "[Subscribe][Event] failed, device_id = %d, group_id = %u", device_id_, event_group_id_);
  GELOGD("[Subscribe][Event] succeeded, device_id = %d, group_id = %u", device_id_, event_group_id_);
  rtEschedEventSummary_t to_drop;
  // need wait before submit of another process, this wait would time out, and it's OK
  (void) rtEschedWaitEvent(device_id_, event_group_id_, 0, 1, &to_drop);

  parent_pid_ = getppid();
  GELOGD("parent_pid = %d", parent_pid_);
  return SUCCESS;
}
Status EngineDaemon::NotifyInitialized() {
  GE_CHK_STATUS_RET(SendEvent(ExecutorEventType::kSuccess, "Init success"),
                   "[Notify][Initialized] submit event failed, device_id = %d", device_id_);
  GELOGD("[Notify][Initialized] success");
  return SUCCESS;
}

Status EngineDaemon::SendEvent(ExecutorEventType type, const std::string &msg) const {
  rtEschedEventSummary_t event_info{};
  event_info.eventId = RT_EVENT_QUEUE_ENQUEUE;
  event_info.pid = parent_pid_;
  event_info.grpId = event_group_id_;
  event_info.subeventId = static_cast<uint32_t>(type);
  if (!msg.empty()) {
    event_info.msg = const_cast<char *>(msg.c_str());
    event_info.msgLen = msg.length();
  } else {
    event_info.msg = nullptr;
    event_info.msgLen = 0;
  }
  // only one grpc server on device 0, send event to grpc server(device 0)
  GE_CHK_STATUS_RET(RtsApiUtils::EschedSubmitEvent(0, event_info),
                    "[Send][Event] failed, device_id = 0, type = %u", static_cast<uint32_t>(type));
  GELOGD("[Send][Event] succeeded, device_id = 0, type = %u", static_cast<uint32_t>(type));
  return SUCCESS;
}
}  // namespace ge
