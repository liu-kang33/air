/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef REMOTE_DEVICE_MANAGER_H_
#define REMOTE_DEVICE_MANAGER_H_

#include <vector>
#include <string>
#include <thread>
#include <mutex>
#include <atomic>
#include "common/config/json_parser.h"
#include "deploy/master/device_mgr/deployer_client.h"
#include "proto/deployer.pb.h"

namespace ge {
class DeviceManger {
 public :
  static DeviceManger &GetInstance() {
    static DeviceManger instance;
    return instance;
  }

  /*
   *  @ingroup ge
   *  @brief   init device manager
   *  @return  SUCCESS or FAILED
   */
  Status Initialize();

  /*
   *  @ingroup ge
   *  @brief   get device info list
   *  @return  device info list
   */
  const std::vector<DeviceInfo> &GetDeviceInfoList();

  /*
   *  @ingroup ge
   *  @brief   get device info by device id
   *  @param   [in]  uint32_t
   *  @param   DeviceInfo **
   *  @return  SUCCESS or FAILED
   */
  Status GetDeviceInfo(uint32_t device_id, const DeviceInfo **device_info);


  /*
   *  @ingroup ge
   *  @brief   get host info
   *  @return  HostInfo
   */
  const HostInfo &GetHostInfo();
 private:

  /*
   *  @ingroup ge
   *  @brief   load config info
   *  @return  SUCCESS or FAILED
   */
  Status LoadConfig();
  ge::HostInformation hostInformation_;
};

class RemoteDevice {
 public:
  explicit RemoteDevice(const DeviceInfo &device_info) : device_info_(device_info) {}
  virtual ~RemoteDevice();

  /*
   *  @ingroup ge
   *  @brief   init remote device
   *  @return  SUCCESS or FAILED
   */
  Status Initialize();

  /*
   *  @ingroup ge
   *  @brief   finalize remote device
   *  @return  SUCCESS or FAILED
   */
  Status Finalize();

  /*
   *  @ingroup ge
   *  @brief   send grpc request
   *  @param   [in]  const DeployerRequest *
   *  @param   [in]  DeployerResponse *
   *  @return: SUCCESS or FAILED
   */
  virtual Status SendRequest(deployer::DeployerRequest &request, deployer::DeployerResponse &response);

  int32_t GetDeviceCount() { return dev_count_; }
 private:

  /*
   *  @ingroup ge
   *  @brief   heartbeats keepalive
   *  @return: None
   */
  void Keepalive();
  DeviceInfo device_info_;
  DeployerClient client_;
  std::mutex mu_;
  std::atomic_bool connected_{};
  std::thread keepalive_thread_;
  int64_t client_id_ = -1;
  int32_t dev_count_ = 1;
};
}  // namespace ge
#endif  // REMOTE_DEVICE_MANAGER_H_