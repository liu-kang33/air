/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef RUNTIME_DEVICE_REMOTE_DEVICE_PROXY_H_
#define RUNTIME_DEVICE_REMOTE_DEVICE_PROXY_H_

#include <memory>
#include "proto/deployer.pb.h"
#include "deploy/master/device_mgr/device_manager.h"

namespace ge {
class DeviceProxy {
 public:
  Status Initialize(const std::vector<DeviceInfo> &device_info_list);
  Status Finalize();
  Status SendRequest(int32_t device_id, deployer::DeployerRequest &request, deployer::DeployerResponse &response);
  Status GetDeviceCount(const int32_t device_id, int32_t &dev_count);

 private:
  virtual std::unique_ptr<RemoteDevice> CreateRemoteDevice(const DeviceInfo &device_info);
  std::vector<std::unique_ptr<RemoteDevice>> remote_devices_;
};
}  // namespace ge

#endif  // RUNTIME_DEVICE_REMOTE_DEVICE_PROXY_H_
