/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/master/device_mgr/device_manager.h"
#include "framework/common/util.h"
#include "framework/common/debug/ge_log.h"
#include "common/config/configurations.h"

namespace ge {
namespace {
constexpr int32_t kHeartbeatIntervalSec = 5;
constexpr int32_t kMaxTrySendCount = 3;
}

Status DeviceManger::Initialize() {
  if (LoadConfig() != SUCCESS) {
    return FAILED;
  }
  return SUCCESS;
}

const std::vector<DeviceInfo> &DeviceManger::GetDeviceInfoList() {
  return hostInformation_.device_info;
}

Status DeviceManger::GetDeviceInfo(uint32_t device_id, const DeviceInfo **device_info) {
  GE_CHECK_NOTNULL(device_info);
  if (device_id >= hostInformation_.device_info.size()) {
    GELOGE(FAILED, "[Check][DeviceId]Invalid device_id=%u, max device_id=%lu", device_id,
           hostInformation_.device_info.size());
    REPORT_CALL_ERROR("E19999", "[Check][DeviceId]Invalid device_id=%u, max device_id=%lu", device_id,
                      hostInformation_.device_info.size());
    return FAILED;
  }
  *device_info = &hostInformation_.device_info[device_id];
  return SUCCESS;
}

const HostInfo &DeviceManger::GetHostInfo() {
  return hostInformation_.host_info;
}

Status DeviceManger::LoadConfig() {
  hostInformation_ = Configurations::GetInstance().GetHostInformation();
  std::sort(hostInformation_.device_info.begin(), hostInformation_.device_info.end(),
            [](const DeviceInfo &a, const DeviceInfo &b) -> bool {
               return (a.ipaddr < b.ipaddr) || ((a.ipaddr == b.ipaddr) && (a.port < b.port));
            });
  return SUCCESS;
}

RemoteDevice::~RemoteDevice() {
  if (connected_) {
    Finalize();
  }
}

Status RemoteDevice::Initialize() {
  std::lock_guard<std::mutex> lk(mu_);
  if (connected_) {
    GELOGI("Already init");
    return SUCCESS;
  }

  std::string address = device_info_.ipaddr + ":" + std::to_string(device_info_.port);
  if (client_.Init(address) != SUCCESS) {
    REPORT_CALL_ERROR("E19999", "Init grpc client failed, address:%s", address.c_str());
    GELOGE(FAILED, "[Init][Client]Init grpc client failed, address:%s", address.c_str());
    return FAILED;
  }
  deployer::DeployerRequest request;
  request.set_type(deployer::kInitRequest);
  auto init_request = request.mutable_init_request();
  init_request->set_token(device_info_.token);
  deployer::DeployerResponse response;
  if (client_.SendRequest(request, response) != SUCCESS) {
    REPORT_CALL_ERROR("E19999", "Send init request failed, response info:%s", response.error_message().c_str());
    GELOGE(FAILED, "[Send][Request]Send init request failed, response info:%s", response.error_message().c_str());
    return FAILED;
  }
  auto error_code = response.error_code();
  if (error_code != 0) {
    REPORT_CALL_ERROR("E19999", "Check response failed. error code =%u, error message=%s", error_code,
                      response.error_message().c_str());
    GELOGE(FAILED, "[Check][Response]Check response failed. error code =%u, error message=%s", error_code,
           response.error_message().c_str());
    return FAILED;
  }
  client_id_ = response.init_response().client_id();
  dev_count_ = response.init_response().dev_count();
  connected_ = true;
  keepalive_thread_ = std::thread([&]() {
    Keepalive();
  });
  return SUCCESS;
}

Status RemoteDevice::Finalize() {
  std::lock_guard<std::mutex> lk(mu_);
  if (!connected_) {
    GELOGI("Already closed");
    return SUCCESS;
  }

  connected_ = false;
  keepalive_thread_.join();
  deployer::DeployerRequest request;
  request.set_type(deployer::kDisconnect);
  request.set_client_id(client_id_);
  deployer::DeployerResponse response;
  if (client_.SendRequest(request, response) != SUCCESS) {
    REPORT_CALL_ERROR("E19999", "Send disconnect request failed, response info:%s", response.error_message().c_str());
    GELOGE(FAILED, "[Send][Request]Send disconnect request failed, response info:%s", response.error_message().c_str());
    return FAILED;
  }
  return SUCCESS;
}

Status RemoteDevice::SendRequest(deployer::DeployerRequest &request, deployer::DeployerResponse &response) {
  std::lock_guard<std::mutex> lk(mu_);
  if (!connected_) {
    REPORT_CALL_ERROR("E19999", "Grpc client has not been inited.");
    GELOGE(FAILED, "[Send][Request]Grpc client has not been inited.");
    return FAILED;
  }
  request.set_client_id(client_id_);
  int32_t cnt = 0;
  while (cnt < kMaxTrySendCount) {
    if (client_.SendRequest(request, response) == SUCCESS) {
      GELOGI("Send request success");
      return SUCCESS;
    }
    ++cnt;
    GELOGW("Send request failed, try send again,count:%d", cnt);
  }
  REPORT_CALL_ERROR("E19999", "Grpc send request failed.");
  GELOGE(FAILED, "[Send][Request]Grpc send request failed.");
  return FAILED;
}

void RemoteDevice::Keepalive() {
  GELOGI("Keepalive task started");
  while (connected_) {
    std::this_thread::sleep_for(std::chrono::seconds(kHeartbeatIntervalSec));
    deployer::DeployerRequest request;
    request.set_client_id(client_id_);
    request.set_type(deployer::kHeartbeat);
    deployer::DeployerResponse response;
    if (client_.SendRequest(request, response) != SUCCESS) {
      GELOGW("Send heartbeat request failed, response info:%s", response.error_message().c_str());
    }
    GELOGI("Success to send heartbeat to client_id[%ld]", client_id_);
  }
  GELOGI("Keepalive task ended.");
}
}  // namespace ge