/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "deploy/master/device_mgr/device_proxy.h"
#include "framework/common/ge_inner_error_codes.h"
#include "framework/common/debug/ge_log.h"
#include "debug/ge_log.h"
#include "debug/ge_util.h"

namespace ge {
Status DeviceProxy::Initialize(const std::vector<DeviceInfo> &device_info_list) {
  for (size_t i = 0U; i < device_info_list.size(); ++i) {
    const auto &device_info = device_info_list[i];
    auto remote_device = CreateRemoteDevice(device_info);
    GE_CHECK_NOTNULL(remote_device);
    GE_CHK_STATUS_RET(remote_device->Initialize(), "Failed to initialize remote device, id = %zu", i);
    GELOGI("Remote device initialized successfully, id = %zu", i);
    remote_devices_.emplace_back(std::move(remote_device));
  }
  return SUCCESS;
}

Status DeviceProxy::Finalize() {
  for (auto &remote_device : remote_devices_) {
    (void) remote_device->Finalize();
  }
  GELOGI("Device proxy finalized");
  return SUCCESS;
}

Status DeviceProxy::SendRequest(int32_t device_id,
                                deployer::DeployerRequest &request,
                                deployer::DeployerResponse &response) {
  if (device_id < 0 || static_cast<size_t>(device_id) >= remote_devices_.size()) {
    GELOGE(PARAM_INVALID,
           "device id out of range, device id = %d, num_devices = %zu",
           device_id,
           remote_devices_.size());
    return PARAM_INVALID;
  }

  auto &remote_device = remote_devices_[device_id];
  return remote_device->SendRequest(request, response);
}

Status DeviceProxy::GetDeviceCount(const int32_t device_id, int32_t &dev_count) {
  if (device_id < 0 || static_cast<size_t>(device_id) >= remote_devices_.size()) {
    GELOGW("device id out of range, device id = %d, num_devices = %zu",
           device_id,
           remote_devices_.size());
    dev_count = 0;
    return SUCCESS;
  }

  auto &remote_device = remote_devices_[device_id];
  dev_count = remote_device->GetDeviceCount();
  return SUCCESS;
}

std::unique_ptr<RemoteDevice> DeviceProxy::CreateRemoteDevice(const DeviceInfo &device_info) {
  return std::unique_ptr<RemoteDevice>(new(std::nothrow)RemoteDevice(device_info));
}
}  // namespace ge