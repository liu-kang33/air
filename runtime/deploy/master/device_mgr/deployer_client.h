/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef REMOTE_DEPLOYER_CLIENT_H_
#define REMOTE_DEPLOYER_CLIENT_H_

#include <string>
#include <memory>
#include "common/plugin/ge_util.h"
#include "proto/deployer.pb.h"
#include "ge/ge_api_error_codes.h"

namespace ge {
class DeployerClient {
 public:
  DeployerClient();
  ~DeployerClient();
  GE_DELETE_ASSIGN_AND_COPY(DeployerClient);

  /*
   *  @ingroup ge
   *  @brief   init deployer client
   *  @param   [in]  std::string &
   *  @return  SUCCESS or FAILED
   */
  Status Init(const std::string &address);

  /*
   *  @ingroup ge
   *  @brief   send grpc request
   *  @param   [in]  const DeployerRequest *
   *  @param   [in]  DeployerResponse *
   *  @return: SUCCESS or FAILED
   */
  Status SendRequest(const deployer::DeployerRequest &request, deployer::DeployerResponse &response);

 private:
  class Impl;
  std::unique_ptr<Impl> impl_;
};
}  // namespace ge
#endif  // REMOTE_DEPLOYER_CLIENT_H_