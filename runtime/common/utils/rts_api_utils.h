/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.SUCCESS (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.SUCCESS
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef AIR_RUNTIME_COMMON_UTILS_RTS_API_UTILS_H_
#define AIR_RUNTIME_COMMON_UTILS_RTS_API_UTILS_H_
#include "framework/common/debug/log.h"
#include "runtime/rt.h"
#include "external/runtime/rt_error_codes.h"

namespace ge {
class RtsApiUtils {
 public:
  static inline Status SetDevice(int32_t device_id) {
    GE_CHK_RT_RET(rtSetDevice(device_id));
    return SUCCESS;
  }

  static inline Status MemGrpAttach(const std::string &name, int32_t timeout) {
    GE_CHK_RT_RET(rtMemGrpAttach(name.c_str(), timeout));
    return SUCCESS;
  }

  static inline Status MbufInit() {
    rtMemBuffCfg_t buff_cfg{};
    auto ret = rtMbufInit(&buff_cfg);
    GE_CHK_BOOL_RET_STATUS(ret == SUCCESS || ret == ACL_ERROR_RT_REPEATED_INIT,
                           RT_FAILED, "Invoke rtMbufInit failed, ret = 0x%X", ret);
    return SUCCESS;
  }

  static inline Status MbufGetBufferAddr(void *mbuf, void **data_addr) {
    GE_CHK_RT_RET(rtMbufGetBuffAddr(mbuf, data_addr));
    return SUCCESS;
  }

  static inline Status MbufGetBufferSize(void *mbuf, uint64_t &size) {
    GE_CHK_RT_RET(rtMbufGetBuffSize(mbuf, &size));
    return SUCCESS;
  }

  static inline Status MbufGetPrivData(void *mbuf, void **priv, uint64_t *size) {
    GE_CHK_RT_RET(rtMbufGetPrivInfo(mbuf, priv, size));
    return SUCCESS;
  }

  static inline Status MemQueueInit(int32_t device_id) {
    auto ret = rtMemQueueInit(device_id);
    GE_CHK_BOOL_RET_STATUS(ret == SUCCESS || ret == ACL_ERROR_RT_REPEATED_INIT,
                           RT_FAILED, "Invoke rtMemQueueInit failed, device_id = %d, ret = 0x%X", device_id, ret);
    return SUCCESS;
  }

  static inline Status MemQueryGetQidByName(int32_t device_id, const std::string &queue_name, uint32_t &queue_id) {
    GE_CHK_RT_RET(rtMemQueueGetQidByName(device_id, queue_name.c_str(), &queue_id));
    return SUCCESS;
  }

  static inline Status MemQueueAttach(int32_t device_id, uint32_t queue_id, int32_t timeout) {
    GE_CHK_RT_RET(rtMemQueueAttach(device_id, queue_id, timeout));
    return SUCCESS;
  }

  static inline Status EschedAttachDevice(int32_t device_id) {
    GE_CHK_RT_RET(rtEschedAttachDevice(device_id));
    return SUCCESS;
  }

  static inline Status EschedCreateGroup(int32_t device_id, uint32_t group_id, rtGroupType_t type) {
    GE_CHK_RT_RET(rtEschedCreateGrp(device_id, group_id, type));
    return SUCCESS;
  }

  static inline Status EschedSubscribeEvent(int32_t device_id, uint32_t group_id, uint32_t thread_id, uint64_t mask) {
    GE_CHK_RT_RET(rtEschedSubscribeEvent(device_id, group_id, thread_id, mask));
    return SUCCESS;
  }

  static inline Status EschedSubmitEvent(int32_t device_id, rtEschedEventSummary_t &event_summary) {
    GE_CHK_RT_RET(rtEschedSubmitEvent(device_id, &event_summary));
    return SUCCESS;
  }

  static inline Status GetDeviceCount(int32_t &dev_count) {
    GE_CHK_RT_RET(rtGetDeviceCount(&dev_count));
    return SUCCESS;
  }
};
}  // namespace ge
#endif  // AIR_RUNTIME_COMMON_UTILS_RTS_API_UTILS_H_
