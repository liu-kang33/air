cmake_minimum_required(VERSION 3.14)
project(GraphEngineGrpc)

if (NOT DEFINED PRODUCT_SIDE)
    set(PRODUCT_SIDE "host")
endif ()

set(GRPC_PROTO_LIST
        "${AIR_CODE_DIR}/runtime/proto/deployer.proto"
        )

set(DEPLOYER_PROTO_LIST
        "${AIR_CODE_DIR}/runtime/proto/deployer.proto"
        )
set(GRPC_SERVER_PROTO_LIST
        "${METADEF_DIR}/proto/ge_ir.proto"
        "${METADEF_DIR}/proto/var_manager.proto"
        "${METADEF_DIR}/proto/task.proto"
        "${AIR_CODE_DIR}/runtime/proto/deployer.proto"
        )

protobuf_generate(deployer DEPLOYER_PROTO_SRCS DEPLOYER_PROTO_HDRS ${DEPLOYER_PROTO_LIST} "--proto_path=${METADEF_DIR}/proto")
protobuf_generate_grpc(deployer GRPC_PROTO_SRCS GRPC_PROTO_HDRS  ${GRPC_PROTO_LIST} "--proto_path=${METADEF_DIR}/proto")
protobuf_generate_grpc(deployer GRPC_SERVER_PROTO_SRCS GRPC_SERVER_PROTO_HDRS  ${GRPC_SERVER_PROTO_LIST} "--proto_path=${METADEF_DIR}/proto")

set(CLUSTER_PROTO_LIST
        "${AIR_CODE_DIR}/runtime/proto/cluster.proto" 
)

protobuf_generate_grpc(cluster CLUSTER_PROTO_SRCS CLUSTER_PROTO_HDRS ${CLUSTER_PROTO_LIST} "--proto_path=${METADEF_DIR}/proto")

########################################## comm #################################
set(HELPER_COMMUNICATION_SRC_LIST
        communication/cluster/cluster_manager.cc
        communication/cluster/cluster_parser.cc
        communication/cluster/cluster_server.cc
        communication/cluster/cluster_data.cc
        communication/cluster/cluster_client.cc
        communication/cluster/cluster_grpc_client.cc
        communication/cluster/cluster_service_impl.cc
        communication/rank_parser.cc
        communication/communication_domain.cc
        )

set(HELPER_COMMUNICATION_INC_LIST
        ${CMAKE_CURRENT_SOURCE_DIR}/communication
        ${CMAKE_BINARY_DIR}/proto_grpc/cluster
        )

########################################## client ###############################
set(HELPER_RUNTIME_SRC_LIST
        common/config/json_parser.cc
        common/config/configurations.cc
        common/data_flow/route/datagw_manager.cc
        common/data_flow/route/network_manager.cc
        common/data_flow/route/helper_exchange_deployer.cc
        common/data_flow/route/queue_schedule_manager.cc
        common/data_flow/queue/helper_exchange_service.cc
        common/mem_grp/memory_group_manager.cc
        deploy/master/device_mgr/deployer_client.cc
        deploy/master/device_mgr/device_manager.cc
        deploy/master/device_mgr/device_proxy.cc
        deploy/master/helper_deploy_planner.cc
        deploy/master/master_model_deployer.cc
        deploy/master/helper_execution_runtime.cc
        )

add_library(grpc_client SHARED
        ${HELPER_RUNTIME_SRC_LIST}
        ${GRPC_PROTO_HDRS}
        ${GRPC_PROTO_SRCS}
        ${HELPER_COMMUNICATION_SRC_LIST}
        ${CLUSTER_PROTO_HDRS}
        ${CLUSTER_PROTO_SRCS}
        )

include_directories(
        ${METADEF_DIR}
        ${METADEF_DIR}/inc
        ${METADEF_DIR}/inc/external
        ${METADEF_DIR}/graph
        ${AIR_CODE_DIR}/inc
        ${AIR_CODE_DIR}/base
        ${AIR_CODE_DIR}/inc/external
        ${CMAKE_BINARY_DIR}/proto_grpc/deployer
        ${CMAKE_CURRENT_SOURCE_DIR}
        #### yellow zone ####
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/open_source/json/include>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc/aicpu>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/ace/npuruntime/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/abl/msprof/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/air/inc/framework>
        #### blue zone ####
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${CMAKE_BINARY_DIR}/protobuf_build-prefix/src/protobuf_build/src/>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${CMAKE_BINARY_DIR}/protoc_grpc_build-prefix/src/protoc_grpc_build/include/>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${METADEF_DIR}/third_party/fwkacllib/inc/mmpa>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/third_party/inc>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/inc/framework>
)

target_include_directories(grpc_client PRIVATE
        ${METADEF_DIR}
        ${METADEF_DIR}/inc
        ${METADEF_DIR}/inc/external
        ${METADEF_DIR}/graph
        ${AIR_CODE_DIR}/base
        ${AIR_CODE_DIR}/inc
        ${AIR_CODE_DIR}/inc/external
        ${CMAKE_BINARY_DIR}/proto/graphengine_protos/proto
        ${CMAKE_BINARY_DIR}/proto/graphengine_protos
        ${CMAKE_BINARY_DIR}/proto_grpc/deployer
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${HELPER_COMMUNICATION_INC_LIST}
        #### yellow zone ####
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/open_source/json/include>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc/aicpu>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/ace/npuruntime/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/abl/msprof/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/air/inc/framework>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/metadef>
        #### blue zone ####
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${CMAKE_BINARY_DIR}/protobuf_build-prefix/src/protobuf_build/src/>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${CMAKE_BINARY_DIR}/protoc_grpc_build-prefix/src/protoc_grpc_build/include/>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${METADEF_DIR}/third_party/fwkacllib/inc/mmpa>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/third_party/inc>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/third_party/metadef>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/inc/framework>
        )

target_compile_options(grpc_client PRIVATE
        -O2
        -fPIC
        -fno-common
        -Wextra
        -Wfloat-equal
        )

add_dependencies(grpc_client graphengine_protos)

target_compile_definitions(grpc_client PRIVATE
        google=ascend_private
        )

target_link_options(grpc_client PRIVATE
        -rdynamic
        -Wl,-Bsymbolic
        -Wl,--exclude-libs,ALL
        )

target_link_libraries(grpc_client PRIVATE
        $<BUILD_INTERFACE:intf_pub>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:$<BUILD_INTERFACE:mmpa_headers>>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:$<BUILD_INTERFACE:slog_headers>>
        -Xlinker "-("
        grpc++
        grpc
        -Xlinker "-)"
        -Wl,--no-as-needed
        slog
        static_mmpa
        runtime
        c_sec
        ge_common
        error_manager
        dgw_client_so
        dl
        -Wl,--as-needed
        json
        )

########################################## model_deployer_daemon ###############################
set(DEPLOYER_DAEMON_SRC_FILE
        common/config/json_parser.cc
        common/config/configurations.cc
        common/data_flow/queue/helper_exchange_service.cc
        common/data_flow/route/datagw_manager.cc
        common/data_flow/route/helper_exchange_deployer.cc
        common/data_flow/route/queue_schedule_manager.cc
        common/mem_grp/memory_group_manager.cc
        deploy/daemon/client_manager.cc
        deploy/daemon/deployer_service_impl.cc
        deploy/daemon/executor_manager.cc
        deploy/daemon/grpc_server.cc
        deploy/daemon/model_deployer_daemon.cc
        )

set(DEPLOYER_DAEMON_INC_LIST
        ${METADEF_DIR}
        ${METADEF_DIR}/inc
        ${METADEF_DIR}/inc/external
        ${METADEF_DIR}/graph
        ${AIR_CODE_DIR}/inc
        ${AIR_CODE_DIR}/inc/external
        ${AIR_CODE_DIR}/base
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_BINARY_DIR}/proto_grpc/deployer
        #### yellow zone ####
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/open_source/json/include>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc/aicpu>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/ace/npuruntime/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/abl/msprof/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/air/inc/framework>
        #### blue zone ####
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${METADEF_DIR}/third_party/fwkacllib/inc/mmpa>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/third_party/inc>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/inc/framework>
        )

add_executable(grpc_server
        ${DEPLOYER_DAEMON_SRC_FILE}
        ${GRPC_SERVER_PROTO_HDRS}
        ${GRPC_SERVER_PROTO_SRCS}
        ${HELPER_COMMUNICATION_SRC_LIST}
        ${CLUSTER_PROTO_HDRS}
        ${CLUSTER_PROTO_SRCS}
        )

target_include_directories(grpc_server PRIVATE
        ${DEPLOYER_DAEMON_INC_LIST}
        ${HELPER_COMMUNICATION_INC_LIST}
        )

target_compile_options(grpc_server PRIVATE
        -O2
        -fPIC
        -fno-common
        -Wextra
        -Wfloat-equal
        )

target_compile_definitions(grpc_server PRIVATE
        #google=ascend_private
        )

target_link_options(grpc_server PRIVATE
        -rdynamic
        -Wl,-Bsymbolic
        -Wl,--exclude-libs,All
        $<$<STREQUAL:${PRODUCT},npuf10>:-Wl,-rpath-link,${TOP_DIR}/vendor/sdk/hi3796/drv>
        $<$<STREQUAL:${PRODUCT},npuf10>:-L$<TARGET_FILE_DIR:slog>>
        )

target_link_libraries(grpc_server PRIVATE
        $<BUILD_INTERFACE:intf_pub>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:$<BUILD_INTERFACE:mmpa_headers>>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:$<BUILD_INTERFACE:slog_headers>>
        -Xlinker "-("
        grpc++
        grpc
        -Xlinker "-)"
        -Wl,--no-as-needed
        slog
        $<$<STREQUAL:${PRODUCT},npuf10>:-lslog>
        static_mmpa
        c_sec
        error_manager
        rt
        dl
        runtime
        graph
        ge_common
        dgw_client_so
        -Wl,--as-needed
        json
        )

########################################## npu_executor ###############################
set(NPU_EXECUTOR_INC_LIST
        ${METADEF_DIR}
        ${METADEF_DIR}/inc
        ${AIR_CODE_DIR}/base
        ${AIR_CODE_DIR}/executor
        ${AIR_CODE_DIR}/inc
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_BINARY_DIR}/proto/graphengine_protos/proto
        ${CMAKE_BINARY_DIR}/proto/deployer
        #### yellow zone ####
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/ace/npuruntime/inc>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:${TOP_DIR}/air/inc/framework>
        #### blue zone ####
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/third_party/inc>
        $<$<BOOL:${ENABLE_OPEN_SRC}>:${AIR_CODE_DIR}/inc/framework>
        )

set(NPU_EXECUTOR_SRC_LIST
        deploy/executor/engine_daemon.cc
        deploy/executor/event_handler.cc
        deploy/executor/executor_context.cc
        deploy/executor/incremental_model_parser.cc
        deploy/executor/npu_executor.cc
        )

add_executable(npu_executor_main
        ${NPU_EXECUTOR_SRC_LIST}
        ${DEPLOYER_PROTO_HDRS}
        ${DEPLOYER_PROTO_SRCS}
        )

add_dependencies(npu_executor_main graphengine_protos)

target_include_directories(npu_executor_main PRIVATE
        ${NPU_EXECUTOR_INC_LIST}
        )

target_compile_options(npu_executor_main PRIVATE
        -g
        -fPIC
        -fno-common
        -Wextra
        -Wfloat-equal
        )

target_compile_definitions(npu_executor_main PRIVATE
        google=ascend_private)

target_link_options(npu_executor_main PRIVATE
        -rdynamic
        -Wl,-Bsymbolic
        -Wl,--exclude-libs,ALL)

target_link_libraries(npu_executor_main PRIVATE
        $<BUILD_INTERFACE:intf_pub>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:$<BUILD_INTERFACE:mmpa_headers>>
        $<$<NOT:$<BOOL:${ENABLE_OPEN_SRC}>>:$<BUILD_INTERFACE:slog_headers>>
        -Wl,--no-as-needed
        ascend_protobuf
        slog
        error_manager
        c_sec
        runtime
        ge_common
        graph
        ge_executor_shared
        static_mmpa
        dl
        rt
        -Wl,--as-needed
        )

set(INSTALL_BASE_DIR "")
set(INSTALL_LIBRARY_DIR lib)

#add_subdirectory(communication)
install(TARGETS grpc_client grpc_server npu_executor_main OPTIONAL
        LIBRARY DESTINATION ${INSTALL_LIBRARY_DIR}
        ARCHIVE DESTINATION ${INSTALL_LIBRARY_DIR}
        RUNTIME DESTINATION ${INSTALL_LIBRARY_DIR}
        )
